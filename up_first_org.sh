#docker build -t dcentra/dnet image/
docker build -t dcentra/dnet-dev -f image/Dockerfile.dev image/
./run_dNetDev.sh example Org1 8081 true
sleep 15s

CC_INSTALL='"type":"chaincode_install", "key":"mycc", "subkey":"2.0"'
CC_UPGRADE='"type":"chaincode_upgrade", "key":"mycc", "subkey":"2.0"'
#curl -v -X POST -H "Content-Type:application/json" -d '{"adminPw":""}' --max-time 1800 http://localhost:4200/api/initNetwork | jq -rc '.log' | sed 's/\\n/\n/g' | cat || true
#docker run --rm --net net_example byrnedo/alpine-curl -v -X POST -H "Content-Type:application/json" -d '{"adminPw":""}' --max-time 1800 http://cli.org1.example:8081/api/initNetwork | jq -rc '.log' | sed 's/\\n/\n/g' | cat || true
#docker run --rm --net net_example byrnedo/alpine-curl -v -X POST -H "Content-Type:application/json" -d '{"adminPw":"", "type":"org_join", "key":"Org2", "value":1, "children":[{${CC_INSTALL}}, {${CC_UPGRADE}}]}' http://cli.org1.example:8081/api/vote
#docker run --rm --net net_example byrnedo/alpine-curl -v -X POST -H "Content-Type:application/json" -d '{"adminPw":"", ${CC_INSTALL}, "value":1}' http://cli.org1.example:8081/api/vote
#docker run --rm --net net_example byrnedo/alpine-curl -v -X POST -H "Content-Type:application/json" -d '{"adminPw":"", ${CC_UPGRADE}, "value":1}' http://cli.org1.example:8081/api/vote