#!/bin/bash

export PATH=${PWD}/bin:${PWD}:$PATH
export FABRIC_CFG_PATH=${PWD}
export VERBOSE=false

CHANNEL_HOST=$HOST_PWD/channel-artifacts
CRYPTO_HOST=$HOST_PWD/crypto-config
CHAIN_HOST=$HOST_PWD/chain

# Obtain CONTAINER_IDS and remove them
# TODO Might want to make this optional - could clear other containers
function clearContainers() {
  CONTAINER_IDS=$(docker ps -a | awk '($2 ~ /dev-peer.*.mycc.*/) {print $1}')
  if [ -z "$CONTAINER_IDS" -o "$CONTAINER_IDS" == " " ]; then
    echo "---- No containers available for deletion ----"
  else
    docker rm -f $CONTAINER_IDS
  fi
}

# Delete any images that were generated as a part of this setup
# specifically the following images are often left behind:
# TODO list generated image naming patterns
function removeUnwantedImages() {
  DOCKER_IMAGE_IDS=$(docker images | awk '($1 ~ /dev-peer.*.mycc.*/) {print $3}')
  if [ -z "$DOCKER_IMAGE_IDS" -o "$DOCKER_IMAGE_IDS" == " " ]; then
    echo "---- No images available for deletion ----"
  else
    docker rmi -f $DOCKER_IMAGE_IDS
  fi
}

# Generate the needed certificates, the genesis block and start the network.
function networkUp() {
  # generate artifacts if they don't exist
  #if [ ! -d "crypto-config" ]; then
    #generateCryptoConfig outsourced to material.sh / generateCerts
    generateCerts
    generateConfigtx
    generateChannelArtifacts
    generateComposeFiles
  #fi
  if [ "${IF_COUCHDB}" == "couchdb" ]; then
    if [ "$CONSENSUS_TYPE" == "kafka" ]; then
      IMAGE_TAG=$IMAGETAG docker-compose -f $COMPOSE_FILE -f $COMPOSE_FILE_KAFKA -f $COMPOSE_FILE_COUCH up -d 2>&1
    elif [ "$CONSENSUS_TYPE" == "etcdraft" ]; then
      IMAGE_TAG=$IMAGETAG docker-compose -f $COMPOSE_FILE -f $COMPOSE_FILE_RAFT2 -f $COMPOSE_FILE_COUCH up -d 2>&1
    else
      IMAGE_TAG=$IMAGETAG docker-compose -f $COMPOSE_FILE -f $COMPOSE_FILE_COUCH up -d 2>&1
    fi
  else
    if [ "$CONSENSUS_TYPE" == "kafka" ]; then
      IMAGE_TAG=$IMAGETAG docker-compose -f $COMPOSE_FILE -f $COMPOSE_FILE_KAFKA up -d 2>&1
    elif [ "$CONSENSUS_TYPE" == "etcdraft" ]; then
      IMAGE_TAG=$IMAGETAG docker-compose -f $COMPOSE_FILE -f $COMPOSE_FILE_RAFT2 up -d 2>&1
    else
      IMAGE_TAG=$IMAGETAG docker-compose -f $COMPOSE_FILE up -d 2>&1
    fi
  fi
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Unable to start network"
    exit 1
  fi

  if [ "$CONSENSUS_TYPE" == "kafka" ]; then
    sleep 1
    echo "Sleeping 60s to allow kafka cluster to complete booting"
    sleep 59
  elif [ "$CONSENSUS_TYPE" == "etcdraft" ]; then
    sleep 1
    echo "Sleeping 30s to allow etcdraft cluster to complete booting"
    sleep 29
  fi

  # now run the end to end script
  IMAGE_TAG=$IMAGETAG ./scripts/script.sh $CHANNEL_NAME $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  #docker exec $CLI_DOMAIN scripts/script.sh $CHANNEL_NAME $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Test failed"
    exit 1
  fi
}

# Tear down running network
function networkDown() {
  docker-compose -f $COMPOSE_FILE -f $COMPOSE_FILE_COUCH -f $COMPOSE_FILE_KAFKA down --volumes --remove-orphans

  # Don't remove the generated artifacts -- note, the ledgers are always removed
  if [ "$MODE" != "restart" ]; then
    # Bring down the network, deleting the volumes
    #Delete any ledger backups
    #docker run -v $PWD:/tmp/first-network --rm hyperledger/fabric-tools:$IMAGETAG rm -Rf /tmp/first-network/ledgers-backup
    rm -Rf /tmp/first-network/ledgers-backup
    #Cleanup the chaincode containers
    clearContainers
    #Cleanup images
    removeUnwantedImages
    # remove orderer block and other channel configuration transactions and certs
    rm -rf channel-artifacts/*.block channel-artifacts/*.tx crypto-config ./org3-artifacts/crypto-config/ channel-artifacts/org3.json
    # remove the docker-compose yaml file that was customized to the example
    rm -f $COMPOSE_FILE_E2E
  fi
}

# We will use the cryptogen tool to generate the cryptographic material (x509 certs)
# for our various network entities.  The certificates are based on a standard PKI
# implementation where validation is achieved by reaching a common trust anchor.
#
# Cryptogen consumes a file - ``crypto-config.yaml`` - that contains the network
# topology and allows us to generate a library of certificates for both the
# Organizations and the components that belong to those Organizations.  Each
# Organization is provisioned a unique root certificate (``ca-cert``), that binds
# specific components (peers and orderers) to that Org.  Transactions and communications
# within Fabric are signed by an entity's private key (``keystore``), and then verified
# by means of a public key (``signcerts``).  You will notice a "count" variable within
# this file.  We use this to specify the number of peers per Organization; in our
# case it's two peers per Org.  The rest of this template is extremely
# self-explanatory.
#
# After we run the tool, the certs will be parked in a folder titled ``crypto-config``.

# Generates Org certs using cryptogen tool
function generateCerts() {
  bash material.sh generate
}

function generateConfigtx() {
cat > configtx.yaml <<EOF
Organizations:

    - &OrdererOrg
        Name: OrdererOrg
        ID: OrdererMSP
        MSPDir: crypto-config/ordererOrganizations/${CONS_DOMAIN}/msp

        Policies:
            Readers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"
            Writers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"
            Admins:
                Type: Signature
                Rule: "OR('OrdererMSP.admin')"

    - &${ORG_NAME}
        Name: ${ORG_MSP}
        ID: ${ORG_MSP}
        MSPDir: crypto-config/peerOrganizations/${ORG_DOMAIN}/msp

        Policies:
            Readers:
                Type: Signature
                Rule: "OR('${ORG_MSP}.admin', '${ORG_MSP}.peer', '${ORG_MSP}.client')"
            Writers:
                Type: Signature
                Rule: "OR('${ORG_MSP}.admin', '${ORG_MSP}.client')"
            Admins:
                Type: Signature
                Rule: "OR('${ORG_MSP}.admin')"

        AnchorPeers:
            - Host: ${PEER0_DOMAIN}
              Port: 7051

Capabilities:
    Channel: &ChannelCapabilities
        V1_3: true

    Orderer: &OrdererCapabilities
        V1_1: true

    Application: &ApplicationCapabilities
        V1_3: true
        V1_2: false
        V1_1: false

Application: &ApplicationDefaults
    Organizations:

    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"

    Capabilities:
        <<: *ApplicationCapabilities

Orderer: &OrdererDefaults

    OrdererType: solo
    Addresses:
        - ${ORDERER_DOMAIN}:7050

    # Batch Timeout: The amount of time to wait before creating a batch
    BatchTimeout: 2s
    # Batch Size: Controls the number of messages batched into a block
    BatchSize:
        # Max Message Count: The maximum number of messages to permit in a batch
        MaxMessageCount: 10
        # Absolute Max Bytes: The absolute maximum number of bytes allowed for
        # the serialized messages in a batch.
        AbsoluteMaxBytes: 99 MB
        # Preferred Max Bytes: The preferred maximum number of bytes allowed for
        # the serialized messages in a batch. A message larger than the preferred
        # max bytes will result in a batch larger than preferred max bytes.
        PreferredMaxBytes: 512 KB

    Kafka:
        # Brokers: A list of Kafka brokers to which the orderer connects
        # NOTE: Use IP:port notation
        Brokers:
            - ${KAFKA_DOMAIN}:9092

    # Organizations is the list of orgs which are defined as participants on
    # the orderer side of the network
    Organizations:

    # Policies defines the set of policies at this level of the config tree
    # For Orderer policies, their canonical path is
    #   /Channel/Orderer/<PolicyName>
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
        # BlockValidation specifies what signatures must be included in the block
        # from the orderer for the peer to validate it.
        BlockValidation:
            Type: ImplicitMeta
            Rule: "ANY Writers"

Channel: &ChannelDefaults
    Policies:
        # Who may invoke the 'Deliver' API
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        # Who may invoke the 'Broadcast' API
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        # By default, who may modify elements at this config level
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"

    # Capabilities describes the channel level capabilities, see the
    # dedicated Capabilities section elsewhere in this file for a full
    # description
    Capabilities:
        <<: *ChannelCapabilities

Profiles:
    BaseChannel:
        Consortium: SampleConsortium
        Application:
            <<: *ApplicationDefaults
            Organizations:
                - *${ORG_NAME}
            Capabilities:
                <<: *ApplicationCapabilities

    SampleDevModeKafka:
        <<: *ChannelDefaults
        Capabilities:
            <<: *ChannelCapabilities
        Orderer:
            <<: *OrdererDefaults
            OrdererType: kafka
            Kafka:
                Brokers:
                - ${KAFKA_DOMAIN}:9092

            Organizations:
            - *OrdererOrg
            Capabilities:
                <<: *OrdererCapabilities
        Application:
            <<: *ApplicationDefaults
            Organizations:
            - <<: *OrdererOrg
        Consortiums:
            SampleConsortium:
                Organizations:
                - *${ORG_NAME}

    SampleMultiNodeEtcdRaft:
        <<: *ChannelDefaults
        Capabilities:
            <<: *ChannelCapabilities
        Orderer:
            <<: *OrdererDefaults
            OrdererType: etcdraft
            EtcdRaft:
                Consenters:
                - Host: $ORDERER_DOMAIN
                  Port: 7050
                  ClientTLSCert: crypto-config/ordererOrganizations/$CONS_DOMAIN/orderers/$ORDERER_DOMAIN/tls/server.crt
                  ServerTLSCert: crypto-config/ordererOrganizations/$CONS_DOMAIN/orderers/$ORDERER_DOMAIN/tls/server.crt
            Addresses:
                - $ORDERER_DOMAIN:7050

            Organizations:
            - *OrdererOrg
            Capabilities:
                <<: *OrdererCapabilities
        Application:
            <<: *ApplicationDefaults
            Organizations:
            - <<: *OrdererOrg
        Consortiums:
            SampleConsortium:
                Organizations:
                - *${ORG_NAME}
EOF

cat configtx.yaml
}

# The `configtxgen tool is used to create four artifacts: orderer **bootstrap
# block**, fabric **channel configuration transaction**, and two **anchor
# peer transactions** - one for each Peer Org.
#
# The orderer block is the genesis block for the ordering service, and the
# channel transaction file is broadcast to the orderer at channel creation
# time.  The anchor peer transactions, as the name might suggest, specify each
# Org's anchor peer on this channel.
#
# Configtxgen consumes a file - ``configtx.yaml`` - that contains the definitions
# for the sample network. There are three members - one Orderer Org (``OrdererOrg``)
# and two Peer Orgs (``Org1`` & ``Org2``) each managing and maintaining two peer nodes.
# This file also specifies a consortium - ``SampleConsortium`` - consisting of our
# two Peer Orgs.  Pay specific attention to the "Profiles" section at the top of
# this file.  You will notice that we have two unique headers. One for the orderer genesis
# block - ``TwoOrgsOrdererGenesis`` - and one for our channel - ``TwoOrgsChannel``.
# These headers are important, as we will pass them in as arguments when we create
# our artifacts.  This file also contains two additional specifications that are worth
# noting.  Firstly, we specify the anchor peers for each Peer Org
# (``peer0.org1.example.com`` & ``peer0.org2.example.com``).  Secondly, we point to
# the location of the MSP directory for each member, in turn allowing us to store the
# root certificates for each Org in the orderer genesis block.  This is a critical
# concept. Now any network entity communicating with the ordering service can have
# its digital signature verified.
#
# This function will generate the crypto material and our four configuration
# artifacts, and subsequently output these files into the ``channel-artifacts``
# folder.
#
# If you receive the following warning, it can be safely ignored:
#
# [bccsp] GetDefault -> WARN 001 Before using BCCSP, please call InitFactories(). Falling back to bootBCCSP.
#
# You can ignore the logs regarding intermediate certs, we are not using them in
# this crypto implementation.

# Generate orderer genesis block, channel configuration transaction and
# anchor peer update transactions
function generateChannelArtifacts() {
  which configtxgen
  if [ "$?" -ne 0 ]; then
    echo "configtxgen tool not found. exiting"
    exit 1
  fi

  echo "##########################################################"
  echo "#########  Generating Orderer Genesis block ##############"
  echo "##########################################################"
  # Note: For some unknown reason (at least for now) the block file can't be
  # named orderer.genesis.block or the orderer will fail to launch!
  echo "CONSENSUS_TYPE="$CONSENSUS_TYPE
  set -x
  if [ "$CONSENSUS_TYPE" == "solo" ]; then
    configtxgen -profile TwoOrgsOrdererGenesis -channelID byfn-sys-channel -outputBlock ./channel-artifacts/genesis.block
  elif [ "$CONSENSUS_TYPE" == "kafka" ]; then
    configtxgen -profile SampleDevModeKafka -channelID byfn-sys-channel -outputBlock ./channel-artifacts/genesis.block
  elif [ "$CONSENSUS_TYPE" == "etcdraft" ]; then
    configtxgen -profile SampleMultiNodeEtcdRaft -channelID byfn-sys-channel -outputBlock ./channel-artifacts/genesis.block
  else
    set +x
    echo "unrecognized CONSESUS_TYPE='$CONSENSUS_TYPE'. exiting"
    exit 1
  fi
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate orderer genesis block..."
    exit 1
  fi
  echo
  echo "#################################################################"
  echo "### Generating channel configuration transaction 'channel.tx' ###"
  echo "#################################################################"
  set -x
  configtxgen -profile BaseChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate channel configuration transaction..."
    exit 1
  fi

  echo
  echo "#################################################################"
  echo "#######    Generating anchor peer update for Org1MSP   ##########"
  echo "#################################################################"
  set -x
  configtxgen -profile BaseChannel -outputAnchorPeersUpdate ./channel-artifacts/${ORG_MSP}anchors.tx -channelID $CHANNEL_NAME -asOrg ${ORG_MSP}
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate anchor peer update for Org1MSP..."
    exit 1
  fi
}

function generateComposeFiles() {
cat > $COMPOSE_FILE_BASE <<EOF
version: '2'

services:

  $ORDERER_DOMAIN:
    container_name: $ORDERER_DOMAIN
    image: hyperledger/fabric-orderer:$IMAGETAG
    extends:
      file: ../base/peer-base.yaml
      service: orderer-base
    volumes:
    #- $CHANNEL_HOST/:/var/hyperledger/orderer
    - $CHANNEL_HOST/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$ORDERER_DOMAIN/msp/:/var/hyperledger/orderer/msp
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$ORDERER_DOMAIN/tls/:/var/hyperledger/orderer/tls
    - $CHAIN_HOST/$ORDERER_DOMAIN:/var/hyperledger/production/orderer

  $PEER0_DOMAIN:
    container_name: $PEER0_DOMAIN
    extends:
      file: ../base/peer-base.yaml
      service: peer-base
    environment:
      - CORE_PEER_ID=$PEER0_DOMAIN
      - CORE_PEER_ADDRESS=$PEER0_DOMAIN:7051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=$PEER0_DOMAIN:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=$PEER1_DOMAIN:7051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=$PEER0_DOMAIN:7051
      - CORE_PEER_LOCALMSPID=$ORG_MSP
      - CORE_OPERATIONS_LISTENADDRESS=0.0.0.0:9443
      - CORE_METRICS_PROVIDER=prometheus
    volumes:
        - /var/run/:/host/var/run/
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER0_DOMAIN/msp:/etc/hyperledger/fabric/msp
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER0_DOMAIN/tls:/etc/hyperledger/fabric/tls
        - $CHAIN_HOST/$PEER0_DOMAIN:/var/hyperledger/production

  $PEER1_DOMAIN:
    container_name: $PEER1_DOMAIN
    extends:
      file: ../base/peer-base.yaml
      service: peer-base
    environment:
      - CORE_PEER_ID=$PEER1_DOMAIN
      - CORE_PEER_ADDRESS=$PEER1_DOMAIN:7051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=$PEER1_DOMAIN:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=$PEER1_DOMAIN:7051
      - CORE_PEER_GOSSIP_BOOTSTRAP=$PEER0_DOMAIN:7051
      - CORE_PEER_LOCALMSPID=$ORG_MSP
      - CORE_OPERATIONS_LISTENADDRESS=0.0.0.0:9443
      - CORE_METRICS_PROVIDER=prometheus
    volumes:
        - /var/run/:/host/var/run/
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER1_DOMAIN/msp:/etc/hyperledger/fabric/msp
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER1_DOMAIN/tls:/etc/hyperledger/fabric/tls
        - $CHAIN_HOST/$PEER1_DOMAIN:/var/hyperledger/production
EOF
cat $COMPOSE_FILE_BASE

CA_PRIVATE_KEY=$(ls "/home/node/app/crypto-config/peerOrganizations/$ORG_DOMAIN/ca/" | grep _sk)
cat > $COMPOSE_FILE <<EOF
version: '2'

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN

services:

  $CA_DOMAIN:
    image: hyperledger/fabric-ca:1.5
    container_name: $CA_DOMAIN
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=$CA_DOMAIN
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server-config/$CA_DOMAIN-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server-config/$CA_PRIVATE_KEY
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/${CA_DOMAIN}-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/${CA_PRIVATE_KEY} -b admin:adminpw -d'
    volumes:
      - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/ca/:/etc/hyperledger/fabric-ca-server-config
    networks:
      - $ORG_NAME

  $ORDERER_DOMAIN:
    extends:
      file: docker-compose-base.yaml
      service: $ORDERER_DOMAIN
    container_name: $ORDERER_DOMAIN
    networks:
      - $CONS_DOMAIN

  $PEER0_DOMAIN:
    container_name: $PEER0_DOMAIN
    extends:
      file: docker-compose-base.yaml
      service: $PEER0_DOMAIN
    networks:
      - $CONS_DOMAIN

  $PEER1_DOMAIN:
    container_name: $PEER1_DOMAIN
    extends:
      file: docker-compose-base.yaml
      service: $PEER1_DOMAIN
    networks:
      - $CONS_DOMAIN
EOF
cat $COMPOSE_FILE

cat > $COMPOSE_FILE_COUCH <<EOF
version: '2'

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN
  $ORG_NAME:

services:
  couchdb.${PEER0_DOMAIN}:
    container_name: couchdb.${PEER0_DOMAIN}
    image: hyperledger/fabric-couchdb
    # Populate the COUCHDB_USER and COUCHDB_PASSWORD to set an admin user and password
    # for CouchDB.  This will prevent CouchDB from operating in an "Admin Party" mode.
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    # Comment/Uncomment the port mapping if you want to hide/expose the CouchDB service,
    # for example map it to utilize Fauxton User Interface in dev environments.
    volumes:
      - $CHAIN_HOST/couchdb.${PEER0_DOMAIN}/data:/opt/couchdb/data
    networks:
      - $ORG_NAME

  $PEER0_DOMAIN:
    environment:
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb.${PEER0_DOMAIN}:5984
      # The CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME and CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD
      # provide the credentials for ledger to connect to CouchDB.  The username and password must
      # match the username and password set for the associated CouchDB.
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=
    depends_on:
      - couchdb.${PEER0_DOMAIN}
    networks:
      - $CONS_DOMAIN
      - $ORG_NAME

  couchdb.${PEER1_DOMAIN}:
    container_name: couchdb.${PEER1_DOMAIN}
    image: hyperledger/fabric-couchdb
    # Populate the COUCHDB_USER and COUCHDB_PASSWORD to set an admin user and password
    # for CouchDB.  This will prevent CouchDB from operating in an "Admin Party" mode.
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    # Comment/Uncomment the port mapping if you want to hide/expose the CouchDB service,
    # for example map it to utilize Fauxton User Interface in dev environments.
    volumes:
      - $CHAIN_HOST/couchdb.${PEER1_DOMAIN}/data:/opt/couchdb/data
    networks:
      - $ORG_NAME

  $PEER1_DOMAIN:
    environment:
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb.${PEER1_DOMAIN}:5984
      # The CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME and CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD
      # provide the credentials for ledger to connect to CouchDB.  The username and password must
      # match the username and password set for the associated CouchDB.
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=
    depends_on:
      - couchdb.${PEER1_DOMAIN}
    networks:
      - $CONS_DOMAIN
      - $ORG_NAME
EOF
cat $COMPOSE_FILE_COUCH

cat > $COMPOSE_FILE_RAFT2 <<EOF
version: '2'

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN

services:
  $ORDERER_DOMAIN:
    extends:
      file: ../base/peer-base.yaml
      service: orderer-base
    container_name: $ORDERER_DOMAIN
    networks:
    - $CONS_DOMAIN
    volumes:
    - $CHANNEL_HOST/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$ORDERER_DOMAIN/msp/:/var/hyperledger/orderer/msp
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$ORDERER_DOMAIN/tls/:/var/hyperledger/orderer/tls
    - $CHAIN_HOST/$ORDERER_DOMAIN:/var/hyperledger/production/orderer
EOF

cat $COMPOSE_FILE_RAFT2
}

# Obtain the OS and Architecture string that will be used to select the correct
# native binaries for your platform, e.g., darwin-amd64 or linux-amd64
OS_ARCH=$(echo "$(uname -s | tr '[:upper:]' '[:lower:]' | sed 's/mingw64_nt.*/windows/')-$(uname -m | sed 's/x86_64/amd64/g')" | awk '{print tolower($0)}')
# timeout duration - the duration the CLI should wait for a response from
# another container before giving up
CLI_TIMEOUT=10
# default for delay between commands
CLI_DELAY=3
# channel name defaults to "mychannel"
CHANNEL_NAME="mychannel"
# use this as the default docker-compose yaml definition
COMPOSE_FILE=network/docker-compose-cli.yaml
COMPOSE_FILE_BASE=network/docker-compose-base.yaml
COMPOSE_FILE_E2E=network/docker-compose-e2e.yaml
#
COMPOSE_FILE_COUCH=network/docker-compose-couch.yaml
# kafka and zookeeper compose file
COMPOSE_FILE_KAFKA=network/docker-compose-kafka.yaml
# two additional etcd/raft orderers
COMPOSE_FILE_RAFT2=network/docker-compose-etcdraft2.yaml
#
# use golang as the default language for chaincode
LANGUAGE=node
# default image tag
IMAGETAG="2.2"
# default consensus type
CONSENSUS_TYPE="etcdraft"
# Parse commandline args
if [ "$1" = "-m" ]; then # supports old usage, muscle memory is powerful!
  shift
fi
MODE=$1
CONS_DOMAIN=cons
ORG_NAME=Org1
shift
# Determine whether starting, stopping, restarting, generating or upgrading
if [ "$MODE" == "up" ]; then
  EXPMODE="Starting"
elif [ "$MODE" == "down" ]; then
  EXPMODE="Stopping"
elif [ "$MODE" == "restart" ]; then
  EXPMODE="Restarting"
elif [ "$MODE" == "generate" ]; then
  EXPMODE="Generating certs and genesis block"
elif [ "$MODE" == "upgrade" ]; then
  EXPMODE="Upgrading the network"
else
  exit 1
fi

while getopts "h?c:t:d:f:s:l:i:o:x:y:v" opt; do
  case "$opt" in
  h | \?)
    printHelp
    exit 0
    ;;
  c)
    CHANNEL_NAME=$OPTARG
    ;;
  t)
    CLI_TIMEOUT=$OPTARG
    ;;
  d)
    CLI_DELAY=$OPTARG
    ;;
  f)
    COMPOSE_FILE=$OPTARG
    ;;
  s)
    IF_COUCHDB=$OPTARG
    ;;
  l)
    LANGUAGE=$OPTARG
    ;;
  i)
    IMAGETAG=$(go env GOARCH)"-"$OPTARG
    ;;
  o)
    CONSENSUS_TYPE=$OPTARG
    ;;
  x)
    CONS_DOMAIN=$OPTARG
    ;;
  y)
    ORG_NAME=$OPTARG
    ;;
  v)
    VERBOSE=true
    ;;
  esac
done

export CONS_DOMAIN=$CONS_DOMAIN

export ORG_NAME=$ORG_NAME
export ORG_LOWER=$(echo "$ORG_NAME" | awk '{print tolower($0)}')
export ORG_DOMAIN=${ORG_LOWER}.${CONS_DOMAIN}
export ORG_MSP=${ORG_NAME}MSP

export ORDERER_DOMAIN=${ORG_LOWER}.orderer.${CONS_DOMAIN}
export ZOOKEEPER_DOMAIN=${ORG_LOWER}.zookeeper.${CONS_DOMAIN}
export KAFKA_DOMAIN=${ORG_LOWER}.kafka.${CONS_DOMAIN}

export PEER0_DOMAIN=peer0.${ORG_DOMAIN}
export PEER1_DOMAIN=peer1.${ORG_DOMAIN}

export CA_DOMAIN=ca.${ORG_DOMAIN}
export CLI_DOMAIN=cli.${ORG_LOWER}

echo $CONS_DOMAIN
echo $ORG_NAME
echo $ORG_DOMAIN
echo $ORG_MSP
echo $PEER0_DOMAIN
echo $PEER1_DOMAIN

echo $(ifconfig)

# Announce what was requested
if [ "${IF_COUCHDB}" == "couchdb" ]; then
  echo
  echo "${EXPMODE} for channel '${CHANNEL_NAME}' with CLI timeout of '${CLI_TIMEOUT}' seconds and CLI delay of '${CLI_DELAY}' seconds and using database '${IF_COUCHDB}'"
else
  echo "${EXPMODE} for channel '${CHANNEL_NAME}' with CLI timeout of '${CLI_TIMEOUT}' seconds and CLI delay of '${CLI_DELAY}' seconds"
fi

#Create the network using docker compose
if [ "${MODE}" == "up" ]; then
  networkUp
elif [ "${MODE}" == "down" ]; then ## Clear the network
  networkDown
elif [ "${MODE}" == "generate" ]; then ## Generate Artifacts
  generateCerts
  replacePrivateKey
  generateChannelArtifacts
elif [ "${MODE}" == "restart" ]; then ## Restart the network
  networkDown
  networkUp
elif [ "${MODE}" == "upgrade" ]; then ## Upgrade the network from version 1.2.x to 1.3.x
  upgradeNetwork
else
  printHelp
  exit 1
fi
