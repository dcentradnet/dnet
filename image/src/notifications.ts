import { PubSub } from "graphql-subscriptions";
import { ObjectId } from 'mongoist';
import { DateTime } from 'luxon';

import { Notification, Subscription } from './generated/graphqlTypes';
import * as database from './db';

// expire notifications after one week
(async function() {
    const db = await database.mongoPromise;
    db.notifications.createIndex({ "lastModifiedDate": 1 }, { expireAfterSeconds: 7*24*60*60 });
})();

export const pubsub = new PubSub();

export async function publishNotification(system: string, message: string): Promise<void> {
    const _id = new ObjectId();
    const notification: Notification = {_id, system, message};

    pubsub.publish('NOTIFICATION', <Subscription>{notification});

    const db = await database.mongoPromise;
    await db.notifications.insert(notification);
}

export async function replayNotifications(timestamp: DateTime = DateTime.now().minus({days:1})): Promise<Notification[]> {
    //const marker = ObjectId.createFromTime(timestamp.toSeconds());

    const db = await database.mongoPromise;
    return db.notifications.findAsCursor().toArray(); //({ _id: { $gt: marker } }).sort({ _id : 1 }).toArray();
}
