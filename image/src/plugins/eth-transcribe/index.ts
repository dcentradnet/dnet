import { to } from 'await-to-js';
import { Request, Response, Express } from 'express';
import * as ethers from 'ethers';
import Web3 from 'web3';

import env from '../../env';
import * as auth from '../../auth';
import invoke from '../../sdk/invoke';

let contract;
export async function install(args: any, express?: Express) {
  // see https://ethereum.stackexchange.com/a/15215
  const options = {
    from: args.address,
    gas: 4712388,
    gasPrice: '100000000000',
    data: undefined
  };
  // FIXME connect wallet?

  const web3 = new Web3(args.rpcUrl);
  //const networkId = await web3.eth.net.getId(); // FIXME use to get address

  const artifacts = args.artifacts;
  const Contract = web3.eth.Contract;
  contract = new Contract(artifacts.abi, artifacts.address, options);

  express.get('/api/plugins/eth-transcribe/verify', auth.authenticate(), async (req: Request, res: Response): Promise<void> => {
    const txId = req.query.txId;
    const [error, verified] = await to(contract.methods.verify(txId));
    if(error) {
      res.status(500).send({error});
      return;
    }

    res.send(verified);
  });

  express.post('/api/plugins/eth-transcribe/invoke', auth.authenticate(), async (req: Request, res: Response): Promise<void> => {
    const body = req.body;

    const txId = await invoke(body.channel || env.vars.CHANNEL_NAME, body.chaincode, body.method, ...body.args); // 32 bytes
    const transcribeReceipt = await contract.methods.transcribe(ethers.utils.bigNumberify(`0x${txId}`)).send();
    // TODO write transcribeTxHash to chaincode?
    
    res.json({txId, transcribeReceipt});
  });
}
