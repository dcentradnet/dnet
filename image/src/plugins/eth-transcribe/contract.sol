pragma solidity ^0.5.0;

contract FabricETHTranscribe {
    mapping(uint256 => bool) transcriptions;

    function isTranscribed(uint256 fabricTxId) public view returns (bool) {
        return transcriptions[fabricTxId];
    }

    function transcribe(uint256 fabricTxId) public {
        transcriptions[fabricTxId] = true;
    }
}
