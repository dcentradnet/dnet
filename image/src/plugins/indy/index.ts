/* eslint-disable */
// see https://github.com/hyperledger/indy-sdk/blob/e577d7a67d2b0a5bb5ec078ecf2dde27fc6440f4/samples/nodejs/src/gettingStarted.js

import { copyFileSync, mkdirSync, writeFileSync } from 'fs';
import * as path from 'path';
import * as indy from 'indy-sdk';
import Dockerode from 'dockerode';
import { Express } from 'express';
import removeRoute from 'express-remove-route';

import env from '../../env';
import * as auth from '../../auth';
import * as utils from '../../utils';
import docker from '../../docker';
import { FormField } from '../../generated/graphqlLobbyTypes';

let agentContainer: Dockerode.Container;

export async function install(express?: Express) {
    // setup express routes
    express.get('/api/plugins/indy/', auth.authenticate(), async (req, res) => {
        res.status(200).send();
    });
};

export async function uninstall(express: Express) {
    removeRoute(express, '/api/plugins/indy');

    agentContainer && await agentContainer.kill();
}

export async function parameters(): Promise<FormField[]> {
    return [
        {
            type: 'text',
            name: 'poolName',
            label: 'Pool name',
            value: 'docker',
            required: false
        },
        {
            type: 'text',
            name: 'poolGenesis',
            label: 'Pool genesis',
            value: '',
            required: false
        }
    ];
}

export async function apply(args: Record<string, string>) {
    const poolName = args.poolName || 'docker';
    const poolTransactionsGenesis = `${poolName}_transactions_genesis`;
    const poolGenesisTxnPath = path.join(__dirname, poolTransactionsGenesis);

    // write pool genesis if supplied
    if(args.poolGenesis && '' !== args.poolGenesis) {
        writeFileSync(poolGenesisTxnPath, args.poolGenesis);
    }

    // connect to pool ledger
    console.log(`Open Pool Ledger: ${poolName}`);
    const poolConfig = {"genesis_txn": poolGenesisTxnPath};
    /*await indy.createPoolLedgerConfig(poolName, poolConfig).catch(() => {});
    await indy.setProtocolVersion(2)
    const poolHandle = await indy.openPoolLedger(poolName);*/

    // copy to plugin dir
    const pluginDirPath = path.join(env.vars.PLUGINS, 'indy');
    const pluginFilePath = path.join(pluginDirPath, 'pool_transactions_genesis');
    mkdirSync(pluginDirPath, {recursive: true});
    copyFileSync(poolGenesisTxnPath, pluginFilePath);

    const hostFilePath = path.join(env.vars.HOST_PWD, 'plugins/indy');
    //const indyAgentLocalPoolFile = '/home/indy/ledger/sandbox/pool_transactions_genesis';

    // start agentjs using dockerode
    // pull image
    const imageName = 'dcentra/indy-agentjs:latest';
    await utils.pullImage(imageName);
    // start container
    agentContainer = await docker.createContainer({
        Image: imageName,
        name: 'dnet-indy-agent',
        Env: [
            `NAME=${env.vars.ORG_NAME}`,
            `EMAIL=`,
            `ICON_SRC=https://cdn3.iconfinder.com/data/icons/black-easy/512/538738-school_512x512.png`,
            `USERNAME=Administrator`,
            `PASSWORD=admin`,
            `PUBLIC_DID_ENDPOINT=173.17.0.2:3000`
        ],
        Volumes: {
            '/home/indy/ledger/sandbox': {}
        },
        ExposedPorts: {
            "3000/tcp": {}
        },
        HostConfig: {
            Binds: [`${hostFilePath}:/home/indy/ledger/sandbox`],
            PortBindings: {
                "3000/tcp": [{
                    "HostIP":"0.0.0.0",
                    "HostPort": "3333"
                }],
            }
        }
    });
    await agentContainer.start();  
}
