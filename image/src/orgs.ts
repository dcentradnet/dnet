import Bluebird from "bluebird";
import { Info, Organization, Subscription } from "./generated/graphqlTypes";
import env from './env';
import * as utils from './utils';
import * as crypt from './crypt';
import * as database from './db';
import docker from './docker';
import { pubsub } from "./notifications";

export async function getOwn(): Promise<Organization> {
    return {_id: await crypt.getPublicKeyString(), name: env.vars.ORG_NAME};
}

export async function getAddresses(): Promise<string[]> {
    return Bluebird.map(docker.listNodesDebuggable(), (node: {ID: string}) => node.ID);
}

export async function getInfo(): Promise<Info> {
    return Bluebird.props({
        consDomain: env.vars.CONS_DOMAIN,
        orgName: env.vars.ORG_NAME,
        channelName: env.vars.CHANNEL_NAME,
        publicIpV4: utils.getPublicIp(),
        //publicIpV6: ipV6,
        publicKey: crypt.getPublicKeyString(),
        isAdminMode: env.vars.ADMIN_MODE,
        isJoined: database.getStatus('isJoined').then(value => !!value)
    });
}

export async function publishStatusUpdate(): Promise<void> {
    pubsub.publish('STATUS_UPDATE', <Subscription['statusUpdate']>{statusUpdate: await getInfo()});
}
