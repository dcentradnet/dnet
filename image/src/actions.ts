import { chain, omit, forEach } from 'lodash';
import { Request, Response, Express } from 'express';
import { Subject, Observable } from 'rxjs';
import { share } from 'rxjs/operators';
import uuidv4 from 'uuid/v4';

//import chaincode from './chaincode';
import * as dapps from './dapps';
import { Action } from './generated/graphqlTypes';
import { publishNotification } from './notifications';

interface PendingAction extends Action {
  observables: {progress: Observable<number>, log: Observable<string>};
}

export type ActionFunc = (type: string, key?: string, subkey?: string, express?: Express) => Promise<Observable<number | string>[] | undefined>;

// FIXME use proper Map
const actions = new Map<string, ActionFunc>();
export function registerAction(type: string, func: ActionFunc): void {
  actions.set(type, func);
}

const pendingActions: PendingAction[] = [];
export async function executeAction(type: string, key: string, subkey?: string, express?: Express): Promise<Observable<number | string>[] | undefined> {
  const func = actions.get(type);
  if (!func) {
    return undefined;
  }

  // start execution of action
  const observables = await func(type, key, subkey, express);
  if (!observables) {
    return undefined;
  }
  const [progress, log] = observables;

  const progress_shared = progress.pipe(share());
  const log_shared = log.pipe(share());

  // add action to pending
  const action: PendingAction = {
    _id: uuidv4(),
    name: `${type} ${key}`,
    observables: {progress: progress_shared as Observable<number>, log: log_shared as Observable<string>},
    progress: 0,
    log: []
  };
  pendingActions.push(action);

  // subscribe to keep action updated
  progress_shared.subscribe({
    next: value => action.progress = value as number,
    //complete: () => ps.unsubscribe()
  });
  log_shared.subscribe({
    next: line => action.log.push(line as string),
    //complete: () => ls.unsubscribe()
  });

  // notify existing listeners
  listeners.forEach(listener => subscribeListenerToAction(listener.req, listener.res, action));

  return [progress_shared, log_shared];
}
export function listPendingActions(): PendingAction[] {
  return pendingActions;
}
export function listPendingActionsRoute(req: Request, res: Response): void {
  res.status(200).json(pendingActions.map(action => omit(action, 'observables')));
}

// keep a map of attached clients
let numListeners = 0;
const listeners = new Map<number, {req: Request, res: Response}>();

function subscribeListenerToAction(req: Request, res: Response, action: PendingAction, replay = false) {
  // extract action info without observables and state
  const actionInfo = omit(action, 'observables', 'progress', 'log');

  // replay if asked for
  if(replay) {
    action.log.forEach((line: string) => {
      const actionEvent = Object.assign({eventName: 'log', eventValue: line}, actionInfo);
      const actionEventString = JSON.stringify(actionEvent);
      console.log('replaying event', actionEventString);
      res.write(`data: ${actionEventString}\n\n`);
    });
  }

  // subscribe to all observables of action
  forEach(action.observables, (observable: Observable<number | string>, name: string) => {
    const subscription = observable.subscribe({
      next: value => {
        const actionEvent = Object.assign({eventName: name, eventValue: value}, actionInfo);
        const actionEventString = JSON.stringify(actionEvent);
        console.log('sending event', actionEventString);
        res.write(`data: ${actionEventString}\n\n`);
      },
      complete: () => {
        (res as any).flush();
        //subscription.unsubscribe();
      }
    });
    req.on('close', () => subscription.unsubscribe());
  });
}

export function addListenerRoute(req: Request, res: Response): void {
  // send header
  req.socket.setTimeout(Number.MAX_VALUE);
  res.writeHead(200, {
    'Content-Type': 'text/event-stream', // important headers
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });
  res.write('\n');

  // subscribe action observables
  // if id is specified, subscribe only to this action
  const { query } = req;
  chain(pendingActions)
    .thru(actions => query.id ? actions.filter(action => action._id === query.id) : actions)
    .forEach(action => subscribeListenerToAction(req, res, action, 'true' === query.replay))
    .value();
  
  // add this client to those we consider "attached"
  listeners.set(numListeners, {req, res});
  // remove this client when he disconnects
  req.on('close', () => listeners.delete(numListeners)); // FIXME collision with subscribeListener?
  // increment number of log listeners
  ++numListeners;
}

export function waitActionRoute(req: Request, res: Response): void {
  req.setTimeout(Number.MAX_VALUE);

  // subscribe to action observable
  const action = pendingActions.find(action => action._id === req.query.id || action.name === req.query.name);
  const subscription = action.observables.progress.subscribe({
    complete: () => {
      res.status(200).send({message: 'Complete!'});
      subscription.unsubscribe();
    }
  });
}

// eslint-disable-next-line no-unused-vars
registerAction('chaincode_install', async(type: string, key: string, subkey: string, express?: Express): Promise<Observable<number | string>[]> => {
  const progress = new Subject<number>();
  const log = new Subject<string>();

  (async function() {
    progress.next(0);

    /*const [error, result] = await to(chaincode.installChaincode(request.key, request.subkey, ccBuffer)); // FIXME
    error && log.next(error);
    result && log.next(result);*/
    progress.next(1);

    progress.complete();
    log.complete();
  
    await publishNotification('Chaincode', `Chaincode has been installed`);
  })();

  return [progress, log];
});

// eslint-disable-next-line no-unused-vars
registerAction('dapp_install', async(type: string, key: string, subkey: string, express?: Express): Promise<Observable<number | string>[]> => {
  const _id = subkey;

  const dapp = await dapps.queryStoreApp(_id);
  return dapps.install(dapp, undefined, express);
});
