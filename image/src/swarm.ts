import { to } from 'await-to-js';
import docker from './docker';
import * as crypt from './crypt';
import * as utils from './utils';
import { SwarmInitParamsInput, SwarmJoinParamsInput } from './generated/graphqlTypes';
import env from './env';

export async function init(params: SwarmInitParamsInput): Promise<void> {
    params ||= {ForceNewCluster: true};
    params.AdvertiseAddr ||= `${await utils.getPublicIp()}:2377`,
    params.ListenAddr ||= '0.0.0.0:2377';

    // init the swarm
    await docker.swarmInit(params);

    // create initial overlay network (okay to fail)
    const [err2, netInfo] = await to(docker.createNetwork({
        Name: "net_overlay",
        CheckDuplicate: false,
        Driver: "overlay",
        Attachable: true
    }));
    console.log(netInfo);

    // wait 10 seconds to make sure the network has been created
    await utils.timeoutPromise(10*1000);

    await docker.getNetwork('net_overlay').connect({
        Container: env.vars.CLI_DOMAIN
    });
}

export async function join(params: SwarmJoinParamsInput): Promise<void> {
    await docker.swarmJoin(params);
}

export async function joinCrypted({crypted, opub}: {crypted: string, opub: string}): Promise<void> {
    const params: SwarmJoinParamsInput = {
        'ListenAddr': '0.0.0.0:2377',
        'AdvertiseAddr': `${await utils.getPublicIp()}:2377`,
        'RemoteAddrs': [],
    };

    const connectionDetails = await crypt.decryptWithOtherPublicKey(crypted, opub);
    const [token, node] = connectionDetails.split(' ');

    params.RemoteAddrs.push(node);
    params.JoinToken = token;

    await docker.swarmJoin(params);
};
