import waitPort from 'wait-port';
import Mongoist from 'mongoist';

import env from './env';
import docker from './docker';
import * as utils from './utils';
import * as south from './south';

const {
  HOST_DB,
  ORG_NAME,  
} = env.vars;

const MONGO_IMAGE = 'mongo:4';

async function startMongo(): Promise<Mongoist> {
  await south.readyPromise;

  // get existing container
  let container = await docker.getContainerByName(env.vars.MONGO_DOMAIN);

  // start new container if none exists
  if(!container) {
    // pull container
    await utils.pullImage(MONGO_IMAGE);

    // start container
    container = await docker.createContainer({
      Image: MONGO_IMAGE,
      name: env.vars.MONGO_DOMAIN,
      Volumes: {
        '/data/db': {}
      },
      HostConfig: {
        Binds: [`${HOST_DB}:/data/db:rw`]
      }
    });
    await container.start();
    /*await docker.run('mongo', [], [process.stdout, process.stderr], {
      'name': env.vars.MONGO_DOMAIN,
      'Hostname': env.vars.MONGO_DOMAIN,
      'Hostdomain': env.vars.MONGO_DOMAIN,
      'Volumes': {
        '/data/db': {}
      },
      'Hostconfig': {
        'Binds': [HOST_DB+":/data/db:rw"]
      }
    });*/

    // connect to network  
    await docker.getNetwork(`net_${ORG_NAME}`).connect({Container: env.vars.MONGO_DOMAIN});
    // wait for container to be up and running
    await waitPort({host: env.vars.MONGO_DOMAIN, port: 27017});
  }
  
  // connect mongdb client
  const db = Mongoist(`mongodb://${env.vars.MONGO_DOMAIN}`);
  
  // create indices
  db.requests.createIndex({type: 1, key: 1, subkey: 1}, {unique: true});

  return db;
}
export const mongoPromise = startMongo();

export type BasicTypes = string | number | boolean | undefined;

// status management
export async function getStatus(key: string): Promise<BasicTypes> {return mongoPromise.then(db => db.status.findOne({key}, {value: 1})).then(doc => doc && doc.value)}
export async function setStatus(key: string, value: BasicTypes): Promise<void> {mongoPromise.then(db => db.status.update({key}, {$set: {value}}, {upsert: true}))}

// settings management
export async function getSetting(key: string): Promise<BasicTypes> {return mongoPromise.then(db => db.settings.findOne({key}, {value: 1})).then(doc => doc && doc.value)}
export async function setSetting(key: string, value: BasicTypes): Promise<void> {mongoPromise.then(db => db.settings.update({key}, {$set: {value}}, {upsert: true}))}
