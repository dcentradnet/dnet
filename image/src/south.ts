import util from 'util';
import { exec as _exec } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';
import { to } from 'await-to-js';
import tmp from 'tmp';
import express, {Request, Response} from 'express';
import bodyParser from 'body-parser';
import multer from 'multer';

import env from './env';
import * as chaincode from './chaincode';

const exec = util.promisify(_exec);

const app = express();
app.use(bodyParser.raw({type: 'application/octet-stream', limit : '2mb'}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const upload = multer({dest: '/tmp/'});

const {
  ORG_NAME,

  CRYPTO,
  CHANNEL,

  CHANNEL_NAME
} = env.vars;

const config_env = env.config;

// autostart network and discovery services
export const readyPromise = (async() => {
  const result = await exec('bash ./network.sh', config_env).catch(e => e);
  console.log(result);
})();

app.get('/orgName', (req: Request, res: Response) => res.send(ORG_NAME));

app.get('/fetchGenesisBlock', async function (req: Request, res: Response) {
  res.sendFile(`${CHANNEL}/genesis.block`);
});

app.get('/fetchChannelConfig', async function (req: Request, res: Response) {
  const channelName = req.query.channelName ? req.query.channelName : CHANNEL_NAME;

  const configBlockFileName = tmp.tmpNameSync();
  const [err] = await to(exec(`peer channel fetch config ${configBlockFileName} -o ${env.vars.ORDERER_DOMAIN}:7050 -c ${channelName} --tls --cafile ${env.vars.ORDERER_CA}`, config_env));
  if(err) {
    return res.status(500).send({error: err});
  }

  res.sendFile(configBlockFileName);
});

app.get('/fetchChannelConfigAsOrderer', async function (req: Request, res: Response) {
  const channelName = req.query.channelName ? req.query.channelName : "byfn-sys-channel";

  const configBlockFileName = tmp.tmpNameSync();
  const [err] = await to(exec(`peer channel fetch config ${configBlockFileName} -o ${env.vars.ORDERER_DOMAIN}:7050 -c ${channelName} --tls --cafile ${env.vars.ORDERER_CA}`, { env: Object.assign({}, env.vars, {
    CORE_PEER_ADDRESS: `${env.vars.ORDERER_DOMAIN}:7050`,
    CORE_PEER_LOCALMSPID: 'OrdererMSP',
    CORE_PEER_MSPCONFIGPATH: `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/users/Admin@${env.vars.CONS_DOMAIN}/msp`,
    CORE_PEER_TLS_ROOTCERT_FILE: `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/tls/ca.crt`
  })}));
  if(err) {
    return res.status(500).send({error: err});
  }

  res.sendFile(configBlockFileName);
});

app.get('/fetchChannelConfigBlock', async function (req: Request, res: Response) {
  const configBlockFileName = tmp.tmpNameSync();
  const [err] = await to(exec(`peer channel fetch 0 ${configBlockFileName} -o ${env.vars.ORDERER_DOMAIN}:7050 -c ${CHANNEL_NAME} --tls --cafile ${env.vars.ORDERER_CA}`, config_env));
  if(err) {
    return res.status(500).send({error: err});
  }

  res.sendFile(configBlockFileName);
});

app.post('/signConfigtx', async function(req: Request, res: Response) {
  const envelopeFileName = tmp.tmpNameSync();
  writeFileSync(envelopeFileName, req.body);

  const [err] = await to(exec(`peer channel signconfigtx -f ${envelopeFileName}`, config_env));
  if(err) {
    return res.status(200).send({error: err});
  }

  res.sendFile(envelopeFileName);
});

app.post('/signConfigtxWithOrderer', async function(req: Request, res: Response) {
  const envelopeFileName = tmp.tmpNameSync();
  writeFileSync(envelopeFileName, req.body);

  // FIXME extract OrgName from envelope
  /*if(!gov.orgMayJoin('Org2')) {
    return res.status(400).send(ORG_NAME+' has not voted yes to join!');
  }*/

  const [err] = await to(exec(`peer channel signconfigtx -f ${envelopeFileName}`, {
    env: {
      CORE_PEER_ADDRESS: `${env.vars.ORDERER_DOMAIN}:7050`,
      CORE_PEER_LOCALMSPID: 'OrdererMSP',
      CORE_PEER_MSPCONFIGPATH: `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/users/Admin@${env.vars.CONS_DOMAIN}/msp`,
      CORE_PEER_TLS_ROOTCERT_FILE: `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/tls/ca.crt`
    }
  }));
  if(err) {
    return res.status(500).send({error: err});
  }

  res.sendFile(envelopeFileName);
});

async function extractChannelNameFromConfigUpdateEnvelope(envelopeFileName: string) {
  const out = await exec(`configtxlator proto_decode --input ${envelopeFileName} --type common.Envelope | jq .payload.data.config_update.channel_id`);
  const channelName = JSON.parse(out.stdout);
  console.log(channelName);

  return channelName;
}

app.post('/updateChannel', async function(req: Request, res: Response) {
  const envelopeFileName = tmp.tmpNameSync();
  writeFileSync(envelopeFileName, req.body);

  const [err1, channelName] = await to(extractChannelNameFromConfigUpdateEnvelope(envelopeFileName));
  if(err1) {
    return res.status(200).send({error: err1});
  }

  // FIXME extract OrgName from envelope
  /*if(!gov.orgMayJoin('Org2')) {
    return res.status(400).send(ORG_NAME+' has not voted yes to join!');
  }*/

  const [err2] = await to(exec(`peer channel update -f ${envelopeFileName} -c ${channelName} -o ${env.vars.ORDERER_DOMAIN}:7050 --tls --cafile ${env.vars.ORDERER_CA}`, config_env));
  if(err2) {
    return res.status(200).send({error: err2});
  }

  res.status(200).send({message: 'Successful!'});
});

app.post('/updateChannelAsOrderer', async function(req: Request, res: Response) {
  const envelopeFileName = tmp.tmpNameSync();
  writeFileSync(envelopeFileName, req.body);
  
  const [err1, channelName] = await to(extractChannelNameFromConfigUpdateEnvelope(envelopeFileName));
  if(err1) {
    return res.status(200).send({error: err1});
  }

  const [err2] = await to(exec(`peer channel update -f ${envelopeFileName} -c ${channelName} -o ${env.vars.ORDERER_DOMAIN}:7050 --tls --cafile ${env.vars.ORDERER_CA}`,
    { env: Object.assign({}, env.vars, {
        CORE_PEER_ADDRESS: `${env.vars.ORDERER_DOMAIN}:7050`,
        CORE_PEER_LOCALMSPID: 'OrdererMSP',
        CORE_PEER_MSPCONFIGPATH: `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/users/Admin@${env.vars.CONS_DOMAIN}/msp`,
        CORE_PEER_TLS_ROOTCERT_FILE: `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/tls/ca.crt`
      })
    }));
  if(err2) {
    return res.status(200).send({error: err2});
  }

  res.status(200).send({message: 'Successful!'});
});

app.post('/setChaincodeContext', async function(req: Request, res: Response) {
  chaincode.setChaincodeContext(req.body);
  res.status(200).send({message: 'Successful!'});
});

app.post('/installChaincodeMulter', upload.single('chaincode'), async function(req: Request & {file: {path: string}}, res: Response) {
  req.setTimeout(10*60*1000);

  const [err] = await to(chaincode.installChaincodeFromFile(req.file.path));
  if(err) {
    return res.status(500).send({error: err});
  }

  res.status(200).send({message: 'Successful!'});
});

app.post('/installChaincode', async function(req: Request, res: Response) {
  req.setTimeout(10*60*1000);

  const [err] = await to(chaincode.installChaincode(req.body));
  if(err) {
    return res.status(500).send({error: err});
  }

  res.status(200).send({message: 'Successful!'});
});

app.post('/instantiateChaincode', async function(req: Request, res: Response) {  
  req.setTimeout(10*60*1000);

  const [err] = await to(chaincode.instantiateChaincode());
  if(err) {
    return res.status(500).send({error: err});
  }

  res.status(200).send({message: 'Successful!'});
});

app.post('/upgradeChaincode', async function(req: Request, res: Response) {  
  req.setTimeout(10*60*1000);

  const [err] = await to(chaincode.upgradeChaincode());
  if(err) {
    return res.status(500).send({error: err});
  }

  res.status(200).send({message: 'Successful!'});
});

app.get('/getOrdererOrgDir', async function (req: Request, res: Response) {
  await exec(`tar -zcvf /tmp/OrdererOrg.tar.gz -C ${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN} .`);
  res.sendFile('/tmp/OrdererOrg.tar.gz');
});

app.get('/getOrdererRootCert', async function (req: Request, res: Response) {
  res.sendFile(`${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/msp/tlscacerts/tlsca.${env.vars.CONS_DOMAIN}-cert.pem`);
});

app.get('/getOrdererCert', async function (req: Request, res: Response) {
  res.sendFile(`${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/tls/server.crt`);
});

app.get('/getPeerRootCert', async function (req: Request, res: Response) {
  const PEER_NUM = req.query.num;

  const PEER_DOMAIN = `peer${PEER_NUM}.${env.vars.ORG_DOMAIN}`;
  res.sendFile(`${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/ca.crt`);
});

app.get('/getPeerCert', async function (req: Request, res: Response) {
  const PEER_NUM = req.query.num;

  const PEER_DOMAIN = `peer${PEER_NUM}.${env.vars.ORG_DOMAIN}`;
  res.sendFile(`${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/server.crt`);
});

app.post('/addOrdererConsenterToConfig', async function(req: Request, res: Response) {
  const config = req.body;

  /*const ordererConfig = config.channel_group.groups.Orderer.groups.OrdererOrg.values.MSP.value.config;

  const ordererAdminCert = readFileSync(
    `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/msp/admincerts/Admin@${env.vars.CONS_DOMAIN}-cert.pem`
  ).toString('base64');
  ordererConfig.admins.push(ordererAdminCert);

  const ordererRootCert = readFileSync(
    `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/msp/cacerts/ca.${env.vars.CONS_DOMAIN}-cert.pem`
  ).toString('base64');
  ordererConfig.root_certs.push(ordererRootCert);

  const ordererTlsRootCert = readFileSync(
    `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/tls/ca.crt`
  ).toString('base64');
  ordererConfig.tls_root_certs.push(ordererTlsRootCert);*/

  const ordererTlsCert = readFileSync(
    `${CRYPTO}/ordererOrganizations/${env.vars.CONS_DOMAIN}/orderers/${env.vars.ORDERER_DOMAIN}/tls/server.crt`
  ).toString('base64');
  config.channel_group.groups.Orderer.values.ConsensusType.value.metadata.consenters.push({
    client_tls_cert: ordererTlsCert,
    host: env.vars.ORDERER_DOMAIN,
    port: 7050,
    server_tls_cert: ordererTlsCert
  });

  res.status(200).json(config);
});

app.post('/addOrdererAddressToConfig', async function(req: Request, res: Response) {
  const config = req.body;
  config.channel_group.values.OrdererAddresses.value.addresses.push(`${env.vars.ORDERER_DOMAIN}:7050`);

  res.status(200).send(config);
});

app.listen(8080, function () {
  console.log('South interface listening on port 8080!');
});
