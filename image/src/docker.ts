import { to } from 'await-to-js';
import Dockerode from 'dockerode';
import Docker from 'dockerode';

import env from './env';

const {
  DEBUG_MODE,
} = env.vars;

const docker = new Docker({socketPath: '/var/run/docker.sock'});
async function getContainerByName(name: string): Promise<Docker.Container | undefined> {
    const containers = await docker.listContainers();
    const containerDesc = containers.find(c => c.Names.includes('/'+name));

    if(!containerDesc) {
        return undefined;
    }

    return docker.getContainer(containerDesc.Id);
}
docker['getContainerByName'] = getContainerByName;

async function listNodesDebuggable() {
    if(DEBUG_MODE) {
        return [{ID: 'cli.org1'}, {ID: 'cli.org2'}];
    }

    const [err, res] = await to(docker.listNodes());
    if(err) {
        return [{ID: 'localhost'}];
    }
    return res;
};
docker['listNodesDebuggable'] = listNodesDebuggable;

export default docker as Dockerode & {getContainerByName, listNodesDebuggable}
