import uuidv4 from 'uuid/v4';
import { pick, flatten } from 'lodash';
import Bluebird from 'bluebird';
import axios, { Method } from 'axios';
import { Express } from 'express';

import env from './env';
import docker from './docker';
import * as database from './db';
import * as lobby from './lobby';
import * as orgs from './orgs';
import * as actions from './actions';
import * as utils from './utils';
import { ConsortiumRequest, Request as GovRequest, RequestCore, Vote, VoteCore } from './generated/graphqlTypes';

const {
  ADMIN_MODE
} = env.vars;

/*
request model
{
  payload: {
    type: string,
    key: string,
    subkey: string,
    value: int, // -1 no 0 na 1 yes
    parent: {type: string, key: string, value: int},
    children: [{type: string, key: string, value: int}]
  },
  sig: string,
  pub: string,
  handler: callback // TODO how does this work with dbs? use type instead
}
*/

export async function addRequest(request: RequestCore): Promise<void> {
  const db = await database.mongoPromise;

  // generate _id if none is present
  if(!request._id) {
    request._id = uuidv4();
  }

  // set quorum if none present
  if(!request.quorum) {
    request.quorum = await orgs.getAddresses();
  }

  await db.collection('requests').insert(request);
}

export async function requests(): Promise<RequestCore[]> {
  const db = await database.mongoPromise;
  
  // get regular requests
  let requests: RequestCore[] = await db.requests.find();
  console.log(requests);
  if(!requests) {
    requests = [];
  }

  // set empty children array if null
  requests.forEach(request => {
    if(!request.children) {
      request.children = [];
    }
  });
  
  // get lobby requests
  const cid = await lobby.cid();
  const lobbyRequests = await lobby.listRequests(cid).catch(() => <ConsortiumRequest[]>[]);
  console.log('lobby requests for cid', cid, lobbyRequests);
  
  // append to request list
  for(const lobbyReq of lobbyRequests) {
    const request = convertLobbyReqToLocalReq(lobbyReq);
    requests.push(request);
  }

  return requests;
}

export async function syncRequests(express: Express): Promise<void> {
  const ownOrgRequests = await requests();

  // find all yet unknown settled requests
  const unknownRequests = await Bluebird.map(orgs.getAddresses(), async(oaddr: string) => {
    return axios.get(`http://${oaddr}/api/sofa/gov`).then(res => res.data.requests);
    //return axios.get(`http://${oaddr}/api/requests`).then(res => res.data); // FIXME deprecated
  }).reduce((m: GovRequest[], i: GovRequest[]) => m.concat(i)). // flatMap
  //then(requests => uniqBy(requests, req => req._id)). // FIXME breaks build
  map(r => Object.assign(r, { __typename: 'RequestCore', parent: r.parent?._id }) as unknown as RequestCore). // only required with sofa API
  filter(r =>
    r.type === 'org_join' &&
    !ownOrgRequests.find(or => or._id === r._id)
  ).each(r => addRequest(r));

  console.log('sync unknown requests', unknownRequests);

  // execute all yet unknown settled requests to bring the node up-to-date
  await Bluebird.each(unknownRequests, ur => onMajorityReached(ur, express));
}

export async function votes(): Promise<Vote[]> {
  const db = await database.mongoPromise;
  const voter = await orgs.getOwn();

  return Bluebird.map(<VoteCore[]>db.votes.find(),
    async(vote: VoteCore) => Object.assign(vote as unknown as Vote, {voter}));
}

// TODO unify with regular request function?
export async function getRequestById(rid: string): Promise<RequestCore | undefined> {
  const db = await database.mongoPromise;

  // find and return local request
  const request = await db.requests.findOne({_id: rid});
  if(request) {
    if(!request.children) {
      request.children = [];
    }
    return request;
  }

  // or find and return lobby request
  const cid = await lobby.cid();
  const lobbyRequests = await lobby.listRequests(cid).catch(() => <ConsortiumRequest[]>[]);
  const lobbyRequest = lobbyRequests.find(r => r._id === rid);
  if(lobbyRequest) {
    return convertLobbyReqToLocalReq(lobbyRequest);
  }
}

function convertLobbyReqToLocalReq(lobbyRequest: ConsortiumRequest): RequestCore {
  return {
    _id: lobbyRequest._id,
    type: 'org_join',
    key: lobbyRequest.cid,
    subkey: lobbyRequest.rpub,
    children: [],
    quorum: [] // FIXME simply wrong - TODO save quorum in lobby request?
  };
}

async function hasBeenVotedFor(type: string, key: string, subkey?: string): Promise<boolean> {
  if(ADMIN_MODE) {
    return true;
  }

  const db = await database.mongoPromise;
  
  // find request
  const request: RequestCore = await db.requests.findOne({type, key, subkey});
  if(!request) {
    console.log('Request does not exist.', request);
    return false;
  }
  console.log('Found request.', request);
  
  // check if request has been settled
  if(request.settled) {
    return true;
  }

  // check for vote
  const vote: VoteCore = await db.votes.findOne({rid: request._id});
  if(!!vote && vote.value > 0) {
    console.log('Vote does exist and is positive.', vote);
    return true;
  }
  console.log('Found vote.', vote);
  
  // verify that request is actually a child of the parent
  const parentRequest: RequestCore = await db.requests.findOne({_id: request.parent}); 
  if(!parentRequest || !parentRequest.children) {
    console.log('Parent request does not exist or has no children.', parentRequest);
    return false;
  }
  const isChild = parentRequest.children.some(childId => childId === request._id);
  if(!isChild) {
    console.log('Request is not child of parent request.', parentRequest);
    return false;
  }

  // check if parent request is settled
  if(parentRequest.settled) {
    return true;
  }

  // check parent vote
  const parentVote: VoteCore = await db.votes.findOne(
    {rid: parentRequest._id},
    {value: 1});
    
  return !!parentVote && parentVote.value > 0;
  //return hasBeenVotedFor(parentRequest.type, parentRequest.key, parentRequest.subkey);
}

export async function orgMayJoin(orgName: string): Promise<boolean> {
  return hasBeenVotedFor('org_join', orgName);
}

export async function orgMayLeave(orgName: string): Promise<boolean> {
  return hasBeenVotedFor('org_leave', orgName);
}

// eslint-disable-next-line no-unused-vars
export async function chaincodeMayBeInstalled(name: string, version: string, hash: string): Promise<boolean> {
  //return hasBeenVotedFor('chaincode_install', name, version+'_'+hash);
  return hasBeenVotedFor('chaincode_install', name, version);
}

export async function chaincodeMayBeInstantiated(name: string, version: string): Promise<boolean> {
  return hasBeenVotedFor('chaincode_instantiate', name, version);
}

export async function chaincodeMayBeUpgraded(name: string, version: string): Promise<boolean> {
  return hasBeenVotedFor('chaincode_upgrade', name, version);
}

export async function vote(vote: VoteCore): Promise<void> {
  const db = await database.mongoPromise;
  
  await db.votes.update(
    pick(vote, 'rid'),
    {$set: pick(vote, 'value')},
    {upsert: true}
  );

  // get request
  const request: RequestCore = await getRequestById(vote.rid);
  console.log('voted request', vote, request);
  
  if('org_join' === request.type) {
    const cid = await lobby.cid();
    if(vote.value > 0) {
      await lobby.addApproval(cid, request.subkey);
    } else if(vote.value < 0) {
      await lobby.removeApproval(cid, request.subkey);
      return; // do not execute action on negative vote
    } else {
      return; // do not execute action on indecisive vote
    }
  }

  // if 2/3 majority reached, then notify all orgs
  if(await isMajorityReached(request)) {
    // mark request as settled
    await db.votes.update(
      pick(vote, 'value'),
      {$set: {settled: true}}
    );

    await forwardToAllOrgs('api/notifyPotentialMajorityReached', request, 'POST');
  }
}

export async function onPotentialMajorityReached(request: RequestCore, express: Express): Promise<void> {
  // check if 2/3 majority was actually reached, then execute action
  if(await isMajorityReached(request)) {
    await onMajorityReached(request, express);
  }
}

async function onMajorityReached(request: RequestCore, express: Express): Promise<void> {
  actions.executeAction(request.type, request.key, request.subkey, express); // no wait
}

async function getClis(): Promise<string[]> {
  const orgs = await utils.getOrgNames();
  return orgs.map(org => `cli.${org.toLowerCase()}`);
}

async function forwardToAllOrgs(route: string, data: any, method: Method): Promise<string[]> {
  return Bluebird.map(getClis(), async cli =>
    axios({
      method: method || 'POST',
      url: `http://${cli}/${route}`,
      ...((!method || 'POST' == method) && {data}),
    }).then(res => res.data)
  );
}

async function isMajorityReached(request: RequestCore): Promise<boolean> {
  const resultsByOrgs = await forwardToAllOrgs('api/votes', undefined, 'GET');
  const votes = flatten<Vote>(resultsByOrgs);
  
  const numVotes = votes.filter((vote: VoteCore) => vote.rid === request._id).length;
  const numOrgs = resultsByOrgs.length;
  const cond = numVotes/numOrgs > 2/3;

  console.log('majority', request.type, request.key, request.subkey, cond);

  return cond;
}

export type VotesByRequest = {[key: string]: Vote[]};

export async function collectVotesByRequestFromAllOrgs(): Promise<VotesByRequest> {
    const votesByOrgArray =
      await Bluebird.map(docker.listNodesDebuggable(), async(node: {ID: string}): Promise<[string, Vote[]]> => {
        const votes: Vote[] = await axios.get(`http://${node.ID}/api/votes`).then(res => res.data);
        return [node.ID, votes];
      });//.filter(([_nodeId, votes]) => '' !== votes);
    console.log(votesByOrgArray);

    const votesByRequest: {[key: string]: Vote[]} = {};
    for (const orgVotes of votesByOrgArray) {
      const [_org, votes] = orgVotes;

      for (const vote of votes) {
        if (!votesByRequest[vote.rid]) {
          votesByRequest[vote.rid] = [];
        }

        votesByRequest[vote.rid].push(vote);
      }
    }
    console.log(votesByRequest);

    return votesByRequest;
}
