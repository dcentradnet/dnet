import { ObjectId } from 'mongoist';
import bcrypt from 'bcrypt';
import env from './env';
import * as database from './db';
import { User, UserCore } from './generated/graphqlTypes';
import * as roles from './roles';

const {
    ADMIN_MODE
} = env.vars;

async function hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 10);
}

const usersCreatedPromise = (async() => {
    console.log(await database.mongoPromise); // FIXME currently required. Otherwise db is undefined. Why?
    const db = await database.mongoPromise;

    // TODO create indices

    const userData = {name: 'Administrator', passHash: await hashPassword('admin')};
    const roleData = {name: 'Administrator'};
    const user = await db.users.findAndModify({query: {name: 'Administrator'}, update: {$set: userData}, new: true, upsert: true});
    const role = await db.roles.findAndModify({query: {name: 'Administrator'}, update: {$set: roleData}, new: true, upsert: true});

    const userRoleData = {userId: user._id, roleId: role._id};
    await db.userRoles.findAndModify({query: userRoleData, update: {$set: userRoleData}, new: true, upsert: true});
    console.log('created standard users and roles', user, role);

    return db;
})();

export async function list(query?: Record<string, string>): Promise<User[]> {
    const db = await usersCreatedPromise;

    return db.users.find(query);
}

export async function put(name: string, password?: string, _id?: string): Promise<UserCore> {
    const db = await usersCreatedPromise;

    const passHash = password && await hashPassword(password);
    const userData = {name, passHash};
    return db.users.findAndModify({query: {_id: ObjectId(_id)}, update: {$set: userData}, new: true, upsert: true});
}

export async function remove(_id: string): Promise<void> {
    const db = await usersCreatedPromise;

    await db.users.remove({_id: ObjectId(_id)}, {justOne: true});
}

export async function checkPasswordByUser(user: User, password: string): Promise<boolean> {
    return !user.passHash || bcrypt.compare(password, user.passHash); // FIXME disallow empty passwords
}

export async function checkPassword(_id: string, password: string): Promise<boolean> {
    const db = await usersCreatedPromise;

    const {passHash} = await db.users.findOne({_id: ObjectId(_id)}, {passHash: 1});
    return bcrypt.compare(password, passHash);
}

export async function hasRole(user: User, roleName: string): Promise<boolean> {
    if(!user.roles) {
        return roles.hasUserRoleByName(user._id, roleName);
    }

    return !!user.roles.find(role => role.name === roleName);
}

export async function isAdmin(user: User): Promise<boolean> {
    if(ADMIN_MODE) {
        return true;
    }

    return hasRole(user, 'Administrator');
}
