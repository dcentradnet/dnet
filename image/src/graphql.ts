import { join } from 'path';
import http from 'http';
import Bluebird from 'bluebird';
import retry from 'bluebird-retry';
import { ChannelInfo, ChaincodeInfo } from 'fabric-client';
import { ObjectId } from 'mongoist';
import Express from 'express';
import axios from 'axios';
import { fetch } from 'cross-fetch';
import { graphql, ExecutionResult, print, execute, subscribe, GraphQLArgs } from 'graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { introspectSchema } from '@graphql-tools/wrap';
import GraphQLJSON, { GraphQLJSONObject } from 'graphql-type-json';
import { addResolversToSchema, delegateToSchema, loadSchemaSync, observableToAsyncIterable, wrapSchema } from 'graphql-tools';
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { useSofa } from 'sofa-api';

import chaincodeQuery from './sdk/query';
import chaincodeInvoke from './sdk/invoke';

import env from './env';
import docker from './docker';
import * as crypt from './crypt';
import * as utils from './utils';
import * as auth from './auth';
import * as gov from './gov';
import * as orgs from './orgs';
import * as lobby from './lobby';
import * as users from './users';
import * as roles from './roles';
import * as perms from './permissions';
import * as fabric from './fabric';
import * as chaincode from './chaincode';
import * as dapps from './dapps';
import * as plugins from './plugins';
import * as actions from './actions';
import * as swarm from './swarm';
import { Chaincode, RequestCore as GovRequestCore, Vote as GovVote, Role, User, Consortium, RoleCore, UserCore,MutationInstallPluginArgs, MutationInstallDappArgs, MutationRemoveRoleFromUserArgs, MutationAttachRoleToUserArgs, MutationRemoveRoleArgs, MutationRemoveUserArgs, MutationPutUserArgs, MutationPutRoleArgs, MutationChaincodeInvokeArgs, MutationVoteArgs, MutationRequestBroadcastArgs, MutationRequestArgs, MutationNetworkExtendArgs, MutationNetworkShutdownArgs, MutationNetworkInitArgs, MutationSwarmJoinCryptedArgs, MutationSwarmJoinArgs, MutationSwarmInitArgs, MutationLobbyRequestAddArgs, MutationLobbyRequestRemoveArgs, SubscriptionChaincodeEventArgs, MutationLobbyConsortiaAddArgs, Organization, MutationRemovePermFromRoleArgs, MutationAttachPermToRoleArgs, MutationPutPermissionArgs, MutationRemovePermissionArgs, Permission, QueryPermissionsArgs, QueryRolesArgs, QueryUsersArgs, MutationLoginUserArgs, VoteCore, Info, Notification, MutationLobbyConsortiumSetArgs, ConsortiumOrganization, MutationLobbyOrganizationPutArgs, LobbyOrganizationArgs} from './generated/graphqlTypes';

import { ExecutionResultDataDefault } from 'graphql/execution/execute';
import { ConsortiumMember } from './generated/graphqlLobbyTypes';
import { collectVotesByRequestFromAllOrgs } from './gov';
import { registerChaincodeEvent } from './sdk/socket';
import { pubsub, replayNotifications } from './notifications';

const schema = loadSchemaSync(join(__dirname, '../models/schema.graphql'), {
    loaders: [new GraphQLFileLoader()]
});

async function subschemaExecutor(subschemaUrl: string, { document, variables }) {
    const query = print(document);

    return retry(async() => {
        try {
            const fetchResult = await fetch(subschemaUrl, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ query, variables }),
            });

            return fetchResult.json();
            // eslint-disable-next-line no-empty
        } catch(e) {}
    }, {timeout: 10*10000, max_tries: 10});
}

const schemaPromise = (async() => {
    // create remote lobby subschema
    const lobbyExecutor = async args => subschemaExecutor('https://dnetlobby.now.sh/api/graphQL', args);
    const lobbySubschema = wrapSchema({
        schema: await introspectSchema(lobbyExecutor),
        executor: lobbyExecutor,
        // subscriber: remoteSubscriber
    });

    // define local resolvers
    const orgName = env.vars.ORG_NAME;
    const resolvers = {
        Organization: {
            _id: async(parent: Organization): Promise<string> => parent?._id || crypt.getPublicKeyString(),
            name: (parent: Organization): string => parent?.name || orgName,
            users: async(parent: Organization): Promise<User[]> => parent?.users || users.list(),
            roles: async(parent: Organization): Promise<Role[]> => parent?.roles || roles.list(),
            //dapps: () => ({}),
            //plugins: () => ({}),
            publicIpV4: async(parent: Organization): Promise<string> => parent?.publicIpV4 || utils.getPublicIp(),
            //publicIpV6: ipV6,
            publicKey: async(parent: Organization): Promise<string> => parent?.publicKey || crypt?.getPublicKeyString(),
            info: async(parent: Organization): Promise<ConsortiumOrganization> => lobby.organization(parent?.publicKey || await crypt?.getPublicKeyString())
        },
        Swarm: {
            nodes: async() => docker.listNodes(),
            inspect: async() => docker.swarmInspect()
        },
        Channel: {
            _id: (parent: ChannelInfo): string => parent.channel_id,
            chaincodes: async(parent: ChannelInfo): Promise<Chaincode[]> =>
                Bluebird.map(chaincode.queryInstantiated(parent.channel_id), async(ccInfo: ChaincodeInfo): Promise<Chaincode> => ({
                    name: ccInfo.name
                }))
        },
        Container: {
            Health: async (parent: {Name: string}) => {
                if (!(parent.Name.includes('orderer') || parent.Name.startsWith('peer'))) {
                    return {'status': 'OK'};
                }
                
                return axios.get(`http://${parent.Name}:9443/healthz`);
            }
        },
        Lobby: {
            organization: async(_parent, {pub}: LobbyOrganizationArgs): Promise<ConsortiumOrganization | undefined> =>
                lobby.organization(pub || await crypt?.getPublicKeyString()),
            requested: async(): Promise<Consortium | undefined> => lobby.requested(),
            joined: async(): Promise<Consortium | undefined> => lobby.joined(),
            member: async(): Promise<ConsortiumMember | undefined> => {
                const requestedCid = await lobby.requestedCid();
                const pub = await crypt.getPublicKeyString();
                if(requestedCid) {
                    return lobby.listMembers(requestedCid, pub).then(m => m[0]);
                }
            }
        },
        RequestCore: {
            settled: async(request: GovRequestCore, _args, context): Promise<boolean> => {
                const votes = context.votesByRequest[request._id] || [];
                return !!votes && votes.some((vote: GovVote) => vote.voter.name === orgName && vote.settled)
            }
        },
        Request: {
            parent: async(request: GovRequestCore) => request.parent && gov.getRequestById(request.parent),
            children: async(request: GovRequestCore) => {
                const children = request.children ?
                  await Bluebird.map(request.children, async c => gov.getRequestById(c)) : [];
                return children;
            },
            quorum: async(request: GovRequestCore): Promise<Organization[]> =>
                Bluebird.map(request.quorum, oaddr => axios.get(`http://${oaddr}/api/sofa/organization`).then(res => res.data)),
            votes: (request: GovRequestCore, _args, context): VoteCore[] => context.votesByRequest[request._id] || [],
            ownvote: async(request: GovRequestCore, _args, context) => {
                const votes = context.votesByRequest[request._id] || [];
                let ownvote = null;
                if(votes) {
                    ownvote = votes.find((vote: GovVote) => vote.voter.name === orgName);
                }
                return ownvote;
            },
            settled: async(request: GovRequestCore, _args, context): Promise<boolean> => { // TODO deduplicate
                const votes = context.votesByRequest[request._id] || [];
                return !!votes && votes.some((vote: GovVote) => vote.voter.name === orgName && vote.settled)
            }
        },
        Governance: {
            request: async(_parent, args): Promise<GovRequestCore> => {
                return gov.getRequestById(args.ID);
            },
            requests: async(_parent, _args, context): Promise<GovRequestCore[]> => {
                context.votesByRequest = await collectVotesByRequestFromAllOrgs();
                return gov.requests();
            }
        },
        User: {
            roles: async(parent): Promise<Role[]> => roles.listUserRoles(parent._id)
        },
        Role: {
            users: async(parent): Promise<User[]> => roles.listUsersByRole(parent._id)
        },
        ChaincodeQuery: {
            installed: async() => chaincode.queryInstalled(),
            instantiated: async() => chaincode.queryInstantiated()
        },
        Dapps: {
            installable: async() => dapps.listStoreApps(),
            installed: async() => dapps.listRunning()
        },
        Plugins: {
            installable: async() => plugins.listInstallable(),
            installed: async() => plugins.listRunning()
        },

        Query: {
            info: async(): Promise<Info> => orgs.getInfo(),
            organization: async(): Promise<Organization> => orgs.getOwn(),
            consortium: () => async(): Promise<Consortium | undefined> => lobby.joined(),
            organizations: async(): Promise<Organization[]> => {
                return Bluebird.map(orgs.getAddresses(), async(oaddr: string): Promise<Organization> => {
                    return axios.get(`http://${oaddr}/api/sofa/organization`).then(res => res.data);
                });
            },

            swarm: () => ({}),
            containers: async() => {
                const result = await docker.getNetwork(`net_${env.vars.CONS_DOMAIN}`).inspect();
                return Object.values(result.Containers);
            },
            channels: async() => fabric.channels(),
            actions: () => actions.listPendingActions(),

            lobby: (_parent, _args, context, info) => ({
                consortia: async(/*use parent parameters*/) => delegateToSchema({
                    schema: lobbySubschema,
                    operation: 'query',
                    fieldName: 'lobby',
                    context,
                    info
                }).then(lobby => lobby.consortia)
            }),
            gov: () => ({}),

            users: async(_parent, {_id}: QueryUsersArgs): Promise<User[]> => users.list(_id && {_id: ObjectId(_id)}),
            roles: async(_parent, {_id}: QueryRolesArgs): Promise<Role[]> => roles.list(_id && {_id: ObjectId(_id)}),
            permissions: async(_parent, {_id}: QueryPermissionsArgs): Promise<Permission[]> => perms.list(_id && {_id: ObjectId(_id)}),

            dapps: () => ({}),
            plugins: () => ({}),

            replayNotifications: async(): Promise<Notification[]> => replayNotifications(),
            chaincodeQuery: async(_parent, args) => chaincodeQuery(args.channel, args.chaincode, args.method, args.args),
        },

        Subscription: {
            notification: {
                subscribe: () => pubsub.asyncIterator('NOTIFICATION')
            },
            statusUpdate: {
                subscribe: () => pubsub.asyncIterator('STATUS_UPDATE')
            },
            requestAdd: {
                subscribe: () => pubsub.asyncIterator('REQUEST_ADD')
            },
            chaincodeEvent: {
                subscribe: ({input}: SubscriptionChaincodeEventArgs) => observableToAsyncIterable(registerChaincodeEvent(input))
            }
        },

        Mutation: {
            swarmInit: async(_parent, {params}: MutationSwarmInitArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }

                await swarm.init(params);
            },
            swarmJoin: async(_parent, {params}: MutationSwarmJoinArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }

                await swarm.join(params);
            },
            swarmJoinCrypted: async(_parent, {crypted, opub}: MutationSwarmJoinCryptedArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }

                await swarm.joinCrypted({crypted, opub});
            },

            networkInit: async(_parent, {consDomain, waitFinish}: MutationNetworkInitArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                
                const [, log] = await actions.executeAction('fabric_init', consDomain);

                if(waitFinish) {
                  await log.toPromise();
                }
            },
            networkShutdown: async(_parent, {consDomain, waitFinish}: MutationNetworkShutdownArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                
                const [, log] = await actions.executeAction('fabric_shutdown', consDomain);

                if(waitFinish) {
                  return await log.toPromise();
                }
            },
            networkExtend: async(_parent, {consDomain, waitFinish}: MutationNetworkExtendArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                
                const [, log] = await actions.executeAction('fabric_extend', consDomain);

                if(waitFinish) {
                  await log.toPromise();
                }
            },

            lobbyConsortiaAdd: async(_parent, {cid, name, desc}: MutationLobbyConsortiaAddArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await lobby.addConsortium(cid, name, desc);
            },
            lobbyConsortiumSet: async(_parent, {cid}: MutationLobbyConsortiumSetArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await lobby.setCid(cid);
            },
            lobbyRequestAdd: async(_parent, {cid}: MutationLobbyRequestAddArgs, {user}: {user: User}) => {
                /*if(!users.hasPermission(user, 'lobby-request-add')) {
                    return;
                }*/
                await lobby.addRequest(cid);
            },
            lobbyRequestRemove: async(_parent, {cid}: MutationLobbyRequestRemoveArgs, {user}: {user: User}) => {
                /*if(!users.hasPermission(user, 'lobby-request-remove')) {
                    return;
                }*/
                await lobby.removeRequest(cid);
            },
            lobbyOrganizationPut: async(_parent, args: MutationLobbyOrganizationPutArgs, {user}: {user: User}) => {
                /*if(!users.hasPermission(user, 'lobby-organization-put')) {
                    return;
                }*/
                await lobby.putOrganization(Object.assign({
                    pub: await crypt.getPublicKeyString()
                }, args.info));
            },
            lobbyOrganizationRemove: async(_parent, _args: void, {user}: {user: User}) => {
                /*if(!users.hasPermission(user, 'lobby-organization-remove')) {
                    return;
                }*/
                await lobby.removeOrganization();
            },

            request: async(_parent, {request}: MutationRequestArgs, {user}: {user: User}) => {
                /*if(!users.hasPermission(user, 'request')) {
                    return;
                }*/
                await gov.addRequest(request);
                pubsub.publish('REQUEST_ADD', request);
            },
            requestBroadcast: async(_parent, {request}: MutationRequestBroadcastArgs, {user}: {user: User}) => {
                /*if(!users.hasPermission(user, 'request')) {
                    return;
                }*/
                await Bluebird.map(orgs.getAddresses(), async(oaddr: string): Promise<void> => {
                    await axios.post(`http://${oaddr}/api/sofa/request`, {request});
                });
            },
            vote: async(_parent, {vote}: MutationVoteArgs, {user}: {user: User}) => {
                /*if(!users.hasPermission(user, 'vote')) {
                    return;
                }*/
                await gov.vote(vote);
            },
            syncRequests: async(_parent, _args, {user, express}: {user: User, express: Express.Express}) => {
                /*if(!users.hasPermission(user, 'vote')) {
                    return;
                }*/
                await gov.syncRequests(express);
            },

            chaincodeInvoke: async(_parent, args: MutationChaincodeInvokeArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await chaincodeInvoke(args.channel, args.chaincode, args.method, args.args);
                pubsub.publish('CHAINCODE_INVOKE', args);
            },

            putUser: async(_parent, args: MutationPutUserArgs, {user}: {user: User}): Promise<UserCore> => {
                if(!users.isAdmin(user)) {
                    return;
                }
                const userPut = await users.put(args.name, args.password, args._id);
                pubsub.publish('USER_PUT', userPut);
                return userPut;
            },
            removeUser: async(_parent, args: MutationRemoveUserArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await users.remove(args._id);
            },
            loginUser: async(_parent, args: MutationLoginUserArgs): Promise<string | null> => {
                if(await users.checkPassword(args._id, args.password)) {
                    return auth.createToken(args._id.toString());
                }
                return null;
            },

            putRole: async(_parent, args: MutationPutRoleArgs, {user}: {user: User}): Promise<RoleCore> => {
                if(!users.isAdmin(user)) {
                    return;
                }
                const rolePut = await roles.put(args.name, args._id);
                pubsub.publish('ROLE_PUT', rolePut);
                return rolePut;
            },
            removeRole: async(_parent, args: MutationRemoveRoleArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await roles.remove(args._id);
            },

            attachRoleToUser: async(_parent, args: MutationAttachRoleToUserArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await roles.attachRoleToUser(args.roleId, args.userId);
            },
            removeRoleFromUser: async(_parent, args: MutationRemoveRoleFromUserArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await roles.removeRoleFromUser(args.roleId, args.userId);
            },

            putPermission: async(_parent, args: MutationPutPermissionArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                const permPut = await perms.put(args.name, args._id);
                pubsub.publish('PERMISSION_PUT', permPut);
                return permPut;
            },
            removePermission: async(_parent, args: MutationRemovePermissionArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                const permRemove = await perms.remove(args._id);
                pubsub.publish('PERMISSION_REMOVE', permRemove);
                return permRemove;
            },

            attachPermToRole: async(_parent, args: MutationAttachPermToRoleArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await perms.attachPermToRole(args.permId, args.roleId);
            },
            removePermFromRole: async(_parent, args: MutationRemovePermFromRoleArgs, {user}: {user: User}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await perms.removePermFromRole(args.permId, args.roleId);
            },

            installPlugin: async(_parent, {plugin}: MutationInstallPluginArgs, {user, express}: {user: User, express: Express.Express}) => {
                if(!users.isAdmin(user)) {
                    return;
                }
                await plugins.install(plugin._id, plugin.parameters, express);
            },
            installDapp: async(_parent, args: MutationInstallDappArgs, {user, express}: {user: User, express: Express.Express}) => {
                if(!users.isAdmin(user)) {
                    return;
                }

                const [progress] = dapps.install(args.dapp, undefined, express);
                // wait for completion
                await progress.toPromise();
            },
        }
    };

    return addResolversToSchema({schema, resolvers});
})();

const root = {
    JSON: GraphQLJSON,
    JSONObject: GraphQLJSONObject,
};

export default async function graphQL(
    query: string, variables: {[key: string]: unknown},
    user: User, express: Express.Express
): Promise<ExecutionResult<ExecutionResultDataDefault>> {
    const schema = await schemaPromise;

    return graphql(schema, query, root, {user, express}, variables);
}

export async function connectGraphQLSubscriptionServer(httpServer: http.Server): Promise<void> {
    const schema = await schemaPromise;

    SubscriptionServer.create({
        // This is the `schema` we just created.
        schema,
        // These are imported from `graphql`.
        execute,
        subscribe,
     }, {
        // This is the `httpServer` we created in a previous step.
        server: httpServer,
        // This `server` is the instance returned from `new ApolloServer`.
        path: '/api/graphQLWS'
     });
}

export async function connectGraphQLRestAPI(express: Express.Express): Promise<void> {
    const schema = await schemaPromise;

    express.use('/api/sofa', useSofa({
        basePath: '/api/sofa',
        schema,
        execute: async(args: GraphQLArgs): Promise<ExecutionResult<unknown>> => {
            return graphql(schema, args.source, root, args.contextValue, args.variableValues);
        },
        context: ({req}: {req: Express.Request}): {user: unknown, express: Express.Express} => {
            req.setTimeout(60*60*1000);

            return {
                user: req.user,
                express
            };
        }
    }));
}
