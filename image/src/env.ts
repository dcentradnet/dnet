const ADMIN_MODE=('true' == process.env.ADMIN_MODE);
const DEBUG_MODE=('true' == process.env.DEBUG_MODE);

console.log('ADMIN_MODE', ADMIN_MODE);
console.log('DEBUG_MODE', DEBUG_MODE);

const HOST_PWD=process.env.HOST_PWD;
const HOST_DB=`${HOST_PWD}/db`
const CRYPTO_HOST=`${HOST_PWD}/crypto-config`

const CONS_DOMAIN=process.argv[2];
const ORG_NAME=process.argv[3];

const ORG_LOWER=ORG_NAME.toLowerCase();
const ORG_MSP=`${ORG_NAME}MSP`;

const CONFIG='/home/node/app/config';
const CRYPTO='/home/node/app/crypto-config';
const CHANNEL='/home/node/app/channel-artifacts';
const WALLET='/home/node/app/wallet';
const LOG='/home/node/app/log';
const PLUGINS='/plugins';

const CHANNEL_NAME = 'main';
let CC_NAME;
let CC_SRC_PATH;

const CORE_PEER_LOCALMSPID=ORG_MSP;
const CORE_PEER_TLS_ENABLED=true;

const vars = {
  ADMIN_MODE,
  DEBUG_MODE,

  HOST_PWD,
  HOST_DB,
  CRYPTO_HOST,

  ORG_NAME,
  
  ORG_LOWER,
  ORG_MSP,
  
  CONFIG,
  CRYPTO,
  CHANNEL,
  WALLET,
  PLUGINS,
  LOG,
  
  CHANNEL_NAME,
  CC_NAME,
  CC_SRC_PATH,

  CORE_PEER_LOCALMSPID,
  CORE_PEER_TLS_ENABLED
};

export function setConsDomain(CONS_DOMAIN: string): void {
  const ORG_DOMAIN=`${ORG_LOWER}.${CONS_DOMAIN}`;

  const PEER0_DOMAIN=`peer0.${ORG_DOMAIN}`;
  const PEER1_DOMAIN=`peer1.${ORG_DOMAIN}`;

  const CA_DOMAIN=`ca.${ORG_DOMAIN}`;
  const CLI_DOMAIN=`cli.${ORG_LOWER}`;
  const MONGO_DOMAIN=`mongo.${ORG_DOMAIN}`;

  const ORDERER_DOMAIN=`${ORG_LOWER}.orderer.${CONS_DOMAIN}`;
  const ZOOKEEPER_DOMAIN=`${ORG_LOWER}.zookeeper.${CONS_DOMAIN}`;
  const KAFKA_DOMAIN=`${ORG_LOWER}.kafka.${CONS_DOMAIN}`;

  const CORE_PEER_ID=CLI_DOMAIN;
  const CORE_PEER_ADDRESS=`${PEER0_DOMAIN}:7051`;
  const CORE_PEER_TLS_CERT_FILE=`${CRYPTO}/peerOrganizations/${ORG_DOMAIN}/peers/${PEER0_DOMAIN}/tls/server.crt`;
  const CORE_PEER_TLS_KEY_FILE=`${CRYPTO}/peerOrganizations/${ORG_DOMAIN}/peers/${PEER0_DOMAIN}/tls/server.key`;
  const CORE_PEER_TLS_ROOTCERT_FILE=`${CRYPTO}/peerOrganizations/${ORG_DOMAIN}/peers/${PEER0_DOMAIN}/tls/ca.crt`;
  const CORE_PEER_MSPCONFIGPATH=`${CRYPTO}/peerOrganizations/${ORG_DOMAIN}/users/Admin@${ORG_DOMAIN}/msp`;
  const ORDERER_CA=`${CRYPTO}/ordererOrganizations/${CONS_DOMAIN}/orderers/${ORDERER_DOMAIN}/msp/tlscacerts/tlsca.${CONS_DOMAIN}-cert.pem`;

  Object.assign(vars, {
    CONS_DOMAIN,

    ORG_LOWER,
    ORG_DOMAIN,
    ORG_MSP,
    
    ORDERER_DOMAIN,
    ZOOKEEPER_DOMAIN,
    KAFKA_DOMAIN,
    
    PEER0_DOMAIN,
    PEER1_DOMAIN,
    
    CA_DOMAIN,
    CLI_DOMAIN,
    MONGO_DOMAIN,

    CORE_PEER_ID,
    CORE_PEER_ADDRESS,
    CORE_PEER_TLS_CERT_FILE,
    CORE_PEER_TLS_KEY_FILE,
    CORE_PEER_TLS_ROOTCERT_FILE,
    CORE_PEER_MSPCONFIGPATH,
    ORDERER_CA
  })
}
setConsDomain(CONS_DOMAIN);

const obj : {vars: any, config: {env: any}, setConsDomain: any} = {
  vars,
  config: {env: vars},
  setConsDomain
};
export default obj;
