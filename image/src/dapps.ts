import { isObject } from 'lodash';
import { Subject, Observable } from 'rxjs';
import { scan, map } from 'rxjs/operators';
import waitPort from 'wait-port';
import axios from 'axios';
import Proxy from 'http-proxy-middleware';
import { Express, Request } from 'express';

import * as auth from './auth';
import env from './env';
import * as database from './db';
import * as utils from './utils';
import docker from './docker';
import * as chaincode from './chaincode';
import { sdk as lobbySdk } from './lobby';
import { Dapp, InstalledDapp } from './generated/graphqlTypes';
import { publishNotification } from './notifications';

async function startWatchtower(): Promise<void> {
  // pull container
  await utils.pullImage('containrrr/watchtower');

  // start container
  const container = await docker.createContainer({
    Image: 'containrrr/watchtower',
    name: 'watchtower',
    Volumes: {
      '/var/run/docker.sock': {},
      '/docker.json': {}
    },
    HostConfig: {
      Binds: [
        '/var/run/docker.sock:/var/run/docker.sock',
        '/home/ubuntu/.config/docker.json:/docker.json:rw'
      ]
    },
    Cmd: ['--interval', '60']
  });
  await container.start();
}
startWatchtower();

export async function queryStoreApp(_id: string): Promise<Dapp> {
  return lobbySdk.ListDapps().then(result => result.store.dapps.find(dapp => dapp._id === _id));
}

export async function listStoreApps(): Promise<Dapp[]> {
  return lobbySdk.ListDapps().then(result => result.store.dapps);
}

export async function listRunning(): Promise<InstalledDapp[]> {
  const db = await database.mongoPromise;
  
  return db.dappsRunning.find();
}

// restream parsed body before proxying
// see https://github.com/chimurai/http-proxy-middleware/issues/40#issuecomment-249430255
// eslint-disable-next-line no-unused-vars
function restream(proxyReq: any, req: Request) {
  proxyReq.headers = req.headers;

  if (req.body) {
    if(isObject(req.body)) {
      const bodyData = JSON.stringify(req.body);
      proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
      proxyReq.write(bodyData);
    } else {
      proxyReq.write(req.body);
    }
  }
}

export function install(dapp: Dapp, options: any, express: Express): Observable<any>[] {
  const progress = new Subject();
  const log = new Subject();

  // execute asynchronously
  (async function() {
    /* Steps
    0. get authorization token
    1. pull docker image
    2. run docker container
    3. wait for port to be available
    4. get chaincode
    5. install chaincode
    6. add to dApp list
    7. add proxy
    */

    // get authorization token for dApp container
    const authToken = auth.createToken(dapp.title);
    progress.next();

    // pull container
    console.log('Pulling dApp container.');
    await utils.pullImage(dapp.dockerImage, options);
    progress.next();
    // start container
    console.log('Starting dApp container.');
    const hostName = `${dapp.title}.${env.vars.ORG_DOMAIN}`;
    const container = await docker.createContainer({
      name: hostName,
      Image: dapp.dockerImage,
      Env: [
        `CLI_DOMAIN=${env.vars.CLI_DOMAIN}`,
        `AUTH_STRING=JWT ${authToken}`
      ]
    });
    await container.start();
    progress.next();

    // connect to org network
    console.log('Connecting dApp to organization network.');
    await docker.getNetwork(`net_${env.vars.ORG_NAME}`).connect({Container: hostName});
    progress.next();

    // wait for container to be up and running
    await waitPort({host: hostName, port: 80});
    progress.next();

    // get chaincode
    console.log('Getting dApp chaincode.');
    const ccBuffer = await axios({
      method: 'GET',
      url: `http://${hostName}/api/chaincode`,
      // `responseType` indicates the type of data that the server will respond with
      // options are: 'arraybuffer', 'document', 'json', 'text', 'stream'
      //   browser only: 'blob'
      responseType: 'arraybuffer', // default
      // `responseEncoding` indicates encoding to use for decoding responses (Node.js only)
      // Note: Ignored for `responseType` of 'stream' or client-side requests
      //responseEncoding: 'null' // default
    }).then(res => res.data);
    const ccContext = await axios({
      method: 'GET',
      url: `http://${hostName}/api/chaincodeContext`
    }).then(res => res.data);
    const ccName = ccContext.name;
    const ccVersion = ccContext.version;
    progress.next();
    // install chaincode
    console.log('Installing dApp chaincode.');
    chaincode.setChaincodeContext(ccContext);
    await chaincode.installChaincode(ccBuffer);
    progress.next();
    // instantiate chaincode
    const chaincodes = await chaincode.queryInstantiated();
    console.log('chaincodes', chaincodes);
    if(!chaincodes.find(cc => cc.name == ccName && cc.version == ccVersion)) {
      console.log('Instantiating dApp chaincode.');
      chaincode.setChaincodeContext(ccContext);
      await chaincode.instantiateChaincode();
    }
    progress.next();

    // add to dApp list
    console.log('Adding to dApp list.');
    const installedDapp: InstalledDapp = {hostName, dapp};
    const db = await database.mongoPromise;
    await db.dappsRunning.insert(installedDapp);
    progress.next();

    // add proxy
    console.log('Adding dApp proxy.');
    express.use(`/dapp_${dapp.title}*`, Proxy({
        target: `http://${hostName}:80`,
        onProxyReq: restream,
        /*pathRewrite: {
          [`^/dapp_${dapp.title}`] : '/'
        }*/
      })
    );
    progress.next();

    progress.complete();
    log.complete();
  
    await publishNotification('Dapps', `dApp ${dapp.title} has been installed`);
  })();

  const numSteps = 10;
  return [progress.pipe(scan(count => count + 1, 1), map(count => count / numSteps)), log];
}
