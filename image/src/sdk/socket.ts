'use strict';

import { Server } from 'http';
import IO, { Socket } from 'socket.io';
import { Observer, Observable, TeardownLogic } from 'rxjs';

import initGateway from './gateway';
import { ChaincodeEvent, ChaincodeEventListenInput } from '../generated/graphqlTypes';

export function registerChaincodeEvent({channelName, chaincodeName, eventName}: ChaincodeEventListenInput): Observable<ChaincodeEvent> {

    return new Observable<ChaincodeEvent>((observer: Observer<ChaincodeEvent>): TeardownLogic => {
        (async () => {
            const gateway = await initGateway();
            const network = await gateway.getNetwork(channelName);
            const contract = network.getContract(chaincodeName);

            console.log('Add contract listener:', channelName, chaincodeName, eventName);

            const listener = await contract.addContractListener('listener', eventName,
                async (err: Error, event: {[key: string]: any;},
                    blockNumber: string, transactionId: string, status: string): Promise<any> => {
                    console.log('Chaincode event received:', err, channelName, chaincodeName, eventName);

                    // exit early on error
                    if(err || !event) {
                        return;
                    }

                    const payloadString = event.payload ? event.payload.toString() : '';
                    const eventInfo: ChaincodeEvent = {
                        eventName,
                        payloadString,
                        blockNumber,
                        transactionId,
                        status
                    };
                    observer.next(eventInfo);
                }, {});

            return () => listener.unregister();
        })().then(null, observer.error);
        // HACK: prevent linter warning when `no-floating-promises` is set
    });
}

export function init(httpServer: Server): void {
    // start socket.io server
    const io = IO(httpServer);
    io.on('connect', function(socket: Socket) {
        console.log('socket connected');

        let subscription;
        socket.on('subscribeChaincodeEvent', (request: ChaincodeEventListenInput) => {
            const observable = registerChaincodeEvent(request);
            subscription = observable.subscribe(evt => socket.emit(evt.eventName, evt));
        });
        socket.on('disconnect', () => {
            console.log('socket disconnected');
            subscription && subscription.unsubscribe();
        }); // TODO remove observable?
    });
}
