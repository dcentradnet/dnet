/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

import { FileSystemWallet, Gateway } from 'fabric-network';
import * as fs from 'fs';

import env from '../env';
const {
    CONFIG,
    WALLET
} = env.vars;

export default async function(identity = 'user-1'): Promise<Gateway> {
    // Create a new file system based wallet for managing identities.
    const walletPath = WALLET;
    const wallet = new FileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(identity);
    if (!userExists) {
        throw new Error(`An identity for the user "${identity}" does not exist in the wallet.`);
    }

    const ccpPath = `${CONFIG}/config_local.json`;
    const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
    const ccp = JSON.parse(ccpJSON)['network-configs'][env.vars.CONS_DOMAIN];

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    await gateway.connect(ccp, {
        wallet, identity: identity, discovery: { enabled: true, asLocalhost: false },
        /*eventHubSelectionOptions: {
            strategy: EXAMPLE_EVENT_HUB_SELECTION_FACTORY
        }*/
    });

    return gateway;
}
