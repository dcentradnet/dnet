/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

import * as fs from 'fs';
import * as path from 'path';
import PQueue from 'p-queue';

import initGateway from './gateway';
import env from '../env';

const {
    LOG
} = env.vars;

const submitQueue = new PQueue({concurrency: 5});
const logQueue = new PQueue({concurrency: 1});

const logStream = fs.createWriteStream(path.join(LOG, 'chaincodeInvoke.log'), {flags:'a'});
async function logChaincodeInvoke(chaincode: string, method: string, ...args: string[]) {
    logStream.write('---');
    logStream.write(JSON.stringify({
        chaincode: chaincode,
        method: method,
        args: args
    }, null, '\t'));
}

export default async function(channel: string, chaincode: string, method: string, ...args: any[]): Promise<string> {
    // Create a new gateway for connecting to our peer node.
    const gateway = await initGateway();
    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork(channel);
    // Get the contract from the network.
    const contract = network.getContract(chaincode);

    // create transaction
    const transaction = contract.createTransaction(method);
    const txIdPromise = new Promise<string>((resolve, reject) =>
        transaction.addCommitListener(async (err: Error, txId: string, status: string, blockNumber: string) => {
            if(err) {
                return reject(err);
            }

            resolve(txId);
        }, {}));

    // Submit the specified transaction.
    const result = await submitQueue.add(() => transaction.submit(...args));
    console.log('Transaction has been submitted', result);

    // Log invoke asynchronously
    logQueue.add(() => logChaincodeInvoke(chaincode, method, ...args));

    // Disconnect from the gateway.
    await gateway.disconnect();

    return txIdPromise;
}
