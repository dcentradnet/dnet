/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

import FabricCAServices from 'fabric-ca-client';
import { FileSystemWallet, X509WalletMixin } from 'fabric-network';
import fs from 'fs';

import env from '../env';
const {
    CONFIG,
    WALLET
} = env.vars;

export default async function(): Promise<void> {
    const ccpPath = `${CONFIG}/config_local.json`;
    const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
    console.log(JSON.parse(ccpJSON));
    const ccp = JSON.parse(ccpJSON)['network-configs'][env.vars.CONS_DOMAIN];

    // Create a new CA client for interacting with the CA.
    const caInfo = ccp.certificateAuthorities[env.vars.CA_DOMAIN];
    const caTLSCACertsPath = caInfo.tlsCACerts.path;
    const caTLSCACerts = fs.readFileSync(caTLSCACertsPath);
    const ca = new FabricCAServices(caInfo.url, { trustedRoots: caTLSCACerts, verify: false }, caInfo.caName);

    // Create a new file system based wallet for managing identities.
    const walletPath = WALLET;
    const wallet = new FileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    // Check to see if we've already enrolled the admin user.
    const adminExists = await wallet.exists('admin');
    if (adminExists) {
        console.log('An identity for the admin user "admin" already exists in the wallet');
        return;
    }

    // Enroll the admin user, and import the new identity into the wallet.
    const enrollment = await ca.enroll({ enrollmentID: 'admin', enrollmentSecret: 'adminpw' });
    const identity = X509WalletMixin.createIdentity(env.vars.ORG_MSP, enrollment.certificate, enrollment.key.toBytes());
    await wallet.import('admin', identity);
    console.log('Successfully enrolled admin user "admin" and imported it into the wallet');
}
