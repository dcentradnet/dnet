/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

import initGateway from './gateway';

export default async function(channel: string, chaincode: string, method: string, ...args: string[]) {
    // Create a new gateway for connecting to our peer node.
    const gateway = await initGateway();

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork(channel);

    // Get the contract from the network.
    const contract = network.getContract(chaincode);

    // Evaluate the specified transaction.
    console.log('query transaction', chaincode, method, ...args);
    const result = await contract.evaluateTransaction(method, ...args);
    console.log(`Transaction has been evaluated, result is: ${result.toString()}`);

    return result;
}
