import { writeFileSync } from 'fs';
import { ChildProcess } from 'child_process';
import { Request, Response } from 'express';

const logFilePath = '/tmp/log.txt';
const logContent = [];

// keep a map of attached clients
let numLogListeners = 0;
const logListeners = new Map<number, Response>();

export function logToListener(res: Response, msg: string): void {
  res.write(`data: ${msg}\n\n`);
  (res as any).flush();
}

export function logToAllListeners(msg: string): void {
  logContent.push(msg);
  writeFileSync(logFilePath, msg);
  
  logListeners.forEach(res => logToListener(res, msg));
}

export function addLogListener(req: Request, res: Response): void {
  // send header
  req.socket.setTimeout(Number.MAX_VALUE);
  res.writeHead(200, {
    'Content-Type': 'text/event-stream', // important headers
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });
  res.write('\n');
  // log current state
  logContent.forEach(line => logToListener(res, line));
  
  // increment num log listeners
  ++numLogListeners;
  // add this client to those we consider "attached"
  logListeners.set(numLogListeners, res);
  // remove this client when he disconnects
  req.on('close', () => logListeners.delete(numLogListeners));
}

export async function logAndWait(streams: ChildProcess): Promise<void> {
  streams.stdout.on('data', msg => logToAllListeners(msg+'\n'));
  streams.stderr.on('data', msg => logToAllListeners(msg+'\n'));
  await (new Promise((resolve) => streams.on('exit', resolve)));
}

export function getLogContentAsString(): string {
  return logContent.join('\n');
}
