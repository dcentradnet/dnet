import retry from 'bluebird-retry';
import ECKey from 'ec-key';
import uuidv4 from 'uuid/v4';
import { fetch } from 'cross-fetch';
import { GraphQLClient } from 'graphql-request';

import docker from './docker';
import * as database from './db';
import * as crypt from './crypt';
import * as utils from './utils';

import './polyfills';
import { Consortium, ConsortiumOrganization, ConsortiumRequest } from './generated/graphqlTypes';
import { getSdk, SignedEnvelopeInput, ConsortiumMember } from './generated/graphqlLobbyTypes';

async function safeFetch(input: RequestInfo, init?: RequestInit): Promise<globalThis.Response> {
  return retry(async() => {
    const result = await fetch(input, init);

    const resultString = await result.clone().text();
    if(resultString.includes("error")) {
      throw new Error();
    }
    console.log(resultString);

    return result;
  }, {timeout: 10*10000, max_tries: 10});
}

const client = new GraphQLClient('https://dnetlobby-git-develop-patrickcharrier.vercel.app/api/graphQL', {
  timeout: 100000,
  fetch: safeFetch
});
export const sdk = getSdk(client);

export async function cid(): Promise<string> {
  return database.getStatus('consId') as Promise<string>;
}

export async function requestedCid(): Promise<string> {
  return database.getStatus('requestedCid') as Promise<string>;
}

export async function joined(): Promise<Consortium | undefined> {
  const _cid = await cid();
  if(_cid) {
    return sdk.ListConsortia({cid: _cid}).then(result => result.lobby.consortia[0]);
  }
}

export async function requested(): Promise<Consortium | undefined> {
  const _cid = await requestedCid();
  if(_cid) {
    return sdk.ListConsortia({cid: _cid}).then(result => result.lobby.consortia[0]);
  }
}

export async function listConsortia(): Promise<Consortium[]> {
  return sdk.ListConsortia().then(result => result.lobby.consortia);
}

export async function organization(pub: string): Promise<ConsortiumOrganization> {
  return sdk.QueryOrganization({pub}).then(result => result.lobby.organization);
}

// see https://fabric-sdk-node.github.io/release-1.4/tutorial-sign-transaction-offline.html
export async function addConsortium(cid: string, name: string, desc: string): Promise<void> {
  // build request to lobby
  const msg = {cid, name, desc};
  const signedConsortium: SignedEnvelopeInput = await crypt.signMessageAsBody(msg);

  await sdk.AddConsortium({signedConsortium});
}

export async function listRequests(cid: string): Promise<ConsortiumRequest[]> {
  if(!cid) {
    return [];
  }

  return sdk.ListRequests({cid}).then(result => result.lobby.consortia[0].requests);
}

export async function addRequest(cid: string): Promise<void> {
  // create crypto material
  // FIXME disable this if already joined
  //env.setConsDomain(body.cid); // FIXME CONS_DOMAIN is not set
  await crypt.createCryptoMaterial(); // FIXME see above

  // create the request
  const msg = {cid};
  const signedRequest: SignedEnvelopeInput = await crypt.signMessageAsBody(msg);

  // submit the request
  await sdk.AddRequest({signedRequest});

  // set requested Cid
  await database.setStatus('requestedCid', cid);
}

export async function removeRequest(cid: string): Promise<void> {
  // build request to lobby
  const msg = {cid};
  const signedRequest: SignedEnvelopeInput = await crypt.signMessageAsBody(msg);

  await sdk.RemoveRequest({signedRequest});

  // remove requested Cid
  await database.setStatus('requestedCid', undefined);
}

export async function listMembers(_cid?: string, _pub?: string): Promise<ConsortiumMember[]> {
  if(!_cid) {
    _cid = await cid();
  }
  if(!_cid) {
    throw new Error('Members cannot be queried because node is not part of a lobby consortium.');
  }

  return sdk.ListMembers({
    ...(!!_cid && {cid: _cid}),
    ...(!!_pub && {pub: _pub})
  }).then(result => result.lobby.consortia[0].members);
}

async function getSwarmToken(): Promise<string> {
  const result = await docker.swarmInspect();
  const publicIp = await utils.getPublicIp();
  return `${result.JoinTokens.Manager} ${publicIp}:2377`;
}

export async function addApproval(cid: string, rpub: string): Promise<void> {
  // compute secret
  const key = await crypt.getPrivateKey();
  const rKey = new ECKey(rpub);
  const secret = key.computeSecret(rKey);
  
  // encrypt token
  const token = await getSwarmToken();
  const encryptedToken = crypt.encrypt(token, secret);

  // build request to lobby
  const msg = {
    cid,
    rpub,
    token: encryptedToken
  };
  const signedApproval: SignedEnvelopeInput = await crypt.signMessageAsBody(msg);

  await sdk.AddApproval({signedApproval});
}

// eslint-disable-next-line no-unused-vars
export async function removeApproval(cid: string, rpub: string): Promise<void> {
  // build request to lobby
  const msg = {cid, rpub};
  const signedApproval: SignedEnvelopeInput = await crypt.signMessageAsBody(msg);
  
  await sdk.RemoveApproval({signedApproval});
}

export async function putOrganization(org: ConsortiumOrganization): Promise<void> {
  await crypt.createCryptoMaterial(); // FIXME see above

  // build request to lobby
  const signedOrg: SignedEnvelopeInput = await crypt.signMessageAsBody(org);

  await sdk.PutOrganization({signedOrg});
}

// eslint-disable-next-line no-unused-vars
export async function removeOrganization(): Promise<void> {
  // build request to lobby
  const signedOrg: SignedEnvelopeInput = await crypt.signMessageAsBody({
    pub: await crypt.getPublicKeyString()
  });

  await sdk.RemoveOrganization({signedOrg});
}

export async function setCid(cid: string): Promise<void> {
  await database.setStatus('consId', cid);
}
