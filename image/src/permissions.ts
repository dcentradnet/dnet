import { ObjectId } from 'mongoist';
import Bluebird from 'bluebird';
import * as database from './db';
import * as roles from './roles';
import { Permission, PermissionCore, Role } from './generated/graphqlTypes';

const permissionsCreatedPromise = (async() => {
    console.log(await database.mongoPromise); // FIXME currently required. Otherwise db is undefined. Why?
    const db = await database.mongoPromise;

    // TODO create indices

    const permissionData = [
        {name: 'gov-request'},
        {name: 'gov-vote'}
    ];
    const permissions = await Bluebird.map(
        permissionData,
        p => db.permissions.findAndModify({query: {name: p.name}, update: {$set: p}, new: true, upsert: true})
    );

    //const rolePermData = {roleId: role._id, roleId: role._id};
    //await db.userRoles.findAndModify({query: rolePermData, update: {$set: rolePermData}, new: true, upsert: true});
    console.log('created standard permissions');

    return db;
})();

export async function list(query?: Record<string, string>): Promise<Permission[]> {
    const db = await database.mongoPromise;

    return db.permissions.find(query);
}

export async function put(name: string, _id?: string): Promise<PermissionCore> {
    const db = await database.mongoPromise;

    return db.permissions.findAndModify({query: {_id: ObjectId(_id)}, update: {$set: {name}}, new: true, upsert: true});
}

export async function remove(_id: string): Promise<void> {
    const db = await database.mongoPromise;

    await db.permissions.remove({_id: ObjectId(_id)}, {justOne: true});
}

export async function attachPermToRole(permId: string, roleId: string): Promise<void> {
    const db = await database.mongoPromise;

    await db.rolePermissions.insert({permId: ObjectId(permId), roleId: ObjectId(roleId)});
}

export async function removePermFromRole(permId: string, roleId: string): Promise<void> {
    const db = await database.mongoPromise;

    await db.rolePermissions.remove({permId: ObjectId(permId), roleId: ObjectId(roleId)}, {justOne: true});
}

export async function listRolesByPerm(permId: string): Promise<Role[]> {
    const db = await database.mongoPromise;

    return Bluebird.map(db.rolePermissions.find({permId}, {roleId: 1}),
                        async({roleId}): Promise<Role> => roles.list({_id: ObjectId(roleId)}).then(l => l[0]));
}

export async function listUserRoles(roleId: string): Promise<Permission[]> {
    const db = await database.mongoPromise;

    return Bluebird.map(db.rolePermissions.find({roleId}, {permId: 1}),
                        async({permId}): Promise<Permission> => list({_id: ObjectId(permId)}).then(l => l[0]));
}

export async function hasRolePermissions(roleId: string, permId: string): Promise<boolean> {
    const db = await database.mongoPromise;

    return db.rolePermissions.findAsCursor({roleId, _id: permId}).limit(1).size().then(size => size > 0);
}

export async function hasRolePermissionByName(roleId: string, roleName: string): Promise<boolean> {
    const db = await database.mongoPromise;

    return db.rolePermissions.findAsCursor({roleId, name: roleName}).limit(1).size().then(size => size > 0);
}