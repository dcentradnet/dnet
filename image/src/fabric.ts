import { spawn } from 'child_process';
import { Subject, Observable } from 'rxjs';
import { scan, map } from 'rxjs/operators';

import enrollAdmin from './sdk/enrollAdmin';
import registerUser from './sdk/registerUser';

import env from './env';
import * as database from './db';
import * as config from './config';
import * as actions from './actions';
import * as orgs from './orgs';
import gateway from './sdk/gateway';
import { ChannelInfo } from 'fabric-client';
import { publishNotification } from './notifications';

const config_env = env.config;

async function logAndWait(log: Subject<string>, streams: any, logToConsole = false): Promise<void> {
  let subscription;
  if(logToConsole) {
      subscription = log.subscribe(line => console.log(line));
  }

  streams.stdout.on('data', line => log.next(line+'\n'));
  streams.stderr.on('data', line => log.next(line+'\n'));
  await (new Promise<void>(resolve => streams.on('exit', () => resolve())));

  if(subscription) {
      subscription.unsubscribe();
  }
}

export async function channels(): Promise<ChannelInfo[]> {
    const gw = await gateway();
    const client = gw.getClient();
    
    const peer = client.getPeer(env.vars.PEER0_DOMAIN);
    const result = await client.queryChannels(peer);

    return result.channels;
}

actions.registerAction('fabric_init', async(type: string, key: string): Promise<Observable<number | string>[]> => {  
    const progress = new Subject<number>();
    const log = new Subject<string>();

    (async function() {
        const consDomain = key;
        env.setConsDomain(consDomain);
        progress.next();

        //const material = spawn('bash', ['material.sh', 'generate'], config_env);
        //await log.logAndWait(material);

        const network = spawn('bash', ['network.sh'], config_env);
        await logAndWait(log, network);
        progress.next();

        const up = spawn('bash', `initFabricNetwork.sh up -c ${env.vars.CHANNEL_NAME} -t 50 -s couchdb -l node -x ${env.vars.CONS_DOMAIN} -y ${env.vars.ORG_NAME} -v`.split(' '));
        await logAndWait(log, up, true);
        progress.next();

        await config.writeConfig(
            'config/config.json',
            'config/config_local.json'
        );
        await enrollAdmin();
        await registerUser();
        progress.next();

        const explorer = spawn('bash', ['explorer.sh'], config_env);
        await logAndWait(log, explorer);
        progress.next();

        await database.setStatus('isJoined', true);
        progress.next();

        progress.complete();
        log.complete();

        orgs.publishStatusUpdate();
        await publishNotification('Fabric', 'Fabric network has been created successfully');
    })();

    const numSteps = 6;
    return [progress.pipe(scan(count => count + 1, 1), map(count => count / numSteps)), log];
});

actions.registerAction('fabric_shutdown', async(type: string, key: string): Promise<Observable<number | string>[]> => {
    const progress = new Subject<number>();
    const log = new Subject<string>();

    (async function() {
        const consDomain = key;
        env.setConsDomain(consDomain);
        progress.next();

        const down = spawn('bash', `initFabricNetwork.sh down -c ${env.vars.CHANNEL_NAME} -t 50 -s couchdb -l node -x ${env.vars.CONS_DOMAIN} -y ${env.vars.ORG_NAME} -v`.split(' '));
        await logAndWait(log, down, true);
        progress.next();

        await database.setStatus('isJoined', false);
        progress.next();

        progress.complete();
        log.complete();

        orgs.publishStatusUpdate();
        await publishNotification('Fabric', 'Fabric network has been left successfully');
    })();

    const numSteps = 3;
    return [progress.pipe(scan(count => count + 1, 1), map(count => count / numSteps)), log];
});

actions.registerAction('fabric_extend', async(type: string, key: string, subkey: string): Promise<Observable<number | string>[]> => {
    const progress = new Subject<number>();
    const log = new Subject<string>();

    (async function() {
        const consDomain = key;
        const consId = subkey;
        env.setConsDomain(consDomain);
        progress.next();

        //const material = spawn('bash', ['material.sh', 'extend'], config_env);
        //await log.logAndWait(material);

        const network = spawn('bash', ['network.sh'], config_env);
        await logAndWait(log, network);
        progress.next();

        const extend = spawn('bash', `extendFabricNetwork.sh up -c ${env.vars.CHANNEL_NAME} -t 50 -s couchdb -l node -x ${env.vars.CONS_DOMAIN} -y ${env.vars.ORG_NAME} -v`.split(' '));
        await logAndWait(log, extend);
        progress.next();

        await config.writeConfig(
            'config/config.json',
            'config/config_local.json'
        );
        await enrollAdmin();
        await registerUser();
        progress.next();

        const explorer = spawn('bash', ['explorer.sh'], config_env);
        await logAndWait(log, explorer);
        progress.next();

        await database.setStatus('isJoined', true);
        progress.next();

        progress.complete();
        log.complete();

        orgs.publishStatusUpdate();
        await publishNotification('Fabric', 'Fabric network has been joined successfully');
    })();

    const numSteps = 6;
    return [progress.pipe(scan(count => count + 1, 1), map(count => count / numSteps)), log];
});
