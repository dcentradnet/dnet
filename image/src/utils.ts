import { exec as _exec } from 'child_process';
import * as util from 'util';
import Bluebird from 'bluebird';
import axios from 'axios';

import env from './env';
import docker from './docker';

const exec = util.promisify(_exec);

const {
  DEBUG_MODE,
} = env.vars;

const config_env = env.config;

export async function timeoutPromise(ms: number): Promise<void> {
  return new Promise(resolve => {
    const handle = setTimeout(() => {
      clearTimeout(handle);
      resolve();
    }, ms);
  });
}

export async function getPublicIp(): Promise<string> {
  return axios.get('https://api.ipify.org').then(res => res.data);
}

export async function getOrgNames(): Promise<string[]> {
  if(DEBUG_MODE) {
    const consulRes = await exec('docker exec consul-debug consul kv get -recurse orgs/', config_env);

    const orgNames = consulRes.stdout.substr(0, -2).split('\n').map(s => s.substr(5));
    console.log(orgNames);
    return ['Org1', 'Org2'];
  }
  
  return Bluebird.map(
    docker.listNodes(),
    async node => axios.get(`http://${node.ID}:8080/orgName`).then(res => res.data)
  );
}

export async function pullImage(imageName: string, options?: any): Promise<void> {
  await (new Promise((resolve, reject) => {
    function callback(err, stream) {
      if(err) {
        return reject(err);
      }

      docker.modem.followProgress(stream, (err, output) => {
        if(err) {
          reject(err);
        } else {
          resolve(output);
        }
      }, () => {}); // eslint-disable-line @typescript-eslint/no-empty-function
    }

    const params: any[] = [imageName];
    options && params.push(options);
    params.push(callback);

    (docker as any).pull(...params);
  }));
}
