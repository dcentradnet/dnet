import { writeFileSync } from 'fs';
import { promise as glob } from 'glob-promise';

import env from "./env";

const {
  ORG_NAME,
  ORG_MSP,

  CHANNEL_NAME,
  CRYPTO
} = env.vars;

export async function generateConfig(KEYS_TARGET: string, CRYPTO_TARGET: string): Promise<any> {

  const orderKeySubdir = `/ordererOrganizations/${env.vars.CONS_DOMAIN}/users/Admin@${env.vars.CONS_DOMAIN}/msp/keystore/`;
  const ordererAdminKeyPath = CRYPTO_TARGET + orderKeySubdir + (await glob('*_sk', { cwd: CRYPTO + orderKeySubdir }))[0];
  console.log('ordererAdminKeyPath', ordererAdminKeyPath);

  const peerKeySubdir = `/peerOrganizations/${env.vars.ORG_DOMAIN}/users/Admin@${env.vars.ORG_DOMAIN}/msp/keystore/`;
  const orgAdminKeyPath = CRYPTO_TARGET + peerKeySubdir + (await glob('*_sk', { cwd: CRYPTO + peerKeySubdir }))[0];
  console.log('orgAdminKeyPath', orgAdminKeyPath);

  const config = {
    "network-configs": {
      [env.vars.CONS_DOMAIN]: {
        "version": "1.0",
        "client": {
          "tlsEnable": true,
          "adminUser": "admin",
          "adminPassword": "adminpw",
          "organization": ORG_NAME,
          "connection": {
            "timeout": {
              "peer": {
                "endorser": "300"
              },
              "orderer": "300"
            }
          }
        },
        "clients": {
          "user-1": {
            "tlsEnable": true,
            "organization": ORG_NAME,
            "channel": CHANNEL_NAME,
            "credentialStore": {
              "path": KEYS_TARGET,
              "cryptoStore": {
                "path": CRYPTO_TARGET
              }
            }
          }
        },
        "channels": {
          [CHANNEL_NAME]: {
            "peers": {
              [env.vars.PEER0_DOMAIN]: {},
              [env.vars.PEER1_DOMAIN]: {}
            },
            "connection": {
              "timeout": {
                "peer": {
                  "endorser": "6000",
                  "eventHub": "6000",
                  "eventReg": "6000"
                }
              }
            }
          }
        },
        "organizations": {
          [ORG_NAME]: {
            "mspid": ORG_MSP,
            "fullpath": true,
            "adminPrivateKey": {
              "path": orgAdminKeyPath
            },
            "signedCert": {
              "path": `${CRYPTO_TARGET}/peerOrganizations/${env.vars.ORG_DOMAIN}/users/Admin@${env.vars.ORG_DOMAIN}/msp/signcerts/Admin@${env.vars.ORG_DOMAIN}-cert.pem`
            },
            "certificateAuthorities": [env.vars.CA_DOMAIN],
            "peers": [env.vars.PEER0_DOMAIN, env.vars.PEER1_DOMAIN]
          },
          "OrdererMSP": {
            "mspid": "OrdererMSP",
            "adminPrivateKey": {
              "path": ordererAdminKeyPath
            }
          }
        },
        "peers": {
          [env.vars.PEER0_DOMAIN]: {
            "tlsCACerts": {
              "path": `${CRYPTO_TARGET}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${env.vars.PEER0_DOMAIN}/msp/tlscacerts/tlsca.${env.vars.ORG_DOMAIN}-cert.pem`
            },
            "url": "grpcs://" + env.vars.PEER0_DOMAIN + ":7051",
            "eventUrl": "grpcs://" + env.vars.PEER0_DOMAIN + ":7053",
            "grpcOptions": {
              "ssl-target-name-override": env.vars.PEER0_DOMAIN
            }
          },
          [env.vars.PEER1_DOMAIN]: {
            "tlsCACerts": {
              "path": `${CRYPTO_TARGET}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${env.vars.PEER1_DOMAIN}/msp/tlscacerts/tlsca.${env.vars.ORG_DOMAIN}-cert.pem`
            },
            "url": `grpcs://${env.vars.PEER1_DOMAIN}:7051`,
            "eventUrl": `grpcs://${env.vars.PEER1_DOMAIN}:7053`,
            "grpcOptions": {
              "ssl-target-name-override": env.vars.PEER1_DOMAIN
            }
          }
        },
        "orderers": {
          [env.vars.ORDERER_DOMAIN]: {
            "url": `grpcs://${env.vars.ORDERER_DOMAIN}:7050`
          }
        },
        "certificateAuthorities": {
          [env.vars.CA_DOMAIN]: {
            "url": `https://${env.vars.CA_DOMAIN}:7054`,
            "httpOptions": {
              "verify": false
            },
            "tlsCACerts": {
              "path": `${CRYPTO_TARGET}/peerOrganizations/${env.vars.ORG_DOMAIN}/ca/ca.${env.vars.ORG_DOMAIN}-cert.pem`
            },
            "caName": env.vars.CA_DOMAIN
          }
        }
      }
    },
    "configtxgenToolPath": "fabric-path/fabric-samples/bin",
    "license": "Apache-2.0"
  };

  return config;
}

const config = {
  "network-configs": {
    [env.vars.CONS_DOMAIN]: {
      "name": env.vars.CONS_DOMAIN,
      "profile": "./connection-profile.json"
    }
  },
  "license": "Apache-2.0"
};

export async function writeConfig(filename: string, filename_local: string): Promise<void> {
  // write explorer config
  const connection_profile = (await generateConfig('/opt/tmp', '/tmp/crypto'))['network-configs'][env.vars.CONS_DOMAIN];
  console.log(JSON.stringify(connection_profile, null, "\t"));
  writeFileSync('config/connection-profile.json', JSON.stringify(connection_profile, null, "\t"));
  writeFileSync(filename, JSON.stringify(config, null, "\t"));

  // write sdk config
  const config_local = await generateConfig('/home/node/app/sdk/wallet', CRYPTO);
  writeFileSync(filename_local, JSON.stringify(config_local, null, "\t"));
}
