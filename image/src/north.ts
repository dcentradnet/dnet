import Bluebird from 'bluebird';
import process from 'process';
import http from 'http';
import { to } from 'await-to-js';
import express from 'express';
import bodyParser from 'body-parser';
import multer from 'multer';
import axios from 'axios';
import path from 'path';
import passport from 'passport';

import query from './sdk/query';
import invoke from './sdk/invoke';

import env from './env';
import * as log from './log';
import * as auth from './auth';
import * as crypt from './crypt';
import * as gov from './gov';
import * as chaincode from './chaincode';
import * as socket from './sdk/socket';
import * as actions from './actions';
import * as database from './db';
import graphQL, { connectGraphQLRestAPI, connectGraphQLSubscriptionServer } from './graphql';
import { RequestCore, User } from './generated/graphqlTypes';

// register actions
import './fabric';

const {
  CHANNEL_NAME
} = env.vars;

process.on('SIGTERM', () => process.exit());

// create express app
const app = express();
app.use(bodyParser.raw({type: 'application/octet-stream', limit : '2mb'}));
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static('client/www'));

// bind express app to http server
const httpServer = http.createServer(app);
httpServer.listen(80, function() {
  console.log('North interface listening on port 80!');
});

// init authentication
app.use(passport.initialize());
// init graphql subscriptions
connectGraphQLSubscriptionServer(httpServer);
// init graphql REST API
connectGraphQLRestAPI(app);

// prepare express multer upload capabilities and storage
const upload = multer({dest: '/tmp/'});

app.get('/api/ready', async function(req: express.Request, res: express.Response) {
  const isReady = Bluebird.resolve(database.mongoPromise).isFulfilled();
  res.status(200).json({isReady});
});

app.post('/api/login', auth.loginRoute);

app.get('/api/requests', async function(req: express.Request, res: express.Response) {
  const [error, requests] = await to(gov.requests());
  if(error) {
    return res.status(500).send({error});
  }

  res.status(200).json(requests);
});
app.get('/api/votes', async function(req: express.Request, res: express.Response) {
  const [error, votes] = await to(gov.votes());
  if(error) {
    return res.status(500).send({error});
  }

  res.status(200).json(votes);
});

app.get('/api/actions', auth.authenticate(), actions.listPendingActionsRoute);
app.get('/api/actionsStream', actions.addListenerRoute);
app.get('/api/waitAction', actions.waitActionRoute);

app.post('/api/decrypt', auth.authenticate(), async function(req: express.Request, res: express.Response) {
  // decrypt
  const decrypted = crypt.decryptWithOtherPublicKey(req.body.crypted, req.body.opub);

  res.status(200).send({result: decrypted});
});

app.post('/api/installChaincode', upload.single('chaincode'), async function(req: express.Request & {file: {path: string}}, res: express.Response) {
  req.setTimeout(60*60*1000);

  chaincode.setChaincodeContext(req.body);
  const [err] = await to(chaincode.installChaincodeFromFile(req.file.path));
  if(err) {
    return res.status(500).send({error: err});
  }

  res.status(200).send({message: 'Successful!'});
});

app.get('/api/log', log.addLogListener);

app.post('/api/forward', auth.authenticate(), async function(req: express.Request, res: express.Response) {
  const body = req.body;
  const result = await axios.get(Object.assign({simple: false}, body.options));
  //res.status(200).send({message: 'express.Request forwarded.'});
  res.status(200).send({result});
});

app.post('/api/notifyPotentialMajorityReached', async function(req: express.Request, res: express.Response) {
  const request: RequestCore = req.body;

  try {
    await gov.onPotentialMajorityReached(request, app);
  } catch(e) {
    return res.status(500).send(e);
  }

  res.status(200).send({message: 'Successful!'});
});

app.post('/api/initNetwork', auth.authenticate(), async function(req: express.Request, res: express.Response) {
  req.setTimeout(30*60*1000);

  const body = req.body;  
  if(!body.waitFinish) {
    res.send();
  }
  
  const [, log] = await actions.executeAction('fabric_init', body.consDomain);
  
  if(body.waitFinish) {
    res.json({log: await log.toPromise()});
  }
});

app.post('/api/shutdownNetwork', auth.authenticate(), async function(req: express.Request, res: express.Response) {
  req.setTimeout(30*60*1000);

  const body = req.body;
  if(!body.waitFinish) {
    res.send();
  }

  const [, log] = await actions.executeAction('fabric_shutdown', body.consDomain);

  if(body.waitFinish) {
    res.json({log: await log.toPromise()});
  }
});

app.post('/api/extendNetwork', auth.authenticate(), async function(req: express.Request, res: express.Response) {
  req.setTimeout(30*60*1000);

  const body = req.body;
  if(!body.waitFinish) {
    res.send();
  }

  const [, log] = await actions.executeAction('fabric_extend', body.consDomain);
  
  if(body.waitFinish) {
    res.json({log: await log.toPromise()});
  }
});

app.post('/api/query', auth.authenticate(), async function(req: express.Request, res: express.Response) {
  req.setTimeout(10*60*1000);

  const body = req.body;
  const [error, result] = await to(query(CHANNEL_NAME, body.chaincode, body.method, ...body.args));
  if(error) {
    return res.status(500).send({error});
  }

  res.send({result});
});

app.post('/api/invoke', auth.authenticate(), async function(req: express.Request, res: express.Response) {
  req.setTimeout(10*60*1000);

  const body = req.body;
  const [error, result] = await to(invoke(CHANNEL_NAME, body.chaincode, body.method, ...body.args));
  if(error) {
    return res.status(500).send({error});
  }

  res.send({result});
});

app.post('/api/graphQL', async (req: express.Request, res: express.Response, next: express.Handler) => {
  req.setTimeout(60*60*1000);

  // authenticate manually to allow for unauthenticated requests
  passport.authenticate('local', async function(err, user: User) {
    const [error, result] = await to(graphQL(req.body.query, req.body.variables, user, app));
    if(error) {
        return res.status(500).send(error);
    }
  
    res.json(result);
  })(req, res, next);
});

// handle frontend routing
// and filter plugin API routes that are added later after startup
app.get(/(?!^\/api|^\/dapp_.*)(^.*$)/, (req: express.Request, res: express.Response) => res.sendFile(path.join(process.cwd(), 'client', 'www', 'index.html')));

// activate websocket event listeners
socket.init(httpServer);

// proxy websocket without initial http request
// see https://github.com/chimurai/http-proxy-middleware#external-websocket-upgrade
// TODO required for plugin forwards?
/*httpServer.on('upgrade', function(req, socket, head) {
  console.log('Upgrading ', req.url);
  if(req.url.startsWith('/rpc')) {
    rpcProxy.upgrade.call(this, req, socket, head);
  }
});*/
