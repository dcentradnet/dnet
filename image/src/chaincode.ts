import Bluebird from 'bluebird';
import * as util from 'util';
import { exec as _exec } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';
import { sha3_512 } from 'js-sha3';
import { ChaincodeInfo } from 'fabric-client';

import env from './env';
import docker from './docker';
import * as gov from './gov';
import * as utils from './utils';
import gateway from './sdk/gateway';

const exec = util.promisify(_exec);

const {
  CRYPTO
} = env.vars;

let {
  CHANNEL_NAME,
  CC_NAME,
  CC_SRC_PATH
} = env.vars;

const config_env = env.config;

let CC_VERSION = '1.0';
let CC_LANG = 'node';
let CC_INIT = '["init","a","90","b","210"]';

export async function queryInstalled(): Promise<ChaincodeInfo[]> {
  const gw = await gateway('admin');
  const client = gw.getClient();  
  const peer = client.getPeer(env.vars.PEER0_DOMAIN);
  const res = await client.queryInstalledChaincodes(peer, true);

  return res.chaincodes;
}

export async function queryInstantiated(channelName = env.vars.CHANNEL_NAME): Promise<ChaincodeInfo[]> {
  const gw = await gateway('admin');
  const client = gw.getClient();
  const peer = client.getPeer(env.vars.PEER0_DOMAIN);
  const channel = client.getChannel(channelName);
  const res = await channel.queryInstantiatedChaincodes(peer, true);

  return res.chaincodes;
}

export async function preparePrivateCollections(): Promise<string> {
  // get org names
  const orgNames = await utils.getOrgNames();

  const POLICY = `OR ('${orgNames.map(name => name+'MSP.member').join('\',\'')}')`;
  const NEW_POLICY = `AND ('${orgNames.map(name => name+'MSP.peer').join('\',\'')}')`;

  const privateCollections = [
    {
        "name": "privateCollection",
        "policy": POLICY,
        "requiredPeerCount": 0,
        "maxPeerCount": 2,
        "blockToLive": 1000000,
        "memberOnlyRead": true
    }
  ];
  
  writeFileSync('/home/node/app/private_collections.json', JSON.stringify(privateCollections));

  return NEW_POLICY;
}

export interface ChaincodeContext {
  channelName: string;
  name: string;
  version: string;
  lang: string;
  init: string;
}

export function setChaincodeContext(context: ChaincodeContext): void {
  CHANNEL_NAME  = context.channelName || 'main';
  CC_NAME       = context.name;
  CC_VERSION    = context.version;
  CC_LANG       = context.lang || 'node';
  CC_INIT       = context.init || '["init","a","90","b","210"]';
  CC_SRC_PATH   = `/chaincode/${CC_NAME}/${CC_VERSION}`;
}

export async function installChaincodeFromFile(filePath: string): Promise<void> {
  const data = readFileSync(filePath);
  const hash = sha3_512(Uint8Array.from(data));
  console.log('Installing chaincode...', CC_NAME, CC_VERSION, hash);

  if(!(await gov.chaincodeMayBeInstalled(CC_NAME, CC_VERSION, hash))) {
    throw new Error(`Not allowed to install chaincode ${CC_NAME} ${CC_VERSION} ${hash}`);
  }
  
  await exec(`mkdir -p ${CC_SRC_PATH} && rm -rf ${CC_SRC_PATH}/* && tar -xvf ${filePath} -C ${CC_SRC_PATH}`);
  await exec(`peer chaincode install -n ${CC_NAME} -v ${CC_VERSION} -l ${CC_LANG} -p ${CC_SRC_PATH}`, config_env);

  const PEER_DOMAIN = env.vars.PEER1_DOMAIN;
  await exec(`peer chaincode install -n ${CC_NAME} -v ${CC_VERSION} -l ${CC_LANG} -p ${CC_SRC_PATH}`, {
    env: Object.assign({}, config_env.env, {
      CORE_PEER_ADDRESS: `${PEER_DOMAIN}:7051`,
      CORE_PEER_TLS_CERT_FILE: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/server.crt`,
      CORE_PEER_TLS_KEY_FILE: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/server.key`,
      CORE_PEER_TLS_ROOTCERT_FILE: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/ca.crt`,
      CORE_PEER_MSPCONFIGPATH: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/users/Admin@${env.vars.ORG_DOMAIN}/msp`
    })
  });
}

export async function installChaincode(data): Promise<void> {
  const hash = sha3_512(Uint8Array.from(data));
  console.log('Installing chaincode...', CC_NAME, CC_VERSION, hash);

  if(!(await gov.chaincodeMayBeInstalled(CC_NAME, CC_VERSION, hash))) {
    throw new Error(`Not allowed to install chaincode ${CC_NAME} ${CC_VERSION} ${hash}`);
  }
  
  writeFileSync('/tmp/chaincode.tar.gz', data, {encoding: null});
  await exec(`mkdir -p ${CC_SRC_PATH} && rm -rf ${CC_SRC_PATH}/* && tar -xvf /tmp/chaincode.tar.gz -C ${CC_SRC_PATH}`);
  await exec(`peer chaincode install -n ${CC_NAME} -v ${CC_VERSION} -l ${CC_LANG} -p ${CC_SRC_PATH}`, config_env);

  const PEER_DOMAIN = env.vars.PEER1_DOMAIN;
  await exec(`peer chaincode install -n ${CC_NAME} -v ${CC_VERSION} -l ${CC_LANG} -p ${CC_SRC_PATH}`, {
    env: Object.assign({}, config_env.env, {
      CORE_PEER_ADDRESS: `${PEER_DOMAIN}:7051`,
      CORE_PEER_TLS_CERT_FILE: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/server.crt`,
      CORE_PEER_TLS_KEY_FILE: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/server.key`,
      CORE_PEER_TLS_ROOTCERT_FILE: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/peers/${PEER_DOMAIN}/tls/ca.crt`,
      CORE_PEER_MSPCONFIGPATH: `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/users/Admin@${env.vars.ORG_DOMAIN}/msp`
    })
  });
}

export async function instantiateChaincode(): Promise<void> {
  console.log('Instantiating chaincode...', CC_NAME, CC_VERSION);

  if(!(await gov.chaincodeMayBeInstantiated(CC_NAME, CC_VERSION))) {
    throw new Error(`Not allowed to instantiate chaincode ${CC_NAME} ${CC_VERSION}`);
  }

  // prepare private collections
  const NEW_POLICY = await preparePrivateCollections();

  await exec(`peer chaincode instantiate -o ${env.vars.ORDERER_DOMAIN}:7050\
    --tls ${env.vars.CORE_PEER_TLS_ENABLED} --cafile ${env.vars.ORDERER_CA}\
    -C ${CHANNEL_NAME} -n ${CC_NAME} -l ${CC_LANG} -v ${CC_VERSION}\
    -c '{"Args": ${CC_INIT}}' -P "${NEW_POLICY}"\
    --collections-config /home/node/app/private_collections.json`, config_env);
}

export async function upgradeChaincode(): Promise<void> {
  console.log('Upgrading chaincode...', CC_NAME, CC_VERSION);

  if(!(await gov.chaincodeMayBeUpgraded(CC_NAME, CC_VERSION))) {
    throw new Error(`Not allowed to upgrade chaincode ${CC_NAME} ${CC_VERSION}`);
  }

  // get containers before upgrade
  const prevContainers = await docker.listContainers();
  // get images before upgrade
  const prevImages = await docker.listImages();

  // prepare private collections
  const NEW_POLICY = await preparePrivateCollections();

  await exec(`peer chaincode upgrade -o ${env.vars.ORDERER_DOMAIN}:7050\
    --tls ${env.vars.CORE_PEER_TLS_ENABLED} --cafile ${env.vars.ORDERER_CA}\
    -C ${CHANNEL_NAME} -n ${CC_NAME} -l ${CC_LANG} -v ${CC_VERSION}\
    -c '{"Args": ${CC_INIT}}' -P "${NEW_POLICY}"\
    --collections-config /home/node/app/private_collections.json`, config_env);
  
  // kill old chaincode containers
  await Bluebird.map(prevContainers, async c => {
    if(c.Names.find(n => n.includes(CC_NAME))) {
      await docker.getContainer(c.Id).remove({force: true});
    }
  });
  // kill old chaincode images
  await Bluebird.map(prevImages, async i => {
    if(i.RepoTags.find(n => n.includes(CC_NAME))) {
      await docker.getImage(i.Id).remove().catch(console.log);
    }
  });
}
