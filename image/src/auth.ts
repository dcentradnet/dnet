import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import {ExtractJwt} from 'passport-jwt';
import {Strategy as LocalStrategy} from 'passport-local';
import {Strategy as JwtStrategy} from 'passport-jwt';
import uuidv4 from 'uuid/v4';

import env from './env';
import * as users from './users';
import {User} from './generated/graphqlTypes';

const {
    ADMIN_MODE
} = env.vars;

passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
    async function(username: string, password: string, done) {
        const user = await users.list({name: username}).then(l => l[0]);
        const passwordValid = await users.checkPasswordByUser(user, password);

        if(passwordValid) {
            done(null, user, {message: 'Login successful.'});
        } else {
            done(null, false, {message: 'Login failed.'});
        }
    }
));

export const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: uuidv4()
};

passport.use(new JwtStrategy(jwtOptions, function(jwt_payload, done) {
    console.log('auth', jwt_payload);
    done(null, {});
}));

export function createToken(id: string): string {
    return jwt.sign(id, jwtOptions.secretOrKey);
}

export function loginRoute(req: Request, res: Response): void {
    // see https://medium.com/front-end-weekly/learn-using-jwt-with-passport-authentication-9761539c4314

    passport.authenticate('local', {session: false}, (err, user: User) => {
        console.log(user);
        req.login(null, {session: false}, (err) => {
            if (err) {
                return res.send(err);
            }
            
            // generate a signed json web token with the contents of user object and return it in the response
            const token = createToken(user._id.toString());
            res.json({_id: user._id, token});
        });
    })(req, res);
}

export function authenticate(): any {
    // disable authentication in Admin mode
    if (ADMIN_MODE) {
        return (req: Request, res: Response, next) => next();
    }

    return passport.authenticate('jwt', { session: false });
}
