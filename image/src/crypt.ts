import { readFileSync } from 'fs';
import { spawn } from 'child_process';
import * as crypto from 'crypto';
import { promise as glob } from 'glob-promise';
import ECKey from 'ec-key';

import env from './env';
import * as log from './log';

const {
    CRYPTO
} = env.vars;
const config_env = env.config;

export async function createCryptoMaterial(): Promise<void> {
  const material = spawn('bash', ['material.sh', 'generate'], config_env);
  await log.logAndWait(material);
}

export async function getPrivateKey(): Promise<ECKey | undefined> {
  const privKeyDir = `${CRYPTO}/peerOrganizations/${env.vars.ORG_DOMAIN}/ca/`;
  const privKeyFile = (await glob('*_sk', {cwd: privKeyDir}))[0];
  if(!privKeyFile) {
    return undefined;
  }

  const privKeyString = readFileSync(privKeyDir+privKeyFile).toString();
  const privKey = new ECKey(privKeyString, 'pem');
  
  return privKey;
}

export async function getPublicKeyString(): Promise<string | undefined> {
  const pubKey = await getPublicKey();
  return pubKey && pubKey.toString();
}

export async function getPublicKey(): Promise<ECKey | undefined> {
  const privKey = await getPrivateKey();
  return privKey && privKey.asPublicECKey();
}

export interface Body {
  msg: any;
  sig: string;
  pub: string;
}
export async function signMessageAsBody(msg: any): Promise<Body> {
  // get private key
  const privKey = await getPrivateKey();
  
  const msgAsString = JSON.stringify(msg);
  console.log(msgAsString);
  
  const body = {
    msg: msg,
    sig: privKey.createSign('SHA256').update(msgAsString).sign('base64'),
    pub: privKey.asPublicECKey().toString()
  };
  
  return body;
}

export function encrypt(text: string, passphrase: string): string {
  const cipher = crypto.createCipheriv('aes-256-cbc', passphrase, new Buffer(passphrase).toString('hex').slice(0, 16)); // FIXME use proper iv
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  
  return crypted;
}

export function decrypt(text: string, passphrase: string): string {
  const decipher = crypto.createDecipheriv('aes-256-cbc', passphrase, new Buffer(passphrase).toString('hex').slice(0, 16)); // FIXME use proper iv
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  
  return dec;
}

export async function decryptWithOtherPublicKey(crypted: string, opub: string): Promise<string> {
  // compute secret
  const key = await getPrivateKey();
  const rKey = new ECKey(opub);
  const secret = key.computeSecret(rKey);
  
  // decrypt
  return decrypt(crypted, secret);
}
