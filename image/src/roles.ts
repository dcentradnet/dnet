import { ObjectId } from 'mongoist';
import Bluebird from 'bluebird';
import * as database from './db';
import * as users from './users';
import { Role, RoleCore, User } from './generated/graphqlTypes';

export async function list(query?: Record<string, string>): Promise<Role[]> {
    const db = await database.mongoPromise;

    return db.roles.find(query);
}

export async function put(name: string, _id?: string): Promise<RoleCore> {
    const db = await database.mongoPromise;

    return db.roles.findAndModify({query: {_id: ObjectId(_id)}, update: {$set: {name}}, new: true, upsert: true});
}

export async function remove(_id: string): Promise<void> {
    const db = await database.mongoPromise;

    await db.roles.remove({_id: ObjectId(_id)}, {justOne: true});
}

export async function attachRoleToUser(roleId: string, userId: string): Promise<void> {
    const db = await database.mongoPromise;

    await db.userRoles.insert({roleId: ObjectId(roleId), userId: ObjectId(userId)});
}

export async function removeRoleFromUser(roleId: string, userId: string): Promise<void> {
    const db = await database.mongoPromise;

    await db.userRoles.remove({roleId: ObjectId(roleId), userId: ObjectId(userId)}, {justOne: true});
}

export async function listUsersByRole(roleId: string): Promise<User[]> {
    const db = await database.mongoPromise;

    return Bluebird.map(db.userRoles.find({roleId}, {userId: 1}),
                        async({userId}): Promise<User> => users.list({_id: ObjectId(userId)}).then(l => l[0]));
}

export async function listUserRoles(userId: string): Promise<Role[]> {
    const db = await database.mongoPromise;

    return Bluebird.map(db.userRoles.find({userId}, {roleId: 1}),
                        async({roleId}): Promise<Role> => list({_id: ObjectId(roleId)}).then(l => l[0]));
}

export async function hasUserRole(userId: string, roleId: string): Promise<boolean> {
    const db = await database.mongoPromise;

    return db.userRoles.findAsCursor({userId, _id: roleId}).limit(1).size().then(size => size > 0);
}

export async function hasUserRoleByName(userId: string, roleName: string): Promise<boolean> {
    const db = await database.mongoPromise;

    return db.userRoles.findAsCursor({userId, name: roleName}).limit(1).size().then(size => size > 0);
}