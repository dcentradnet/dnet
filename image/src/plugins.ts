import { Express } from 'express';

import * as database from './db';

//import * as ipfs from './plugins/ipfs/index';
import * as indy from './plugins/indy/index';
import * as ethTranscribe from './plugins/eth-transcribe/index';
import { Plugin } from './generated/graphqlTypes';

export async function listInstallable(): Promise<(Plugin & {plugin: any})[]> {
    // FIXME this should be queried from the lobby
    /*return Promise.resolve({
        'IPFS': ipfs,
        'Indy': indy,
        'ETHTranscribe': ethTranscribe
    });*/

    const running = await listRunning();

    return [
      {
        _id: 'babaf10e-7107-4501-b485-78358e5080a3',
        title: 'Indy', version: '1.0', imageId: '',
        installed: !!running.find(r => r.title === 'Indy'),
        parameters: await indy.parameters(),
        plugin: indy
      },
      //{title: 'Vault', version: '1.0', imageId: '', plugin: vault},
      /*{title: 'IPFS', version: '1.0', imageId: '', plugin: ipfs, parameters: {
        swarmKey: 'Swarm key'
      }},*/
    ];
}

export async function listRunning(): Promise<Plugin[]> {
  const db = await database.mongoPromise;
  
  return db.pluginsRunning.find();
}

export async function addRunning(plugin: Plugin): Promise<void> {
  const db = await database.mongoPromise;
  
  return db.pluginsRunning.insert(plugin);
}

export async function install(_id: string, args: string[], express: Express): Promise<void> {
  console.log(_id, args);
  const installable = await listInstallable();
  const {plugin} = installable.find(p => p._id === _id);
  
  await plugin.install(express);
  await plugin.apply(args);
}
