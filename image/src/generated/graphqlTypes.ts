export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  JSON: any,
};

export type Action = {
   __typename?: 'Action',
  _id: Scalars['String'],
  name: Scalars['String'],
  progress?: Maybe<Scalars['Float']>,
  log?: Maybe<Array<Maybe<Scalars['String']>>>,
};

export type Buffer = {
   __typename?: 'Buffer',
  type?: Maybe<Scalars['String']>,
  data?: Maybe<Array<Maybe<Scalars['Int']>>>,
};

export type Chaincode = {
   __typename?: 'Chaincode',
  name: Scalars['String'],
};

export type ChaincodeEvent = {
   __typename?: 'ChaincodeEvent',
  eventName: Scalars['String'],
  payloadString: Scalars['String'],
  blockNumber: Scalars['String'],
  transactionId: Scalars['String'],
  status: Scalars['String'],
};

export type ChaincodeEventListenInput = {
  channelName: Scalars['String'],
  chaincodeName: Scalars['String'],
  eventName: Scalars['String'],
};

export type ChaincodeQuery = {
   __typename?: 'ChaincodeQuery',
  installed?: Maybe<Array<Chaincode>>,
  instantiated?: Maybe<Array<Chaincode>>,
};

export type Channel = {
   __typename?: 'Channel',
  _id: Scalars['String'],
  chaincodes?: Maybe<Array<Chaincode>>,
};

export type Consortium = {
   __typename?: 'Consortium',
  cid: Scalars['ID'],
  name: Scalars['String'],
  desc?: Maybe<Scalars['String']>,
  founder?: Maybe<Scalars['String']>,
  requests?: Maybe<Array<ConsortiumRequest>>,
  approvals?: Maybe<Array<ConsortiumApproval>>,
  members?: Maybe<Array<ConsortiumMember>>,
};


export type ConsortiumApprovalsArgs = {
  pub?: Maybe<Scalars['String']>
};


export type ConsortiumMembersArgs = {
  pub?: Maybe<Scalars['String']>
};

export type ConsortiumApproval = {
   __typename?: 'ConsortiumApproval',
  _id: Scalars['ID'],
  cid: Scalars['ID'],
  rpub?: Maybe<Scalars['String']>,
  pub?: Maybe<Scalars['String']>,
  sig?: Maybe<Scalars['String']>,
};

export type ConsortiumLocal = {
   __typename?: 'ConsortiumLocal',
  organizations?: Maybe<Array<Organization>>,
};

export type ConsortiumMember = {
   __typename?: 'ConsortiumMember',
  _id: Scalars['ID'],
  cid: Scalars['ID'],
  pub?: Maybe<Scalars['String']>,
  apub?: Maybe<Scalars['String']>,
  token?: Maybe<Scalars['String']>,
};

export type ConsortiumOrganization = {
   __typename?: 'ConsortiumOrganization',
  pub: Scalars['String'],
  name: Scalars['String'],
  about?: Maybe<Scalars['String']>,
  imageId?: Maybe<Scalars['String']>,
  founder?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
};

export type ConsortiumOrganizationInput = {
  name: Scalars['String'],
  about?: Maybe<Scalars['String']>,
  imageId?: Maybe<Scalars['String']>,
  founder?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
};

export type ConsortiumRequest = {
   __typename?: 'ConsortiumRequest',
  _id: Scalars['ID'],
  cid: Scalars['ID'],
  rpub: Scalars['String'],
  sig: Scalars['String'],
  confirmed?: Maybe<Scalars['Boolean']>,
  approvals?: Maybe<Array<ConsortiumApproval>>,
};

export type Container = {
   __typename?: 'Container',
  Name: Scalars['String'],
  Health?: Maybe<Scalars['JSON']>,
};

export type Dapp = {
   __typename?: 'Dapp',
  _id?: Maybe<Scalars['ID']>,
  title: Scalars['String'],
  version: Scalars['String'],
  imageId?: Maybe<Scalars['String']>,
  dockerImage: Scalars['String'],
  pricing?: Maybe<Pricing>,
};

export type DappManualInstallInput = {
  title: Scalars['String'],
  version: Scalars['String'],
  dockerImage: Scalars['String'],
  options?: Maybe<Scalars['JSON']>,
};

export type Dapps = {
   __typename?: 'Dapps',
  installable?: Maybe<Array<Dapp>>,
  installed?: Maybe<Array<InstalledDapp>>,
};

export type FormField = {
   __typename?: 'FormField',
  type: Scalars['String'],
  name: Scalars['String'],
  label: Scalars['String'],
  value: Scalars['String'],
  required: Scalars['Boolean'],
};

export type Governance = {
   __typename?: 'Governance',
  request?: Maybe<Request>,
  requests?: Maybe<Array<Request>>,
};


export type GovernanceRequestArgs = {
  id?: Maybe<Scalars['ID']>
};

export type Info = {
   __typename?: 'Info',
  consDomain?: Maybe<Scalars['String']>,
  orgName?: Maybe<Scalars['String']>,
  channelName?: Maybe<Scalars['String']>,
  publicIpV4?: Maybe<Scalars['String']>,
  publicKey?: Maybe<Scalars['String']>,
  isAdminMode?: Maybe<Scalars['Boolean']>,
  isJoined?: Maybe<Scalars['Boolean']>,
};

export type InstalledDapp = {
   __typename?: 'InstalledDapp',
  hostName: Scalars['String'],
  dapp: Dapp,
};


export type Lobby = {
   __typename?: 'Lobby',
  organization?: Maybe<ConsortiumOrganization>,
  consortia?: Maybe<Array<Consortium>>,
  requested?: Maybe<Consortium>,
  joined?: Maybe<Consortium>,
  member?: Maybe<ConsortiumMember>,
};


export type LobbyOrganizationArgs = {
  pub?: Maybe<Scalars['String']>
};

export type Mutation = {
   __typename?: 'Mutation',
  swarmInit?: Maybe<Scalars['Boolean']>,
  swarmJoin?: Maybe<Scalars['Boolean']>,
  swarmJoinCrypted?: Maybe<Scalars['Boolean']>,
  networkInit?: Maybe<Scalars['Boolean']>,
  networkShutdown?: Maybe<Scalars['Boolean']>,
  networkExtend?: Maybe<Scalars['Boolean']>,
  lobbyConsortiaAdd?: Maybe<Scalars['Boolean']>,
  lobbyConsortiumSet?: Maybe<Scalars['Boolean']>,
  lobbyRequestAdd?: Maybe<Scalars['Boolean']>,
  lobbyRequestRemove?: Maybe<Scalars['Boolean']>,
  lobbyOrganizationPut?: Maybe<Scalars['Boolean']>,
  lobbyOrganizationRemove?: Maybe<Scalars['Boolean']>,
  request?: Maybe<Scalars['Boolean']>,
  requestBroadcast?: Maybe<Scalars['Boolean']>,
  vote?: Maybe<Scalars['Int']>,
  syncRequests?: Maybe<Scalars['Boolean']>,
  chaincodeInvoke?: Maybe<Scalars['Boolean']>,
  putUser?: Maybe<UserCore>,
  removeUser?: Maybe<Scalars['Boolean']>,
  loginUser?: Maybe<Scalars['String']>,
  putRole?: Maybe<RoleCore>,
  removeRole?: Maybe<Scalars['Boolean']>,
  attachRoleToUser?: Maybe<Scalars['Boolean']>,
  removeRoleFromUser?: Maybe<Scalars['Boolean']>,
  putPermission?: Maybe<PermissionCore>,
  removePermission?: Maybe<Scalars['Boolean']>,
  attachPermToRole?: Maybe<Scalars['Boolean']>,
  removePermFromRole?: Maybe<Scalars['Boolean']>,
  installPlugin?: Maybe<Scalars['Boolean']>,
  installDapp?: Maybe<Scalars['Boolean']>,
};


export type MutationSwarmInitArgs = {
  params?: Maybe<SwarmInitParamsInput>
};


export type MutationSwarmJoinArgs = {
  params?: Maybe<SwarmJoinParamsInput>
};


export type MutationSwarmJoinCryptedArgs = {
  crypted: Scalars['String'],
  opub: Scalars['String']
};


export type MutationNetworkInitArgs = {
  consDomain: Scalars['String'],
  waitFinish?: Maybe<Scalars['Boolean']>
};


export type MutationNetworkShutdownArgs = {
  consDomain: Scalars['String'],
  waitFinish?: Maybe<Scalars['Boolean']>
};


export type MutationNetworkExtendArgs = {
  consDomain: Scalars['String'],
  waitFinish?: Maybe<Scalars['Boolean']>
};


export type MutationLobbyConsortiaAddArgs = {
  cid: Scalars['String'],
  name: Scalars['String'],
  desc: Scalars['String']
};


export type MutationLobbyConsortiumSetArgs = {
  cid: Scalars['String']
};


export type MutationLobbyRequestAddArgs = {
  cid: Scalars['String']
};


export type MutationLobbyRequestRemoveArgs = {
  cid: Scalars['String']
};


export type MutationLobbyOrganizationPutArgs = {
  info?: Maybe<ConsortiumOrganizationInput>
};


export type MutationRequestArgs = {
  request?: Maybe<RequestInput>
};


export type MutationRequestBroadcastArgs = {
  request?: Maybe<RequestInput>
};


export type MutationVoteArgs = {
  vote?: Maybe<VoteInput>
};


export type MutationChaincodeInvokeArgs = {
  channel?: Maybe<Scalars['String']>,
  chaincode?: Maybe<Scalars['String']>,
  method?: Maybe<Scalars['String']>,
  args?: Maybe<Array<Maybe<Scalars['String']>>>
};


export type MutationPutUserArgs = {
  _id?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>
};


export type MutationRemoveUserArgs = {
  _id?: Maybe<Scalars['String']>
};


export type MutationLoginUserArgs = {
  _id?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>
};


export type MutationPutRoleArgs = {
  _id?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>
};


export type MutationRemoveRoleArgs = {
  _id?: Maybe<Scalars['String']>
};


export type MutationAttachRoleToUserArgs = {
  roleId?: Maybe<Scalars['String']>,
  userId?: Maybe<Scalars['String']>
};


export type MutationRemoveRoleFromUserArgs = {
  roleId?: Maybe<Scalars['String']>,
  userId?: Maybe<Scalars['String']>
};


export type MutationPutPermissionArgs = {
  _id?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>
};


export type MutationRemovePermissionArgs = {
  _id?: Maybe<Scalars['String']>
};


export type MutationAttachPermToRoleArgs = {
  permId?: Maybe<Scalars['String']>,
  roleId?: Maybe<Scalars['String']>
};


export type MutationRemovePermFromRoleArgs = {
  permId?: Maybe<Scalars['String']>,
  roleId?: Maybe<Scalars['String']>
};


export type MutationInstallPluginArgs = {
  plugin?: Maybe<PluginInstallInput>
};


export type MutationInstallDappArgs = {
  dapp?: Maybe<DappManualInstallInput>
};

export type Notification = {
   __typename?: 'Notification',
  _id: Scalars['ID'],
  system?: Maybe<Scalars['String']>,
  message: Scalars['String'],
  url?: Maybe<Scalars['String']>,
  timestamp?: Maybe<Scalars['Int']>,
};

export type Organization = {
   __typename?: 'Organization',
  _id: Scalars['ID'],
  name: Scalars['String'],
  publicIpV4?: Maybe<Scalars['String']>,
  publicKey?: Maybe<Scalars['String']>,
  users?: Maybe<Array<User>>,
  roles?: Maybe<Array<Role>>,
  info?: Maybe<ConsortiumOrganization>,
};


export type OrganizationUsersArgs = {
  _id?: Maybe<Scalars['String']>
};


export type OrganizationRolesArgs = {
  _id?: Maybe<Scalars['String']>
};

export type Permission = {
   __typename?: 'Permission',
  _id: Scalars['String'],
  name: Scalars['String'],
  roles?: Maybe<Array<RoleCore>>,
};

export type PermissionCore = {
   __typename?: 'PermissionCore',
  _id: Scalars['String'],
  name: Scalars['String'],
};

export type Plugin = {
   __typename?: 'Plugin',
  _id?: Maybe<Scalars['ID']>,
  title: Scalars['String'],
  version: Scalars['String'],
  imageId?: Maybe<Scalars['String']>,
  pricing?: Maybe<Pricing>,
  parameters?: Maybe<Array<FormField>>,
  installed?: Maybe<Scalars['Boolean']>,
};

export type PluginInstallInput = {
  _id: Scalars['String'],
  version: Scalars['String'],
  parameters?: Maybe<Scalars['JSON']>,
};

export type Plugins = {
   __typename?: 'Plugins',
  installable?: Maybe<Array<Plugin>>,
  installed?: Maybe<Array<Plugin>>,
};

export type Pricing = {
   __typename?: 'Pricing',
  currency: Scalars['String'],
  initialPrice?: Maybe<Scalars['Int']>,
  pricePerMonth?: Maybe<Scalars['Int']>,
  paymentInfo?: Maybe<Scalars['String']>,
};

export type Query = {
   __typename?: 'Query',
  info?: Maybe<Info>,
  organization?: Maybe<Organization>,
  organizations?: Maybe<Array<Organization>>,
  consortium?: Maybe<Consortium>,
  swarm?: Maybe<Swarm>,
  containers?: Maybe<Array<Container>>,
  channels?: Maybe<Array<Channel>>,
  chaincode?: Maybe<ChaincodeQuery>,
  actions?: Maybe<Array<Action>>,
  lobby?: Maybe<Lobby>,
  gov?: Maybe<Governance>,
  users?: Maybe<Array<User>>,
  roles?: Maybe<Array<Role>>,
  permissions?: Maybe<Array<Permission>>,
  dapps?: Maybe<Dapps>,
  plugins?: Maybe<Plugins>,
  replayNotifications?: Maybe<Array<Notification>>,
  chaincodeQuery?: Maybe<Buffer>,
};


export type QueryUsersArgs = {
  _id?: Maybe<Scalars['String']>
};


export type QueryRolesArgs = {
  _id?: Maybe<Scalars['String']>
};


export type QueryPermissionsArgs = {
  _id?: Maybe<Scalars['String']>
};


export type QueryChaincodeQueryArgs = {
  chaincode?: Maybe<Scalars['String']>,
  method?: Maybe<Scalars['String']>,
  args?: Maybe<Array<Maybe<Scalars['String']>>>
};

export type Request = {
   __typename?: 'Request',
  _id: Scalars['String'],
  type: Scalars['String'],
  key: Scalars['String'],
  subkey: Scalars['String'],
  parent?: Maybe<RequestCore>,
  children?: Maybe<Array<RequestCore>>,
  quorum?: Maybe<Array<Organization>>,
  votes?: Maybe<Array<VoteCore>>,
  ownvote?: Maybe<VoteCore>,
  settled?: Maybe<Scalars['Boolean']>,
};

export type RequestCore = {
   __typename?: 'RequestCore',
  _id: Scalars['String'],
  type: Scalars['String'],
  key: Scalars['String'],
  subkey: Scalars['String'],
  parent?: Maybe<Scalars['String']>,
  children?: Maybe<Array<Scalars['String']>>,
  quorum?: Maybe<Array<Scalars['String']>>,
  ownvote?: Maybe<VoteCore>,
  settled?: Maybe<Scalars['Boolean']>,
};

export type RequestInput = {
  _id: Scalars['String'],
  type: Scalars['String'],
  key: Scalars['String'],
  subkey: Scalars['String'],
  parent?: Maybe<Scalars['String']>,
  children?: Maybe<Array<Scalars['String']>>,
};

export type Role = {
   __typename?: 'Role',
  _id: Scalars['String'],
  name: Scalars['String'],
  users?: Maybe<Array<UserCore>>,
};

export type RoleCore = {
   __typename?: 'RoleCore',
  _id: Scalars['String'],
  name: Scalars['String'],
};

export type Subscription = {
   __typename?: 'Subscription',
  notification?: Maybe<Notification>,
  statusUpdate?: Maybe<Info>,
  requestAdd?: Maybe<RequestCore>,
  chaincodeEvent?: Maybe<ChaincodeEvent>,
};


export type SubscriptionChaincodeEventArgs = {
  input?: Maybe<ChaincodeEventListenInput>
};

export type Swarm = {
   __typename?: 'Swarm',
  nodes?: Maybe<Scalars['JSON']>,
  inspect?: Maybe<Scalars['JSON']>,
};

export type SwarmInitParamsInput = {
  ListenAddr?: Maybe<Scalars['String']>,
  AdvertiseAddr?: Maybe<Scalars['String']>,
  ForceNewCluster?: Maybe<Scalars['Boolean']>,
};

export type SwarmJoinParamsInput = {
  ListenAddr?: Maybe<Scalars['String']>,
  AdvertiseAddr?: Maybe<Scalars['String']>,
  RemoteAddrs?: Maybe<Array<Scalars['String']>>,
  JoinToken?: Maybe<Scalars['String']>,
};

export type User = {
   __typename?: 'User',
  _id: Scalars['String'],
  name: Scalars['String'],
  passHash?: Maybe<Scalars['String']>,
  roles?: Maybe<Array<RoleCore>>,
};

export type UserCore = {
   __typename?: 'UserCore',
  _id: Scalars['String'],
  name: Scalars['String'],
};

export type Vote = {
   __typename?: 'Vote',
  _id?: Maybe<Scalars['String']>,
  rid: Scalars['String'],
  value: Scalars['Int'],
  request?: Maybe<RequestCore>,
  children?: Maybe<Array<RequestCore>>,
  settled?: Maybe<Scalars['Boolean']>,
  voter?: Maybe<Organization>,
};

export type VoteCore = {
   __typename?: 'VoteCore',
  _id?: Maybe<Scalars['String']>,
  rid: Scalars['String'],
  value: Scalars['Int'],
  settled?: Maybe<Scalars['Boolean']>,
  voter?: Maybe<Organization>,
};

export type VoteInput = {
  _id?: Maybe<Scalars['String']>,
  rid: Scalars['String'],
  value: Scalars['Int'],
};
