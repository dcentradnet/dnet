import { GraphQLClient } from 'graphql-request';
import { print } from 'graphql';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  JSON: any,
};

export type Consortium = {
   __typename?: 'Consortium',
  cid: Scalars['ID'],
  name: Scalars['String'],
  desc?: Maybe<Scalars['String']>,
  founder?: Maybe<Scalars['String']>,
  requests?: Maybe<Array<ConsortiumRequest>>,
  approvals?: Maybe<Array<ConsortiumApproval>>,
  members?: Maybe<Array<ConsortiumMember>>,
};


export type ConsortiumApprovalsArgs = {
  pub?: Maybe<Scalars['String']>
};


export type ConsortiumMembersArgs = {
  pub?: Maybe<Scalars['String']>
};

export type ConsortiumApproval = {
   __typename?: 'ConsortiumApproval',
  _id: Scalars['ID'],
  cid: Scalars['ID'],
  rpub?: Maybe<Scalars['String']>,
  pub?: Maybe<Scalars['String']>,
  sig?: Maybe<Scalars['String']>,
};

export type ConsortiumMember = {
   __typename?: 'ConsortiumMember',
  _id: Scalars['ID'],
  cid: Scalars['ID'],
  pub?: Maybe<Scalars['String']>,
  apub?: Maybe<Scalars['String']>,
  token?: Maybe<Scalars['String']>,
};

export type ConsortiumOrganization = {
   __typename?: 'ConsortiumOrganization',
  pub: Scalars['String'],
  name: Scalars['String'],
  about?: Maybe<Scalars['String']>,
  imageId?: Maybe<Scalars['String']>,
  founder?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
};

export type ConsortiumRequest = {
   __typename?: 'ConsortiumRequest',
  _id: Scalars['ID'],
  cid: Scalars['ID'],
  rpub: Scalars['String'],
  sig: Scalars['String'],
  confirmed?: Maybe<Scalars['Boolean']>,
  approvals?: Maybe<Array<ConsortiumApproval>>,
};

export type Dapp = {
   __typename?: 'Dapp',
  _id?: Maybe<Scalars['ID']>,
  title: Scalars['String'],
  version: Scalars['String'],
  imageId?: Maybe<Scalars['String']>,
  dockerImage: Scalars['String'],
  pricing?: Maybe<Pricing>,
};

export type FormField = {
   __typename?: 'FormField',
  type: Scalars['String'],
  name: Scalars['String'],
  label: Scalars['String'],
  value: Scalars['String'],
  required: Scalars['Boolean'],
};

export type FormFieldOption = {
   __typename?: 'FormFieldOption',
  key: Scalars['String'],
  label: Scalars['String'],
};


export type Lobby = {
   __typename?: 'Lobby',
  consortia?: Maybe<Array<Consortium>>,
  organization?: Maybe<ConsortiumOrganization>,
};


export type LobbyConsortiaArgs = {
  cid?: Maybe<Scalars['String']>
};


export type LobbyOrganizationArgs = {
  pub?: Maybe<Scalars['String']>
};

export type Mutation = {
   __typename?: 'Mutation',
  addConsortium?: Maybe<Scalars['Boolean']>,
  addRequest?: Maybe<Scalars['Boolean']>,
  removeRequest?: Maybe<Scalars['Boolean']>,
  addApproval?: Maybe<Scalars['Boolean']>,
  removeApproval?: Maybe<Scalars['Boolean']>,
  putOrganization?: Maybe<Scalars['Boolean']>,
  removeOrganization?: Maybe<Scalars['Boolean']>,
};


export type MutationAddConsortiumArgs = {
  signedConsortium?: Maybe<SignedEnvelopeInput>
};


export type MutationAddRequestArgs = {
  signedRequest?: Maybe<SignedEnvelopeInput>
};


export type MutationRemoveRequestArgs = {
  signedRequest?: Maybe<SignedEnvelopeInput>
};


export type MutationAddApprovalArgs = {
  signedApproval?: Maybe<SignedEnvelopeInput>
};


export type MutationRemoveApprovalArgs = {
  signedApproval?: Maybe<SignedEnvelopeInput>
};


export type MutationPutOrganizationArgs = {
  signedOrg?: Maybe<SignedEnvelopeInput>
};


export type MutationRemoveOrganizationArgs = {
  signedOrg?: Maybe<SignedEnvelopeInput>
};

export type Plugin = {
   __typename?: 'Plugin',
  _id?: Maybe<Scalars['ID']>,
  title: Scalars['String'],
  version: Scalars['String'],
  imageId?: Maybe<Scalars['String']>,
  pricing?: Maybe<Pricing>,
  parameters?: Maybe<Array<FormField>>,
  installed?: Maybe<Scalars['Boolean']>,
};

export type Pricing = {
   __typename?: 'Pricing',
  currency: Scalars['String'],
  initialPrice?: Maybe<Scalars['Int']>,
  pricePerMonth?: Maybe<Scalars['Int']>,
  paymentInfo?: Maybe<Scalars['String']>,
};

export type Query = {
   __typename?: 'Query',
  lobby?: Maybe<Lobby>,
  store?: Maybe<Store>,
};

export type SignedEnvelopeInput = {
  msg: Scalars['JSON'],
  sig: Scalars['String'],
  pub: Scalars['String'],
};

export type Store = {
   __typename?: 'Store',
  dapps?: Maybe<Array<Dapp>>,
  plugins?: Maybe<Array<Plugin>>,
};

export type LobbyFragment = (
  { __typename?: 'Lobby' }
  & { consortia?: Maybe<Array<(
    { __typename?: 'Consortium' }
    & ConsortiumNoNestingFragment
  )>>, organization?: Maybe<(
    { __typename?: 'ConsortiumOrganization' }
    & ConsortiumOrganizationNoNestingFragment
  )> }
);

export type ConsortiumFragment = (
  { __typename?: 'Consortium' }
  & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'>
  & { requests?: Maybe<Array<(
    { __typename?: 'ConsortiumRequest' }
    & ConsortiumRequestNoNestingFragment
  )>>, approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalNoNestingFragment
  )>>, members?: Maybe<Array<(
    { __typename?: 'ConsortiumMember' }
    & ConsortiumMemberNoNestingFragment
  )>> }
);

export type ConsortiumRequestFragment = (
  { __typename?: 'ConsortiumRequest' }
  & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'>
  & { approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalNoNestingFragment
  )>> }
);

export type ConsortiumApprovalFragment = (
  { __typename?: 'ConsortiumApproval' }
  & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>
);

export type ConsortiumMemberFragment = (
  { __typename?: 'ConsortiumMember' }
  & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token'>
);

export type ConsortiumOrganizationFragment = (
  { __typename?: 'ConsortiumOrganization' }
  & Pick<ConsortiumOrganization, 'pub' | 'name' | 'about' | 'imageId' | 'founder' | 'email'>
);

export type StoreFragment = (
  { __typename?: 'Store' }
  & { dapps?: Maybe<Array<(
    { __typename?: 'Dapp' }
    & DappNoNestingFragment
  )>>, plugins?: Maybe<Array<(
    { __typename?: 'Plugin' }
    & PluginNoNestingFragment
  )>> }
);

export type DappFragment = (
  { __typename?: 'Dapp' }
  & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingNoNestingFragment
  )> }
);

export type PricingFragment = (
  { __typename?: 'Pricing' }
  & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>
);

export type PluginFragment = (
  { __typename?: 'Plugin' }
  & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingNoNestingFragment
  )>, parameters?: Maybe<Array<(
    { __typename?: 'FormField' }
    & FormFieldNoNestingFragment
  )>> }
);

export type FormFieldFragment = (
  { __typename?: 'FormField' }
  & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>
);

export type FormFieldOptionFragment = (
  { __typename?: 'FormFieldOption' }
  & Pick<FormFieldOption, 'key' | 'label'>
);

export type ConsortiumNoNestingFragment = (
  { __typename?: 'Consortium' }
  & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'>
);

export type ConsortiumRequestNoNestingFragment = (
  { __typename?: 'ConsortiumRequest' }
  & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'>
);

export type ConsortiumApprovalNoNestingFragment = (
  { __typename?: 'ConsortiumApproval' }
  & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>
);

export type ConsortiumMemberNoNestingFragment = (
  { __typename?: 'ConsortiumMember' }
  & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token'>
);

export type ConsortiumOrganizationNoNestingFragment = (
  { __typename?: 'ConsortiumOrganization' }
  & Pick<ConsortiumOrganization, 'pub' | 'name' | 'about' | 'imageId' | 'founder' | 'email'>
);

export type DappNoNestingFragment = (
  { __typename?: 'Dapp' }
  & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'>
);

export type PricingNoNestingFragment = (
  { __typename?: 'Pricing' }
  & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>
);

export type PluginNoNestingFragment = (
  { __typename?: 'Plugin' }
  & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'>
);

export type FormFieldNoNestingFragment = (
  { __typename?: 'FormField' }
  & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>
);

export type FormFieldOptionNoNestingFragment = (
  { __typename?: 'FormFieldOption' }
  & Pick<FormFieldOption, 'key' | 'label'>
);

export type LobbyDeepNestingFragment = (
  { __typename?: 'Lobby' }
  & { consortia?: Maybe<Array<(
    { __typename?: 'Consortium' }
    & ConsortiumDeepNestingFragment
  )>>, organization?: Maybe<(
    { __typename?: 'ConsortiumOrganization' }
    & ConsortiumOrganizationDeepNestingFragment
  )> }
);

export type ConsortiumDeepNestingFragment = (
  { __typename?: 'Consortium' }
  & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'>
  & { requests?: Maybe<Array<(
    { __typename?: 'ConsortiumRequest' }
    & ConsortiumRequestDeepNestingFragment
  )>>, approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalDeepNestingFragment
  )>>, members?: Maybe<Array<(
    { __typename?: 'ConsortiumMember' }
    & ConsortiumMemberDeepNestingFragment
  )>> }
);

export type ConsortiumRequestDeepNestingFragment = (
  { __typename?: 'ConsortiumRequest' }
  & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'>
  & { approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalDeepNestingFragment
  )>> }
);

export type ConsortiumApprovalDeepNestingFragment = (
  { __typename?: 'ConsortiumApproval' }
  & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>
);

export type ConsortiumMemberDeepNestingFragment = (
  { __typename?: 'ConsortiumMember' }
  & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token'>
);

export type ConsortiumOrganizationDeepNestingFragment = (
  { __typename?: 'ConsortiumOrganization' }
  & Pick<ConsortiumOrganization, 'pub' | 'name' | 'about' | 'imageId' | 'founder' | 'email'>
);

export type StoreDeepNestingFragment = (
  { __typename?: 'Store' }
  & { dapps?: Maybe<Array<(
    { __typename?: 'Dapp' }
    & DappDeepNestingFragment
  )>>, plugins?: Maybe<Array<(
    { __typename?: 'Plugin' }
    & PluginDeepNestingFragment
  )>> }
);

export type DappDeepNestingFragment = (
  { __typename?: 'Dapp' }
  & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingDeepNestingFragment
  )> }
);

export type PricingDeepNestingFragment = (
  { __typename?: 'Pricing' }
  & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>
);

export type PluginDeepNestingFragment = (
  { __typename?: 'Plugin' }
  & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingDeepNestingFragment
  )>, parameters?: Maybe<Array<(
    { __typename?: 'FormField' }
    & FormFieldDeepNestingFragment
  )>> }
);

export type FormFieldDeepNestingFragment = (
  { __typename?: 'FormField' }
  & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>
);

export type FormFieldOptionDeepNestingFragment = (
  { __typename?: 'FormFieldOption' }
  & Pick<FormFieldOption, 'key' | 'label'>
);

export type ListConsortiaQueryVariables = Exact<{
  cid?: Maybe<Scalars['String']>;
}>;


export type ListConsortiaQuery = (
  { __typename?: 'Query' }
  & { lobby?: Maybe<(
    { __typename?: 'Lobby' }
    & { consortia?: Maybe<Array<(
      { __typename?: 'Consortium' }
      & ConsortiumFragment
    )>> }
  )> }
);

export type ListRequestsQueryVariables = Exact<{
  cid: Scalars['String'];
}>;


export type ListRequestsQuery = (
  { __typename?: 'Query' }
  & { lobby?: Maybe<(
    { __typename?: 'Lobby' }
    & { consortia?: Maybe<Array<(
      { __typename?: 'Consortium' }
      & { requests?: Maybe<Array<(
        { __typename?: 'ConsortiumRequest' }
        & ConsortiumRequestFragment
      )>> }
    )>> }
  )> }
);

export type ListApprovalsQueryVariables = Exact<{
  cid: Scalars['String'];
  pub?: Maybe<Scalars['String']>;
}>;


export type ListApprovalsQuery = (
  { __typename?: 'Query' }
  & { lobby?: Maybe<(
    { __typename?: 'Lobby' }
    & { consortia?: Maybe<Array<(
      { __typename?: 'Consortium' }
      & { approvals?: Maybe<Array<(
        { __typename?: 'ConsortiumApproval' }
        & ConsortiumApprovalFragment
      )>> }
    )>> }
  )> }
);

export type ListMembersQueryVariables = Exact<{
  cid: Scalars['String'];
  pub?: Maybe<Scalars['String']>;
}>;


export type ListMembersQuery = (
  { __typename?: 'Query' }
  & { lobby?: Maybe<(
    { __typename?: 'Lobby' }
    & { consortia?: Maybe<Array<(
      { __typename?: 'Consortium' }
      & { members?: Maybe<Array<(
        { __typename?: 'ConsortiumMember' }
        & ConsortiumMemberFragment
      )>> }
    )>> }
  )> }
);

export type QueryOrganizationQueryVariables = Exact<{
  pub: Scalars['String'];
}>;


export type QueryOrganizationQuery = (
  { __typename?: 'Query' }
  & { lobby?: Maybe<(
    { __typename?: 'Lobby' }
    & { organization?: Maybe<(
      { __typename?: 'ConsortiumOrganization' }
      & ConsortiumOrganizationFragment
    )> }
  )> }
);

export type ListDappsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListDappsQuery = (
  { __typename?: 'Query' }
  & { store?: Maybe<(
    { __typename?: 'Store' }
    & { dapps?: Maybe<Array<(
      { __typename?: 'Dapp' }
      & DappFragment
    )>> }
  )> }
);

export type AddConsortiumMutationVariables = Exact<{
  signedConsortium?: Maybe<SignedEnvelopeInput>;
}>;


export type AddConsortiumMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'addConsortium'>
);

export type AddRequestMutationVariables = Exact<{
  signedRequest?: Maybe<SignedEnvelopeInput>;
}>;


export type AddRequestMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'addRequest'>
);

export type RemoveRequestMutationVariables = Exact<{
  signedRequest?: Maybe<SignedEnvelopeInput>;
}>;


export type RemoveRequestMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'removeRequest'>
);

export type AddApprovalMutationVariables = Exact<{
  signedApproval?: Maybe<SignedEnvelopeInput>;
}>;


export type AddApprovalMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'addApproval'>
);

export type RemoveApprovalMutationVariables = Exact<{
  signedApproval?: Maybe<SignedEnvelopeInput>;
}>;


export type RemoveApprovalMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'removeApproval'>
);

export type PutOrganizationMutationVariables = Exact<{
  signedOrg?: Maybe<SignedEnvelopeInput>;
}>;


export type PutOrganizationMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'putOrganization'>
);

export type RemoveOrganizationMutationVariables = Exact<{
  signedOrg?: Maybe<SignedEnvelopeInput>;
}>;


export type RemoveOrganizationMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'removeOrganization'>
);

export const ConsortiumNoNestingFragmentDoc = gql`
    fragment ConsortiumNoNesting on Consortium {
  cid
  name
  desc
  founder
}
    `;
export const ConsortiumOrganizationNoNestingFragmentDoc = gql`
    fragment ConsortiumOrganizationNoNesting on ConsortiumOrganization {
  pub
  name
  about
  imageId
  founder
  email
}
    `;
export const LobbyFragmentDoc = gql`
    fragment Lobby on Lobby {
  consortia {
    ...ConsortiumNoNesting
  }
  organization {
    ...ConsortiumOrganizationNoNesting
  }
}
    ${ConsortiumNoNestingFragmentDoc}
${ConsortiumOrganizationNoNestingFragmentDoc}`;
export const ConsortiumRequestNoNestingFragmentDoc = gql`
    fragment ConsortiumRequestNoNesting on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
}
    `;
export const ConsortiumApprovalNoNestingFragmentDoc = gql`
    fragment ConsortiumApprovalNoNesting on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumMemberNoNestingFragmentDoc = gql`
    fragment ConsortiumMemberNoNesting on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const ConsortiumFragmentDoc = gql`
    fragment Consortium on Consortium {
  cid
  name
  desc
  founder
  requests {
    ...ConsortiumRequestNoNesting
  }
  approvals {
    ...ConsortiumApprovalNoNesting
  }
  members {
    ...ConsortiumMemberNoNesting
  }
}
    ${ConsortiumRequestNoNestingFragmentDoc}
${ConsortiumApprovalNoNestingFragmentDoc}
${ConsortiumMemberNoNestingFragmentDoc}`;
export const ConsortiumRequestFragmentDoc = gql`
    fragment ConsortiumRequest on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
  approvals {
    ...ConsortiumApprovalNoNesting
  }
}
    ${ConsortiumApprovalNoNestingFragmentDoc}`;
export const ConsortiumApprovalFragmentDoc = gql`
    fragment ConsortiumApproval on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumMemberFragmentDoc = gql`
    fragment ConsortiumMember on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const ConsortiumOrganizationFragmentDoc = gql`
    fragment ConsortiumOrganization on ConsortiumOrganization {
  pub
  name
  about
  imageId
  founder
  email
}
    `;
export const DappNoNestingFragmentDoc = gql`
    fragment DappNoNesting on Dapp {
  _id
  title
  version
  imageId
  dockerImage
}
    `;
export const PluginNoNestingFragmentDoc = gql`
    fragment PluginNoNesting on Plugin {
  _id
  title
  version
  imageId
  installed
}
    `;
export const StoreFragmentDoc = gql`
    fragment Store on Store {
  dapps {
    ...DappNoNesting
  }
  plugins {
    ...PluginNoNesting
  }
}
    ${DappNoNestingFragmentDoc}
${PluginNoNestingFragmentDoc}`;
export const PricingNoNestingFragmentDoc = gql`
    fragment PricingNoNesting on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const DappFragmentDoc = gql`
    fragment Dapp on Dapp {
  _id
  title
  version
  imageId
  dockerImage
  pricing {
    ...PricingNoNesting
  }
}
    ${PricingNoNestingFragmentDoc}`;
export const PricingFragmentDoc = gql`
    fragment Pricing on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const FormFieldNoNestingFragmentDoc = gql`
    fragment FormFieldNoNesting on FormField {
  type
  name
  label
  value
  required
}
    `;
export const PluginFragmentDoc = gql`
    fragment Plugin on Plugin {
  _id
  title
  version
  imageId
  pricing {
    ...PricingNoNesting
  }
  parameters {
    ...FormFieldNoNesting
  }
  installed
}
    ${PricingNoNestingFragmentDoc}
${FormFieldNoNestingFragmentDoc}`;
export const FormFieldFragmentDoc = gql`
    fragment FormField on FormField {
  type
  name
  label
  value
  required
}
    `;
export const FormFieldOptionFragmentDoc = gql`
    fragment FormFieldOption on FormFieldOption {
  key
  label
}
    `;
export const FormFieldOptionNoNestingFragmentDoc = gql`
    fragment FormFieldOptionNoNesting on FormFieldOption {
  key
  label
}
    `;
export const ConsortiumApprovalDeepNestingFragmentDoc = gql`
    fragment ConsortiumApprovalDeepNesting on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumRequestDeepNestingFragmentDoc = gql`
    fragment ConsortiumRequestDeepNesting on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
  approvals {
    ...ConsortiumApprovalDeepNesting
  }
}
    ${ConsortiumApprovalDeepNestingFragmentDoc}`;
export const ConsortiumMemberDeepNestingFragmentDoc = gql`
    fragment ConsortiumMemberDeepNesting on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const ConsortiumDeepNestingFragmentDoc = gql`
    fragment ConsortiumDeepNesting on Consortium {
  cid
  name
  desc
  founder
  requests {
    ...ConsortiumRequestDeepNesting
  }
  approvals {
    ...ConsortiumApprovalDeepNesting
  }
  members {
    ...ConsortiumMemberDeepNesting
  }
}
    ${ConsortiumRequestDeepNestingFragmentDoc}
${ConsortiumApprovalDeepNestingFragmentDoc}
${ConsortiumMemberDeepNestingFragmentDoc}`;
export const ConsortiumOrganizationDeepNestingFragmentDoc = gql`
    fragment ConsortiumOrganizationDeepNesting on ConsortiumOrganization {
  pub
  name
  about
  imageId
  founder
  email
}
    `;
export const LobbyDeepNestingFragmentDoc = gql`
    fragment LobbyDeepNesting on Lobby {
  consortia {
    ...ConsortiumDeepNesting
  }
  organization {
    ...ConsortiumOrganizationDeepNesting
  }
}
    ${ConsortiumDeepNestingFragmentDoc}
${ConsortiumOrganizationDeepNestingFragmentDoc}`;
export const PricingDeepNestingFragmentDoc = gql`
    fragment PricingDeepNesting on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const DappDeepNestingFragmentDoc = gql`
    fragment DappDeepNesting on Dapp {
  _id
  title
  version
  imageId
  dockerImage
  pricing {
    ...PricingDeepNesting
  }
}
    ${PricingDeepNestingFragmentDoc}`;
export const FormFieldDeepNestingFragmentDoc = gql`
    fragment FormFieldDeepNesting on FormField {
  type
  name
  label
  value
  required
}
    `;
export const PluginDeepNestingFragmentDoc = gql`
    fragment PluginDeepNesting on Plugin {
  _id
  title
  version
  imageId
  pricing {
    ...PricingDeepNesting
  }
  parameters {
    ...FormFieldDeepNesting
  }
  installed
}
    ${PricingDeepNestingFragmentDoc}
${FormFieldDeepNestingFragmentDoc}`;
export const StoreDeepNestingFragmentDoc = gql`
    fragment StoreDeepNesting on Store {
  dapps {
    ...DappDeepNesting
  }
  plugins {
    ...PluginDeepNesting
  }
}
    ${DappDeepNestingFragmentDoc}
${PluginDeepNestingFragmentDoc}`;
export const FormFieldOptionDeepNestingFragmentDoc = gql`
    fragment FormFieldOptionDeepNesting on FormFieldOption {
  key
  label
}
    `;
export const ListConsortiaDocument = gql`
    query ListConsortia($cid: String) {
  lobby {
    consortia(cid: $cid) {
      ...Consortium
    }
  }
}
    ${ConsortiumFragmentDoc}`;
export const ListRequestsDocument = gql`
    query ListRequests($cid: String!) {
  lobby {
    consortia(cid: $cid) {
      requests {
        ...ConsortiumRequest
      }
    }
  }
}
    ${ConsortiumRequestFragmentDoc}`;
export const ListApprovalsDocument = gql`
    query ListApprovals($cid: String!, $pub: String) {
  lobby {
    consortia(cid: $cid) {
      approvals(pub: $pub) {
        ...ConsortiumApproval
      }
    }
  }
}
    ${ConsortiumApprovalFragmentDoc}`;
export const ListMembersDocument = gql`
    query ListMembers($cid: String!, $pub: String) {
  lobby {
    consortia(cid: $cid) {
      members(pub: $pub) {
        ...ConsortiumMember
      }
    }
  }
}
    ${ConsortiumMemberFragmentDoc}`;
export const QueryOrganizationDocument = gql`
    query QueryOrganization($pub: String!) {
  lobby {
    organization(pub: $pub) {
      ...ConsortiumOrganization
    }
  }
}
    ${ConsortiumOrganizationFragmentDoc}`;
export const ListDappsDocument = gql`
    query ListDapps {
  store {
    dapps {
      ...Dapp
    }
  }
}
    ${DappFragmentDoc}`;
export const AddConsortiumDocument = gql`
    mutation AddConsortium($signedConsortium: SignedEnvelopeInput) {
  addConsortium(signedConsortium: $signedConsortium)
}
    `;
export const AddRequestDocument = gql`
    mutation AddRequest($signedRequest: SignedEnvelopeInput) {
  addRequest(signedRequest: $signedRequest)
}
    `;
export const RemoveRequestDocument = gql`
    mutation RemoveRequest($signedRequest: SignedEnvelopeInput) {
  removeRequest(signedRequest: $signedRequest)
}
    `;
export const AddApprovalDocument = gql`
    mutation AddApproval($signedApproval: SignedEnvelopeInput) {
  addApproval(signedApproval: $signedApproval)
}
    `;
export const RemoveApprovalDocument = gql`
    mutation RemoveApproval($signedApproval: SignedEnvelopeInput) {
  removeApproval(signedApproval: $signedApproval)
}
    `;
export const PutOrganizationDocument = gql`
    mutation PutOrganization($signedOrg: SignedEnvelopeInput) {
  putOrganization(signedOrg: $signedOrg)
}
    `;
export const RemoveOrganizationDocument = gql`
    mutation RemoveOrganization($signedOrg: SignedEnvelopeInput) {
  removeOrganization(signedOrg: $signedOrg)
}
    `;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction();
export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    ListConsortia(variables?: ListConsortiaQueryVariables, requestHeaders?: Headers): Promise<ListConsortiaQuery> {
      return withWrapper(() => client.request<ListConsortiaQuery>(print(ListConsortiaDocument), variables, requestHeaders));
    },
    ListRequests(variables: ListRequestsQueryVariables, requestHeaders?: Headers): Promise<ListRequestsQuery> {
      return withWrapper(() => client.request<ListRequestsQuery>(print(ListRequestsDocument), variables, requestHeaders));
    },
    ListApprovals(variables: ListApprovalsQueryVariables, requestHeaders?: Headers): Promise<ListApprovalsQuery> {
      return withWrapper(() => client.request<ListApprovalsQuery>(print(ListApprovalsDocument), variables, requestHeaders));
    },
    ListMembers(variables: ListMembersQueryVariables, requestHeaders?: Headers): Promise<ListMembersQuery> {
      return withWrapper(() => client.request<ListMembersQuery>(print(ListMembersDocument), variables, requestHeaders));
    },
    QueryOrganization(variables: QueryOrganizationQueryVariables, requestHeaders?: Headers): Promise<QueryOrganizationQuery> {
      return withWrapper(() => client.request<QueryOrganizationQuery>(print(QueryOrganizationDocument), variables, requestHeaders));
    },
    ListDapps(variables?: ListDappsQueryVariables, requestHeaders?: Headers): Promise<ListDappsQuery> {
      return withWrapper(() => client.request<ListDappsQuery>(print(ListDappsDocument), variables, requestHeaders));
    },
    AddConsortium(variables?: AddConsortiumMutationVariables, requestHeaders?: Headers): Promise<AddConsortiumMutation> {
      return withWrapper(() => client.request<AddConsortiumMutation>(print(AddConsortiumDocument), variables, requestHeaders));
    },
    AddRequest(variables?: AddRequestMutationVariables, requestHeaders?: Headers): Promise<AddRequestMutation> {
      return withWrapper(() => client.request<AddRequestMutation>(print(AddRequestDocument), variables, requestHeaders));
    },
    RemoveRequest(variables?: RemoveRequestMutationVariables, requestHeaders?: Headers): Promise<RemoveRequestMutation> {
      return withWrapper(() => client.request<RemoveRequestMutation>(print(RemoveRequestDocument), variables, requestHeaders));
    },
    AddApproval(variables?: AddApprovalMutationVariables, requestHeaders?: Headers): Promise<AddApprovalMutation> {
      return withWrapper(() => client.request<AddApprovalMutation>(print(AddApprovalDocument), variables, requestHeaders));
    },
    RemoveApproval(variables?: RemoveApprovalMutationVariables, requestHeaders?: Headers): Promise<RemoveApprovalMutation> {
      return withWrapper(() => client.request<RemoveApprovalMutation>(print(RemoveApprovalDocument), variables, requestHeaders));
    },
    PutOrganization(variables?: PutOrganizationMutationVariables, requestHeaders?: Headers): Promise<PutOrganizationMutation> {
      return withWrapper(() => client.request<PutOrganizationMutation>(print(PutOrganizationDocument), variables, requestHeaders));
    },
    RemoveOrganization(variables?: RemoveOrganizationMutationVariables, requestHeaders?: Headers): Promise<RemoveOrganizationMutation> {
      return withWrapper(() => client.request<RemoveOrganizationMutation>(print(RemoveOrganizationDocument), variables, requestHeaders));
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;