#!/bin/bash

echo "$DEBUG_MODE"
if [[ "$DEBUG_MODE" == "true" ]]; then
  echo "Networking in debug mode"
  docker network create net_$CONS_DOMAIN

  docker run -d --name=consul-debug --net=net_$CONS_DOMAIN consul:latest agent -server -bootstrap -bind='{{ GetInterfaceIP "eth0" }}' -client='0.0.0.0'
  sleep 15s

  docker exec consul-debug consul kv put orgs/$ORG_NAME

  # Join the consortial docker network
  docker network connect net_$CONS_DOMAIN $(cat /etc/hostname) || true
else
  echo "Networking in production mode"
  docker network create --attachable --driver overlay net_$CONS_DOMAIN || true

  # start in regular mode
  SELF_ID=$(docker node ls --format "{{.Self}}:{{.ID}}" | grep true | cut -c 6-)

  # Join the consortial docker network
  docker network connect net_$CONS_DOMAIN --alias=$SELF_ID $(cat /etc/hostname) || true
fi

# Create and join the organizational docker network
docker network create net_$ORG_NAME || true
docker network connect net_$ORG_NAME $(cat /etc/hostname) || true
