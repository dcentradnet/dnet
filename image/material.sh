#!/bin/bash

METHOD=$1

if [ "$METHOD" == "extend" ]; then
cat > crypto-config.yaml <<EOF
OrdererOrgs:
  - Name: Orderer
    Domain: ${CONS_DOMAIN}
    Specs:
      - Hostname: org1.orderer
      - Hostname: ${ORG_LOWER}.orderer
PeerOrgs:
  - Name: ${ORG_NAME}
    Domain: ${ORG_DOMAIN}
    EnableNodeOUs: true
    Template:
      Count: 2
    Users:
      Count: 1
EOF
else
cat > crypto-config.yaml <<EOF
OrdererOrgs:
  - Name: Orderer
    Domain: ${CONS_DOMAIN}
    Specs:
      - Hostname: ${ORG_LOWER}.orderer
PeerOrgs:
  - Name: ${ORG_NAME}
    Domain: ${ORG_DOMAIN}
    EnableNodeOUs: true
    Template:
      Count: 2
    Users:
      Count: 1
EOF
fi

# Generates Org certs using cryptogen tool
which cryptogen
if [ "$?" -ne 0 ]; then
  echo "cryptogen tool not found. exiting"
  exit 1
fi
echo
echo "##########################################################"
echo "##### Generate certificates using cryptogen tool #########"
echo "##########################################################"

#if [ -d "crypto-config/peerOrganizations" ]; then
  #rm -Rf crypto-config
  #return
  #exit 0
#fi
set -x
if [ "$METHOD" == "extend" ]; then
  cryptogen extend --config=./crypto-config.yaml --input="crypto-config"
else
  cryptogen generate --config=./crypto-config.yaml
fi
res=$?
set +x
if [ $res -ne 0 ]; then
  echo "Failed to generate certificates..."
  exit 1
fi
