#!/bin/bash

# get all CLI containers of all orgs
if [ "$DEBUG_MODE" == "true" ]; then
ORG_NAMES=$(docker exec consul-debug consul kv get -recurse orgs/ | sed 's/orgs\/\|\://g')
else
SELF_ID=$(docker node ls --format "{{.Self}}:{{.ID}}" | grep true | cut -c 6-)

ORG_IDS=$(docker node ls --format "{{.ID}}")
ORG_NAMES=$(for id in $ORG_IDS; do curl --fail -w "\n" "http://$id:8080/orgName"; done)
fi
echo $ORG_NAMES
ORG_LOWERS=$(echo $ORG_NAMES | awk '{print tolower($0)}')

# get first org and cli
first_org=$(echo $ORG_LOWERS | awk '{print $1}')
if [[ "$first_org" == "$ORG_LOWER" ]]; then
first_org=$(echo $ORG_LOWERS | awk '{print $2}')
fi
first_org_cli="cli.${first_org}"
