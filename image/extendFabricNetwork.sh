#!/bin/bash

export PATH=${PWD}/bin:${PWD}:$PATH
export FABRIC_CFG_PATH=${PWD}
export VERBOSE=false

export CRYPTO=/home/node/app/crypto-config

export CHANNEL_HOST=$HOST_PWD/channel-artifacts
export CRYPTO_HOST=$HOST_PWD/crypto-config
export CHAIN_HOST=$HOST_PWD/chain

# Obtain CONTAINER_IDS and remove them
# TODO Might want to make this optional - could clear other containers
function clearContainers () {
  CONTAINER_IDS=$(docker ps -aq)
  if [ -z "$CONTAINER_IDS" -o "$CONTAINER_IDS" == " " ]; then
    echo "---- No containers available for deletion ----"
  else
    docker rm -f $CONTAINER_IDS
  fi
}

# Delete any images that were generated as a part of this setup
# specifically the following images are often left behind:
# TODO list generated image naming patterns
function removeUnwantedImages() {
  DOCKER_IMAGE_IDS=$(docker images|awk '($1 ~ /dev-peer.*.mycc.*/) {print $3}')
  if [ -z "$DOCKER_IMAGE_IDS" -o "$DOCKER_IMAGE_IDS" == " " ]; then
    echo "---- No images available for deletion ----"
  else
    docker rmi -f $DOCKER_IMAGE_IDS
  fi
}

# Generate the needed certificates, the genesis block and start the network.
function networkUp () {
  # generate artifacts if they don't exist
  #if [ ! -d "org-artifacts/crypto-config" ]; then
  #if [ ! -d "crypto-config" ]; then
    #mkdir org-artifacts
    #generateCryptoConfig outsourced to material.sh / generateCerts
    generateCerts
    generateConfigtx
    generateChannelArtifacts
    #createConfigTx
    generateComposeFile
  #fi

  # join org into channels
  bash scripts/step1org3.sh byfn-sys-channel $ORG_MSP $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  bash scripts/step1org3.sh $CHANNEL_NAME $ORG_MSP $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE

  # join orderer to consenters
  bash scripts/step1org3orderer.sh byfn-sys-channel $ORG_MSP consenter $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  bash scripts/step1org3orderer.sh $CHANNEL_NAME $ORG_MSP consenter $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  
  # start oderer to sync blocks
  if [ "$CONSENSUS_TYPE" == "kafka" ]; then
    IMAGE_TAG=$IMAGETAG CONS_DOMAIN=$CONS_DOMAIN ORG_NAME=$ORG_NAME ORG_MSP=$ORG_MSP NEW_ORDERER_DOMAIN=$NEW_ORDERER_DOMAIN PEER0_DOMAIN=$PEER0_DOMAIN PEER1_DOMAIN=$PEER1_DOMAIN docker-compose -f $COMPOSE_FILE_KAFKA -f $COMPOSE_FILE_COUCH_ORG3 up -d 2>&1
  elif  [ "$CONSENSUS_TYPE" == "etcdraft" ]; then
    IMAGE_TAG=$IMAGETAG CONS_DOMAIN=$CONS_DOMAIN ORG_NAME=$ORG_NAME ORG_MSP=$ORG_MSP NEW_ORDERER_DOMAIN=$NEW_ORDERER_DOMAIN PEER0_DOMAIN=$PEER0_DOMAIN PEER1_DOMAIN=$PEER1_DOMAIN docker-compose -f $COMPOSE_FILE_RAFT2 -f $COMPOSE_FILE_COUCH_ORG3 up -d 2>&1
  fi

  # wait for sync
  sleep 2m

  # join orderer to addresses
  #bash scripts/step1org3orderer.sh byfn-sys-channel $ORG_MSP address $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  #bash scripts/step1org3orderer.sh $CHANNEL_NAME $ORG_MSP address $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE

  # start other containers
  if [ "${IF_COUCHDB}" == "couchdb" ]; then
    IMAGE_TAG=$IMAGETAG CONS_DOMAIN=$CONS_DOMAIN ORG_NAME=$ORG_NAME ORG_MSP=$ORG_MSP NEW_ORDERER_DOMAIN=$NEW_ORDERER_DOMAIN PEER0_DOMAIN=$PEER0_DOMAIN PEER1_DOMAIN=$PEER1_DOMAIN docker-compose -f $COMPOSE_FILE_ORG3 -f $COMPOSE_FILE_COUCH_ORG3 up -d 2>&1
  else
    IMAGE_TAG=$IMAGETAG CONS_DOMAIN=$CONS_DOMAIN ORG_NAME=$ORG_NAME ORG_MSP=$ORG_MSP NEW_ORDERER_DOMAIN=$NEW_ORDERER_DOMAIN PEER0_DOMAIN=$PEER0_DOMAIN PEER1_DOMAIN=$PEER1_DOMAIN docker-compose -f $COMPOSE_FILE_ORG3 up -d 2>&1
  fi
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Unable to start Org3 network"
    exit 1
  fi

  #createConfigTx

  echo
  echo "###############################################################"
  echo "############### Have Org3 peers join network ##################"
  echo "###############################################################"
  ./scripts/step2org3.sh $CHANNEL_NAME $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Unable to have Org3 peers join network"
    exit 1
  fi
  echo
  echo "###############################################################"
  echo "##### Upgrade chaincode to have Org3 peers on the network #####"
  echo "###############################################################"
  ./scripts/step3org3.sh $CHANNEL_NAME $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Unable to add Org3 peers on network"
    exit 1
  fi
}

# Tear down running network
function networkDown () {
  docker-compose -f $COMPOSE_FILE -f $COMPOSE_FILE_KAFKA -f $COMPOSE_FILE_ORG3 -f $COMPOSE_FILE_COUCH down --volumes --remove-orphans
  # Don't remove containers, images, etc if restarting
  if [ "$MODE" != "restart" ]; then
    #Cleanup the chaincode containers
    clearContainers
    #Cleanup images
    removeUnwantedImages
    # remove orderer block and other channel configuration transactions and certs
    rm -rf channel-artifacts/*.block channel-artifacts/*.tx crypto-config $CRYPTO_HOST/ channel-artifacts/org3.json
    # remove the docker-compose yaml file that was customized to the example
    rm -f $COMPOSE_FILE
  fi
}

function generateConfigtx() {
cat > ./configtx.yaml <<EOF
Organizations:

  - &OrdererOrg
    Name: OrdererOrg
    ID: OrdererMSP
    MSPDir: crypto-config/ordererOrganizations/${CONS_DOMAIN}/msp

    Policies:
        Readers:
            Type: Signature
            Rule: "OR('OrdererMSP.member')"
        Writers:
            Type: Signature
            Rule: "OR('OrdererMSP.member')"
        Admins:
            Type: Signature
            Rule: "OR('OrdererMSP.admin')"

  - &${ORG_NAME}
    Name: ${ORG_MSP}
    ID: ${ORG_MSP}
    MSPDir: crypto-config/peerOrganizations/${ORG_DOMAIN}/msp

    Policies:
        Readers:
            Type: Signature
            Rule: "OR('${ORG_MSP}.admin', '${ORG_MSP}.peer', '${ORG_MSP}.client')"
        Writers:
            Type: Signature
            Rule: "OR('${ORG_MSP}.admin', '${ORG_MSP}.client')"
        Admins:
            Type: Signature
            Rule: "OR('${ORG_MSP}.admin')"
      
    AnchorPeers:
      - Host: ${PEER0_DOMAIN}
        Port: 7051

Capabilities:
    Channel: &ChannelCapabilities
        V1_3: true

    Orderer: &OrdererCapabilities
        V1_1: true

    Application: &ApplicationCapabilities
        V1_3: true
        V1_2: false
        V1_1: false

Application: &ApplicationDefaults
    Organizations:

    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"

    Capabilities:
        <<: *ApplicationCapabilities

Orderer: &OrdererDefaults

    OrdererType: solo
    Addresses:
        - ${NEW_ORDERER_DOMAIN}:7050

    # Batch Timeout: The amount of time to wait before creating a batch
    BatchTimeout: 2s
    # Batch Size: Controls the number of messages batched into a block
    BatchSize:
        # Max Message Count: The maximum number of messages to permit in a batch
        MaxMessageCount: 10
        # Absolute Max Bytes: The absolute maximum number of bytes allowed for
        # the serialized messages in a batch.
        AbsoluteMaxBytes: 99 MB
        # Preferred Max Bytes: The preferred maximum number of bytes allowed for
        # the serialized messages in a batch. A message larger than the preferred
        # max bytes will result in a batch larger than preferred max bytes.
        PreferredMaxBytes: 512 KB

    Kafka:
        # Brokers: A list of Kafka brokers to which the orderer connects
        # NOTE: Use IP:port notation
        Brokers:
            - ${KAFKA_DOMAIN}:9092

    # Organizations is the list of orgs which are defined as participants on
    # the orderer side of the network
    Organizations:

    # Policies defines the set of policies at this level of the config tree
    # For Orderer policies, their canonical path is
    #   /Channel/Orderer/<PolicyName>
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
        # BlockValidation specifies what signatures must be included in the block
        # from the orderer for the peer to validate it.
        BlockValidation:
            Type: ImplicitMeta
            Rule: "ANY Writers"

Channel: &ChannelDefaults
    Policies:
        # Who may invoke the 'Deliver' API
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        # Who may invoke the 'Broadcast' API
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        # By default, who may modify elements at this config level
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"

    # Capabilities describes the channel level capabilities, see the
    # dedicated Capabilities section elsewhere in this file for a full
    # description
    Capabilities:
        <<: *ChannelCapabilities

Profiles:
    BaseChannel:
        Consortium: SampleConsortium
        Application:
            <<: *ApplicationDefaults
            Organizations:
                - *${ORG_NAME}
            Capabilities:
                <<: *ApplicationCapabilities

    SampleDevModeKafka:
        <<: *ChannelDefaults
        Capabilities:
            <<: *ChannelCapabilities
        Orderer:
            <<: *OrdererDefaults
            OrdererType: kafka
            Kafka:
                Brokers:
                - ${KAFKA_DOMAIN}:9092

            Organizations:
            - *OrdererOrg
            Capabilities:
                <<: *OrdererCapabilities
        Application:
            <<: *ApplicationDefaults
            Organizations:
            - <<: *OrdererOrg
        Consortiums:
            SampleConsortium:
                Organizations:
                - *${ORG_NAME}

    SampleMultiNodeEtcdRaft:
        <<: *ChannelDefaults
        Capabilities:
            <<: *ChannelCapabilities
        Orderer:
            <<: *OrdererDefaults
            OrdererType: etcdraft
            EtcdRaft:
                Consenters:
                - Host: $NEW_ORDERER_DOMAIN
                  Port: 7050
                  ClientTLSCert: $CRYPTO/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/tls/server.crt
                  ServerTLSCert: $CRYPTO/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/tls/server.crt
            Addresses:
                - $NEW_ORDERER_DOMAIN:7050

            Organizations:
            - *OrdererOrg
            Capabilities:
                <<: *OrdererCapabilities
        Application:
            <<: *ApplicationDefaults
            Organizations:
            - <<: *OrdererOrg
        Consortiums:
            SampleConsortium:
                Organizations:
                - *${ORG_NAME}
EOF

cat ./configtx.yaml
}

# Use the CLI container to create the configuration transaction needed to add
# Org3 to the network
function createConfigTx () {
  echo
  echo "###############################################################"
  echo "####### Generate and submit config tx to add Org3 #############"
  echo "###############################################################"
  bash scripts/step1org3orderer.sh byfn-sys-channel $ORG_MSP $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  bash scripts/step1org3orderer.sh $CHANNEL_NAME $ORG_MSP $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  bash scripts/step1org3.sh byfn-sys-channel $ORG_MSP $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  bash scripts/step1org3.sh $CHANNEL_NAME $ORG_MSP $CLI_DELAY $LANGUAGE $CLI_TIMEOUT $VERBOSE
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Unable to create config tx"
    exit 1
  fi
}

# Pulls existing orderer certs
function generateCerts() {
  . discovery.sh

	CRYPTO_ORDERERORG_DIR=$CRYPTO/ordererOrganizations/${CONS_DOMAIN}
	echo "$CRYPTO_ORDERERORG_DIR"
	mkdir -p $CRYPTO_ORDERERORG_DIR
	curl -v --fail -o /tmp/OrdererOrg.tar.gz http://$first_org_cli:8080/getOrdererOrgDir
  tar -xvf /tmp/OrdererOrg.tar.gz -C $CRYPTO_ORDERERORG_DIR

  bash material.sh extend
}

# Generate channel configuration transaction
function generateChannelArtifacts() {
  which configtxgen
  if [ "$?" -ne 0 ]; then
    echo "configtxgen tool not found. exiting"
    exit 1
  fi
  echo "##########################################################"
  echo "#########  Generating Org3 config material ###############"
  echo "##########################################################"
  (
   export FABRIC_CFG_PATH=$PWD
   set -x
   configtxgen -printOrg ${ORG_MSP} > ./channel-artifacts/org3.json
   res=$?
   set +x
   if [ $res -ne 0 ]; then
     echo "Failed to generate Org3 config material..."
     exit 1
   fi
  )
}

function generateComposeFile() {
cat > $COMPOSE_FILE_ORG3 <<EOF
version: '2'

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN

services:

  # WARNING also look at docker-compose-couchdb-org3.yaml
  $NEW_ORDERER_DOMAIN:
    container_name: $NEW_ORDERER_DOMAIN
    image: hyperledger/fabric-orderer:$IMAGETAG
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      # enabled TLS
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_KAFKA_TOPIC_REPLICATIONFACTOR=1
      - ORDERER_KAFKA_VERBOSE=true
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_OPERATIONS_LISTENADDRESS=0.0.0.0:9443
      - ORDERER_METRICS_PROVIDER=prometheus
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    volumes:
    #- $CHANNEL_HOST/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
    - $CHANNEL_HOST/:/var/hyperledger/orderer/
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/msp:/var/hyperledger/orderer/msp
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/tls:/var/hyperledger/orderer/tls
    - $CHAIN_HOST/$NEW_ORDERER_DOMAIN:/var/hyperledger/production/orderer
    networks:
    - $CONS_DOMAIN

  $PEER0_DOMAIN:
    container_name: $PEER0_DOMAIN
    extends:
      file: ../base/peer-base.yaml
      service: peer-base
    environment:
      - CORE_PEER_ID=$PEER0_DOMAIN
      - CORE_PEER_ADDRESS=$PEER0_DOMAIN:7051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=$PEER0_DOMAIN:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=$PEER1_DOMAIN:7051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=$PEER0_DOMAIN:7051
      - CORE_PEER_LOCALMSPID=$ORG_MSP
      - CORE_OPERATIONS_LISTENADDRESS=0.0.0.0:9443
      - CORE_METRICS_PROVIDER=prometheus
    volumes:
        - /var/run/:/host/var/run/
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER0_DOMAIN/msp:/etc/hyperledger/fabric/msp
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER0_DOMAIN/tls:/etc/hyperledger/fabric/tls
        - $CHAIN_HOST/$PEER0_DOMAIN:/var/hyperledger/production
    networks:
      - $CONS_DOMAIN

  $PEER1_DOMAIN:
    container_name: $PEER1_DOMAIN
    extends:
      file: ../base/peer-base.yaml
      service: peer-base
    environment:
      - CORE_PEER_ID=$PEER1_DOMAIN
      - CORE_PEER_ADDRESS=$PEER1_DOMAIN:7051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=$PEER1_DOMAIN:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=$PEER0_DOMAIN:7051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=$PEER1_DOMAIN:7051
      - CORE_PEER_LOCALMSPID=$ORG_MSP
      - CORE_OPERATIONS_LISTENADDRESS=0.0.0.0:9443
      - CORE_METRICS_PROVIDER=prometheus
    volumes:
        - /var/run/:/host/var/run/
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER1_DOMAIN/msp:/etc/hyperledger/fabric/msp
        - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/peers/$PEER1_DOMAIN/tls:/etc/hyperledger/fabric/tls
        - $CHAIN_HOST/$PEER1_DOMAIN:/var/hyperledger/production
    networks:
      - $CONS_DOMAIN
EOF
cat $COMPOSE_FILE_ORG3

CA_PRIVATE_KEY=$(ls /home/node/app/crypto-config/peerOrganizations/$ORG_DOMAIN/ca/ | grep _sk)
cat > $COMPOSE_FILE_COUCH_ORG3 <<EOF
version: '2'

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN
  $ORG_NAME:

services:

  $CA_DOMAIN:
    container_name: $CA_DOMAIN
    image: hyperledger/fabric-ca:1.5
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=$CA_DOMAIN
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server-config/$CA_DOMAIN-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server-config/$CA_PRIVATE_KEY
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/${CA_DOMAIN}-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/${CA_PRIVATE_KEY} -b admin:adminpw -d'
    volumes:
      - $CRYPTO_HOST/peerOrganizations/$ORG_DOMAIN/ca/:/etc/hyperledger/fabric-ca-server-config
    networks:
      - $ORG_NAME

  $NEW_ORDERER_DOMAIN:
    container_name: $NEW_ORDERER_DOMAIN
    image: hyperledger/fabric-orderer:$IMAGETAG
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      # enabled TLS
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_KAFKA_TOPIC_REPLICATIONFACTOR=1
      - ORDERER_KAFKA_VERBOSE=true
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_OPERATIONS_LISTENADDRESS=0.0.0.0:9443
      - ORDERER_METRICS_PROVIDER=prometheus
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    volumes:
    #- $CHANNEL_HOST/channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
    - $CHANNEL_HOST/:/var/hyperledger/orderer/
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/msp:/var/hyperledger/orderer/msp
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/tls/:/var/hyperledger/orderer/tls
    - $CHAIN_HOST/$NEW_ORDERER_DOMAIN:/var/hyperledger/production/orderer
    networks:
      - $CONS_DOMAIN

  couchdb.${PEER0_DOMAIN}:
    container_name: couchdb.${PEER0_DOMAIN}
    image: hyperledger/fabric-couchdb
    # Populate the COUCHDB_USER and COUCHDB_PASSWORD to set an admin user and password
    # for CouchDB.  This will prevent CouchDB from operating in an "Admin Party" mode.
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    # Comment/Uncomment the port mapping if you want to hide/expose the CouchDB service,
    # for example map it to utilize Fauxton User Interface in dev environments.
    volumes:
      - $CHAIN_HOST/couchdb.${PEER0_DOMAIN}/data:/opt/couchdb/data
    networks:
      - $ORG_NAME

  ${PEER0_DOMAIN}:
    environment:
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb.${PEER0_DOMAIN}:5984
      # The CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME and CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD
      # provide the credentials for ledger to connect to CouchDB.  The username and password must
      # match the username and password set for the associated CouchDB.
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=
    depends_on:
      - couchdb.${PEER0_DOMAIN}
    networks:
      - $CONS_DOMAIN
      - $ORG_NAME

  couchdb.${PEER1_DOMAIN}:
    container_name: couchdb.${PEER1_DOMAIN}
    image: hyperledger/fabric-couchdb
    # Populate the COUCHDB_USER and COUCHDB_PASSWORD to set an admin user and password
    # for CouchDB.  This will prevent CouchDB from operating in an "Admin Party" mode.
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    # Comment/Uncomment the port mapping if you want to hide/expose the CouchDB service,
    # for example map it to utilize Fauxton User Interface in dev environments.
    volumes:
      - $CHAIN_HOST/couchdb.${PEER1_DOMAIN}/data:/opt/couchdb/data
    networks:
      - $ORG_NAME

  ${PEER1_DOMAIN}:
    environment:
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb.${PEER1_DOMAIN}:5984
      # The CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME and CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD
      # provide the credentials for ledger to connect to CouchDB.  The username and password must
      # match the username and password set for the associated CouchDB.
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=
    depends_on:
      - couchdb.${PEER1_DOMAIN}
    networks:
      - $CONS_DOMAIN
      - $ORG_NAME
EOF
cat $COMPOSE_FILE_COUCH_ORG3

cat > $COMPOSE_FILE_KAFKA_ORG3 <<EOF
version: '2'

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN

services:
  $NEW_ZOOKEEPER_DOMAIN:
    container_name: $NEW_ZOOKEEPER_DOMAIN
    image: hyperledger/fabric-zookeeper
    environment:
      ZOOKEEPER_CLIENT_PORT: 32181
      ZOOKEEPER_TICK_TIME: 2000
    networks:
    - $CONS_DOMAIN

  $NEW_KAFKA_DOMAIN:
    container_name: $NEW_KAFKA_DOMAIN
    image: hyperledger/fabric-kafka
    depends_on:
    - $NEW_ZOOKEEPER_DOMAIN
    environment:
      - KAFKA_BROKER_ID=1
      - KAFKA_ZOOKEEPER_CONNECT=$NEW_ZOOKEEPER_DOMAIN:2181
      - KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://$NEW_KAFKA_DOMAIN:9092
      - KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1
      - KAFKA_MESSAGE_MAX_BYTES=1048576 # 1 * 1024 * 1024 B
      - KAFKA_REPLICA_FETCH_MAX_BYTES=1048576 # 1 * 1024 * 1024 B
      - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false
      - KAFKA_LOG_RETENTION_MS=-1
      - KAFKA_MIN_INSYNC_REPLICAS=1
      - KAFKA_DEFAULT_REPLICATION_FACTOR=1
    networks:
    - $CONS_DOMAIN
EOF
cat $COMPOSE_FILE_KAFKA_ORG3

cat > $COMPOSE_FILE_RAFT2 <<EOF
version: '2'

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN

services:
  $NEW_ORDERER_DOMAIN:
    extends:
      file: ../base/peer-base.yaml
      service: orderer-base
    container_name: $NEW_ORDERER_DOMAIN
    networks:
    - $CONS_DOMAIN
    volumes:
    - $CHANNEL_HOST/:/var/hyperledger/orderer
    #- $CHANNEL_HOST/orderer.genesis.block:/var/hyperledger/orderer/orderer.genesis.block
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/msp/:/var/hyperledger/orderer/msp
    - $CRYPTO_HOST/ordererOrganizations/$CONS_DOMAIN/orderers/$NEW_ORDERER_DOMAIN/tls/:/var/hyperledger/orderer/tls
    - $CHAIN_HOST/$NEW_ORDERER_DOMAIN/:/var/hyperledger/production/orderer
EOF

cat $COMPOSE_FILE_RAFT2
}

# Obtain the OS and Architecture string that will be used to select the correct
# native binaries for your platform
OS_ARCH=$(echo "$(uname -s|tr '[:upper:]' '[:lower:]'|sed 's/mingw64_nt.*/windows/')-$(uname -m | sed 's/x86_64/amd64/g')" | awk '{print tolower($0)}')
# timeout duration - the duration the CLI should wait for a response from
# another container before giving up
CLI_TIMEOUT=10
#default for delay
CLI_DELAY=10
# channel name defaults to "mychannel"
CHANNEL_NAME="mychannel"
# use this as the default docker-compose yaml definition
COMPOSE_FILE=network/docker-compose-cli.yaml
#
COMPOSE_FILE_COUCH=network/docker-compose-couch.yaml
# use this as the default docker-compose yaml definition
COMPOSE_FILE_ORG3=network/docker-compose-org3.yaml
#
COMPOSE_FILE_COUCH_ORG3=network/docker-compose-couch-org3.yaml
# kafka and zookeeper compose file
COMPOSE_FILE_KAFKA=network/docker-compose-kafka.yaml
COMPOSE_FILE_KAFKA_ORG3=network/docker-compose-kafka-org3.yaml
# two additional etcd/raft orderers
COMPOSE_FILE_RAFT2=network/docker-compose-etcdraft2.yaml
# use golang as the default language for chaincode
LANGUAGE=golang
# default image tag
IMAGETAG="2.2"

# Parse commandline args
if [ "$1" = "-m" ];then	# supports old usage, muscle memory is powerful!
    shift
fi
MODE=$1;shift
# Determine whether starting, stopping, restarting or generating for announce
if [ "$MODE" == "up" ]; then
  EXPMODE="Starting"
elif [ "$MODE" == "down" ]; then
  EXPMODE="Stopping"
elif [ "$MODE" == "restart" ]; then
  EXPMODE="Restarting"
elif [ "$MODE" == "generate" ]; then
  EXPMODE="Generating certs and genesis block for"
else
  printHelp
  exit 1
fi
while getopts "h?c:t:d:f:s:l:i:x:y:v" opt; do
  case "$opt" in
    h|\?)
      printHelp
      exit 0
    ;;
    c)  CHANNEL_NAME=$OPTARG
    ;;
    t)  CLI_TIMEOUT=$OPTARG
    ;;
    d)  CLI_DELAY=$OPTARG
    ;;
    f)  COMPOSE_FILE=$OPTARG
    ;;
    s)  IF_COUCHDB=$OPTARG
    ;;
    l)  LANGUAGE=$OPTARG
    ;;
    i)  IMAGETAG=$OPTARG
    ;;
    o)  CONSENSUS_TYPE=$OPTARG
    ;;
    x)  CONS_DOMAIN=$OPTARG
    ;;
    y)  ORG_NAME=$OPTARG
    ;;
    v)  VERBOSE=true
    ;;
  esac
done

export CONS_DOMAIN=$CONS_DOMAIN

export ORG_NAME=$ORG_NAME
export ORG_LOWER=$(echo "$ORG_NAME" | awk '{print tolower($0)}')
export ORG_DOMAIN=${ORG_LOWER}.${CONS_DOMAIN}
export ORG_MSP=${ORG_NAME}MSP

export ORDERER_DOMAIN=org1.orderer.${CONS_DOMAIN}
export PEER0_DOMAIN=peer0.${ORG_DOMAIN}
export PEER1_DOMAIN=peer1.${ORG_DOMAIN}

export CA_DOMAIN=ca.${ORG_DOMAIN}
export CLI_DOMAIN=cli.${ORG_LOWER}

export NEW_ORDERER_DOMAIN=${ORG_LOWER}.orderer.${CONS_DOMAIN}
export NEW_ZOOKEEPER_DOMAIN=${ORG_LOWER}.zookeeper.${CONS_DOMAIN}
export NEW_KAFKA_DOMAIN=${ORG_LOWER}.kafka.${CONS_DOMAIN}

echo $CONS_DOMAIN
echo $ORG_NAME
echo $ORG_DOMAIN
echo $ORG_MSP
echo $PEER0_DOMAIN
echo $PEER1_DOMAIN

# Announce what was requested
  if [ "${IF_COUCHDB}" == "couchdb" ]; then
        echo
        echo "${EXPMODE} with channel '${CHANNEL_NAME}' and CLI timeout of '${CLI_TIMEOUT}' seconds and CLI delay of '${CLI_DELAY}' seconds and using database '${IF_COUCHDB}'"
  else
        echo "${EXPMODE} with channel '${CHANNEL_NAME}' and CLI timeout of '${CLI_TIMEOUT}' seconds and CLI delay of '${CLI_DELAY}' seconds"
  fi

#Create the network using docker compose
if [ "${MODE}" == "up" ]; then
  networkUp
elif [ "${MODE}" == "down" ]; then ## Clear the network
  networkDown
elif [ "${MODE}" == "generate" ]; then ## Generate Artifacts
  #generateCerts
  generateChannelArtifacts
  createConfigTx
elif [ "${MODE}" == "restart" ]; then ## Restart the network
  networkDown
  networkUp
else
  exit 1
fi
