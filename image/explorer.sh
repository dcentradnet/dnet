#!/bin/bash
EXPLORERDB_DOMAIN=explorerdb.$ORG_DOMAIN
EXPLORER_DOMAIN=explorer.$ORG_DOMAIN
EXPLORER_VERSION=0.3.9.3

mkdir -p explorer/db/
cp explorer_db/* explorer/db/

cat > docker-compose-explorer.yaml <<EOF
version: '2.1'

#volumes:
#  pgdata:

networks:
  $CONS_DOMAIN:
    external:
      name: net_$CONS_DOMAIN
  $ORG_NAME:
    external:
      name: net_$ORG_NAME

services:

  $EXPLORERDB_DOMAIN:
    image: hyperledger/explorer-db:$EXPLORER_VERSION
    container_name: $EXPLORERDB_DOMAIN
    hostname: $EXPLORERDB_DOMAIN
    environment:
      - DATABASE_DATABASE=fabricexplorer
      - DATABASE_USERNAME=hppoc
      - DATABASE_PASSWORD=password
    volumes:
      #- $HOST_PWD/explorer/db/createdb.sh:/docker-entrypoint-initdb.d/createdb.sh
      - $HOST_PWD/explorer/db:/docker-entrypoint-initdb.d
      - $HOST_PWD/explorer/pgdata:/var/lib/postgresql/data
    networks:
      - $ORG_NAME

  $EXPLORER_DOMAIN:
    image: hyperledger/explorer:$EXPLORER_VERSION
    container_name: $EXPLORER_DOMAIN
    hostname: $EXPLORER_DOMAIN
    environment:
      - DATABASE_HOST=$EXPLORERDB_DOMAIN
      - DATABASE_USERNAME=hppoc
      - DATABASE_PASSWD=password
      - ENROLL_ID=hlbeuser
      - DISCOVERY_AS_LOCALHOST=false
    volumes:
      #- $HOST_PWD/config/config.json:/opt/explorer/app/platform/fabric/config.json
      #- $HOST_PWD/config:/opt/explorer/app/platform/fabric
      - $HOST_PWD/config:/config
      - $HOST_PWD/crypto-config:/tmp/crypto
      - $HOST_PWD/wallet:/opt/tmp
    command: sh -c "cp /config/config.json /config/connection-profile.json /opt/explorer/app/platform/fabric/ && sleep 60 && node /opt/explorer/main.js && tail -f /dev/null"
    depends_on:
      - $EXPLORERDB_DOMAIN
    ports:
      - 8084:8080
    networks:
      - $CONS_DOMAIN
      - $ORG_NAME
EOF
cat docker-compose-explorer.yaml

docker-compose -f docker-compose-explorer.yaml up -d
