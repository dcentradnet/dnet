#!/bin/bash

echo
echo "========= Finish adding Org to network ========= "
echo
CHANNEL_NAME="$1"
DELAY="$2"
LANGUAGE="$3"
TIMEOUT="$4"
VERBOSE="$5"
: ${CHANNEL_NAME:="mychannel"}
: ${DELAY:="3"}
: ${LANGUAGE:="golang"}
: ${TIMEOUT:="10"}
: ${VERBOSE:="false"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=5

CC_SRC_PATH="/chaincode/"

# import utils
. scripts/utils.sh

echo "===================== Get org certs ===================== "
for org_lower in ${ORG_LOWERS}; do
	if [[ "$org_lower" == "$ORG_LOWER" ]]; then
		continue
	fi

	org_domain="${org_lower}.${CONS_DOMAIN}"
	cli_domain="cli.${org_lower}"

	org_name=$org_lower
	orderer_domain="${org_name}.orderer.${CONS_DOMAIN}"

	echo $org_name
	echo $orderer_domain

	CRYPTO_ORDERER_DIR=$CRYPTO/ordererOrganizations/${CONS_DOMAIN}/orderers/${orderer_domain}
	CRYPTO_ORDERERTLSCA_DIR=$CRYPTO_ORDERER_DIR/msp/tlscacerts
	echo "$CRYPTO_ORDERERTLSCA_DIR"
	mkdir -p $CRYPTO_ORDERERTLSCA_DIR
	curl -v --fail -o $CRYPTO_ORDERERTLSCA_DIR/tlsca.${CONS_DOMAIN}-cert.pem http://$cli_domain:8080/getOrdererRootCert
	
	CRYPTO_ORDERERTLS_DIR=$CRYPTO_ORDERER_DIR/tls
	echo "$CRYPTO_ORDERERTLS_DIR"
	mkdir -p $CRYPTO_ORDERERTLS_DIR
	curl -v --fail -o $CRYPTO_ORDERERTLS_DIR/ca.crt http://$cli_domain:8080/getOrdererRootCert
	curl -v --fail -o $CRYPTO_ORDERERTLS_DIR/server.crt http://$cli_domain:8080/getOrdererCert

	peer0_domain="peer0.${org_name}.${CONS_DOMAIN}"
	CRYPTO_TLS_DIR=$CRYPTO/peerOrganizations/${org_domain}/peers/${peer0_domain}/tls
	echo "$CRYPTO_TLS_DIR"
	mkdir -p $CRYPTO_TLS_DIR
	curl -v --fail -o $CRYPTO_TLS_DIR/ca.crt http://$cli_domain:8080/getPeerRootCert?num=0
	curl -v --fail -o $CRYPTO_TLS_DIR/server.crt http://$cli_domain:8080/getPeerCert?num=0

	peer1_domain="peer1.${org_name}.${CONS_DOMAIN}"
	CRYPTO_TLS_DIR=$CRYPTO/peerOrganizations/${org_domain}/peers/${peer1_domain}/tls
	echo "$CRYPTO_TLS_DIR"
	mkdir -p $CRYPTO_TLS_DIR
	curl -v --fail -o $CRYPTO_TLS_DIR/ca.crt http://$cli_domain:8080/getPeerRootCert?num=1
	curl -v --fail -o $CRYPTO_TLS_DIR/server.crt http://$cli_domain:8080/getPeerCert?num=1
done

echo
echo "========= Finished adding Org to your first network! ========= "
echo

docker logs $NEW_ORDERER_DOMAIN

exit 0
