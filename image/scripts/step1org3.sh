#!/bin/bash

CHANNEL_NAME="$1"
NEW_ORG_MSP="$2"
DELAY="$3"
LANGUAGE="$4"
TIMEOUT="$5"
VERBOSE="$6"
: ${CHANNEL_NAME:="mychannel"}
: ${DELAY:="3"}
: ${LANGUAGE:="golang"}
: ${TIMEOUT:="10"}
: ${VERBOSE:="false"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=5

CC_SRC_PATH="/chaincode/"

# import utils
. scripts/utils.sh

echo
echo "========= Creating config transaction to add org to network =========== "
echo

# Fetch the config for the channel, writing it to config.json
echo "Fetching the most recent configuration block for the channel"
#fetchChannelConfig ${CHANNEL_NAME} config.json
set -x
if [ "byfn-sys-channel" == "${CHANNEL_NAME}" ]; then
	curl -v --fail -o config_block.pb http://$first_org_cli:8080/fetchChannelConfigAsOrderer?channelName=byfn-sys-channel
else
	curl -v --fail -o config_block.pb http://$first_org_cli:8080/fetchChannelConfig?channelName=$CHANNEL_NAME
fi
set +x

echo "Decoding config block to JSON and isolating config to config.json"
set -x
configtxlator proto_decode --input config_block.pb --type common.Block | jq .data.data[0].payload.data.config >config.json
set +x

# Modify the two configurations to append the new org and its orderer
set -x
jq -s ".[0] * {\"channel_group\":{\"groups\":{\"Application\":{\"groups\": {\"${NEW_ORG_MSP}\":.[1]}}}}}" config.json ./channel-artifacts/org3.json > modified_config.json
#jq -s ".[0] * {\"channel_group\":{\"groups\":{\"Consortiums\":{\"groups\": {\"SampleConsortium\": {\"groups\": {\"${NEW_ORG_MSP}\":.[1]}}}}}}}" config.json ./channel-artifacts/org3.json > modified_config.json
set +x

echo "config.json"
cat config.json
echo "./channel-artifacts/org.json"
cat ./channel-artifacts/org.json
echo "modified_config.json"
cat modified_config.json

# Compute a config update, based on the differences between config.json and modified_config.json, write it as a transaction to org_update_in_envelope.pb
createConfigUpdate ${CHANNEL_NAME} config.json modified_config.json org_update_in_envelope.pb

echo
echo "========= Config transaction to add org to network created ===== "
echo

# let config update sign by all orgs except first and own through signConfigtx route
for org_lower in $ORG_LOWERS; do
	if [[ "$org_lower" == "$ORG_LOWER" ]]; then
		continue
	fi
	echo $org_lower

	cli="cli.${org_lower}"
	curl -v --fail -X POST --header "Content-Type:application/octet-stream" --data-binary @org_update_in_envelope.pb -o org_update_in_envelope.pb http://$cli:8080/signConfigtx
done

echo
echo "========= Update channel using first org ========="
echo
set -x
if [ "byfn-sys-channel" == "${CHANNEL_NAME}" ]; then
	curl -v --fail -X POST --header "Content-Type:application/octet-stream" --data-binary @org_update_in_envelope.pb http://$first_org_cli:8080/updateChannelAsOrderer
else
	curl -v --fail -X POST --header "Content-Type:application/octet-stream" --data-binary @org_update_in_envelope.pb http://$first_org_cli:8080/updateChannel
fi
set +x

echo
echo "========= Config transaction to add org to network submitted! =========== "
echo

exit 0
