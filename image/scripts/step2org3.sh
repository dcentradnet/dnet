#!/bin/bash

echo
echo "========= Getting Org onto network ========= "
echo
CHANNEL_NAME="$1"
DELAY="$2"
LANGUAGE="$3"
TIMEOUT="$4"
VERBOSE="$5"
: ${CHANNEL_NAME:="mychannel"}
: ${DELAY:="3"}
: ${LANGUAGE:="golang"}
: ${TIMEOUT:="10"}
: ${VERBOSE:="false"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=10

CC_SRC_PATH="/chaincode/"

# import utils
. scripts/utils.sh

echo "Fetching channel config block from orderer..."
set -x
curl -v --fail -o $CHANNEL_NAME.block http://$first_org_cli:8080/fetchChannelConfigBlock
res=$?
set +x
cat log.txt
verifyResult $res "Fetching config block from orderer has Failed"

joinChannelWithRetry 0 3
echo "===================== peer0.org joined channel '$CHANNEL_NAME' ===================== "
joinChannelWithRetry 1 3
echo "===================== peer1.org joined channel '$CHANNEL_NAME' ===================== "

echo
echo "========= Org is now halfway onto your first network ========= "
echo

exit 0
