#FIXME separate between new orderer and prev orderer
export CRYPTO=/home/node/app/crypto-config

export GOPATH=/opt/gopath
export DOCKER_HOST=unix:///var/run/docker.sock
export CORE_VM_ENDPOINT=unix:///var/run/docker.sock
#export FABRIC_LOGGING_SPEC=DEBUG
#export FABRIC_LOGGING_SPEC=INFO
export CORE_PEER_ID=$CLI_DOMAIN
export CORE_PEER_ADDRESS=$PEER0_DOMAIN:7051
export CORE_PEER_LOCALMSPID=$ORG_MSP
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_CERT_FILE=$CRYPTO/peerOrganizations/$ORG_DOMAIN/peers/$PEER0_DOMAIN/tls/server.crt
export CORE_PEER_TLS_KEY_FILE=$CRYPTO/peerOrganizations/$ORG_DOMAIN/peers/$PEER0_DOMAIN/tls/server.key
export CORE_PEER_TLS_ROOTCERT_FILE=$CRYPTO/peerOrganizations/$ORG_DOMAIN/peers/$PEER0_DOMAIN/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=$CRYPTO/peerOrganizations/$ORG_DOMAIN/users/Admin@$ORG_DOMAIN/msp
export ORDERER_CA=$CRYPTO/ordererOrganizations/${CONS_DOMAIN}/orderers/${ORDERER_DOMAIN}/msp/tlscacerts/tlsca.${CONS_DOMAIN}-cert.pem

# discover other orgs and choose first org
. discovery.sh

# verify the result of the end-to-end test
verifyResult() {
  if [ $1 -ne 0 ]; then
    echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
    echo "========= ERROR !!! FAILED to execute End-2-End Scenario ==========="
    echo
    exit 1
  fi
}

# Set OrdererOrg.Admin globals
setOrdererGlobals() {
  CORE_PEER_LOCALMSPID="OrdererMSP"
  CORE_PEER_TLS_ROOTCERT_FILE=$CRYPTO/ordererOrganizations/${CONS_DOMAIN}/orderers/${ORDERER_DOMAIN}/msp/tlscacerts/tlsca.${CONS_DOMAIN}-cert.pem
  CORE_PEER_MSPCONFIGPATH=$CRYPTO/ordererOrganizations/${CONS_DOMAIN}/users/Admin@${CONS_DOMAIN}/msp
}

setGlobals() {
  PEER=$1
  ORG=$2

  CORE_PEER_LOCALMSPID=${ORG_MSP}
  CORE_PEER_TLS_ROOTCERT_FILE=$CRYPTO/peerOrganizations/${ORG_DOMAIN}/peers/${PEER0_DOMAIN}/tls/ca.crt
  CORE_PEER_MSPCONFIGPATH=$CRYPTO/peerOrganizations/${ORG_DOMAIN}/users/Admin@${ORG_DOMAIN}/msp
  if [ $PEER -eq 0 ]; then
    CORE_PEER_ADDRESS=${PEER0_DOMAIN}:7051
  else
    CORE_PEER_ADDRESS=${PEER1_DOMAIN}:7051
  fi

  if [ "$VERBOSE" == "true" ]; then
    env | grep CORE
  fi
}

updateAnchorPeers() {
  PEER=$1
  ORG=$2
  setGlobals $PEER $ORG

  if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
    set -x
    peer channel update -o ${ORDERER_DOMAIN}:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx >&log.txt
    res=$?
    set +x
  else
    set -x
    peer channel update -o ${ORDERER_DOMAIN}:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
    res=$?
    set +x
  fi
  cat log.txt
  verifyResult $res "Anchor peer update failed"
  echo "===================== Anchor peers updated for org '$CORE_PEER_LOCALMSPID' on channel '$CHANNEL_NAME' ===================== "
  sleep $DELAY
  echo
}

## Sometimes Join takes time hence RETRY at least 5 times
joinChannelWithRetry() {
  PEER=$1
  ORG=$2
  setGlobals $PEER $ORG

  set -x
  peer channel join -b $CHANNEL_NAME.block >&log.txt
  res=$?
  set +x
  if [ $res -ne 0 -a $COUNTER -lt $MAX_RETRY ]; then
    COUNTER=$(expr $COUNTER + 1)
    echo "peer${PEER}.org${ORG} failed to join the channel, Retry after $DELAY seconds"
    sleep $DELAY
    joinChannelWithRetry $PEER $ORG
  else
    COUNTER=1
  fi
  verifyResult $res "After $MAX_RETRY attempts, peer${PEER}.org${ORG} has failed to join channel '$CHANNEL_NAME' "
}

function generatePrivateCollectionsConfig() {
cat > /home/node/app/private_collections.json <<EOF
[
 {
    "name": "privateCollection",
    "policy": "OR('${ORG_MSP}.member')",
    "requiredPeerCount": 0,
    "maxPeerCount": 2,
    "blockToLive": 1000000,
    "memberOnlyRead": true
 }
]
EOF

cat /home/node/app/private_collections.json
}

# signConfigtxAsPeerOrg <org> <configtx.pb>
# Set the peerOrg admin of an org and signing the config update
signConfigtxAsPeerOrg() {
  PEERORG=$1
  TX=$2
  setGlobals 0 $PEERORG
  set -x
  peer channel signconfigtx -f "${TX}"
  set +x
}

# createConfigUpdate <channel_id> <original_config.json> <modified_config.json> <output.pb>
# Takes an original and modified config, and produces the config update tx
# which transitions between the two
createConfigUpdate() {
  CHANNEL=$1
  ORIGINAL=$2
  MODIFIED=$3
  OUTPUT=$4

  set -x
  configtxlator proto_encode --input "${ORIGINAL}" --type common.Config >original_config.pb
  configtxlator proto_encode --input "${MODIFIED}" --type common.Config >modified_config.pb
  configtxlator compute_update --channel_id "${CHANNEL}" --original original_config.pb --updated modified_config.pb >config_update.pb
  configtxlator proto_decode --input config_update.pb --type common.ConfigUpdate >config_update.json
  echo '{"payload":{"header":{"channel_header":{"channel_id":"'$CHANNEL'", "type":2}},"data":{"config_update":'$(cat config_update.json)'}}}' | jq . >config_update_in_envelope.json
  configtxlator proto_encode --input config_update_in_envelope.json --type common.Envelope >"${OUTPUT}"
  set +x
}

# parsePeerConnectionParameters $@
# Helper function that takes the parameters from a chaincode operation
# (e.g. invoke, query, instantiate) and checks for an even number of
# peers and associated org, then sets $PEER_CONN_PARMS and $PEERS
parsePeerConnectionParameters() {
  PEER_CONTAINERS=$(docker network inspect net_${CONS_DOMAIN} | grep -v dev | grep peer | sed 's/\(\"Name\"\: \"\|\",\)//g;s/|/\n/g' )
  echo $PEER_CONTAINERS

  PEER_CONN_PARMS=""
  for peer_domain in ${PEER_CONTAINERS}; do
    PEER_CONN_PARMS="$PEER_CONN_PARMS --peerAddresses ${peer_domain}:7051"

    peer_org_domain=$(echo $peer_domain | sed 's/^peer.\.\(.*\)$/\1/')
    CORE_PEER_TLS_ROOTCERT_FILE=$CRYPTO/peerOrganizations/${peer_org_domain}/peers/${peer_domain}/tls/ca.crt
    TLSINFO="--tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE"
    PEER_CONN_PARMS="$PEER_CONN_PARMS $TLSINFO"
  done
}
