#!/bin/bash

CHANNEL_NAME="$1"
NEW_ORG_MSP="$2"
OP_TYPE="$3"
DELAY="$4"
LANGUAGE="$5"
TIMEOUT="$6"
VERBOSE="$7"
: ${CHANNEL_NAME:="mychannel"}
: ${DELAY:="3"}
: ${LANGUAGE:="golang"}
: ${TIMEOUT:="10"}
: ${VERBOSE:="false"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=5

CC_SRC_PATH="/chaincode/"

# import utils
. scripts/utils.sh

echo
echo "========= Creating config transaction to add org to network =========== "
echo

# Fetch the config for the channel, writing it to config.json
echo "Fetching the most recent configuration block for the channel"
set -x
curl -v --fail -o config_block_orderer.pb http://$first_org_cli:8080/fetchChannelConfigAsOrderer?channelName=$CHANNEL_NAME
set +x

echo "Decoding config block to JSON and isolating config to config.json"
set -x
configtxlator proto_decode --input config_block_orderer.pb --type common.Block | jq .data.data[0].payload.data.config >orderer_config.json
set +x

# Modify the two configurations to append the new org and its orderer
set -x
if [ "consenter" == "${OP_TYPE}" ]; then
    curl -v --fail -X POST -d @orderer_config.json -H "Content-Type: application/json" -o modified_orderer_config.json http://localhost:8080/addOrdererConsenterToConfig
elif [ "address" == "${OP_TYPE}" ]; then
    curl -v --fail -X POST -d @orderer_config.json -H "Content-Type: application/json" -o modified_orderer_config.json http://localhost:8080/addOrdererAddressToConfig
fi
set +x

echo "orderer_config.json"
cat orderer_config.json
echo "modified_orderer_config.json"
cat modified_orderer_config.json

# Compute a config update, based on the differences between config.json and modified_config.json, write it as a transaction to org_update_in_envelope.pb
createConfigUpdate ${CHANNEL_NAME} orderer_config.json modified_orderer_config.json orderer_update_in_envelope.pb

echo
echo "========= Config transaction to add orderer to network created ===== "
echo

echo
echo "========= Update channel using first orderer ========="
echo
set -x
curl -v --fail -X POST --header "Content-Type:application/octet-stream" --data-binary @orderer_update_in_envelope.pb http://$first_org_cli:8080/updateChannelAsOrderer
set +x

echo
echo "========= Config transaction to add orderer to network submitted! =========== "
echo

sleep 30

# see https://stackoverflow.com/a/57625802
set -x
if [ "byfn-sys-channel" == "${CHANNEL_NAME}" ] && [ "consenter" == "${OP_TYPE}" ]; then
    curl -v --fail -o /home/node/app/channel-artifacts/orderer.genesis.block http://$first_org_cli:8080/fetchChannelConfigAsOrderer

    echo "debug_config.json"
    configtxlator proto_decode --input /home/node/app/channel-artifacts/orderer.genesis.block --type common.Block | jq .data.data[0].payload.data.config | echo
fi
set +x

exit 0
