import { browser, by, element, ExpectedConditions } from 'protractor';

describe('new App', () => {
  let firstLobbyTab, secondLobbyTab;

  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000;
    browser.manage().timeouts().implicitlyWait(1000);
  });
  afterAll(async () => {
    await browser.switchTo().window(firstLobbyTab);
    await clickButton('Terminate instance');

    await browser.switchTo().window(secondLobbyTab);
    await clickButton('Terminate instance');
  });

  it('dNetLobby', async () => {
    // const lobbyDomain = 'https://dnetlobby.now.sh';
    const lobbyDomain = 'http://localhost:8091';
    const tenMinutes = 10 * 60 * 1000;

    await browser.get(lobbyDomain);
    firstLobbyTab = await browser.getWindowHandle();
    await setFormInputValue('orgName', 'Org1');

    secondLobbyTab = await openNewTab(lobbyDomain);
    await setFormInputValue('orgName', 'Org2');
    await clickSubmit();
    /*await browser.wait(ExpectedConditions.presenceOf(element(by.cssContainingText('ion-button', 'Visit your instance'))),
                       tenMinutes, 'Element taking too long to appear in the DOM');*/
    /*await browser.wait(ExpectedConditions.presenceOf(element(by.cssContainingText('ion-button', 'Terminate instance'))),
                       tenMinutes, 'Element taking too long to appear in the DOM');*/
    // await browser.sleep(5 * 60 * 1000);
    await clickButton('Visit your instance');
    // await clickButton('Terminate instance');

    // await browser.sleep(tenMinutes);
  });
});

async function openNewTab(url: string): Promise<string> {
  await browser.executeScript('window.open()');
  const newTab = await browser.getAllWindowHandles().then(handles => handles[handles.length - 1]);

  await browser.switchTo().window(newTab);
  await browser.get(url);

  return newTab;
}

async function setFormInputValue(formControlName: string, value: string) {
  const orgNameElement = element(by.css(`ion-input[formcontrolname="${formControlName}"]`)).element(by.css('input'));
  // await orgNameElement.clear();
  // await orgNameElement.sendKeys(value);
}

async function clickSubmit() {
  return element(by.css('[type="submit"]')).click();
}

async function clickButton(buttonText: string) {
  return element(by.cssContainingText('ion-button', buttonText)).click();
}
