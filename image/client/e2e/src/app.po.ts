import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(destination: string): Promise<any> {
    return browser.get(destination);
  }

  async getTitle(): Promise<string> {
    return browser.getTitle();
  }

  async getPageTitleText(tagName: string): Promise<string> {
    return element(by.tagName(tagName)).element(by.deepCss('ion-title')).getText();
  }
}
