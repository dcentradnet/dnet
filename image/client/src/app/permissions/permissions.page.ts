import { remove } from 'lodash';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ListPermissionsGQL, ListPermissionsQuery, Permission, PutPermissionGQL, RemovePermissionGQL } from 'src/generated/queries';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.page.html',
  styleUrls: ['./permissions.page.scss'],
})
export class PermissionsPage implements OnInit {
  public permissions: Observable<ListPermissionsQuery['permissions']>;

  constructor(
    public route: ActivatedRoute,
    public navCtrl: NavController,
    public alertController: AlertController,
    public listGQL: ListPermissionsGQL,
    public putGQL: PutPermissionGQL,
    public removeGQL: RemovePermissionGQL,
  ) { }

  ngOnInit(): void {
    this.permissions = this.route.params.pipe(
      switchMap(() => this.listGQL.fetch()),
      map(res => res.data.permissions)
    );
  }

  async createPermission(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Create permission',
      inputs: [
        {
          name: 'name',
          type: 'text',
          id: 'name',
          // value: '',
          placeholder: 'gov-request'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();

    const {data} = await alert.onDidDismiss();
    if (!data) {
      return;
    }

    const name = data.values.name;
    const result = await this.putGQL.mutate({name}).toPromise();
    //const _id = result.data.putGQL._id;
    //this.permissions.push({_id, name});

    this.navCtrl.navigateForward(`permissions/${result.data.putPermission._id}`);
  }

  async removePermission(perm: Permission): Promise<void> {
    await this.removeGQL.mutate(perm).toPromise();
    remove(this.permissions, (p: Permission) => p._id === perm._id);
  }

}
