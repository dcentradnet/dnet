import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContainerDiagramComponent } from '../container-diagram/container-diagram.component';
import { ContainersPage } from './containers.page';
// import { ContainersPageRoutingModule } from './tabs-routing.module';

const routes: Routes = [
  {
    path: '',
    component: ContainersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    // ContainersPageRoutingModule
  ],
  declarations: [ContainersPage, ContainerDiagramComponent],
  entryComponents: [ContainerDiagramComponent]
})
export class ContainersPageModule {}
