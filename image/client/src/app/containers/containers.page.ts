import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ContainersPageGQL, ContainersPageQuery } from 'src/generated/queries';

@Component({
  selector: 'app-containers',
  templateUrl: './containers.page.html',
  styleUrls: ['./containers.page.scss'],
})
export class ContainersPage implements OnInit {
  public containers: Observable<ContainersPageQuery['containers']>;

  constructor(public gql: ContainersPageGQL) { }

  ngOnInit(): void {
    this.containers = this.gql.watch().valueChanges.pipe(map(res => res.data.containers));
  }
}
