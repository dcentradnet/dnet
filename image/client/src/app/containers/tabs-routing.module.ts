import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContainersPage } from './containers.page';

const routes: Routes = [
  {
    path: 'containers',
    component: ContainersPage,
    children:
      [
        {
          path: 'diagram',
          children:
            [
              {
                path: '',
                loadChildren: () => import('./diagram/diagram.module').then(m => m.DiagramPageModule)
              }
            ]
        },
        {
          path: 'list',
          children:
            [
              {
                path: '',
                loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
              }
            ]
        },
        {
          path: '',
          redirectTo: '/diagram',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: '/containers/diagram',
    pathMatch: 'full'
  }
];

@NgModule({
  imports:
    [
      RouterModule.forChild(routes)
    ],
  exports:
    [
      RouterModule
    ]
})
export class ContainersPageRoutingModule {}