import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PickerController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Permission, PutPermissionGQL, QueryPermissionGQL } from 'src/generated/queries';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.page.html',
  styleUrls: ['./permission.page.scss'],
})
export class PermissionPage implements OnInit {
  subscription: Subscription;
  perm: Permission;

  constructor(
    public route: ActivatedRoute,
    public pickerController: PickerController,
    public queryPermGQL: QueryPermissionGQL,
    public putPermGQL: PutPermissionGQL
  ) { }

  ngOnInit(): void {
    this.subscription = this.route.params.pipe(switchMap(
      p => this.queryPermGQL.watch({_id: p.id}).valueChanges
    )).subscribe(d => this.perm = d.data.permissions[0]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  async save(): Promise<void> {
    await this.putPermGQL.mutate(this.perm).toPromise();
  }
}
