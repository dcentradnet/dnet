import { Subject } from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';

import { HttpService } from '../http.service';
import { LogStreamComponent } from '../log-stream/log-stream.component';
import { OrganizationsService } from '../organizations.service';
import { StatusService } from '../status.service';
import { tap } from 'rxjs/operators';
import { Info } from 'src/generated/graphqlTypes';

@Component({
  selector: 'app-node',
  templateUrl: './node.page.html',
  styleUrls: ['./node.page.scss'],
})
export class NodePage implements OnInit, OnDestroy {

  info: Info;
  @ViewChild(LogStreamComponent, { static: true }) logStream: LogStreamComponent;
  swarm: any;

  constructor(public http: HttpService,
              public alertController: AlertController,
              public statusService: StatusService,
              public organizations: OrganizationsService) {
  }

  async ngOnInit(): Promise<void> {
    this.info = await this.http.get<Info>('/api/info').toPromise();
  }

  ngOnDestroy(): void {
    // this.logStream.ngOnDestroy();
  }

  async initNetwork(): Promise<void> {
    if (!this.swarm) {
      return this.alertNoSwarm();
    }

    const alert = await this.alertController.create({
      header: 'Input consortium details',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();

    const alertData = await alert.onDidDismiss();
    if ('cancel' === alertData.role) {
      return;
    }

    const consName = alertData.data.values.name;
    console.log(consName);

    //await this.http.post<any>('/api/initNetwork', {consDomain: consName}).pipe(tap(() => this.statusService.update())).toPromise();
  }

  private async alertNoSwarm() {
    const alert = await this.alertController.create({
      header: 'Not part of a Docker swarm network',
      message: 'Your node is not part of a Docker swarm yet. Please create or join a swarm before creating or joining a Fabric network.',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
    await alert.onDidDismiss();
  }

  async shutdownNetwork() {
    if (!(await this.askConfirmation('Would you like to shutdown your node?'))) {
      return;
    }

    //await this.http.post<any>('/api/shutdownNetwork', {}).pipe(tap(() => this.statusService.update())).toPromise();
  }

  async joinNetwork(): Promise<void> {
    if (!this.swarm) {
      return this.alertNoSwarm();
    }

    if (!(await this.askConfirmation('Would you like to join the consortium?'))) {
      return;
    }

    //await this.http.post<any>('/api/extendNetwork', {}).pipe(tap(() => this.statusService.update())).toPromise();
  }

  private async askConfirmation(message: string): Promise<boolean> {
    const alert = await this.alertController.create({
      header: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();

    const alertData = await alert.onDidDismiss();
    return 'cancel' !== alertData.role;
  }
}
