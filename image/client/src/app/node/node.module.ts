import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LogStreamModule } from '../log-stream/log-stream.module';
import { NodePage } from './node.page';

const routes: Routes = [
  {
    path: '',
    component: NodePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    LogStreamModule
  ],
  declarations: [NodePage]
})
export class NodePageModule {}
