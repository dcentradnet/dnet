import 'lodash.combinations';
import _ from 'lodash';
import { Subscription } from 'rxjs';
import * as ColorHash from 'color-hash';
import { Component, OnInit } from '@angular/core';
import { Network } from 'vis';

import { StatusService } from '../status.service';
import { OrganizationsService } from '../organizations.service';
import { OrganizationsPageGQL, OrganizationsPageQuery } from 'src/generated/queries';

const LENGTH_MAIN = 350,
LENGTH_SERVER = 150,
LENGTH_SUB = 50,
WIDTH_SCALE = 2,
GREEN = 'green',
RED = '#C5000B',
ORANGE = 'orange',
// GRAY = '#666666',
GRAY = 'gray',
BLACK = '#2B1B17';

const colorHash = new ColorHash();

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.page.html',
  styleUrls: ['./organizations.page.scss'],
})
export class OrganizationsPage implements OnInit {

  subscription: Subscription;
  orgs: OrganizationsPageQuery['organizations'] = [];

  nodes = null;
  edges = null;
  network = null;

  constructor(
    public status: StatusService,
    public orgService: OrganizationsService,
    public gql: OrganizationsPageGQL) { }

  async ngOnInit(): Promise<void> {
    this.subscription = this.gql.watch().valueChanges.subscribe(({data}) => {
      this.orgs = data.organizations;
      this.draw();
    });
  }

  ngDestroy(): void {
    this.subscription.unsubscribe();
  }

  // Called when the Visualization API is loaded.
  draw(): void {
    this.nodes = [];
    this.edges = [];

    const orgNodes = [];
    for (const org of this.orgs) {
      const nodeColor = colorHash.hex(org);

      // org node
      const orgNodeIndex = org.publicKey;
      this.nodes.push({
        id: orgNodeIndex,
        label: org.name,
        group: 'org',
        value: 1,
        color: nodeColor,
        // font: {color: invert(nodeColor)}
        font: {color: 'white'}
      });
      orgNodes.push(orgNodeIndex);

      // users
      const usersNodeIndex = this.nodes.length;
      this.nodes.push({
        id: usersNodeIndex,
        label: `users.${org.name}`,
        group: 'users',
        value: 1,
        color: nodeColor,
        font: {color: 'white'}
      });
      this.edges.push({
        from: orgNodeIndex,
        to: usersNodeIndex,
        length: LENGTH_SUB,
        color: GRAY,
        fontColor: GRAY,
        width: WIDTH_SCALE,
      });

      // dApps
      const dAppsNodeIndex = this.nodes.length;
      this.nodes.push({
        id: dAppsNodeIndex,
        label: `dapps.${org.name}`,
        group: 'users',
        value: 1,
        color: nodeColor,
        font: {color: 'white'}
      });
      this.edges.push({
        from: orgNodeIndex,
        to: dAppsNodeIndex,
        length: LENGTH_SUB,
        color: GRAY,
        fontColor: GRAY,
        width: WIDTH_SCALE,
      });
    }

    _.combinations(orgNodes, 2).forEach(([a, b]) => this.edges.push({
        from: a,
        to: b,
        label: 'Blockchain',
        length: LENGTH_SUB,
        color: GRAY,
        fontColor: GRAY,
        width: WIDTH_SCALE,
      })
    );

    // TODO implement "chosen" function
    // TODO make orgs clickable for info overlay
    /*
    // TODO place nodes in circle
    .map((node, index, arr) => {
      const angle = 2 * Math.PI * (index / arr.length + 0.75);
      node.x = 400 * Math.cos(angle);
      node.y = 400 * Math.sin(angle);
      if (index % 2 === 0) {
        node.value = index + 1;
      }
      return node;
    });
    */

    // create a network
    const container = document.getElementById('network');
    const data = {
      nodes: this.nodes,
      edges: this.edges,
    };
    const options = {
      nodes: {
        scaling: {
          label: {
            min: 20,
            max: 20
          }
        },
      },
      edges: {
        color: GRAY,
        smooth: false,
      },
      physics: {
        barnesHut: { gravitationalConstant: -30000 },
        stabilization: { iterations: 2500 },
      },
      groups: {
        org: {
          shape: 'circle'
        }
      },
    };
    this.network = new Network(container, data, options);

    // on click show organization overlay
    this.network.on( 'click', (properties) => {
      const ids = properties.nodes;
      if (ids && typeof ids[0] === 'string' && ids[0].startsWith('-----BEGIN PUBLIC KEY-----')) {
        console.log(ids[0]);
        this.orgService.presentOrgPopover(ids[0]);
      }
    });
  }
}
