import { Observable, concat } from 'rxjs';
import { map, publish, shareReplay } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { Info } from 'src/generated/graphqlTypes';
import { StatusServiceGQL, StatusUpdateGQL } from 'src/generated/queries';

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  public infoObservable: Observable<Info>;

  constructor(public gql: StatusServiceGQL, public statusUpdateGQL: StatusUpdateGQL) {
    this.infoObservable = concat(
      this.gql.fetch().pipe(map(res => res.data.info)),
      this.statusUpdateGQL.subscribe().pipe(map(res => res.data.statusUpdate)),
    );//.pipe(shareReplay(1));
  }
}
