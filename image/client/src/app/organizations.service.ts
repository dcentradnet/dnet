import { map } from 'rxjs/operators';
import Bluebird from 'bluebird';
import { Injectable } from '@angular/core';
import { PopoverController } from '@ionic/angular';

import { HttpService } from './http.service';
import { OrganizationsPageGQL } from 'src/generated/queries';
import { OrganizationComponent } from './organization/organization.component';

@Injectable({
  providedIn: 'root'
})
export class OrganizationsService {

  constructor(public http: HttpService,
              public popoverCtrl: PopoverController,
              public gql: OrganizationsPageGQL) { }

  async getClis(): Promise<string[]> {
    return Bluebird.map(this.getOrgs(), org => `cli.${org.toLowerCase()}`);
  }

  async getOrgs(): Promise<string[]> {
    return this.gql.fetch().pipe(map(res => res.data.organizations.map(o => o.name))).toPromise();
  }

  async presentOrgPopover(pub: string, editable = false): Promise<void> {
    const popover = await this.popoverCtrl.create({
      component: OrganizationComponent,
      translucent: true,
      componentProps: {pub, editable}
    });
    await popover.present();

    await popover.onDidDismiss();
  }
}
