import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public ready: Promise<void>;
  private token: string;

  constructor(
    private storage: StorageService,
    private http: HttpClient
  ) {
    this.ready = this.init();
  }

  private async init(): Promise<void> {
    if (!this.token) {
      this.token = await this.storage.get('token');
    }

    if (!this.token) {
      await this.login('Administrator', 'admin');
    }
  }

  async login(username: string, password: string): Promise<boolean> {
    this.token = await this.http.post<{token: string}>('/api/login', {username, password}).toPromise().then(res => res.token);

    await this.storage.set('token', this.token);

    return !!this.token;
  }

  getToken(): string {
    return this.token;
  }
}
