import { Subscription, Observable } from 'rxjs';
import { Terminal } from 'xterm';
import { DisplayOption } from './display-option';

export interface NgTerminal {
    /**
     * getter only provided
     * observable connected to inputs which user typed on the div of terminal
     */
    readonly keyInput: Observable<string>;
    /**
     * getter only provided
     * return the core object of Terminal in xterm
     */
    readonly underlying: Terminal;
    /**
     * write charactors to terminal directly
     * @param chars charactors to write
     */
    write(chars: string);
    /**
     * change row, col, draggable
     */
    setDisplayOption(opt: DisplayOption): void;
}
