import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

import { LogStreamModule } from '../log-stream/log-stream.module';
import { LogStreamModalComponent } from './log-stream-modal.component';

@NgModule({
     declarations: [
       LogStreamModalComponent
     ],
     imports: [
       IonicModule,
       CommonModule,
       LogStreamModule
     ],
     entryComponents: [
       LogStreamModalComponent
     ]
})
export class LogStreamModalModule {}
