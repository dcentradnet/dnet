import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-log-stream-modal',
  templateUrl: './log-stream-modal.component.html',
  styleUrls: ['./log-stream-modal.component.scss'],
})
export class LogStreamModalComponent implements OnInit {

  @Input() public eventSource = '/api/log';

  constructor(public modalCtrl: ModalController) { }

  ngOnInit(): void {}

  close(): void {
    this.modalCtrl.dismiss();
  }

}
