import { Component, OnInit } from '@angular/core';

import { PluginsService } from '../plugins.service';
import { ModalController } from '@ionic/angular';

import { PluginSettingsPage } from './plugin-settings/plugin-settings.page';
import { Plugin } from 'src/generated/graphqlTypes';
import { PluginsPageGQL } from 'src/generated/queries';

@Component({
  selector: 'app-plugins',
  templateUrl: './plugins.page.html',
  styleUrls: ['./plugins.page.scss'],
})
export class PluginsPage implements OnInit {
  public installable: Plugin[];
  public installed: Plugin[];

  constructor(public modalController: ModalController,
              public pluginsService: PluginsService,
              public gql: PluginsPageGQL) { }

  async ngOnInit(): Promise<void> {
    /*this.installable = await this.pluginsService.getInstallable();
    this.installed = await this.pluginsService.getInstalled();*/

    this.gql.watch().valueChanges.subscribe(({data}) => {
      this.installed = data.plugins.installed;
      this.installable = data.plugins.installable;
    });
  }

  async install(plugin: Plugin): Promise<void> {
    const args = await this.showSettings(plugin);
    return this.pluginsService.installPlugin(Object.assign({}, plugin, {args}));
  }

  async showSettings(plugin: Plugin): Promise<{[key: string]: (string | number | boolean)}> {
    const modal = await this.modalController.create({
      component: PluginSettingsPage,
      componentProps: {plugin}
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    return data;
  }

}
