import { mapValues } from 'lodash';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Plugin } from '../../../generated/graphqlTypes';
import { DynamicFormBuilderComponent } from 'src/app/dynamic-form-builder/dynamic-form-builder.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-plugin-settings',
  templateUrl: './plugin-settings.page.html',
  styleUrls: ['./plugin-settings.page.scss'],
})
export class PluginSettingsPage implements OnInit {
  plugin: Plugin;

  @ViewChild('ngForm') form: DynamicFormBuilderComponent;

  constructor(public modalCtrl: ModalController,
              public navParams: NavParams,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.plugin = this.navParams.get('plugin');
  }

  submit(): void {
    const value = this.form.form.getRawValue();
    this.modalCtrl.dismiss(value);
  }

  cancel(): void {
    this.modalCtrl.dismiss();
  }

}
