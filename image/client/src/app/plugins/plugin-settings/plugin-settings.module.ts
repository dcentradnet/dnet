import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DynamicFormBuilderModule } from '../../dynamic-form-builder/dynamic-form-builder.module';
import { PluginSettingsPage } from './plugin-settings.page';

const routes: Routes = [
  {
    path: '',
    component: PluginSettingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DynamicFormBuilderModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PluginSettingsPage]
})
export class PluginSettingsPageModule {}
