import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PluginsPage } from './plugins.page';
import { PluginSettingsPageModule } from './plugin-settings/plugin-settings.module';
import { PluginSettingsPage } from './plugin-settings/plugin-settings.page';

const routes: Routes = [
  {
    path: '',
    component: PluginsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    // PluginSettingsPageModule
  ],
  declarations: [PluginsPage],
  // entryComponents: [PluginSettingsPage]
})
export class PluginsPageModule {}
