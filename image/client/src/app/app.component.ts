import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { StatusService } from './status.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Lobby',
      url: '/lobby',
      icon: 'reorder-three-outline',
      mode: 'lobby'
    },
    {
      title: 'Store',
      url: '/store',
      icon: 'storefront-outline',
      mode: 'lobby'
    },
    {
      title: 'Requests',
      url: '/requests',
      icon: 'git-pull-request'
    },
    {
      title: 'dApps',
      url: '/d-apps',
      icon: 'apps'
    },
    {
      title: 'Actions',
      url: '/actions',
      icon: 'play'
    },
    {
      title: 'Users',
      url: '/users',
      icon: 'person-outline'
    },
    {
      title: 'Roles',
      url: '/roles',
      icon: 'ribbon-outline'
    },
    {
      title: 'Permissions',
      url: '/permissions',
      icon: 'key'
    },
    {
      title: 'Node',
      url: '/node',
      icon: 'desktop',
      mode: 'manual'
    },
    {
      title: 'Swarm',
      url: '/swarm',
      icon: 'bonfire',
      mode: 'manual'
    },
    {
      title: 'Organizations',
      url: '/organizations',
      icon: 'people',
      mode: 'manual'
    },
    {
      title: 'Containers',
      url: '/containers',
      icon: 'pulse',
      mode: 'manual'
    },
    {
      title: 'Channels',
      url: '/channels',
      icon: 'boat',
      mode: 'manual'
    },
    {
      title: 'Chaincodes',
      url: '/chaincodes',
      icon: 'code-working',
      mode: 'manual'
    },
    {
      title: 'Plugins',
      url: '/plugins',
      icon: 'add',
      mode: 'manual'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public statusService: StatusService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openExplorer() {
    window.open(`${window.location.protocol}//${window.location.hostname}:8084`);
  }
}
