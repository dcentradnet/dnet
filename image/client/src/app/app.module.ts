import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/timeout';

// import { NgModule } from '@angular/core';
import { Injectable, InjectionToken, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { NgTerminalModule } from './ng-terminal/ng-terminal.module';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage-angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { StatusComponent } from './status/status.component';
import { PluginSettingsPage } from './plugins/plugin-settings/plugin-settings.page';
import { PluginSettingsPageModule } from './plugins/plugin-settings/plugin-settings.module';
import { GraphQLModule } from './graphql.module';
import { NotificationsComponent } from './notifications/notifications.component';
import { PopoverComponent } from './notifications/popover/popover.component';
import { OrganizationComponent } from './organization/organization.component';

// see https://stackoverflow.com/a/45986060
const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');
const defaultTimeout = 10 * 60 * 1000;
@Injectable()
export class TimeoutInterceptor implements HttpInterceptor {
  protected defaultTimeout = defaultTimeout;
  // constructor(@Inject(DEFAULT_TIMEOUT) protected defaultTimeout) {} // FIXME causes problems in production

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const timeout = Number(req.headers.get('timeout')) || this.defaultTimeout;

    const reqCustom = req.clone({
      headers: req.headers.delete('timeout')
    });

    return next.handle(reqCustom).timeout(timeout);
  }
}

@NgModule({
  declarations: [AppComponent, StatusComponent, NotificationsComponent, PopoverComponent, OrganizationComponent],
  entryComponents: [StatusComponent, NotificationsComponent, PopoverComponent, OrganizationComponent, PluginSettingsPage],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    GraphQLModule,
    NgTerminalModule,
    PluginSettingsPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    [{ provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true }],
    [{ provide: DEFAULT_TIMEOUT, useValue: defaultTimeout }]
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
