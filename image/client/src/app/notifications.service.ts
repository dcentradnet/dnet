import { concat, defer, Observable } from 'rxjs';
import { map, scan, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
//import { LocalNotifications } from '@ionic-native/local-notifications';
//import { Badge } from '@ionic-native/badge/ngx';

import { Notification } from 'src/generated/graphqlTypes';
import { NotificationGQL, ReplayNotificationsGQL } from 'src/generated/queries';

export function doOnSubscribe<T>(onSubscribe: () => void): (source: Observable<T>) =>  Observable<T> {
  return function inner(source: Observable<T>): Observable<T> {
      return defer(() => {
        onSubscribe();

        return source;
      });
  };
}

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  public single: Observable<Notification>;
  public collected: Observable<Notification[]>;

  constructor(
      public replayNotificationsGQL: ReplayNotificationsGQL,
      public notificationGQL: NotificationGQL,
      //public nativeNotifications: LocalNotifications,
      //public badge: Badge
    ) {
    this.single = concat(
      this.replayNotificationsGQL.fetch().pipe(switchMap(res => res.data.replayNotifications)),
      this.notificationGQL.subscribe().pipe(map(res => res.data.notification))
    );
    this.collected = this.single.pipe(scan((a, c) => [...a, c], []));

    /*this.single.subscribe({next: notification =>
      this.nativeNotifications.schedule({
        id: 0,
        text: notification.message
      })
    });*/

    //this.collected.subscribe({next: notifications => this.badge.set(notifications.length)});
  }
}
