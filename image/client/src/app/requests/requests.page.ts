//import ObjectId from 'bson-objectid';
import {QueryRef} from 'apollo-angular';
import { sha3_512 } from 'js-sha3';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';


import { HttpService } from '../http.service';
import { Request, VoteCore } from 'src/generated/graphqlTypes';
import { RequestBroadcastGQL, RequestsPageGQL, RequestsPageQuery, VoteGQL } from 'src/generated/queries';
import { OrganizationsService } from '../organizations.service';

async function loadFile(f: File): Promise<ArrayBuffer> {
  return new Promise<ArrayBuffer>((resolve, reject) => {
    const reader = new FileReader();

    // Closure to capture the file information.
    reader.onload   = (e: any) => resolve(e.target.result);
    reader.onerror  = (      ) => reject();

    // Read in the image file as a data URL.
    reader.readAsArrayBuffer(f);
  });
}

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage {
  private watch: QueryRef<RequestsPageQuery>;
  public requests: Observable<RequestsPageQuery['gov']['requests']>;
  hash: string;

  constructor(public alertController: AlertController,
              public http: HttpService,
              public orgService: OrganizationsService,
              public gql: RequestsPageGQL,
              public voteGQL: VoteGQL,
              public requestBroadcastGQL: RequestBroadcastGQL) {
    this.watch = this.gql.watch();
    this.requests = this.watch.valueChanges.pipe(map(res => res.data.gov.requests));
  }

  async vote(event, request: Request): Promise<void> {
    console.log(event);

    const vote: VoteCore = {
        'rid': request._id,
        'value': parseInt(event.detail.value, 10)
    };
    await this.voteGQL.mutate({vote}).toPromise();
    await this.watch.refetch();
  }

  async sendChaincodeInstallRequest(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Enter chaincode info',
      inputs: [
        {
          name: 'name',
          type: 'text',
          value: 'mycc'
        },
        {
          name: 'version',
          type: 'text',
          value: '2.0'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
    const alertData = await alert.onDidDismiss();
    const result = alertData.data.values;

    const request = {
      //_id: new ObjectId().toString(),
      _id: '0', // FIXME
      type: 'chaincode_install',
      key: result.name,
      subkey: `${result.version}_${this.hash}`
    };
    await this.requestBroadcastGQL.mutate({request}).toPromise();
  }

  async hashFile(file: File): Promise<void> {
    const buffer = await loadFile(file);
    this.hash = sha3_512(buffer);
  }

  async fileChanged($event: any): Promise<void> {
    await this.hashFile($event.target.files[0]);
  }

}
