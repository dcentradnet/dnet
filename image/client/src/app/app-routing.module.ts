import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  { path: 'swarm', loadChildren: () => import('./swarm/swarm.module').then(m => m.SwarmPageModule) },
  { path: 'requests', loadChildren: () => import('./requests/requests.module').then(m => m.RequestsPageModule) },
  { path: 'containers', loadChildren: () => import('./containers/containers.module').then(m => m.ContainersPageModule) },
  { path: 'chaincodes', loadChildren: () => import('./chaincodes/chaincodes.module').then(m => m.ChaincodesPageModule) },
  { path: 'organizations', loadChildren: () => import('./organizations/organizations.module').then(m => m.OrganizationsPageModule) },
  { path: 'lobby', loadChildren: () => import('./lobby/lobby.module').then(m => m.LobbyPageModule) },
  { path: 'channels', loadChildren: () => import('./channels/channels.module').then(m => m.ChannelsPageModule) },
  { path: 'node', loadChildren: () => import('./node/node.module').then(m => m.NodePageModule) },
  { path: 'store', loadChildren: () => import('./store/store.module').then(m => m.StorePageModule) },
  { path: 'd-apps', loadChildren: () => import('./d-apps/d-apps.module').then(m => m.DAppsPageModule) },
  { path: 'actions', loadChildren: () => import('./actions/actions.module').then(m => m.ActionsPageModule) },
  { path: 'plugins', loadChildren: () => import('./plugins/plugins.module').then(m => m.PluginsPageModule) },
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then( m => m.UsersPageModule)
  },
  {
    path: 'roles',
    loadChildren: () => import('./roles/roles.module').then( m => m.RolesPageModule)
  },
  {
    path: 'roles/:id',
    loadChildren: () => import('./role/role.module').then( m => m.RolePageModule)
  },
  {
    path: 'users/:id',
    loadChildren: () => import('./user/user.module').then( m => m.UserPageModule)
  },
  {
    path: 'permissions',
    loadChildren: () => import('./permissions/permissions.module').then( m => m.PermissionsPageModule)
  },
  {
    path: 'permissions/:id',
    loadChildren: () => import('./permission/permission.module').then( m => m.PermissionPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'edit-organization',
    loadChildren: () => import('./edit-organization/edit-organization.module').then( m => m.EditOrganizationPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
