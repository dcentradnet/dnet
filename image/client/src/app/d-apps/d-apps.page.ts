import { isEmpty, pick } from 'lodash';
import { Component, OnInit } from '@angular/core';

import { AlertController } from '@ionic/angular';
import { StatusService } from '../status.service';
import { DappsPageGQL, DappsPageQuery, InstallDappGQL } from 'src/generated/queries';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DappManualInstallInput } from 'src/generated/graphqlTypes';

@Component({
  selector: 'app-d-apps',
  templateUrl: './d-apps.page.html',
  styleUrls: ['./d-apps.page.scss'],
})
export class DAppsPage implements OnInit {
  public dapps: Observable<DappsPageQuery['dapps']['installed']>;

  constructor(public status: StatusService,
              public alertController: AlertController,
              public gql: DappsPageGQL,
              public installDappGQL: InstallDappGQL) { }

  ngOnInit(): void {
    this.dapps = this.gql.watch().valueChanges.pipe(map(res => res.data.dapps.installed));
  }

  async install(): Promise<void> {
    const isAdminMode = await this.status.infoObservable.toPromise().then(res => res.isAdminMode);

    // show request
    const alertRequest = await this.alertController.create({
      header: isAdminMode ? 'Install dApp' : 'Install dApp manually',
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: 'dMsg'
        }, {
          name: 'imageName',
          type: 'text',
          placeholder: 'dcentra/dmsg:master'
        }, {
          name: 'serveraddress',
          type: 'url',
          placeholder: 'registry.gitlab.com'
        }, {
          name: 'username',
          type: 'text',
          placeholder: 'gitlab-ci-token'
        }, {
          name: 'password',
          type: 'text',
          placeholder: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alertRequest.present();
    const alertData = await alertRequest.onDidDismiss();

    // \"options\":{\"authconfig\":{\"username\":\"gitlab-ci-token\", \"password\":\"$CI_JOB_TOKEN\", \"serveraddress\":\"$CI_REGISTRY\"}}
    const authconfig = pick(alertData.data.values, 'serveraddress', 'username', 'password');

    const dapp: DappManualInstallInput = {
      title: alertData.data.values.title,
      version: 'manual',
      dockerImage: alertData.data.values.imageName,
      ...(isEmpty(authconfig) && {options: {authconfig}})
    };
    await this.installDappGQL.mutate({dapp});
  }

}
