import { remove } from 'lodash';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { User } from 'src/generated/graphqlTypes';
import { ListUsersGQL, ListUsersQuery, PutUserGQL, RemoveUserGQL } from 'src/generated/queries';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  public users: Observable<ListUsersQuery['users']>;

  constructor(
    public route: ActivatedRoute,
    public navCtrl: NavController,
    public alertController: AlertController,
    public listGQL: ListUsersGQL,
    public putGQL: PutUserGQL,
    public removeGQL: RemoveUserGQL,
  ) { }

  ngOnInit(): void {
    this.users = this.route.params.pipe(
      switchMap(() => this.listGQL.fetch()),
      map(res => res.data.users)
    );
  }

  async createUser(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Create user',
      inputs: [
        {
          name: 'name',
          type: 'text',
          id: 'name',
          placeholder: 'JohnDoe'
        },
        {
          name: 'password',
          type: 'password',
          id: 'password',
          placeholder: 'Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();

    const {data} = await alert.onDidDismiss();
    if (!data) {
      return;
    }

    const name = data.values.name;
    const password = data.values.password;
    const result = await this.putGQL.mutate({name, password}).toPromise();
    //const _id = result.data.putUser._id;
    //this.users.push({_id, name});

    this.navCtrl.navigateForward(`users/${result.data.putUser._id}`);
  }

  async removeUser(user: User): Promise<void> {
    await this.removeGQL.mutate({_id: user._id}).toPromise();
    remove(this.users, (u: User) => u._id === user._id);
  }

}
