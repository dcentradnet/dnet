import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { ConsortiumOrganizationInput } from 'src/generated/graphqlTypes';
import { LobbyOrganizationPutGQL, OrganizationComponentGQL, OrganizationComponentQuery } from 'src/generated/queries';
import { LobbyService } from '../lobby.service';

@Component({
  selector: 'app-edit-organization',
  templateUrl: './edit-organization.page.html',
  styleUrls: ['./edit-organization.page.scss'],
})
export class EditOrganizationPage implements OnInit {
  public formGroup: FormGroup;
  public orgAvatarImageFile: File = null;

  constructor(private lobbyService: LobbyService,
              private formBuilder: FormBuilder,
              public gql: OrganizationComponentGQL,
              public putOrganizationGQL: LobbyOrganizationPutGQL
  ) { }

  async ngOnInit(): Promise<void> {
    const org = await this.gql.fetch().pipe(map(res => res.data?.lobby?.organization)).toPromise();
    this.formGroup = this.formBuilder.group({
       name: new FormControl(org?.name || '', Validators.pattern('^[a-zA-Z0-9_.+-]+$')),
       email: new FormControl(org?.email || '', Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')),
       about: [org?.about || ''],
    });
  }

  handleFileInput(files: FileList): void {
      this.orgAvatarImageFile = files.item(0);
  }

  async onSubmit(info: ConsortiumOrganizationInput): Promise<void> {
   // upload avatar image
   info.imageId = await this.lobbyService.putImage(this.orgAvatarImageFile);
   // upload info
   await this.putOrganizationGQL.mutate({info}).toPromise();
 }
}
