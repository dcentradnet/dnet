/* eslint-disable prefer-spread */
import { Observable, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthenticationService } from './authentication.service';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  public ready: Promise<void>;
  private options: any;

  constructor(public http: HttpClient,
              public auth: AuthenticationService) {
    this.ready = this.init();
  }

  private async init() {
    await this.auth.ready;

    this.options = {
      headers: {
        Authorization: `JWT ${this.auth.getToken()}`
      }
    };
  }

  get<T>(...args: any[]): Observable<T> {
    args.push(this.options);
    return from(this.ready).pipe(switchMap(() => this.http.get.apply(this.http, args) as Observable<T>));
  }

  post<T>(...args: any[]): Observable<T> {
    args.push(this.options);
    return from(this.ready).pipe(switchMap(() => this.http.post.apply(this.http, args) as Observable<T>));
  }
}
