import { v4 as uuidv4 } from 'uuid';

import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { StatusService } from '../status.service';
import { AuthenticationService } from '../authentication.service';
import { OrganizationsService } from '../organizations.service';
import { Dapp, RequestCore } from 'src/generated/graphqlTypes';
import { RequestBroadcastGQL, StorePageGQL, StorePageQuery } from 'src/generated/queries';

@Component({
  selector: 'app-store',
  templateUrl: './store.page.html',
  styleUrls: ['./store.page.scss'],
})
export class StorePage implements OnInit {

  /*dapps = [
      {
        img: 'https://www.oneadvanced.com/globalassets/images/awards-and-accreditations/smaller.png',
        title: 'Enterprise Resource Planning',
        short: 'helps your consortium to keep track of its inventory',
        initialPrice: 1000,
        name: 'nginx',
        image: 'nginx'
      }, {
        img: 'https://techcrunch.com/wp-content/uploads/2018/12/getty-messaging.jpg',
        title: 'Super Secure Auditable Messaging',
        short: 'messaging on the Blockchain',
        initialPrice: 10
      }, {
        img: 'https://dflow.tk/assets/imgs/dFlow.png',
        title: 'dFlow',
        short: 'organize your consortia through business flows',
        initialPrice: 100
      }, {
        img: 'https://images.financialexpress.com/2016/06/tax-PTI-L.jpg',
        title: 'TaxMaster',
        short: 'organize and optimize the taxes of your consortium',
        initialPrice: 20
      }, {
        img: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/05/Google_Messages_logo.svg/1200px-Google_Messages_logo.svg.png',
        title: 'dMsg',
        short: 'Blockchain-based chat',
        initialPrice: 20,
        image: 'dcentra/dmsg'
      }
    ];*/

  public dapps: Observable<StorePageQuery['dapps']['installable']>;

  constructor(public alertController: AlertController,
              public loadingController: LoadingController,
              public status: StatusService,
              public authentication: AuthenticationService,
              public organizations: OrganizationsService,
              public storeGQL: StorePageGQL,
              public requestBroadcastGQL: RequestBroadcastGQL) { }

  ngOnInit(): void {
    this.dapps = this.storeGQL.watch().valueChanges.pipe(map(res => res.data.dapps.installable));
  }

  async installRequest(dapp: Dapp): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Install dApp',
      message: `Would you like to send an install request for the decentralized application ${dapp.title}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
    const reason = await alert.onDidDismiss();
    console.log(reason);

    if ('cancel' === reason.role) {
      return;
    }

    const dAppInstallReq: RequestCore = {
      _id: uuidv4(),
      type: 'dapp_install',
      key: dapp.title,
      subkey: dapp._id
    };
    const ccInstallReq: RequestCore = {
      _id: uuidv4(),
      type: 'chaincode_install',
      key: dapp.title,
      subkey: dapp.version || '1.0',
      parent: dAppInstallReq._id
    };
    const ccInstantiateReq: RequestCore = {
      _id: uuidv4(),
      type: 'chaincode_instantiate',
      key: dapp.title,
      subkey: dapp.version || '1.0',
      parent: dAppInstallReq._id
    };

    const dAppInstallReqPromise = this.requestBroadcastGQL.mutate({
      request: Object.assign({children: [ccInstallReq._id, ccInstantiateReq._id]}, dAppInstallReq)
    }).toPromise();
    const ccInstallReqPromise = this.requestBroadcastGQL.mutate({request: ccInstallReq}).toPromise();
    const ccInstantiateReqPromise = this.requestBroadcastGQL.mutate({request: ccInstantiateReq}).toPromise();

    await Promise.all([dAppInstallReqPromise, ccInstallReqPromise, ccInstantiateReqPromise]);
  }

}
