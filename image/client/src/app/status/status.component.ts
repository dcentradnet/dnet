import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';

import { StatusService } from '../status.service';
import { UserService } from '../user.service';
import { OrganizationsService } from '../organizations.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
})
export class StatusComponent {
  mode = 'lobby';

  constructor(
    public popoverCtrl: PopoverController,
    public service: StatusService,
    public userService: UserService,
    public orgService: OrganizationsService
  ) { }

  changeMode(ev: any): void {
    this.mode = ev.detail.value;
  }

  public async login(): Promise<void> {
    await this.userService.login();
  }

  async presentOrgPopover(ev: any): Promise<void> {
    const pub = await this.service.gql.fetch().pipe(map(res => res.data.info.publicKey)).toPromise();
    await this.orgService.presentOrgPopover(pub, true);
  }

  async presentConsPopover(ev: any): Promise<void> {
    /*const pub = await this.service.gql.fetch().pipe(map(res => res.data.info.publicKey)).toPromise();

    const popover = await this.popoverCtrl.create({
      component: OrganizationComponent,
      event: ev,
      translucent: true,
      componentProps: {pub}
    });
    await popover.present();

    await popover.onDidDismiss();*/
  }

}
