import { remove } from 'lodash';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ListRolesGQL, ListRolesQuery, PutRoleGQL, RemoveRoleGQL, Role } from 'src/generated/queries';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.page.html',
  styleUrls: ['./roles.page.scss'],
})
export class RolesPage implements OnInit {

  public roles: Observable<ListRolesQuery['roles']>;

  constructor(
    public route: ActivatedRoute,
    public navCtrl: NavController,
    public alertController: AlertController,
    public listGQL: ListRolesGQL,
    public putGQL: PutRoleGQL,
    public removeGQL: RemoveRoleGQL,
  ) { }

  ngOnInit(): void {
    this.roles = this.route.params.pipe(
      switchMap(() => this.listGQL.fetch()),
      map(res => res.data.roles)
    );
  }

  async createRole(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Create role',
      inputs: [
        {
          name: 'name',
          type: 'text',
          id: 'name',
          // value: '',
          placeholder: 'Procurement'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();

    const {data} = await alert.onDidDismiss();
    if (!data) {
      return;
    }

    const name = data.values.name;
    const result = await this.putGQL.mutate({name}).toPromise();
    //const _id = result.data.putRole._id;
    //this.roles.push({_id, name});

    this.navCtrl.navigateForward(`roles/${result.data.putRole._id}`);
  }

  async removeRole(role: Role): Promise<void> {
    await this.removeGQL.mutate({_id: role._id}).toPromise();
    remove(this.roles, (r: Role) => r._id === role._id);
  }

}
