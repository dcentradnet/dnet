import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PickerController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Role } from 'src/generated/graphqlTypes';
import { QueryRoleGQL, PutRoleGQL, RemoveRoleFromUserGQL, AttachRoleToUserGQL } from 'src/generated/queries';

@Component({
  selector: 'app-role',
  templateUrl: './role.page.html',
  styleUrls: ['./role.page.scss'],
})
export class RolePage implements OnInit {
  subscription: Subscription;
  role: Role;

  constructor(
    public route: ActivatedRoute,
    public pickerController: PickerController,
    public queryRole: QueryRoleGQL,
    public putRole: PutRoleGQL,
    public removeRoleFromUser: RemoveRoleFromUserGQL,
    public attachRoleToUser: AttachRoleToUserGQL
  ) { }

  ngOnInit(): void {
    this.subscription = this.route.params.pipe(switchMap(
      p => this.queryRole.watch({_id: p.id}).valueChanges
    )).subscribe(d => this.role = d.data.roles[0]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  async save(): Promise<void> {
    await this.putRole.mutate(this.role).toPromise();
  }

}
