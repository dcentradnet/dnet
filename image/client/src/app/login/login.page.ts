import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/generated/graphqlTypes';
import { ListUsersGQL, ListUsersQuery } from 'src/generated/queries';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public formGroup: FormGroup;
  public users: Observable<ListUsersQuery['users']>;

  constructor(
    public modalCtrl: ModalController,
    public formBuilder: FormBuilder,
    public listUsers: ListUsersGQL,
    public auth: AuthenticationService
  ) {
    this.users = this.listUsers.watch().valueChanges.pipe(map(res => res.data.users));
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      user: new FormControl('Administrator', Validators.required),
      password: new FormControl('', Validators.required),
    }); //, this.passwordMatchValidator);
  }

  async onSubmit({user, password}: {user: User, password: string}): Promise<void> {
    await this.auth.login(user.name, password);

    this.modalCtrl.dismiss({user});
  }

  onCancel(): void {
    this.modalCtrl.dismiss();
  }

}
