import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ChaincodesPage } from '../chaincodes/chaincodes.page';
import { Channel } from 'src/generated/graphqlTypes';
import { ChannelsPageGQL, ChannelsPageQuery } from 'src/generated/queries';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-channels',
  templateUrl: './channels.page.html',
  styleUrls: ['./channels.page.scss'],
})
export class ChannelsPage {
  public channels: Observable<ChannelsPageQuery['channels']>;

  constructor(public modalController: ModalController,
              public gql: ChannelsPageGQL) { }

  ngOnInit(): void {
    this.channels = this.gql.watch().valueChanges.pipe(map(res => res.data.channels));
  }

  async showChaincodeActions(channel: Channel): Promise<void> {
    const modal = await this.modalController.create({
      component: ChaincodesPage,
      componentProps: { channel_id: channel._id }
    });
    return modal.present();
  }

}
