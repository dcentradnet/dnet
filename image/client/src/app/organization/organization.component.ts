import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrganizationComponentGQL, OrganizationComponentQuery } from 'src/generated/queries';
import { LobbyService } from '../lobby.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
})
export class OrganizationComponent {
  @Input() pub: string;
  @Input() editable: boolean;
  org$: Observable<OrganizationComponentQuery['lobby']['organization']>;

  constructor(
    public lobbyService: LobbyService,
    public gql: OrganizationComponentGQL
  ) {
    this.org$ = this.gql.fetch({pub: this.pub}).pipe(map(res => res.data?.lobby?.organization));
  }

}
