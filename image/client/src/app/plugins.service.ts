import { Injectable } from '@angular/core';

import { HttpService } from './http.service';

export class PluginInfo {
  title: string;
  args: {[key: string]: (string | number | boolean)};
}

@Injectable({
  providedIn: 'root'
})
export class PluginsService {

  constructor(public http: HttpService) { }

  async installPlugin(plugin: PluginInfo): Promise<void> {
    return this.http.post<void>('/api/installPlugin', plugin).toPromise();
  }
}
