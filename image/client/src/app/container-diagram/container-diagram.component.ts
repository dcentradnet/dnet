import 'lodash.combinations';
import _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { Network } from 'vis';
import { OrganizationsService } from '../organizations.service';

const LENGTH_MAIN = 350,
LENGTH_SERVER = 150,
LENGTH_SUB = 50,
WIDTH_SCALE = 2,
GREEN = 'green',
RED = '#C5000B',
ORANGE = 'orange',
// GRAY = '#666666',
GRAY = 'gray',
BLACK = '#2B1B17';

@Component({
  selector: 'app-container-diagram',
  templateUrl: './container-diagram.component.html',
  styleUrls: ['./container-diagram.component.scss'],
})
export class ContainerDiagramComponent implements OnInit {

  orgs: string[];

  nodes = null;
  edges = null;
  network = null;

  constructor(public organizations: OrganizationsService) { }

  async ngOnInit(): Promise<void> {
    this.orgs = await this.organizations.getOrgs();
    console.log(this.orgs);

    this.draw();
  }

  // Called when the Visualization API is loaded.
  draw(): void {
    this.nodes = [];
    this.edges = [];

    const raftNodes = [];
    for (const org of this.orgs) {
      // org node
      const orgNodeIndex = this.nodes.length;
      this.nodes.push({ id: orgNodeIndex, label: org, group: 'org', value: 10 });

      // raft node
      const raftNodeIndex = this.nodes.length;
      this.nodes.push({
        id: raftNodeIndex,
        label: `raft.${org}`,
        group: 'raft',
        value: 2,
      });
      raftNodes.push(raftNodeIndex);

      // mongo node
      const mongoNodeIndex = this.nodes.length;
      this.nodes.push({
        id: mongoNodeIndex,
        label: `mongo.${org}`,
        group: 'mongo',
        value: 2,
      });
      this.edges.push({
        from: orgNodeIndex,
        to: mongoNodeIndex,
        length: LENGTH_SUB,
        color: GRAY,
        fontColor: GRAY,
        width: WIDTH_SCALE,
      });

      // ca node
      const caNodeIndex = this.nodes.length;
      this.nodes.push({
        id: caNodeIndex,
        label: `ca.${org}`,
        group: 'ca',
        value: 2,
      });
      this.edges.push({
        from: orgNodeIndex,
        to: caNodeIndex,
        length: LENGTH_SUB,
        color: GRAY,
        fontColor: GRAY,
        width: WIDTH_SCALE,
      });

      // peer nodes
      for (let peerNo = 0; peerNo <= 1; ++peerNo) {
        const peerNodeIndex = this.nodes.length;

        this.nodes.push({
          id: peerNodeIndex,
          label: `peer${peerNo}.${org}`,
          group: 'peer',
          value: 2,
        });
        this.edges.push({
          from: orgNodeIndex,
          to: peerNodeIndex,
          length: LENGTH_SUB,
          color: GRAY,
          fontColor: GRAY,
          width: WIDTH_SCALE,
        });
        this.edges.push({
          from: raftNodeIndex,
          to: peerNodeIndex,
          length: LENGTH_SUB,
          color: GRAY,
          fontColor: GRAY,
          width: WIDTH_SCALE,
        });
      }
    }

    _.combinations(raftNodes, 2).forEach(([a, b]) => this.edges.push({
        from: a,
        to: b,
        length: LENGTH_SUB,
        color: GRAY,
        fontColor: GRAY,
        width: WIDTH_SCALE,
      })
    );

    // TODO implement "chosen" function
    // TODO make orgs clickable for info overlay
    /*
    // TODO place nodes in circle
    .map((node, index, arr) => {
      const angle = 2 * Math.PI * (index / arr.length + 0.75);
      node.x = 400 * Math.cos(angle);
      node.y = 400 * Math.sin(angle);
      if (index % 2 === 0) {
        node.value = index + 1;
      }
      return node;
    });
    */

    // legend
    const network = document.getElementById('network');
    const x = -network.clientWidth / 2 + 50;
    const y = -network.clientHeight / 2 + 50;
    const step = 70;
    this.nodes.push({
      id: 1000,
      x: x,
      y: y,
      label: 'Organization',
      group: 'org',
      value: 1,
      fixed: true,
      physics: false,
    });

    // create a network
    const container = document.getElementById('network');
    const data = {
      nodes: this.nodes,
      edges: this.edges,
    };
    const options = {
      nodes: {
        scaling: {
          min: 16,
          max: 32,
        },
      },
      edges: {
        color: GRAY,
        smooth: false,
      },
      physics: {
        barnesHut: { gravitationalConstant: -30000 },
        stabilization: { iterations: 2500 },
      },
      groups: {
        org: {
          shape: 'triangle',
          color: '#FF9900', // orange
        },
        peer: {
          shape: 'dot',
          color: '#2B7CE9', // blue
        }
      },
    };
    this.network = new Network(container, data, options);
  }

}
