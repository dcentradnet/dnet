import { v4 as uuidv4 } from 'uuid';

import { Buffer } from 'buffer/';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpService } from '../http.service';
import { OrganizationsService } from '../organizations.service';
import { StatusService } from '../status.service';
import { ChaincodePageGQL, QueryChaincodeGQL, RequestBroadcastGQL, VoteGQL } from 'src/generated/queries';
import { ChaincodeQuery, RequestCore } from 'src/generated/graphqlTypes';

@Component({
  selector: 'app-chaincodes',
  templateUrl: './chaincodes.page.html',
  styleUrls: ['./chaincodes.page.scss'],
})
export class ChaincodesPage implements OnInit {

  public installed: Observable<ChaincodeQuery['installed']>;
  public instantiated: Observable<ChaincodeQuery['instantiated']>;

  constructor(public http: HttpService,
              public route: ActivatedRoute,
              public status: StatusService,
              public organizations: OrganizationsService,
              public alertController: AlertController,
              public gql: ChaincodePageGQL,
              public requestBroadcastGQL: RequestBroadcastGQL,
              public voteGQL: VoteGQL,
              public queryChaincodeGQL: QueryChaincodeGQL) { }

  ngOnInit(): void {
    /*this.$installed = this.route.params.pipe(
      switchMap(() => this.gql.fetch().pipe(map(p => p.data.chaincode.installed)))
    );
    this.$instantiated = this.route.params.pipe(
      switchMap(() => this.gql.fetch().pipe(map(p => p.data.chaincode.instantiated)))
    );*/

    /*this.subscription = this.gql.watch().valueChanges.subscribe(({data}) => {
      this.installed = data.chaincode.installed;
      this.instantiated = data.chaincode.instantiated;
    });*/

    this.installed = this.gql.watch().valueChanges.pipe(map(p => p.data.chaincode.installed));
    this.instantiated = this.gql.watch().valueChanges.pipe(map(p => p.data.chaincode.instantiated));
  }

  async install(): Promise<void> {
    const isAdminMode = await this.status.infoObservable.toPromise().then(res => res.isAdminMode);

    // show request
    const alertRequest = await this.alertController.create({
      header: isAdminMode ? 'Install chaincode' : 'Send chaincode install request',
      inputs: [
        {
          name: 'ccName',
          type: 'text',
          placeholder: 'fabcar'
        }, {
          name: 'ccVersion',
          type: 'text',
          placeholder: '1.0'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alertRequest.present();
    const alertData = await alertRequest.onDidDismiss();
    const installInfo: {ccName: string, ccVersion: string} = alertData.data.values;

    if (isAdminMode) {
      //await this.organizations.forwardToAllOrgs('/api/sofa/installChaincode', installInfo); // FIXME
    } else {
      const request: RequestCore = {
        _id: uuidv4(),
        type: 'chaincode_install',
        key: installInfo.ccName,
        subkey: installInfo.ccVersion
      };
      await this.requestBroadcastGQL.mutate({request}).toPromise();
      await this.voteGQL.mutate({vote: {rid: request._id, value: 1}}).toPromise();
    }
  }

  async instantiate(): Promise<void> {
    const isAdminMode = await this.status.infoObservable.toPromise().then(res => res.isAdminMode);

    // show request
    const alertRequest = await this.alertController.create({
      header: isAdminMode ? 'Instantiate chaincode' : 'Send chaincode instantiate request',
      inputs: [
        {
          name: 'ccName',
          type: 'text',
          placeholder: 'fabcar'
        }, {
          name: 'ccVersion',
          type: 'text',
          placeholder: '1.0'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alertRequest.present();
    const alertData = await alertRequest.onDidDismiss();
    const instantiateInfo: {ccName: string, ccVersion: string} = alertData.data.values;

    if (isAdminMode) {
      //await this.organizations.forwardToAllOrgs('/api/sofa/chaincodeInstantiate', instantiateInfo); //FIXME
    } else {
      const request = {
        _id: uuidv4(),
        type: 'chaincode_instantiate',
        key: instantiateInfo.ccName,
        subkey: instantiateInfo.ccVersion
      };
      await this.requestBroadcastGQL.mutate({request}).toPromise();
      await this.voteGQL.mutate({vote: {rid: request._id, value: 1}}).toPromise();
    }
  }

  async query(): Promise<void> {
    // show request
    const alertRequest = await this.alertController.create({
      header: 'Input query information',
      inputs: [
        {
          name: 'chaincode',
          type: 'text',
          placeholder: 'Chaincode'
        }, {
          name: 'method',
          type: 'text',
          placeholder: 'Method'
        }, {
          name: 'args',
          type: 'text',
          placeholder: 'Arguments'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alertRequest.present();
    const alertData = await alertRequest.onDidDismiss();
    const queryInfo = alertData.data.values;
    queryInfo.args = queryInfo.args.split(',');
    console.log(queryInfo);

    // do query
    const queryResponse = await this.queryChaincodeGQL.fetch(queryInfo).toPromise().then(res => res.data.chaincodeQuery);

    // show response
    const alertResponse = await this.alertController.create({
      header: 'Query response',
      message: Buffer.from(queryResponse.data).toString(),
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alertResponse.present();
    await alertResponse.onDidDismiss();
  }

  async invoke(): Promise<void> {
    // show request
    const alertRequest = await this.alertController.create({
      header: 'Input invoke information',
      inputs: [
        {
          name: 'chaincode',
          type: 'text',
          placeholder: 'Chaincode'
        }, {
          name: 'method',
          type: 'text',
          placeholder: 'Method'
        }, {
          name: 'args',
          type: 'text',
          placeholder: 'Arguments'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alertRequest.present();
    const alertData = await alertRequest.onDidDismiss();
    const invokeInfo = alertData.data.values;
    invokeInfo.args = invokeInfo.args.split(',');
    console.log(invokeInfo);

    // do query
    const invokeResponse = await this.http.post<any>('/api/invoke', invokeInfo).toPromise(); // FIXME

    // show response
    const alertResponse = await this.alertController.create({
      header: 'Invoke response',
      message: Buffer.from(invokeResponse.result.data).toString(),
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alertResponse.present();
    await alertResponse.onDidDismiss();
  }

}
