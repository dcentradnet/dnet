import {APOLLO_OPTIONS} from 'apollo-angular';
import {DefaultOptions} from '@apollo/client/core';
import url from 'url';
import origin from 'get-location-origin';
import {NgModule} from '@angular/core';

import {HttpLink, ApolloLink, InMemoryCache, concat} from '@apollo/client/core';

import {split } from '@apollo/client/core';
import {WebSocketLink} from '@apollo/client/link/ws';
import {getOperationDefinition} from '@apollo/client/utilities';

import {AuthenticationService} from './authentication.service';

const path = 'api/graphQL';
const pathWS = 'api/graphQLWS';

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
};

export function createApollo(authService: AuthenticationService): any {
  // create a http link
  const httpLink = new HttpLink({uri: path});
  // create a WebSocket link
  const parsedUrl = url.parse(origin);
  const wsProtocol = 'https:' === parsedUrl.protocol ? 'wss' : 'ws';
  const wsLink = new WebSocketLink({
    uri: `${wsProtocol}://${parsedUrl.host}/${pathWS}`,
    options: {
      reconnect: true,
    },
  });

  // using the ability to split links, you can send data to each link
  // depending on what kind of operation is being sent
  const link = split(
    // split based on operation type
    ({query}) => {
      const {operation} = getOperationDefinition(query);
      return operation === 'subscription';
    },
    wsLink,
    httpLink,
  );

  // define authentication service
  const authMiddleware = new ApolloLink((operation, forward) => {
    // add the authorization to the headers
    operation.setContext(({ headers = {} }) => ({
      headers: {
        ...headers,
        authorization: authService.getToken(),
      }
    }));

    return forward(operation);
  });

  return {
    link: concat(authMiddleware, link),
    cache: new InMemoryCache(),
    defaultOptions
  };
}

@NgModule({
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [AuthenticationService],
    },
  ],
})
export class GraphQLModule {}
