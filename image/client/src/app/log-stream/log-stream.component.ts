import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgTerminal } from '../ng-terminal/ng-terminal';
import { NgTerminalComponent } from '../ng-terminal/ng-terminal.component';
import { Terminal } from 'xterm';

export interface Event {
  data: string;
}

@Component({
  selector: 'app-log-stream',
  templateUrl: './log-stream.component.html',
  styleUrls: ['./log-stream.component.scss'],
})
export class LogStreamComponent implements OnInit, OnDestroy {

  @Input() eventSource = '/api/log';
  @Input() mapFunc: (Event) => string = this.mapEvent;

  showTerminal = false;
  @ViewChild(NgTerminalComponent) child: NgTerminal;
  underlying: Terminal;

  constructor(public http: HttpClient) {
  }

  async ngOnInit(): Promise<void> {
    // TODO this is currently required for terminal to be loaded correctly
    await (new Promise((resolve, reject) => setInterval(resolve, 100)));

    this.showTerminal = true;

    const source = new EventSource(this.eventSource);
    // source.onmessage = e => this.child && this.child.write(e.data+'\n\r');
    source.onmessage = e => this.child && this.child.write(this.mapEvent(e).substr(0, 80) + '\n\r');
  }

  ngAfterViewChecked(): void {
  }

  ngOnDestroy(): void {
    this.showTerminal = false;
  }

  mapEvent(e: Event): string {
    return JSON.parse(e.data).eventValue;
  }

}
