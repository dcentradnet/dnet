import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

import { NgTerminalModule } from '../ng-terminal/ng-terminal.module';
import { LogStreamComponent } from './log-stream.component';

@NgModule({
     declarations: [
       LogStreamComponent
     ],
     imports: [
       IonicModule,
       CommonModule,
       NgTerminalModule
     ],
     exports: [
       LogStreamComponent
     ],
     entryComponents: [
       LogStreamComponent
     ]
})
export class LogStreamModule {}
