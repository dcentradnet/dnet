import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { LogStreamModalComponent } from '../log-stream-modal/log-stream-modal.component';
import { map, mergeMap } from 'rxjs/operators';

import { ActionsPageGQL, ActionsPageQuery } from 'src/generated/queries';
import { Observable } from 'rxjs';
import { Action } from 'src/generated/graphqlTypes';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.page.html',
  styleUrls: ['./actions.page.scss'],
})
export class ActionsPage implements OnInit {
  public actions: Observable<ActionsPageQuery['actions']>;
  public actionsProgress: {[key: string]: Action} = {};

  constructor(public gql: ActionsPageGQL,
              public modalController: ModalController) { }

  ngOnInit(): void {
    this.actions = this.gql.watch().valueChanges.pipe(map(res => res.data.actions));
    this.actions.pipe(mergeMap(action => action)).subscribe(actionIn => {
      let action = this.actionsProgress[actionIn._id];
      if (!action) {
        action = this.actionsProgress[actionIn._id] = Object.assign({log: []}, actionIn);
      }
    });

    const source = new EventSource('/api/actionsStream');
    source.onmessage = e => this.onActionEvent(JSON.parse(e.data));
  }

  onActionEvent(event: any): void {
    let action = this.actionsProgress[event._id];
    if (!action) {
      action = this.actionsProgress[event._id] = Object.assign({log: []}, event); // TODO pick
    }

    switch (event.eventName) {
      case 'progress':
        action.progress = event.eventValue;
        break;
      case 'log':
        action.log.push(event.eventValue);
        break;
    }
  }

  async showLog(action: Action): Promise<void> {
    const modal = await this.modalController.create({
      component: LogStreamModalComponent,
      componentProps: {
        eventSource: `/api/actionsStream?id=${action._id}&replay=true`
      }
    });
    await modal.present();
  }

}
