import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

import { HttpService } from '../http.service';
import { StatusService } from '../status.service';
import { AuthenticationService } from '../authentication.service';
import { LobbyService } from '../lobby.service';
import { Consortium } from 'src/generated/graphqlTypes';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.page.html',
  styleUrls: ['./lobby.page.scss'],
})
export class LobbyPage implements OnInit {

  constructor(public http: HttpService,
              public alertController: AlertController,
              public loadingController: LoadingController,
              public toastCtrl: ToastController,
              public status: StatusService,
              public authentication: AuthenticationService,
              public statusService: StatusService,
              public lobbyService: LobbyService) {}

  async ngOnInit(): Promise<void> {
    await this.lobbyService.update();
  }

  async createConsortium(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Input consortium details',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name'
        }, {
          name: 'desc',
          type: 'text',
          placeholder: 'Description'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();

    const alertData = await alert.onDidDismiss();
    if ('cancel' === alertData.role) {
      return;
    }

    await this.create(alertData.data.values.name, alertData.data.values.desc);
  }

  async joinRequest(consortium: Consortium): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Join consortium',
      message: `Would you like to send a join request to the '${consortium.name}' consortium`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
    const reason = await alert.onDidDismiss();
    console.log(reason);

    if ('cancel' === reason.role) {
      return;
    }

    // submit
    await this.lobbyService.submitJoinRequest(consortium);
  }

  refresh(): void {
    this.lobbyService.update();
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  async withdrawRequest(): Promise<void> {
    const consortium = this.lobbyService.requested;

    const alert = await this.alertController.create({
      header: 'Withdraw join request',
      message: `Would you like to withdraw your join request to the '${consortium.name}' consortium`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
    const reason = await alert.onDidDismiss();
    console.log(reason);

    if ('cancel' === reason.role) {
      return;
    }

    this.lobbyService.withdrawJoinRequest(consortium);
  }

  async create(name: string, desc: string): Promise<void> {
    const toast = await this.toastCtrl.create({
      message: 'Your node is creating the consortium. Please see the actions page for a progress report.',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();

    await this.lobbyService.create(name, desc);
  }

  async enter(): Promise<void> {
    const toast = await this.toastCtrl.create({
      message: 'Your node is joining the consortium. Please see the actions page for a progress report.',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();

    // HACK
    await this.ngOnInit();

    await this.lobbyService.join(this.lobbyService.requested);
  }

}
