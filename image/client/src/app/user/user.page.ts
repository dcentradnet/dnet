import { remove } from 'lodash';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { PickerController } from '@ionic/angular';
import { Role, RoleCore, User } from 'src/generated/graphqlTypes';
import { AttachRoleToUserGQL, ListRolesGQL, PutUserGQL, QueryUserGQL, RemoveRoleFromUserGQL } from 'src/generated/queries';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit, OnDestroy {
  subscription: Subscription;
  user: User;

  constructor(
    public route: ActivatedRoute,
    public pickerController: PickerController,
    public queryUserGQL: QueryUserGQL,
    public putUserGQL: PutUserGQL,
    public listRolesGQL: ListRolesGQL,
    public removeRoleFromUserGQL: RemoveRoleFromUserGQL,
    public attachRoleToUserGQL: AttachRoleToUserGQL
  ) { }

  ngOnInit(): void {
    this.subscription = this.route.params.pipe(switchMap(
      p => this.queryUserGQL.watch({_id: p.id}).valueChanges
    )).subscribe(d => this.user = d.data.users[0]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  async save(): Promise<void> {
    await this.putUserGQL.mutate(this.user).toPromise();
  }

  async removeRole(role: Role): Promise<void> {
    await this.removeRoleFromUserGQL.mutate({roleId: role._id, userId: this.user._id}).toPromise();
    remove(this.user.roles, (r: RoleCore) => r._id === role._id);
  }

  async attachRole(): Promise<void> {
    // const roles = await this.listRoles.watch().valueChanges.toPromise();
    const roles = await this.listRolesGQL.fetch().toPromise();
    const options = roles.data.roles.map(r => ({
        text: r.name,
        value: r._id
    }));

    const picker = await this.pickerController.create({
      columns: [{name: 'role', options}],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: (value) => {
            console.log(`Got Value ${value}`);
          }
        }
      ]
    });
    await picker.present();

    const {data} = await picker.onDidDismiss();
    if (!data) {
      return;
    }

    await this.attachRoleToUserGQL.mutate({roleId: data.role.value, userId: this.user._id}).toPromise();
    this.user.roles.push({_id: data.role.value, name: data.role.text});
  }

}
