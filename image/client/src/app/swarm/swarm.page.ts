import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertController } from '@ionic/angular';

import { HttpService } from '../http.service';
import { Info, SwarmInitParamsInput, SwarmJoinParamsInput } from 'src/generated/graphqlTypes';
import { SwarmPageGQL, SwarmPageQuery } from 'src/generated/queries';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-swarm',
  templateUrl: './swarm.page.html',
  styleUrls: ['./swarm.page.scss'],
})
export class SwarmPage implements OnInit, OnDestroy {
  subscription: Subscription;
  info: SwarmPageQuery['info'];
  swarm: SwarmPageQuery['swarm']['inspect'];
  nodes: SwarmPageQuery['swarm']['nodes'];

  constructor(public http: HttpService,
              public alertController: AlertController,
              public gql: SwarmPageGQL) {}

  async ngOnInit(): Promise<void> {
    this.subscription = this.gql.watch().valueChanges.subscribe(({data}) => {
      this.info = data.info;
      this.swarm = data.swarm.inspect;
      this.nodes = data.swarm.nodes;
    });
  }

  async ngOnDestroy(): Promise<void> {
    this.subscription.unsubscribe();
  }

  async initSwarm(): Promise<void> {
    const swarmInit: SwarmInitParamsInput = {
      'ListenAddr': '0.0.0.0:2377',
      'AdvertiseAddr': `${this.info.publicIpV4}:2377`,
    };

    const result = await this.http.post<any>('/api/swarm/init', swarmInit).toPromise();
    console.log(result);
  }

  async joinSwarm(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Input swarm connection details',
      inputs: [
        {
          name: 'connectionDetails',
          type: 'text',
          placeholder: 'Connection details'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();

    const alertData = await alert.onDidDismiss();
    const connectionDetails = alertData.data.values.connectionDetails;
    console.log(connectionDetails);

    const [token, node] = connectionDetails.split(' ');
    const swarmJoin: SwarmJoinParamsInput = {
      'ListenAddr': '0.0.0.0:2377',
      'AdvertiseAddr': this.info.publicIpV4 + ':2377',
      'RemoteAddrs': [
        node
      ],
      'JoinToken': token
    };

    const result = await this.http.post<any>('/api/swarm/join', swarmJoin).toPromise();
    console.log(result);
  }

  async leaveSwarm(): Promise<void> {
    await this.http.ready;
    const result = await this.http.post<any>('/api/swarm/leave', {}).toPromise();
    console.log(result);
  }

}
