import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { User } from 'src/generated/graphqlTypes';
import { LoginPage } from './login/login.page';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public loggedIn: User;

  constructor(
    private storage: StorageService,
    private modalCtrl: ModalController
  ) {
    this.init();
  }

  private async init(): Promise<void> {
    if (!this.loggedIn) {
      this.loggedIn = await this.storage.get('loggedIn');
    }

    if (!this.loggedIn) {
      await this.login();
    }
  }

  public async login(): Promise<void> {
    const modal = await this.modalCtrl.create({
      component: LoginPage,
      componentProps: {
      }
    });
    await modal.present();

    const result = await modal.onDidDismiss();
    if (!result?.data?.user) {
      return;
    }

    this.loggedIn = result.data.user;
    await this.storage.set('loggedIn', this.loggedIn);
  }
}
