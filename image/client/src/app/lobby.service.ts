import {QueryRef} from 'apollo-angular';
import { isObject } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Info, Consortium, ConsortiumMember } from 'src/generated/graphqlTypes';
import { CreateConsortiumGQL, JoinConsortiumGQL, LobbyRequestAddGQL, LobbyRequestRemoveGQL, LobbyServiceGQL, LobbyServiceQuery } from 'src/generated/queries';


@Injectable({
  providedIn: 'root'
})
export class LobbyService {

  ready: Promise<void>;
  watch: QueryRef<LobbyServiceQuery>;
  subscription: Subscription;

  info: Info;
  consortia: Consortium[];
  requested: Consortium;
  joined: Consortium;
  member: ConsortiumMember;

  public GETFILE_URL = 'https://dnetlobby-git-develop-patrickcharrier.vercel.app/api/getfile';
  public PUTFILE_URL = 'https://dnetlobby-git-develop-patrickcharrier.vercel.app/api/putfile';

  constructor(
    public http: HttpService,
    private lobbyGQL: LobbyServiceGQL,
    private createConsortiumGQL: CreateConsortiumGQL,
    private joinConsortiumGQL: JoinConsortiumGQL,
    private requestAddGQL: LobbyRequestAddGQL,
    private requestRemoveGQL: LobbyRequestRemoveGQL
  ) {
    this.watch = this.lobbyGQL.watch();
    this.subscription = this.watch.valueChanges.subscribe(({data}) => {
      this.info = data.info;
      this.consortia = data.lobby.consortia;
      this.requested = data.lobby.requested;
      this.joined = data.lobby.joined;
      this.member = data.lobby.member;
    });
  }

  update(): void {
    this.watch.refetch();
  }

  async submitJoinRequest(consortium: Consortium): Promise<void> {
    await this.requestAddGQL.mutate(consortium).toPromise();

    this.requested = consortium;
  }

  async withdrawJoinRequest(consortium: Consortium): Promise<void> {
    await this.requestRemoveGQL.mutate(consortium).toPromise();

    this.requested = undefined;
  }

  async create(name: string, desc: string): Promise<void> {
    await this.createConsortiumGQL.mutate({
      consId: uuidv4(),
      consDomain: name,
      consDesc: desc,
      waitFinish: true
    }).toPromise();
  }

  async join(consortium: Consortium): Promise<void> {
    await this.joinConsortiumGQL.mutate({
      crypted: this.member.token,
      opub: this.member.apub,
      consId: consortium.cid,
      consDomain: consortium.name
    }).toPromise();
  }

  isJoinRequestSubmitted(): boolean {
    return isObject(this.requested);
  }

  isJoinRequestConfirmed(): boolean {
    return isObject(this.member);
  }

  isJoined(): boolean {
    return this.info?.isJoined;
  }

  async getImage(id: string): Promise<unknown> {
    return this.http.get(`${this.GETFILE_URL}?id=${id}`).toPromise();
  }

  async putImage(file: File): Promise<string> {
    const formData = new FormData();
    formData.append('image', file, file.name);

    const {id} = await this.http.post<{id: string}>(this.PUTFILE_URL, formData).toPromise();
    return id;
  }
}
