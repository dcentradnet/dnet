import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'src/app/notifications.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent {

  constructor(public service: NotificationsService) { }
}
