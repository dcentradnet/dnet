import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { NotificationsService } from '../notifications.service';
import { PopoverComponent } from './popover/popover.component';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent {

  constructor(
    public service: NotificationsService,
    public popoverController: PopoverController
  ) { }

  async presentPopover(ev: any): Promise<void> {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      cssClass: 'popover-class',
      event: ev,
      translucent: true
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
