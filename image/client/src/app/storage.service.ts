import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  public ready: Promise<void>;
  private storage: Storage | null = null;

  constructor(private storageFactory: Storage) {
    this.ready = this.init();
  }

  async init(): Promise<void> {
    // If using, define drivers here: await this.storage.defineDriver(/*...*/);
    this.storage = await this.storageFactory.create();
  }

  public async get(key: string): Promise<any> {
    await this.ready;
    return this.storage?.get(key);
  }

  public async set(key: string, value: any): Promise<void> {
    await this.ready;
    await this.storage?.set(key, value);
  }
}
