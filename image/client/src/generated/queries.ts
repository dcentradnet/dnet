import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  JSON: any;
};

export type Action = {
  __typename?: 'Action';
  _id: Scalars['String'];
  log?: Maybe<Array<Maybe<Scalars['String']>>>;
  name: Scalars['String'];
  progress?: Maybe<Scalars['Float']>;
};

export type Buffer = {
  __typename?: 'Buffer';
  data?: Maybe<Array<Maybe<Scalars['Int']>>>;
  type?: Maybe<Scalars['String']>;
};

export type Chaincode = {
  __typename?: 'Chaincode';
  name: Scalars['String'];
};

export type ChaincodeEvent = {
  __typename?: 'ChaincodeEvent';
  blockNumber: Scalars['String'];
  eventName: Scalars['String'];
  payloadString: Scalars['String'];
  status: Scalars['String'];
  transactionId: Scalars['String'];
};

export type ChaincodeEventListenInput = {
  chaincodeName: Scalars['String'];
  channelName: Scalars['String'];
  eventName: Scalars['String'];
};

export type ChaincodeQuery = {
  __typename?: 'ChaincodeQuery';
  installed?: Maybe<Array<Chaincode>>;
  instantiated?: Maybe<Array<Chaincode>>;
};

export type Channel = {
  __typename?: 'Channel';
  _id: Scalars['String'];
  chaincodes?: Maybe<Array<Chaincode>>;
};

export type Consortium = {
  __typename?: 'Consortium';
  approvals?: Maybe<Array<ConsortiumApproval>>;
  cid: Scalars['ID'];
  desc?: Maybe<Scalars['String']>;
  founder?: Maybe<Scalars['String']>;
  members?: Maybe<Array<ConsortiumMember>>;
  name: Scalars['String'];
  requests?: Maybe<Array<ConsortiumRequest>>;
};


export type ConsortiumApprovalsArgs = {
  pub?: Maybe<Scalars['String']>;
};


export type ConsortiumMembersArgs = {
  pub?: Maybe<Scalars['String']>;
};

export type ConsortiumApproval = {
  __typename?: 'ConsortiumApproval';
  _id: Scalars['ID'];
  cid: Scalars['ID'];
  pub?: Maybe<Scalars['String']>;
  rpub?: Maybe<Scalars['String']>;
  sig?: Maybe<Scalars['String']>;
};

export type ConsortiumLocal = {
  __typename?: 'ConsortiumLocal';
  organizations?: Maybe<Array<Organization>>;
};

export type ConsortiumMember = {
  __typename?: 'ConsortiumMember';
  _id: Scalars['ID'];
  apub?: Maybe<Scalars['String']>;
  cid: Scalars['ID'];
  pub?: Maybe<Scalars['String']>;
  token?: Maybe<Scalars['String']>;
};

export type ConsortiumOrganization = {
  __typename?: 'ConsortiumOrganization';
  about?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  founder?: Maybe<Scalars['String']>;
  imageId?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  pub: Scalars['String'];
};

export type ConsortiumOrganizationInput = {
  about?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  founder?: Maybe<Scalars['String']>;
  imageId?: Maybe<Scalars['String']>;
  name: Scalars['String'];
};

export type ConsortiumRequest = {
  __typename?: 'ConsortiumRequest';
  _id: Scalars['ID'];
  approvals?: Maybe<Array<ConsortiumApproval>>;
  cid: Scalars['ID'];
  confirmed?: Maybe<Scalars['Boolean']>;
  rpub: Scalars['String'];
  sig: Scalars['String'];
};

export type Container = {
  __typename?: 'Container';
  Health?: Maybe<Scalars['JSON']>;
  Name: Scalars['String'];
};

export type Dapp = {
  __typename?: 'Dapp';
  _id?: Maybe<Scalars['ID']>;
  dockerImage: Scalars['String'];
  imageId?: Maybe<Scalars['String']>;
  pricing?: Maybe<Pricing>;
  title: Scalars['String'];
  version: Scalars['String'];
};

export type DappManualInstallInput = {
  dockerImage: Scalars['String'];
  options?: Maybe<Scalars['JSON']>;
  title: Scalars['String'];
  version: Scalars['String'];
};

export type Dapps = {
  __typename?: 'Dapps';
  installable?: Maybe<Array<Dapp>>;
  installed?: Maybe<Array<InstalledDapp>>;
};

export type FormField = {
  __typename?: 'FormField';
  label: Scalars['String'];
  name: Scalars['String'];
  required: Scalars['Boolean'];
  type: Scalars['String'];
  value: Scalars['String'];
};

export type Governance = {
  __typename?: 'Governance';
  request?: Maybe<Request>;
  requests?: Maybe<Array<Request>>;
};


export type GovernanceRequestArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type Info = {
  __typename?: 'Info';
  channelName?: Maybe<Scalars['String']>;
  consDomain?: Maybe<Scalars['String']>;
  isAdminMode?: Maybe<Scalars['Boolean']>;
  isJoined?: Maybe<Scalars['Boolean']>;
  orgName?: Maybe<Scalars['String']>;
  publicIpV4?: Maybe<Scalars['String']>;
  publicKey?: Maybe<Scalars['String']>;
};

export type InstalledDapp = {
  __typename?: 'InstalledDapp';
  dapp: Dapp;
  hostName: Scalars['String'];
};

export type Lobby = {
  __typename?: 'Lobby';
  consortia?: Maybe<Array<Consortium>>;
  joined?: Maybe<Consortium>;
  member?: Maybe<ConsortiumMember>;
  organization?: Maybe<ConsortiumOrganization>;
  requested?: Maybe<Consortium>;
};


export type LobbyOrganizationArgs = {
  pub?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  attachPermToRole?: Maybe<Scalars['Boolean']>;
  attachRoleToUser?: Maybe<Scalars['Boolean']>;
  chaincodeInvoke?: Maybe<Scalars['Boolean']>;
  installDapp?: Maybe<Scalars['Boolean']>;
  installPlugin?: Maybe<Scalars['Boolean']>;
  lobbyConsortiaAdd?: Maybe<Scalars['Boolean']>;
  lobbyConsortiumSet?: Maybe<Scalars['Boolean']>;
  lobbyOrganizationPut?: Maybe<Scalars['Boolean']>;
  lobbyOrganizationRemove?: Maybe<Scalars['Boolean']>;
  lobbyRequestAdd?: Maybe<Scalars['Boolean']>;
  lobbyRequestRemove?: Maybe<Scalars['Boolean']>;
  loginUser?: Maybe<Scalars['String']>;
  networkExtend?: Maybe<Scalars['Boolean']>;
  networkInit?: Maybe<Scalars['Boolean']>;
  networkShutdown?: Maybe<Scalars['Boolean']>;
  putPermission?: Maybe<PermissionCore>;
  putRole?: Maybe<RoleCore>;
  putUser?: Maybe<UserCore>;
  removePermFromRole?: Maybe<Scalars['Boolean']>;
  removePermission?: Maybe<Scalars['Boolean']>;
  removeRole?: Maybe<Scalars['Boolean']>;
  removeRoleFromUser?: Maybe<Scalars['Boolean']>;
  removeUser?: Maybe<Scalars['Boolean']>;
  request?: Maybe<Scalars['Boolean']>;
  requestBroadcast?: Maybe<Scalars['Boolean']>;
  swarmInit?: Maybe<Scalars['Boolean']>;
  swarmJoin?: Maybe<Scalars['Boolean']>;
  swarmJoinCrypted?: Maybe<Scalars['Boolean']>;
  syncRequests?: Maybe<Scalars['Boolean']>;
  vote?: Maybe<Scalars['Int']>;
};


export type MutationAttachPermToRoleArgs = {
  permId?: Maybe<Scalars['String']>;
  roleId?: Maybe<Scalars['String']>;
};


export type MutationAttachRoleToUserArgs = {
  roleId?: Maybe<Scalars['String']>;
  userId?: Maybe<Scalars['String']>;
};


export type MutationChaincodeInvokeArgs = {
  args?: Maybe<Array<Maybe<Scalars['String']>>>;
  chaincode?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
  method?: Maybe<Scalars['String']>;
};


export type MutationInstallDappArgs = {
  dapp?: Maybe<DappManualInstallInput>;
};


export type MutationInstallPluginArgs = {
  plugin?: Maybe<PluginInstallInput>;
};


export type MutationLobbyConsortiaAddArgs = {
  cid: Scalars['String'];
  desc: Scalars['String'];
  name: Scalars['String'];
};


export type MutationLobbyConsortiumSetArgs = {
  cid: Scalars['String'];
};


export type MutationLobbyOrganizationPutArgs = {
  info?: Maybe<ConsortiumOrganizationInput>;
};


export type MutationLobbyRequestAddArgs = {
  cid: Scalars['String'];
};


export type MutationLobbyRequestRemoveArgs = {
  cid: Scalars['String'];
};


export type MutationLoginUserArgs = {
  _id?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};


export type MutationNetworkExtendArgs = {
  consDomain: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
};


export type MutationNetworkInitArgs = {
  consDomain: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
};


export type MutationNetworkShutdownArgs = {
  consDomain: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
};


export type MutationPutPermissionArgs = {
  _id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};


export type MutationPutRoleArgs = {
  _id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};


export type MutationPutUserArgs = {
  _id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};


export type MutationRemovePermFromRoleArgs = {
  permId?: Maybe<Scalars['String']>;
  roleId?: Maybe<Scalars['String']>;
};


export type MutationRemovePermissionArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type MutationRemoveRoleArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type MutationRemoveRoleFromUserArgs = {
  roleId?: Maybe<Scalars['String']>;
  userId?: Maybe<Scalars['String']>;
};


export type MutationRemoveUserArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type MutationRequestArgs = {
  request?: Maybe<RequestInput>;
};


export type MutationRequestBroadcastArgs = {
  request?: Maybe<RequestInput>;
};


export type MutationSwarmInitArgs = {
  params?: Maybe<SwarmInitParamsInput>;
};


export type MutationSwarmJoinArgs = {
  params?: Maybe<SwarmJoinParamsInput>;
};


export type MutationSwarmJoinCryptedArgs = {
  crypted: Scalars['String'];
  opub: Scalars['String'];
};


export type MutationVoteArgs = {
  vote?: Maybe<VoteInput>;
};

export type Notification = {
  __typename?: 'Notification';
  _id: Scalars['ID'];
  message: Scalars['String'];
  system?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['Int']>;
  url?: Maybe<Scalars['String']>;
};

export type Organization = {
  __typename?: 'Organization';
  _id: Scalars['ID'];
  info?: Maybe<ConsortiumOrganization>;
  name: Scalars['String'];
  publicIpV4?: Maybe<Scalars['String']>;
  publicKey?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Role>>;
  users?: Maybe<Array<User>>;
};


export type OrganizationRolesArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type OrganizationUsersArgs = {
  _id?: Maybe<Scalars['String']>;
};

export type Permission = {
  __typename?: 'Permission';
  _id: Scalars['String'];
  name: Scalars['String'];
  roles?: Maybe<Array<RoleCore>>;
};

export type PermissionCore = {
  __typename?: 'PermissionCore';
  _id: Scalars['String'];
  name: Scalars['String'];
};

export type Plugin = {
  __typename?: 'Plugin';
  _id?: Maybe<Scalars['ID']>;
  imageId?: Maybe<Scalars['String']>;
  installed?: Maybe<Scalars['Boolean']>;
  parameters?: Maybe<Array<FormField>>;
  pricing?: Maybe<Pricing>;
  title: Scalars['String'];
  version: Scalars['String'];
};

export type PluginInstallInput = {
  _id: Scalars['String'];
  parameters?: Maybe<Scalars['JSON']>;
  version: Scalars['String'];
};

export type Plugins = {
  __typename?: 'Plugins';
  installable?: Maybe<Array<Plugin>>;
  installed?: Maybe<Array<Plugin>>;
};

export type Pricing = {
  __typename?: 'Pricing';
  currency: Scalars['String'];
  initialPrice?: Maybe<Scalars['Int']>;
  paymentInfo?: Maybe<Scalars['String']>;
  pricePerMonth?: Maybe<Scalars['Int']>;
};

export type Query = {
  __typename?: 'Query';
  actions?: Maybe<Array<Action>>;
  chaincode?: Maybe<ChaincodeQuery>;
  chaincodeQuery?: Maybe<Buffer>;
  channels?: Maybe<Array<Channel>>;
  consortium?: Maybe<Consortium>;
  containers?: Maybe<Array<Container>>;
  dapps?: Maybe<Dapps>;
  gov?: Maybe<Governance>;
  info?: Maybe<Info>;
  lobby?: Maybe<Lobby>;
  organization?: Maybe<Organization>;
  organizations?: Maybe<Array<Organization>>;
  permissions?: Maybe<Array<Permission>>;
  plugins?: Maybe<Plugins>;
  replayNotifications?: Maybe<Array<Notification>>;
  roles?: Maybe<Array<Role>>;
  swarm?: Maybe<Swarm>;
  users?: Maybe<Array<User>>;
};


export type QueryChaincodeQueryArgs = {
  args?: Maybe<Array<Maybe<Scalars['String']>>>;
  chaincode?: Maybe<Scalars['String']>;
  method?: Maybe<Scalars['String']>;
};


export type QueryPermissionsArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type QueryRolesArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type QueryUsersArgs = {
  _id?: Maybe<Scalars['String']>;
};

export type Request = {
  __typename?: 'Request';
  _id: Scalars['String'];
  children?: Maybe<Array<RequestCore>>;
  key: Scalars['String'];
  ownvote?: Maybe<VoteCore>;
  parent?: Maybe<RequestCore>;
  quorum?: Maybe<Array<Organization>>;
  settled?: Maybe<Scalars['Boolean']>;
  subkey: Scalars['String'];
  type: Scalars['String'];
  votes?: Maybe<Array<VoteCore>>;
};

export type RequestCore = {
  __typename?: 'RequestCore';
  _id: Scalars['String'];
  children?: Maybe<Array<Scalars['String']>>;
  key: Scalars['String'];
  ownvote?: Maybe<VoteCore>;
  parent?: Maybe<Scalars['String']>;
  quorum?: Maybe<Array<Scalars['String']>>;
  settled?: Maybe<Scalars['Boolean']>;
  subkey: Scalars['String'];
  type: Scalars['String'];
};

export type RequestInput = {
  _id: Scalars['String'];
  children?: Maybe<Array<Scalars['String']>>;
  key: Scalars['String'];
  parent?: Maybe<Scalars['String']>;
  subkey: Scalars['String'];
  type: Scalars['String'];
};

export type Role = {
  __typename?: 'Role';
  _id: Scalars['String'];
  name: Scalars['String'];
  users?: Maybe<Array<UserCore>>;
};

export type RoleCore = {
  __typename?: 'RoleCore';
  _id: Scalars['String'];
  name: Scalars['String'];
};

export type Subscription = {
  __typename?: 'Subscription';
  chaincodeEvent?: Maybe<ChaincodeEvent>;
  notification?: Maybe<Notification>;
  requestAdd?: Maybe<RequestCore>;
  statusUpdate?: Maybe<Info>;
};


export type SubscriptionChaincodeEventArgs = {
  input?: Maybe<ChaincodeEventListenInput>;
};

export type Swarm = {
  __typename?: 'Swarm';
  inspect?: Maybe<Scalars['JSON']>;
  nodes?: Maybe<Scalars['JSON']>;
};

export type SwarmInitParamsInput = {
  AdvertiseAddr?: Maybe<Scalars['String']>;
  ForceNewCluster?: Maybe<Scalars['Boolean']>;
  ListenAddr?: Maybe<Scalars['String']>;
};

export type SwarmJoinParamsInput = {
  AdvertiseAddr?: Maybe<Scalars['String']>;
  JoinToken?: Maybe<Scalars['String']>;
  ListenAddr?: Maybe<Scalars['String']>;
  RemoteAddrs?: Maybe<Array<Scalars['String']>>;
};

export type User = {
  __typename?: 'User';
  _id: Scalars['String'];
  name: Scalars['String'];
  passHash?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<RoleCore>>;
};

export type UserCore = {
  __typename?: 'UserCore';
  _id: Scalars['String'];
  name: Scalars['String'];
};

export type Vote = {
  __typename?: 'Vote';
  _id?: Maybe<Scalars['String']>;
  children?: Maybe<Array<RequestCore>>;
  request?: Maybe<RequestCore>;
  rid: Scalars['String'];
  settled?: Maybe<Scalars['Boolean']>;
  value: Scalars['Int'];
  voter?: Maybe<Organization>;
};

export type VoteCore = {
  __typename?: 'VoteCore';
  _id?: Maybe<Scalars['String']>;
  rid: Scalars['String'];
  settled?: Maybe<Scalars['Boolean']>;
  value: Scalars['Int'];
  voter?: Maybe<Organization>;
};

export type VoteInput = {
  _id?: Maybe<Scalars['String']>;
  rid: Scalars['String'];
  value: Scalars['Int'];
};

export type InfoFragment = { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined };

export type OrganizationFragment = { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined };

export type UserFragment = { __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined };

export type RoleCoreFragment = { __typename?: 'RoleCore', _id: string, name: string };

export type RoleFragment = { __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined };

export type UserCoreFragment = { __typename?: 'UserCore', _id: string, name: string };

export type ConsortiumOrganizationFragment = { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined };

export type ConsortiumFragment = { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined };

export type ConsortiumRequestFragment = { __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined };

export type ConsortiumApprovalFragment = { __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined };

export type ConsortiumMemberFragment = { __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined };

export type SwarmFragment = { __typename?: 'Swarm', nodes?: any | null | undefined, inspect?: any | null | undefined };

export type ContainerFragment = { __typename?: 'Container', Name: string, Health?: any | null | undefined };

export type ChannelFragment = { __typename?: 'Channel', _id: string, chaincodes?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined };

export type ChaincodeFragment = { __typename?: 'Chaincode', name: string };

export type ChaincodeQueryFragment = { __typename?: 'ChaincodeQuery', installed?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined, instantiated?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined };

export type ActionFragment = { __typename?: 'Action', _id: string, name: string, progress?: number | null | undefined, log?: Array<string | null | undefined> | null | undefined };

export type LobbyFragment = { __typename?: 'Lobby', organization?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined, consortia?: Array<{ __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined }> | null | undefined, requested?: { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined } | null | undefined, joined?: { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined } | null | undefined, member?: { __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined } | null | undefined };

export type GovernanceFragment = { __typename?: 'Governance', request?: { __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined } | null | undefined, requests?: Array<{ __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined }> | null | undefined };

export type RequestFragment = { __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined, parent?: { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined } | null | undefined, children?: Array<{ __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined }> | null | undefined, quorum?: Array<{ __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined }> | null | undefined, votes?: Array<{ __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined }> | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined } | null | undefined };

export type RequestCoreFragment = { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined } | null | undefined };

export type VoteCoreFragment = { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined } | null | undefined };

export type PermissionFragment = { __typename?: 'Permission', _id: string, name: string, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined };

export type DappsFragment = { __typename?: 'Dapps', installable?: Array<{ __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string }> | null | undefined, installed?: Array<{ __typename?: 'InstalledDapp', hostName: string }> | null | undefined };

export type DappFragment = { __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined };

export type PricingFragment = { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined };

export type InstalledDappFragment = { __typename?: 'InstalledDapp', hostName: string, dapp: { __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string } };

export type PluginsFragment = { __typename?: 'Plugins', installable?: Array<{ __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined }> | null | undefined, installed?: Array<{ __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined }> | null | undefined };

export type PluginFragment = { __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined, parameters?: Array<{ __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean }> | null | undefined };

export type FormFieldFragment = { __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean };

export type NotificationFragment = { __typename?: 'Notification', _id: string, system?: string | null | undefined, message: string, url?: string | null | undefined, timestamp?: number | null | undefined };

export type BufferFragment = { __typename?: 'Buffer', type?: string | null | undefined, data?: Array<number | null | undefined> | null | undefined };

export type PermissionCoreFragment = { __typename?: 'PermissionCore', _id: string, name: string };

export type ChaincodeEventFragment = { __typename?: 'ChaincodeEvent', eventName: string, payloadString: string, blockNumber: string, transactionId: string, status: string };

export type ConsortiumLocalFragment = { __typename?: 'ConsortiumLocal', organizations?: Array<{ __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined }> | null | undefined };

export type VoteFragment = { __typename?: 'Vote', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, request?: { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined } | null | undefined, children?: Array<{ __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined }> | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined } | null | undefined };

export type InfoNoNestingFragment = { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined };

export type OrganizationNoNestingFragment = { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined };

export type UserNoNestingFragment = { __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined };

export type RoleCoreNoNestingFragment = { __typename?: 'RoleCore', _id: string, name: string };

export type RoleNoNestingFragment = { __typename?: 'Role', _id: string, name: string };

export type UserCoreNoNestingFragment = { __typename?: 'UserCore', _id: string, name: string };

export type ConsortiumOrganizationNoNestingFragment = { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined };

export type ConsortiumNoNestingFragment = { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined };

export type ConsortiumRequestNoNestingFragment = { __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined };

export type ConsortiumApprovalNoNestingFragment = { __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined };

export type ConsortiumMemberNoNestingFragment = { __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined };

export type SwarmNoNestingFragment = { __typename?: 'Swarm', nodes?: any | null | undefined, inspect?: any | null | undefined };

export type ContainerNoNestingFragment = { __typename?: 'Container', Name: string, Health?: any | null | undefined };

export type ChannelNoNestingFragment = { __typename?: 'Channel', _id: string };

export type ChaincodeNoNestingFragment = { __typename?: 'Chaincode', name: string };

export type ActionNoNestingFragment = { __typename?: 'Action', _id: string, name: string, progress?: number | null | undefined, log?: Array<string | null | undefined> | null | undefined };

export type RequestNoNestingFragment = { __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined };

export type RequestCoreNoNestingFragment = { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined };

export type VoteCoreNoNestingFragment = { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined };

export type PermissionNoNestingFragment = { __typename?: 'Permission', _id: string, name: string };

export type DappNoNestingFragment = { __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string };

export type PricingNoNestingFragment = { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined };

export type InstalledDappNoNestingFragment = { __typename?: 'InstalledDapp', hostName: string };

export type PluginNoNestingFragment = { __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined };

export type FormFieldNoNestingFragment = { __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean };

export type NotificationNoNestingFragment = { __typename?: 'Notification', _id: string, system?: string | null | undefined, message: string, url?: string | null | undefined, timestamp?: number | null | undefined };

export type BufferNoNestingFragment = { __typename?: 'Buffer', type?: string | null | undefined, data?: Array<number | null | undefined> | null | undefined };

export type PermissionCoreNoNestingFragment = { __typename?: 'PermissionCore', _id: string, name: string };

export type ChaincodeEventNoNestingFragment = { __typename?: 'ChaincodeEvent', eventName: string, payloadString: string, blockNumber: string, transactionId: string, status: string };

export type VoteNoNestingFragment = { __typename?: 'Vote', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined };

export type InfoDeepNestingFragment = { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined };

export type OrganizationDeepNestingFragment = { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined };

export type UserDeepNestingFragment = { __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined };

export type RoleCoreDeepNestingFragment = { __typename?: 'RoleCore', _id: string, name: string };

export type RoleDeepNestingFragment = { __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined };

export type UserCoreDeepNestingFragment = { __typename?: 'UserCore', _id: string, name: string };

export type ConsortiumOrganizationDeepNestingFragment = { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined };

export type ConsortiumDeepNestingFragment = { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined };

export type ConsortiumRequestDeepNestingFragment = { __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined };

export type ConsortiumApprovalDeepNestingFragment = { __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined };

export type ConsortiumMemberDeepNestingFragment = { __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined };

export type SwarmDeepNestingFragment = { __typename?: 'Swarm', nodes?: any | null | undefined, inspect?: any | null | undefined };

export type ContainerDeepNestingFragment = { __typename?: 'Container', Name: string, Health?: any | null | undefined };

export type ChannelDeepNestingFragment = { __typename?: 'Channel', _id: string, chaincodes?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined };

export type ChaincodeDeepNestingFragment = { __typename?: 'Chaincode', name: string };

export type ChaincodeQueryDeepNestingFragment = { __typename?: 'ChaincodeQuery', installed?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined, instantiated?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined };

export type ActionDeepNestingFragment = { __typename?: 'Action', _id: string, name: string, progress?: number | null | undefined, log?: Array<string | null | undefined> | null | undefined };

export type LobbyDeepNestingFragment = { __typename?: 'Lobby', organization?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined, consortia?: Array<{ __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined }> | null | undefined, requested?: { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined } | null | undefined, joined?: { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined } | null | undefined, member?: { __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined } | null | undefined };

export type GovernanceDeepNestingFragment = { __typename?: 'Governance', request?: { __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined, parent?: { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined } | null | undefined, children?: Array<{ __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined }> | null | undefined, quorum?: Array<{ __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined }> | null | undefined, votes?: Array<{ __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined }> | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined } | null | undefined, requests?: Array<{ __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined, parent?: { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined } | null | undefined, children?: Array<{ __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined }> | null | undefined, quorum?: Array<{ __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined }> | null | undefined, votes?: Array<{ __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined }> | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined }> | null | undefined };

export type RequestDeepNestingFragment = { __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined, parent?: { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined } | null | undefined, children?: Array<{ __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined }> | null | undefined, quorum?: Array<{ __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined }> | null | undefined, votes?: Array<{ __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined }> | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined };

export type RequestCoreDeepNestingFragment = { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined };

export type VoteCoreDeepNestingFragment = { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined };

export type PermissionDeepNestingFragment = { __typename?: 'Permission', _id: string, name: string, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined };

export type DappsDeepNestingFragment = { __typename?: 'Dapps', installable?: Array<{ __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined }> | null | undefined, installed?: Array<{ __typename?: 'InstalledDapp', hostName: string, dapp: { __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined } }> | null | undefined };

export type DappDeepNestingFragment = { __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined };

export type PricingDeepNestingFragment = { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined };

export type InstalledDappDeepNestingFragment = { __typename?: 'InstalledDapp', hostName: string, dapp: { __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined } };

export type PluginsDeepNestingFragment = { __typename?: 'Plugins', installable?: Array<{ __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined, parameters?: Array<{ __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean }> | null | undefined }> | null | undefined, installed?: Array<{ __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined, parameters?: Array<{ __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean }> | null | undefined }> | null | undefined };

export type PluginDeepNestingFragment = { __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined, parameters?: Array<{ __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean }> | null | undefined };

export type FormFieldDeepNestingFragment = { __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean };

export type NotificationDeepNestingFragment = { __typename?: 'Notification', _id: string, system?: string | null | undefined, message: string, url?: string | null | undefined, timestamp?: number | null | undefined };

export type BufferDeepNestingFragment = { __typename?: 'Buffer', type?: string | null | undefined, data?: Array<number | null | undefined> | null | undefined };

export type PermissionCoreDeepNestingFragment = { __typename?: 'PermissionCore', _id: string, name: string };

export type ChaincodeEventDeepNestingFragment = { __typename?: 'ChaincodeEvent', eventName: string, payloadString: string, blockNumber: string, transactionId: string, status: string };

export type ConsortiumLocalDeepNestingFragment = { __typename?: 'ConsortiumLocal', organizations?: Array<{ __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined }> | null | undefined };

export type VoteDeepNestingFragment = { __typename?: 'Vote', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, request?: { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined } | null | undefined, children?: Array<{ __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined } | null | undefined }> | null | undefined, voter?: { __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined, roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined, info?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined };

export type StatusServiceQueryVariables = Exact<{ [key: string]: never; }>;


export type StatusServiceQuery = { __typename?: 'Query', info?: { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined } | null | undefined };

export type LobbyServiceQueryVariables = Exact<{ [key: string]: never; }>;


export type LobbyServiceQuery = { __typename?: 'Query', info?: { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined } | null | undefined, lobby?: { __typename?: 'Lobby', consortia?: Array<{ __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined }> | null | undefined, member?: { __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined } | null | undefined, requested?: { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined } | null | undefined, joined?: { __typename?: 'Consortium', cid: string, name: string, desc?: string | null | undefined, founder?: string | null | undefined, requests?: Array<{ __typename?: 'ConsortiumRequest', _id: string, cid: string, rpub: string, sig: string, confirmed?: boolean | null | undefined }> | null | undefined, approvals?: Array<{ __typename?: 'ConsortiumApproval', _id: string, cid: string, rpub?: string | null | undefined, pub?: string | null | undefined, sig?: string | null | undefined }> | null | undefined, members?: Array<{ __typename?: 'ConsortiumMember', _id: string, cid: string, pub?: string | null | undefined, apub?: string | null | undefined, token?: string | null | undefined }> | null | undefined } | null | undefined } | null | undefined };

export type SwarmPageQueryVariables = Exact<{ [key: string]: never; }>;


export type SwarmPageQuery = { __typename?: 'Query', info?: { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined } | null | undefined, swarm?: { __typename?: 'Swarm', nodes?: any | null | undefined, inspect?: any | null | undefined } | null | undefined };

export type OrganizationsPageQueryVariables = Exact<{ [key: string]: never; }>;


export type OrganizationsPageQuery = { __typename?: 'Query', organizations?: Array<{ __typename?: 'Organization', name: string, publicKey?: string | null | undefined }> | null | undefined };

export type RequestsPageQueryVariables = Exact<{ [key: string]: never; }>;


export type RequestsPageQuery = { __typename?: 'Query', info?: { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined } | null | undefined, gov?: { __typename?: 'Governance', requests?: Array<{ __typename?: 'Request', _id: string, type: string, key: string, subkey: string, settled?: boolean | null | undefined, parent?: { __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined } | null | undefined, children?: Array<{ __typename?: 'RequestCore', _id: string, type: string, key: string, subkey: string, parent?: string | null | undefined, children?: Array<string> | null | undefined, quorum?: Array<string> | null | undefined, settled?: boolean | null | undefined }> | null | undefined, quorum?: Array<{ __typename?: 'Organization', _id: string, name: string, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined }> | null | undefined, votes?: Array<{ __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined }> | null | undefined, ownvote?: { __typename?: 'VoteCore', _id?: string | null | undefined, rid: string, value: number, settled?: boolean | null | undefined } | null | undefined }> | null | undefined } | null | undefined };

export type ChannelsPageQueryVariables = Exact<{ [key: string]: never; }>;


export type ChannelsPageQuery = { __typename?: 'Query', channels?: Array<{ __typename?: 'Channel', _id: string, chaincodes?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined }> | null | undefined };

export type ContainersPageQueryVariables = Exact<{ [key: string]: never; }>;


export type ContainersPageQuery = { __typename?: 'Query', containers?: Array<{ __typename?: 'Container', Name: string, Health?: any | null | undefined }> | null | undefined };

export type ChaincodePageQueryVariables = Exact<{ [key: string]: never; }>;


export type ChaincodePageQuery = { __typename?: 'Query', chaincode?: { __typename?: 'ChaincodeQuery', installed?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined, instantiated?: Array<{ __typename?: 'Chaincode', name: string }> | null | undefined } | null | undefined };

export type StorePageQueryVariables = Exact<{ [key: string]: never; }>;


export type StorePageQuery = { __typename?: 'Query', dapps?: { __typename?: 'Dapps', installable?: Array<{ __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined }> | null | undefined } | null | undefined };

export type DappsPageQueryVariables = Exact<{ [key: string]: never; }>;


export type DappsPageQuery = { __typename?: 'Query', dapps?: { __typename?: 'Dapps', installed?: Array<{ __typename?: 'InstalledDapp', hostName: string, dapp: { __typename?: 'Dapp', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, dockerImage: string } }> | null | undefined } | null | undefined };

export type PluginsPageQueryVariables = Exact<{ [key: string]: never; }>;


export type PluginsPageQuery = { __typename?: 'Query', plugins?: { __typename?: 'Plugins', installable?: Array<{ __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined, parameters?: Array<{ __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean }> | null | undefined }> | null | undefined, installed?: Array<{ __typename?: 'Plugin', _id?: string | null | undefined, title: string, version: string, imageId?: string | null | undefined, installed?: boolean | null | undefined, pricing?: { __typename?: 'Pricing', currency: string, initialPrice?: number | null | undefined, pricePerMonth?: number | null | undefined, paymentInfo?: string | null | undefined } | null | undefined, parameters?: Array<{ __typename?: 'FormField', type: string, name: string, label: string, value: string, required: boolean }> | null | undefined }> | null | undefined } | null | undefined };

export type ActionsPageQueryVariables = Exact<{ [key: string]: never; }>;


export type ActionsPageQuery = { __typename?: 'Query', actions?: Array<{ __typename?: 'Action', _id: string, name: string, progress?: number | null | undefined, log?: Array<string | null | undefined> | null | undefined }> | null | undefined };

export type OrganizationComponentQueryVariables = Exact<{
  pub?: Maybe<Scalars['String']>;
}>;


export type OrganizationComponentQuery = { __typename?: 'Query', lobby?: { __typename?: 'Lobby', organization?: { __typename?: 'ConsortiumOrganization', pub: string, name: string, about?: string | null | undefined, imageId?: string | null | undefined, founder?: string | null | undefined, email?: string | null | undefined } | null | undefined } | null | undefined };

export type ListUsersQueryVariables = Exact<{ [key: string]: never; }>;


export type ListUsersQuery = { __typename?: 'Query', users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined };

export type QueryUserQueryVariables = Exact<{
  _id: Scalars['String'];
}>;


export type QueryUserQuery = { __typename?: 'Query', users?: Array<{ __typename?: 'User', _id: string, name: string, passHash?: string | null | undefined, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined };

export type ReplayNotificationsQueryVariables = Exact<{ [key: string]: never; }>;


export type ReplayNotificationsQuery = { __typename?: 'Query', replayNotifications?: Array<{ __typename?: 'Notification', _id: string, system?: string | null | undefined, message: string, url?: string | null | undefined, timestamp?: number | null | undefined }> | null | undefined };

export type NotificationSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type NotificationSubscription = { __typename?: 'Subscription', notification?: { __typename?: 'Notification', _id: string, system?: string | null | undefined, message: string, url?: string | null | undefined, timestamp?: number | null | undefined } | null | undefined };

export type StatusUpdateSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type StatusUpdateSubscription = { __typename?: 'Subscription', statusUpdate?: { __typename?: 'Info', consDomain?: string | null | undefined, orgName?: string | null | undefined, channelName?: string | null | undefined, publicIpV4?: string | null | undefined, publicKey?: string | null | undefined, isAdminMode?: boolean | null | undefined, isJoined?: boolean | null | undefined } | null | undefined };

export type LoginUserMutationVariables = Exact<{
  _id: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginUserMutation = { __typename?: 'Mutation', loginUser?: string | null | undefined };

export type PutUserMutationVariables = Exact<{
  _id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  password?: Maybe<Scalars['String']>;
}>;


export type PutUserMutation = { __typename?: 'Mutation', putUser?: { __typename?: 'UserCore', _id: string, name: string } | null | undefined };

export type RemoveUserMutationVariables = Exact<{
  _id: Scalars['String'];
}>;


export type RemoveUserMutation = { __typename?: 'Mutation', removeUser?: boolean | null | undefined };

export type ListRolesQueryVariables = Exact<{ [key: string]: never; }>;


export type ListRolesQuery = { __typename?: 'Query', roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined };

export type QueryRoleQueryVariables = Exact<{
  _id: Scalars['String'];
}>;


export type QueryRoleQuery = { __typename?: 'Query', roles?: Array<{ __typename?: 'Role', _id: string, name: string, users?: Array<{ __typename?: 'UserCore', _id: string, name: string }> | null | undefined }> | null | undefined };

export type PutRoleMutationVariables = Exact<{
  _id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
}>;


export type PutRoleMutation = { __typename?: 'Mutation', putRole?: { __typename?: 'RoleCore', _id: string, name: string } | null | undefined };

export type RemoveRoleMutationVariables = Exact<{
  _id: Scalars['String'];
}>;


export type RemoveRoleMutation = { __typename?: 'Mutation', removeRole?: boolean | null | undefined };

export type AttachRoleToUserMutationVariables = Exact<{
  roleId: Scalars['String'];
  userId: Scalars['String'];
}>;


export type AttachRoleToUserMutation = { __typename?: 'Mutation', attachRoleToUser?: boolean | null | undefined };

export type RemoveRoleFromUserMutationVariables = Exact<{
  roleId: Scalars['String'];
  userId: Scalars['String'];
}>;


export type RemoveRoleFromUserMutation = { __typename?: 'Mutation', removeRoleFromUser?: boolean | null | undefined };

export type ListPermissionsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListPermissionsQuery = { __typename?: 'Query', permissions?: Array<{ __typename?: 'Permission', _id: string, name: string, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined };

export type QueryPermissionQueryVariables = Exact<{
  _id: Scalars['String'];
}>;


export type QueryPermissionQuery = { __typename?: 'Query', permissions?: Array<{ __typename?: 'Permission', _id: string, name: string, roles?: Array<{ __typename?: 'RoleCore', _id: string, name: string }> | null | undefined }> | null | undefined };

export type PutPermissionMutationVariables = Exact<{
  _id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
}>;


export type PutPermissionMutation = { __typename?: 'Mutation', putPermission?: { __typename?: 'PermissionCore', _id: string, name: string } | null | undefined };

export type RemovePermissionMutationVariables = Exact<{
  _id: Scalars['String'];
}>;


export type RemovePermissionMutation = { __typename?: 'Mutation', removePermission?: boolean | null | undefined };

export type AttachPermissionToRoleMutationVariables = Exact<{
  permId: Scalars['String'];
  roleId: Scalars['String'];
}>;


export type AttachPermissionToRoleMutation = { __typename?: 'Mutation', attachPermToRole?: boolean | null | undefined };

export type RemovePermFromRoleMutationVariables = Exact<{
  permId: Scalars['String'];
  roleId: Scalars['String'];
}>;


export type RemovePermFromRoleMutation = { __typename?: 'Mutation', removePermFromRole?: boolean | null | undefined };

export type RequestMutationVariables = Exact<{
  request?: Maybe<RequestInput>;
}>;


export type RequestMutation = { __typename?: 'Mutation', request?: boolean | null | undefined };

export type RequestBroadcastMutationVariables = Exact<{
  request?: Maybe<RequestInput>;
}>;


export type RequestBroadcastMutation = { __typename?: 'Mutation', requestBroadcast?: boolean | null | undefined };

export type VoteMutationVariables = Exact<{
  vote?: Maybe<VoteInput>;
}>;


export type VoteMutation = { __typename?: 'Mutation', vote?: number | null | undefined };

export type LobbyRequestAddMutationVariables = Exact<{
  cid: Scalars['String'];
}>;


export type LobbyRequestAddMutation = { __typename?: 'Mutation', lobbyRequestAdd?: boolean | null | undefined };

export type LobbyRequestRemoveMutationVariables = Exact<{
  cid: Scalars['String'];
}>;


export type LobbyRequestRemoveMutation = { __typename?: 'Mutation', lobbyRequestRemove?: boolean | null | undefined };

export type LobbyOrganizationPutMutationVariables = Exact<{
  info?: Maybe<ConsortiumOrganizationInput>;
}>;


export type LobbyOrganizationPutMutation = { __typename?: 'Mutation', lobbyOrganizationPut?: boolean | null | undefined };

export type LobbyOrganizationRemoveMutationVariables = Exact<{ [key: string]: never; }>;


export type LobbyOrganizationRemoveMutation = { __typename?: 'Mutation', lobbyOrganizationRemove?: boolean | null | undefined };

export type CreateConsortiumMutationVariables = Exact<{
  consId: Scalars['String'];
  consDomain: Scalars['String'];
  consDesc: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
}>;


export type CreateConsortiumMutation = { __typename?: 'Mutation', swarmInit?: boolean | null | undefined, networkInit?: boolean | null | undefined, lobbyConsortiaAdd?: boolean | null | undefined, lobbyConsortiumSet?: boolean | null | undefined };

export type JoinConsortiumMutationVariables = Exact<{
  crypted: Scalars['String'];
  opub: Scalars['String'];
  consId: Scalars['String'];
  consDomain: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
}>;


export type JoinConsortiumMutation = { __typename?: 'Mutation', swarmJoinCrypted?: boolean | null | undefined, networkExtend?: boolean | null | undefined, lobbyConsortiumSet?: boolean | null | undefined, syncRequests?: boolean | null | undefined };

export type InstallDappMutationVariables = Exact<{
  dapp?: Maybe<DappManualInstallInput>;
}>;


export type InstallDappMutation = { __typename?: 'Mutation', installDapp?: boolean | null | undefined };

export type QueryChaincodeQueryVariables = Exact<{
  chaincode?: Maybe<Scalars['String']>;
  method?: Maybe<Scalars['String']>;
  args?: Maybe<Array<Maybe<Scalars['String']>> | Maybe<Scalars['String']>>;
}>;


export type QueryChaincodeQuery = { __typename?: 'Query', chaincodeQuery?: { __typename?: 'Buffer', type?: string | null | undefined, data?: Array<number | null | undefined> | null | undefined } | null | undefined };

export const InfoFragmentDoc = gql`
    fragment Info on Info {
  consDomain
  orgName
  channelName
  publicIpV4
  publicKey
  isAdminMode
  isJoined
}
    `;
export const UserNoNestingFragmentDoc = gql`
    fragment UserNoNesting on User {
  _id
  name
  passHash
}
    `;
export const RoleNoNestingFragmentDoc = gql`
    fragment RoleNoNesting on Role {
  _id
  name
}
    `;
export const ConsortiumOrganizationNoNestingFragmentDoc = gql`
    fragment ConsortiumOrganizationNoNesting on ConsortiumOrganization {
  pub
  name
  about
  imageId
  founder
  email
}
    `;
export const OrganizationFragmentDoc = gql`
    fragment Organization on Organization {
  _id
  name
  publicIpV4
  publicKey
  users {
    ...UserNoNesting
  }
  roles {
    ...RoleNoNesting
  }
  info {
    ...ConsortiumOrganizationNoNesting
  }
}
    ${UserNoNestingFragmentDoc}
${RoleNoNestingFragmentDoc}
${ConsortiumOrganizationNoNestingFragmentDoc}`;
export const RoleCoreNoNestingFragmentDoc = gql`
    fragment RoleCoreNoNesting on RoleCore {
  _id
  name
}
    `;
export const UserFragmentDoc = gql`
    fragment User on User {
  _id
  name
  passHash
  roles {
    ...RoleCoreNoNesting
  }
}
    ${RoleCoreNoNestingFragmentDoc}`;
export const RoleCoreFragmentDoc = gql`
    fragment RoleCore on RoleCore {
  _id
  name
}
    `;
export const UserCoreNoNestingFragmentDoc = gql`
    fragment UserCoreNoNesting on UserCore {
  _id
  name
}
    `;
export const RoleFragmentDoc = gql`
    fragment Role on Role {
  _id
  name
  users {
    ...UserCoreNoNesting
  }
}
    ${UserCoreNoNestingFragmentDoc}`;
export const UserCoreFragmentDoc = gql`
    fragment UserCore on UserCore {
  _id
  name
}
    `;
export const ConsortiumOrganizationFragmentDoc = gql`
    fragment ConsortiumOrganization on ConsortiumOrganization {
  pub
  name
  about
  imageId
  founder
  email
}
    `;
export const ConsortiumRequestNoNestingFragmentDoc = gql`
    fragment ConsortiumRequestNoNesting on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
}
    `;
export const ConsortiumApprovalNoNestingFragmentDoc = gql`
    fragment ConsortiumApprovalNoNesting on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumMemberNoNestingFragmentDoc = gql`
    fragment ConsortiumMemberNoNesting on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const ConsortiumFragmentDoc = gql`
    fragment Consortium on Consortium {
  cid
  name
  desc
  founder
  requests {
    ...ConsortiumRequestNoNesting
  }
  approvals {
    ...ConsortiumApprovalNoNesting
  }
  members {
    ...ConsortiumMemberNoNesting
  }
}
    ${ConsortiumRequestNoNestingFragmentDoc}
${ConsortiumApprovalNoNestingFragmentDoc}
${ConsortiumMemberNoNestingFragmentDoc}`;
export const ConsortiumRequestFragmentDoc = gql`
    fragment ConsortiumRequest on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
  approvals {
    ...ConsortiumApprovalNoNesting
  }
}
    ${ConsortiumApprovalNoNestingFragmentDoc}`;
export const ConsortiumApprovalFragmentDoc = gql`
    fragment ConsortiumApproval on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumMemberFragmentDoc = gql`
    fragment ConsortiumMember on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const SwarmFragmentDoc = gql`
    fragment Swarm on Swarm {
  nodes
  inspect
}
    `;
export const ContainerFragmentDoc = gql`
    fragment Container on Container {
  Name
  Health
}
    `;
export const ChaincodeNoNestingFragmentDoc = gql`
    fragment ChaincodeNoNesting on Chaincode {
  name
}
    `;
export const ChannelFragmentDoc = gql`
    fragment Channel on Channel {
  _id
  chaincodes {
    ...ChaincodeNoNesting
  }
}
    ${ChaincodeNoNestingFragmentDoc}`;
export const ChaincodeFragmentDoc = gql`
    fragment Chaincode on Chaincode {
  name
}
    `;
export const ChaincodeQueryFragmentDoc = gql`
    fragment ChaincodeQuery on ChaincodeQuery {
  installed {
    ...ChaincodeNoNesting
  }
  instantiated {
    ...ChaincodeNoNesting
  }
}
    ${ChaincodeNoNestingFragmentDoc}`;
export const ActionFragmentDoc = gql`
    fragment Action on Action {
  _id
  name
  progress
  log
}
    `;
export const ConsortiumNoNestingFragmentDoc = gql`
    fragment ConsortiumNoNesting on Consortium {
  cid
  name
  desc
  founder
}
    `;
export const LobbyFragmentDoc = gql`
    fragment Lobby on Lobby {
  organization {
    ...ConsortiumOrganizationNoNesting
  }
  consortia {
    ...ConsortiumNoNesting
  }
  requested {
    ...ConsortiumNoNesting
  }
  joined {
    ...ConsortiumNoNesting
  }
  member {
    ...ConsortiumMemberNoNesting
  }
}
    ${ConsortiumOrganizationNoNestingFragmentDoc}
${ConsortiumNoNestingFragmentDoc}
${ConsortiumMemberNoNestingFragmentDoc}`;
export const RequestNoNestingFragmentDoc = gql`
    fragment RequestNoNesting on Request {
  _id
  type
  key
  subkey
  settled
}
    `;
export const GovernanceFragmentDoc = gql`
    fragment Governance on Governance {
  request {
    ...RequestNoNesting
  }
  requests {
    ...RequestNoNesting
  }
}
    ${RequestNoNestingFragmentDoc}`;
export const RequestCoreNoNestingFragmentDoc = gql`
    fragment RequestCoreNoNesting on RequestCore {
  _id
  type
  key
  subkey
  parent
  children
  quorum
  settled
}
    `;
export const OrganizationNoNestingFragmentDoc = gql`
    fragment OrganizationNoNesting on Organization {
  _id
  name
  publicIpV4
  publicKey
}
    `;
export const VoteCoreNoNestingFragmentDoc = gql`
    fragment VoteCoreNoNesting on VoteCore {
  _id
  rid
  value
  settled
}
    `;
export const RequestFragmentDoc = gql`
    fragment Request on Request {
  _id
  type
  key
  subkey
  parent {
    ...RequestCoreNoNesting
  }
  children {
    ...RequestCoreNoNesting
  }
  quorum {
    ...OrganizationNoNesting
  }
  votes {
    ...VoteCoreNoNesting
  }
  ownvote {
    ...VoteCoreNoNesting
  }
  settled
}
    ${RequestCoreNoNestingFragmentDoc}
${OrganizationNoNestingFragmentDoc}
${VoteCoreNoNestingFragmentDoc}`;
export const RequestCoreFragmentDoc = gql`
    fragment RequestCore on RequestCore {
  _id
  type
  key
  subkey
  parent
  children
  quorum
  ownvote {
    ...VoteCoreNoNesting
  }
  settled
}
    ${VoteCoreNoNestingFragmentDoc}`;
export const VoteCoreFragmentDoc = gql`
    fragment VoteCore on VoteCore {
  _id
  rid
  value
  settled
  voter {
    ...OrganizationNoNesting
  }
}
    ${OrganizationNoNestingFragmentDoc}`;
export const PermissionFragmentDoc = gql`
    fragment Permission on Permission {
  _id
  name
  roles {
    ...RoleCoreNoNesting
  }
}
    ${RoleCoreNoNestingFragmentDoc}`;
export const DappNoNestingFragmentDoc = gql`
    fragment DappNoNesting on Dapp {
  _id
  title
  version
  imageId
  dockerImage
}
    `;
export const InstalledDappNoNestingFragmentDoc = gql`
    fragment InstalledDappNoNesting on InstalledDapp {
  hostName
}
    `;
export const DappsFragmentDoc = gql`
    fragment Dapps on Dapps {
  installable {
    ...DappNoNesting
  }
  installed {
    ...InstalledDappNoNesting
  }
}
    ${DappNoNestingFragmentDoc}
${InstalledDappNoNestingFragmentDoc}`;
export const PricingNoNestingFragmentDoc = gql`
    fragment PricingNoNesting on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const DappFragmentDoc = gql`
    fragment Dapp on Dapp {
  _id
  title
  version
  imageId
  dockerImage
  pricing {
    ...PricingNoNesting
  }
}
    ${PricingNoNestingFragmentDoc}`;
export const PricingFragmentDoc = gql`
    fragment Pricing on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const InstalledDappFragmentDoc = gql`
    fragment InstalledDapp on InstalledDapp {
  hostName
  dapp {
    ...DappNoNesting
  }
}
    ${DappNoNestingFragmentDoc}`;
export const PluginNoNestingFragmentDoc = gql`
    fragment PluginNoNesting on Plugin {
  _id
  title
  version
  imageId
  installed
}
    `;
export const PluginsFragmentDoc = gql`
    fragment Plugins on Plugins {
  installable {
    ...PluginNoNesting
  }
  installed {
    ...PluginNoNesting
  }
}
    ${PluginNoNestingFragmentDoc}`;
export const FormFieldNoNestingFragmentDoc = gql`
    fragment FormFieldNoNesting on FormField {
  type
  name
  label
  value
  required
}
    `;
export const PluginFragmentDoc = gql`
    fragment Plugin on Plugin {
  _id
  title
  version
  imageId
  pricing {
    ...PricingNoNesting
  }
  parameters {
    ...FormFieldNoNesting
  }
  installed
}
    ${PricingNoNestingFragmentDoc}
${FormFieldNoNestingFragmentDoc}`;
export const FormFieldFragmentDoc = gql`
    fragment FormField on FormField {
  type
  name
  label
  value
  required
}
    `;
export const NotificationFragmentDoc = gql`
    fragment Notification on Notification {
  _id
  system
  message
  url
  timestamp
}
    `;
export const BufferFragmentDoc = gql`
    fragment Buffer on Buffer {
  type
  data
}
    `;
export const PermissionCoreFragmentDoc = gql`
    fragment PermissionCore on PermissionCore {
  _id
  name
}
    `;
export const ChaincodeEventFragmentDoc = gql`
    fragment ChaincodeEvent on ChaincodeEvent {
  eventName
  payloadString
  blockNumber
  transactionId
  status
}
    `;
export const ConsortiumLocalFragmentDoc = gql`
    fragment ConsortiumLocal on ConsortiumLocal {
  organizations {
    ...OrganizationNoNesting
  }
}
    ${OrganizationNoNestingFragmentDoc}`;
export const VoteFragmentDoc = gql`
    fragment Vote on Vote {
  _id
  rid
  value
  request {
    ...RequestCoreNoNesting
  }
  children {
    ...RequestCoreNoNesting
  }
  settled
  voter {
    ...OrganizationNoNesting
  }
}
    ${RequestCoreNoNestingFragmentDoc}
${OrganizationNoNestingFragmentDoc}`;
export const InfoNoNestingFragmentDoc = gql`
    fragment InfoNoNesting on Info {
  consDomain
  orgName
  channelName
  publicIpV4
  publicKey
  isAdminMode
  isJoined
}
    `;
export const SwarmNoNestingFragmentDoc = gql`
    fragment SwarmNoNesting on Swarm {
  nodes
  inspect
}
    `;
export const ContainerNoNestingFragmentDoc = gql`
    fragment ContainerNoNesting on Container {
  Name
  Health
}
    `;
export const ChannelNoNestingFragmentDoc = gql`
    fragment ChannelNoNesting on Channel {
  _id
}
    `;
export const ActionNoNestingFragmentDoc = gql`
    fragment ActionNoNesting on Action {
  _id
  name
  progress
  log
}
    `;
export const PermissionNoNestingFragmentDoc = gql`
    fragment PermissionNoNesting on Permission {
  _id
  name
}
    `;
export const NotificationNoNestingFragmentDoc = gql`
    fragment NotificationNoNesting on Notification {
  _id
  system
  message
  url
  timestamp
}
    `;
export const BufferNoNestingFragmentDoc = gql`
    fragment BufferNoNesting on Buffer {
  type
  data
}
    `;
export const PermissionCoreNoNestingFragmentDoc = gql`
    fragment PermissionCoreNoNesting on PermissionCore {
  _id
  name
}
    `;
export const ChaincodeEventNoNestingFragmentDoc = gql`
    fragment ChaincodeEventNoNesting on ChaincodeEvent {
  eventName
  payloadString
  blockNumber
  transactionId
  status
}
    `;
export const VoteNoNestingFragmentDoc = gql`
    fragment VoteNoNesting on Vote {
  _id
  rid
  value
  settled
}
    `;
export const InfoDeepNestingFragmentDoc = gql`
    fragment InfoDeepNesting on Info {
  consDomain
  orgName
  channelName
  publicIpV4
  publicKey
  isAdminMode
  isJoined
}
    `;
export const SwarmDeepNestingFragmentDoc = gql`
    fragment SwarmDeepNesting on Swarm {
  nodes
  inspect
}
    `;
export const ContainerDeepNestingFragmentDoc = gql`
    fragment ContainerDeepNesting on Container {
  Name
  Health
}
    `;
export const ChaincodeDeepNestingFragmentDoc = gql`
    fragment ChaincodeDeepNesting on Chaincode {
  name
}
    `;
export const ChannelDeepNestingFragmentDoc = gql`
    fragment ChannelDeepNesting on Channel {
  _id
  chaincodes {
    ...ChaincodeDeepNesting
  }
}
    ${ChaincodeDeepNestingFragmentDoc}`;
export const ChaincodeQueryDeepNestingFragmentDoc = gql`
    fragment ChaincodeQueryDeepNesting on ChaincodeQuery {
  installed {
    ...ChaincodeDeepNesting
  }
  instantiated {
    ...ChaincodeDeepNesting
  }
}
    ${ChaincodeDeepNestingFragmentDoc}`;
export const ActionDeepNestingFragmentDoc = gql`
    fragment ActionDeepNesting on Action {
  _id
  name
  progress
  log
}
    `;
export const ConsortiumOrganizationDeepNestingFragmentDoc = gql`
    fragment ConsortiumOrganizationDeepNesting on ConsortiumOrganization {
  pub
  name
  about
  imageId
  founder
  email
}
    `;
export const ConsortiumApprovalDeepNestingFragmentDoc = gql`
    fragment ConsortiumApprovalDeepNesting on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumRequestDeepNestingFragmentDoc = gql`
    fragment ConsortiumRequestDeepNesting on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
  approvals {
    ...ConsortiumApprovalDeepNesting
  }
}
    ${ConsortiumApprovalDeepNestingFragmentDoc}`;
export const ConsortiumMemberDeepNestingFragmentDoc = gql`
    fragment ConsortiumMemberDeepNesting on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const ConsortiumDeepNestingFragmentDoc = gql`
    fragment ConsortiumDeepNesting on Consortium {
  cid
  name
  desc
  founder
  requests {
    ...ConsortiumRequestDeepNesting
  }
  approvals {
    ...ConsortiumApprovalDeepNesting
  }
  members {
    ...ConsortiumMemberDeepNesting
  }
}
    ${ConsortiumRequestDeepNestingFragmentDoc}
${ConsortiumApprovalDeepNestingFragmentDoc}
${ConsortiumMemberDeepNestingFragmentDoc}`;
export const LobbyDeepNestingFragmentDoc = gql`
    fragment LobbyDeepNesting on Lobby {
  organization {
    ...ConsortiumOrganizationDeepNesting
  }
  consortia {
    ...ConsortiumDeepNesting
  }
  requested {
    ...ConsortiumDeepNesting
  }
  joined {
    ...ConsortiumDeepNesting
  }
  member {
    ...ConsortiumMemberDeepNesting
  }
}
    ${ConsortiumOrganizationDeepNestingFragmentDoc}
${ConsortiumDeepNestingFragmentDoc}
${ConsortiumMemberDeepNestingFragmentDoc}`;
export const RoleCoreDeepNestingFragmentDoc = gql`
    fragment RoleCoreDeepNesting on RoleCore {
  _id
  name
}
    `;
export const UserDeepNestingFragmentDoc = gql`
    fragment UserDeepNesting on User {
  _id
  name
  passHash
  roles {
    ...RoleCoreDeepNesting
  }
}
    ${RoleCoreDeepNestingFragmentDoc}`;
export const UserCoreDeepNestingFragmentDoc = gql`
    fragment UserCoreDeepNesting on UserCore {
  _id
  name
}
    `;
export const RoleDeepNestingFragmentDoc = gql`
    fragment RoleDeepNesting on Role {
  _id
  name
  users {
    ...UserCoreDeepNesting
  }
}
    ${UserCoreDeepNestingFragmentDoc}`;
export const OrganizationDeepNestingFragmentDoc = gql`
    fragment OrganizationDeepNesting on Organization {
  _id
  name
  publicIpV4
  publicKey
  users {
    ...UserDeepNesting
  }
  roles {
    ...RoleDeepNesting
  }
  info {
    ...ConsortiumOrganizationDeepNesting
  }
}
    ${UserDeepNestingFragmentDoc}
${RoleDeepNestingFragmentDoc}
${ConsortiumOrganizationDeepNestingFragmentDoc}`;
export const VoteCoreDeepNestingFragmentDoc = gql`
    fragment VoteCoreDeepNesting on VoteCore {
  _id
  rid
  value
  settled
  voter {
    ...OrganizationDeepNesting
  }
}
    ${OrganizationDeepNestingFragmentDoc}`;
export const RequestCoreDeepNestingFragmentDoc = gql`
    fragment RequestCoreDeepNesting on RequestCore {
  _id
  type
  key
  subkey
  parent
  children
  quorum
  ownvote {
    ...VoteCoreDeepNesting
  }
  settled
}
    ${VoteCoreDeepNestingFragmentDoc}`;
export const RequestDeepNestingFragmentDoc = gql`
    fragment RequestDeepNesting on Request {
  _id
  type
  key
  subkey
  parent {
    ...RequestCoreDeepNesting
  }
  children {
    ...RequestCoreDeepNesting
  }
  quorum {
    ...OrganizationDeepNesting
  }
  votes {
    ...VoteCoreDeepNesting
  }
  ownvote {
    ...VoteCoreDeepNesting
  }
  settled
}
    ${RequestCoreDeepNestingFragmentDoc}
${OrganizationDeepNestingFragmentDoc}
${VoteCoreDeepNestingFragmentDoc}`;
export const GovernanceDeepNestingFragmentDoc = gql`
    fragment GovernanceDeepNesting on Governance {
  request {
    ...RequestDeepNesting
  }
  requests {
    ...RequestDeepNesting
  }
}
    ${RequestDeepNestingFragmentDoc}`;
export const PermissionDeepNestingFragmentDoc = gql`
    fragment PermissionDeepNesting on Permission {
  _id
  name
  roles {
    ...RoleCoreDeepNesting
  }
}
    ${RoleCoreDeepNestingFragmentDoc}`;
export const PricingDeepNestingFragmentDoc = gql`
    fragment PricingDeepNesting on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const DappDeepNestingFragmentDoc = gql`
    fragment DappDeepNesting on Dapp {
  _id
  title
  version
  imageId
  dockerImage
  pricing {
    ...PricingDeepNesting
  }
}
    ${PricingDeepNestingFragmentDoc}`;
export const InstalledDappDeepNestingFragmentDoc = gql`
    fragment InstalledDappDeepNesting on InstalledDapp {
  hostName
  dapp {
    ...DappDeepNesting
  }
}
    ${DappDeepNestingFragmentDoc}`;
export const DappsDeepNestingFragmentDoc = gql`
    fragment DappsDeepNesting on Dapps {
  installable {
    ...DappDeepNesting
  }
  installed {
    ...InstalledDappDeepNesting
  }
}
    ${DappDeepNestingFragmentDoc}
${InstalledDappDeepNestingFragmentDoc}`;
export const FormFieldDeepNestingFragmentDoc = gql`
    fragment FormFieldDeepNesting on FormField {
  type
  name
  label
  value
  required
}
    `;
export const PluginDeepNestingFragmentDoc = gql`
    fragment PluginDeepNesting on Plugin {
  _id
  title
  version
  imageId
  pricing {
    ...PricingDeepNesting
  }
  parameters {
    ...FormFieldDeepNesting
  }
  installed
}
    ${PricingDeepNestingFragmentDoc}
${FormFieldDeepNestingFragmentDoc}`;
export const PluginsDeepNestingFragmentDoc = gql`
    fragment PluginsDeepNesting on Plugins {
  installable {
    ...PluginDeepNesting
  }
  installed {
    ...PluginDeepNesting
  }
}
    ${PluginDeepNestingFragmentDoc}`;
export const NotificationDeepNestingFragmentDoc = gql`
    fragment NotificationDeepNesting on Notification {
  _id
  system
  message
  url
  timestamp
}
    `;
export const BufferDeepNestingFragmentDoc = gql`
    fragment BufferDeepNesting on Buffer {
  type
  data
}
    `;
export const PermissionCoreDeepNestingFragmentDoc = gql`
    fragment PermissionCoreDeepNesting on PermissionCore {
  _id
  name
}
    `;
export const ChaincodeEventDeepNestingFragmentDoc = gql`
    fragment ChaincodeEventDeepNesting on ChaincodeEvent {
  eventName
  payloadString
  blockNumber
  transactionId
  status
}
    `;
export const ConsortiumLocalDeepNestingFragmentDoc = gql`
    fragment ConsortiumLocalDeepNesting on ConsortiumLocal {
  organizations {
    ...OrganizationDeepNesting
  }
}
    ${OrganizationDeepNestingFragmentDoc}`;
export const VoteDeepNestingFragmentDoc = gql`
    fragment VoteDeepNesting on Vote {
  _id
  rid
  value
  request {
    ...RequestCoreDeepNesting
  }
  children {
    ...RequestCoreDeepNesting
  }
  settled
  voter {
    ...OrganizationDeepNesting
  }
}
    ${RequestCoreDeepNestingFragmentDoc}
${OrganizationDeepNestingFragmentDoc}`;
export const StatusServiceDocument = gql`
    query StatusService {
  info {
    ...Info
  }
}
    ${InfoFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class StatusServiceGQL extends Apollo.Query<StatusServiceQuery, StatusServiceQueryVariables> {
    document = StatusServiceDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LobbyServiceDocument = gql`
    query LobbyService {
  info {
    ...Info
  }
  lobby {
    consortia {
      ...Consortium
    }
    member {
      ...ConsortiumMember
    }
    requested {
      ...Consortium
    }
    joined {
      ...Consortium
    }
  }
}
    ${InfoFragmentDoc}
${ConsortiumFragmentDoc}
${ConsortiumMemberFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class LobbyServiceGQL extends Apollo.Query<LobbyServiceQuery, LobbyServiceQueryVariables> {
    document = LobbyServiceDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const SwarmPageDocument = gql`
    query SwarmPage {
  info {
    ...Info
  }
  swarm {
    nodes
    inspect
  }
}
    ${InfoFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class SwarmPageGQL extends Apollo.Query<SwarmPageQuery, SwarmPageQueryVariables> {
    document = SwarmPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const OrganizationsPageDocument = gql`
    query OrganizationsPage {
  organizations {
    name
    publicKey
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class OrganizationsPageGQL extends Apollo.Query<OrganizationsPageQuery, OrganizationsPageQueryVariables> {
    document = OrganizationsPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RequestsPageDocument = gql`
    query RequestsPage {
  info {
    ...Info
  }
  gov {
    requests {
      ...Request
    }
  }
}
    ${InfoFragmentDoc}
${RequestFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class RequestsPageGQL extends Apollo.Query<RequestsPageQuery, RequestsPageQueryVariables> {
    document = RequestsPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ChannelsPageDocument = gql`
    query ChannelsPage {
  channels {
    ...Channel
  }
}
    ${ChannelFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ChannelsPageGQL extends Apollo.Query<ChannelsPageQuery, ChannelsPageQueryVariables> {
    document = ChannelsPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ContainersPageDocument = gql`
    query ContainersPage {
  containers {
    ...Container
  }
}
    ${ContainerFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ContainersPageGQL extends Apollo.Query<ContainersPageQuery, ContainersPageQueryVariables> {
    document = ContainersPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ChaincodePageDocument = gql`
    query ChaincodePage {
  chaincode {
    ...ChaincodeQuery
  }
}
    ${ChaincodeQueryFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ChaincodePageGQL extends Apollo.Query<ChaincodePageQuery, ChaincodePageQueryVariables> {
    document = ChaincodePageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const StorePageDocument = gql`
    query StorePage {
  dapps {
    installable {
      ...Dapp
    }
  }
}
    ${DappFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class StorePageGQL extends Apollo.Query<StorePageQuery, StorePageQueryVariables> {
    document = StorePageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DappsPageDocument = gql`
    query DappsPage {
  dapps {
    installed {
      ...InstalledDapp
    }
  }
}
    ${InstalledDappFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class DappsPageGQL extends Apollo.Query<DappsPageQuery, DappsPageQueryVariables> {
    document = DappsPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const PluginsPageDocument = gql`
    query PluginsPage {
  plugins {
    installable {
      ...Plugin
    }
    installed {
      ...Plugin
    }
  }
}
    ${PluginFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class PluginsPageGQL extends Apollo.Query<PluginsPageQuery, PluginsPageQueryVariables> {
    document = PluginsPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ActionsPageDocument = gql`
    query ActionsPage {
  actions {
    ...Action
  }
}
    ${ActionFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ActionsPageGQL extends Apollo.Query<ActionsPageQuery, ActionsPageQueryVariables> {
    document = ActionsPageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const OrganizationComponentDocument = gql`
    query OrganizationComponent($pub: String) {
  lobby {
    organization(pub: $pub) {
      ...ConsortiumOrganization
    }
  }
}
    ${ConsortiumOrganizationFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class OrganizationComponentGQL extends Apollo.Query<OrganizationComponentQuery, OrganizationComponentQueryVariables> {
    document = OrganizationComponentDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListUsersDocument = gql`
    query ListUsers {
  users {
    ...User
  }
}
    ${UserFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListUsersGQL extends Apollo.Query<ListUsersQuery, ListUsersQueryVariables> {
    document = ListUsersDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const QueryUserDocument = gql`
    query QueryUser($_id: String!) {
  users(_id: $_id) {
    ...User
  }
}
    ${UserFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class QueryUserGQL extends Apollo.Query<QueryUserQuery, QueryUserQueryVariables> {
    document = QueryUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ReplayNotificationsDocument = gql`
    query ReplayNotifications {
  replayNotifications {
    ...Notification
  }
}
    ${NotificationFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ReplayNotificationsGQL extends Apollo.Query<ReplayNotificationsQuery, ReplayNotificationsQueryVariables> {
    document = ReplayNotificationsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const NotificationDocument = gql`
    subscription notification {
  notification {
    ...Notification
  }
}
    ${NotificationFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class NotificationGQL extends Apollo.Subscription<NotificationSubscription, NotificationSubscriptionVariables> {
    document = NotificationDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const StatusUpdateDocument = gql`
    subscription statusUpdate {
  statusUpdate {
    ...Info
  }
}
    ${InfoFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class StatusUpdateGQL extends Apollo.Subscription<StatusUpdateSubscription, StatusUpdateSubscriptionVariables> {
    document = StatusUpdateDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LoginUserDocument = gql`
    mutation LoginUser($_id: String!, $password: String!) {
  loginUser(_id: $_id, password: $password)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginUserGQL extends Apollo.Mutation<LoginUserMutation, LoginUserMutationVariables> {
    document = LoginUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const PutUserDocument = gql`
    mutation PutUser($_id: String, $name: String!, $password: String) {
  putUser(_id: $_id, name: $name, password: $password) {
    ...UserCore
  }
}
    ${UserCoreFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class PutUserGQL extends Apollo.Mutation<PutUserMutation, PutUserMutationVariables> {
    document = PutUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveUserDocument = gql`
    mutation RemoveUser($_id: String!) {
  removeUser(_id: $_id)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveUserGQL extends Apollo.Mutation<RemoveUserMutation, RemoveUserMutationVariables> {
    document = RemoveUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListRolesDocument = gql`
    query ListRoles {
  roles {
    ...Role
  }
}
    ${RoleFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListRolesGQL extends Apollo.Query<ListRolesQuery, ListRolesQueryVariables> {
    document = ListRolesDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const QueryRoleDocument = gql`
    query QueryRole($_id: String!) {
  roles(_id: $_id) {
    ...Role
  }
}
    ${RoleFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class QueryRoleGQL extends Apollo.Query<QueryRoleQuery, QueryRoleQueryVariables> {
    document = QueryRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const PutRoleDocument = gql`
    mutation PutRole($_id: String, $name: String!) {
  putRole(_id: $_id, name: $name) {
    ...RoleCore
  }
}
    ${RoleCoreFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class PutRoleGQL extends Apollo.Mutation<PutRoleMutation, PutRoleMutationVariables> {
    document = PutRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveRoleDocument = gql`
    mutation RemoveRole($_id: String!) {
  removeRole(_id: $_id)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveRoleGQL extends Apollo.Mutation<RemoveRoleMutation, RemoveRoleMutationVariables> {
    document = RemoveRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AttachRoleToUserDocument = gql`
    mutation AttachRoleToUser($roleId: String!, $userId: String!) {
  attachRoleToUser(roleId: $roleId, userId: $userId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AttachRoleToUserGQL extends Apollo.Mutation<AttachRoleToUserMutation, AttachRoleToUserMutationVariables> {
    document = AttachRoleToUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveRoleFromUserDocument = gql`
    mutation RemoveRoleFromUser($roleId: String!, $userId: String!) {
  removeRoleFromUser(roleId: $roleId, userId: $userId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveRoleFromUserGQL extends Apollo.Mutation<RemoveRoleFromUserMutation, RemoveRoleFromUserMutationVariables> {
    document = RemoveRoleFromUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListPermissionsDocument = gql`
    query ListPermissions {
  permissions {
    ...Permission
  }
}
    ${PermissionFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListPermissionsGQL extends Apollo.Query<ListPermissionsQuery, ListPermissionsQueryVariables> {
    document = ListPermissionsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const QueryPermissionDocument = gql`
    query QueryPermission($_id: String!) {
  permissions(_id: $_id) {
    ...Permission
  }
}
    ${PermissionFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class QueryPermissionGQL extends Apollo.Query<QueryPermissionQuery, QueryPermissionQueryVariables> {
    document = QueryPermissionDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const PutPermissionDocument = gql`
    mutation PutPermission($_id: String, $name: String!) {
  putPermission(_id: $_id, name: $name) {
    ...PermissionCore
  }
}
    ${PermissionCoreFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class PutPermissionGQL extends Apollo.Mutation<PutPermissionMutation, PutPermissionMutationVariables> {
    document = PutPermissionDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemovePermissionDocument = gql`
    mutation RemovePermission($_id: String!) {
  removePermission(_id: $_id)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemovePermissionGQL extends Apollo.Mutation<RemovePermissionMutation, RemovePermissionMutationVariables> {
    document = RemovePermissionDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AttachPermissionToRoleDocument = gql`
    mutation AttachPermissionToRole($permId: String!, $roleId: String!) {
  attachPermToRole(permId: $permId, roleId: $roleId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AttachPermissionToRoleGQL extends Apollo.Mutation<AttachPermissionToRoleMutation, AttachPermissionToRoleMutationVariables> {
    document = AttachPermissionToRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemovePermFromRoleDocument = gql`
    mutation RemovePermFromRole($permId: String!, $roleId: String!) {
  removePermFromRole(permId: $permId, roleId: $roleId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemovePermFromRoleGQL extends Apollo.Mutation<RemovePermFromRoleMutation, RemovePermFromRoleMutationVariables> {
    document = RemovePermFromRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RequestDocument = gql`
    mutation Request($request: RequestInput) {
  request(request: $request)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RequestGQL extends Apollo.Mutation<RequestMutation, RequestMutationVariables> {
    document = RequestDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RequestBroadcastDocument = gql`
    mutation RequestBroadcast($request: RequestInput) {
  requestBroadcast(request: $request)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RequestBroadcastGQL extends Apollo.Mutation<RequestBroadcastMutation, RequestBroadcastMutationVariables> {
    document = RequestBroadcastDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const VoteDocument = gql`
    mutation Vote($vote: VoteInput) {
  vote(vote: $vote)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class VoteGQL extends Apollo.Mutation<VoteMutation, VoteMutationVariables> {
    document = VoteDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LobbyRequestAddDocument = gql`
    mutation LobbyRequestAdd($cid: String!) {
  lobbyRequestAdd(cid: $cid)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LobbyRequestAddGQL extends Apollo.Mutation<LobbyRequestAddMutation, LobbyRequestAddMutationVariables> {
    document = LobbyRequestAddDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LobbyRequestRemoveDocument = gql`
    mutation LobbyRequestRemove($cid: String!) {
  lobbyRequestRemove(cid: $cid)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LobbyRequestRemoveGQL extends Apollo.Mutation<LobbyRequestRemoveMutation, LobbyRequestRemoveMutationVariables> {
    document = LobbyRequestRemoveDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LobbyOrganizationPutDocument = gql`
    mutation lobbyOrganizationPut($info: ConsortiumOrganizationInput) {
  lobbyOrganizationPut(info: $info)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LobbyOrganizationPutGQL extends Apollo.Mutation<LobbyOrganizationPutMutation, LobbyOrganizationPutMutationVariables> {
    document = LobbyOrganizationPutDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LobbyOrganizationRemoveDocument = gql`
    mutation LobbyOrganizationRemove {
  lobbyOrganizationRemove
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LobbyOrganizationRemoveGQL extends Apollo.Mutation<LobbyOrganizationRemoveMutation, LobbyOrganizationRemoveMutationVariables> {
    document = LobbyOrganizationRemoveDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateConsortiumDocument = gql`
    mutation CreateConsortium($consId: String!, $consDomain: String!, $consDesc: String!, $waitFinish: Boolean) {
  swarmInit(params: {ForceNewCluster: true})
  networkInit(consDomain: $consDomain, waitFinish: $waitFinish)
  lobbyConsortiaAdd(cid: $consId, name: $consDomain, desc: $consDesc)
  lobbyConsortiumSet(cid: $consId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateConsortiumGQL extends Apollo.Mutation<CreateConsortiumMutation, CreateConsortiumMutationVariables> {
    document = CreateConsortiumDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const JoinConsortiumDocument = gql`
    mutation JoinConsortium($crypted: String!, $opub: String!, $consId: String!, $consDomain: String!, $waitFinish: Boolean) {
  swarmJoinCrypted(crypted: $crypted, opub: $opub)
  networkExtend(consDomain: $consDomain, waitFinish: $waitFinish)
  lobbyConsortiumSet(cid: $consId)
  syncRequests
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class JoinConsortiumGQL extends Apollo.Mutation<JoinConsortiumMutation, JoinConsortiumMutationVariables> {
    document = JoinConsortiumDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const InstallDappDocument = gql`
    mutation InstallDapp($dapp: DappManualInstallInput) {
  installDapp(dapp: $dapp)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class InstallDappGQL extends Apollo.Mutation<InstallDappMutation, InstallDappMutationVariables> {
    document = InstallDappDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const QueryChaincodeDocument = gql`
    query QueryChaincode($chaincode: String, $method: String, $args: [String]) {
  chaincodeQuery(chaincode: $chaincode, method: $method, args: $args) {
    ...Buffer
  }
}
    ${BufferFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class QueryChaincodeGQL extends Apollo.Query<QueryChaincodeQuery, QueryChaincodeQueryVariables> {
    document = QueryChaincodeDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }