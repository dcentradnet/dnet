import ObjectId from 'bson-objectid';
import to from 'await-to-js';
import * as util from 'util';
import { readFileSync } from 'fs';
import { exec as _exec } from 'child_process';
import chai from 'chai';
import axios from 'axios';
import Docker from 'dockerode';
import { sha3_512 } from 'js-sha3';
import sleep from 'sleep-promise';
import io from 'socket.io-client';
import chaiAsPromised from 'chai-as-promised';

import * as utils from '../image/src/utils';
import { Sdk, Chaincode, RequestCore, VoteCore } from '../sdk/generated/queries';
import { getHeaders } from './helpers/auth';
import { getAuthenticatedSdk } from './helpers/graphl';
import { sendAndVerifyRequest, sendAndVerifyVote } from './helpers/gov';

const exec = util.promisify(_exec);

chai.use(chaiAsPromised);
//chai.use(require('chai-http'));

const expect = chai.expect;
const docker = new Docker({socketPath: process.env.DOCKER_HOST || '/var/run/docker.sock'});

const ORG1_URL = process.env.ORG1_URL || 'http://localhost';
const ORG2_URL = process.env.ORG2_URL || 'http://localhost';
const ORG1_SOUTH_URL = 'http://cli.org1:8080';
const ORG2_SOUTH_URL = 'http://cli.org2:8080';
const ADMIN_MODE = ('true' == process.env.ADMIN_MODE);

const ccName = 'fabcar';
const ccLang = 'node';
const ccInit = '["init","a","90","b","210"]';

async function timeoutPromise(ms: number) {
  return new Promise(resolve => {
    const handle = setTimeout(() => {
      clearTimeout(handle);
      resolve();
    }, ms);
  });
}

let headers1, sdk1: Sdk;
let headers2, sdk2: Sdk;
describe('chaincode', function () {
   this.timeout(30 * 60 * 1000); // 30 minutes

   before(async () => {
      headers1 = await getHeaders(ORG1_URL);
      headers2 = await getHeaders(ORG2_URL);

      sdk1 = await getAuthenticatedSdk(ORG1_URL, headers1);
      sdk2 = await getAuthenticatedSdk(ORG2_URL, headers2);
   });

   step('should install chaincode', async () => {
      // TODO pack and hash chaincode
      //const ccPack = await (new NodePackager).package('/tmp/chaincode');
      //const ccHash = sha3_512(Uint8Array.from(ccPack));
      await exec('apt update && apt install tar && tar -zcvf /tmp/chaincode.tar.gz -C /tmp/chaincode .');
      const ccPack = readFileSync('/tmp/chaincode.tar.gz');
      const ccHash = sha3_512(Uint8Array.from(ccPack));

      const ccVersion = '1.0';

      // send requests
      const ccInstallRequest: RequestCore = {
         _id: new ObjectId().toString(),
         type: 'chaincode_install',
         key: ccName,
         subkey: ccVersion
      };
      const ccInstantiateRequest: RequestCore = {
         _id: new ObjectId().toString(),
         type: 'chaincode_instantiate',
         key: ccName,
         subkey: ccVersion
      };
      await sendAndVerifyRequest(sdk1, sdk2, ccInstallRequest);
      await sendAndVerifyRequest(sdk1, sdk2, ccInstantiateRequest);

      // send votes
      await sendAndVerifyVote(sdk1, sdk2, <VoteCore>{
         rid: ccInstallRequest._id,
         value: 1
      });
      await sendAndVerifyVote(sdk1, sdk2, <VoteCore>{
         rid: ccInstantiateRequest._id,
         value: 1
      });

      await axios({
         method: 'POST',
         url: `${ORG1_SOUTH_URL}/setChaincodeContext`,
         headers: headers1,
         data: {
            name: ccName,
            version: ccVersion,
            lang: ccLang,
            init: ccInit
         }
      });
      await axios({
         method: 'POST',
         url: `${ORG2_SOUTH_URL}/setChaincodeContext`,
         headers: headers2,
         data: {
            name: ccName,
            version: ccVersion,
            lang: ccLang,
            init: ccInit
         }
      });
      console.log('Set chaincode context.');
      await axios({
         method: 'POST',
         url: `${ORG1_SOUTH_URL}/installChaincode`,
         headers: Object.assign({
            'Content-Type': 'application/octet-stream'
         }, headers1),
         //encoding: null,
         data: ccPack
      });
      await axios({
         method: 'POST',
         url: `${ORG2_SOUTH_URL}/installChaincode`,
         headers: Object.assign({
            'Content-Type': 'application/octet-stream'
         }, headers2),
         //encoding: null,
         data: ccPack
      });
      console.log('Installed chaincode.');
      await postWithTolerance(`${ORG1_SOUTH_URL}/instantiateChaincode`, headers1, undefined, 30);
      console.log('Instantiated chaincode.');
      await sleep(60*1000);

      // query chaincode until it is answering to make sure it is initialized
      // FIXME do this during normal initialization/upgrade?
      await postWithTolerance(`${ORG1_URL}/api/query`, headers1, {
         chaincode: ccName,
         method: 'query',
         args: ['a']
      }, 10);
      await postWithTolerance(`${ORG2_URL}/api/query`, headers2, {
         chaincode: ccName,
         method: 'query',
         args: ['a']
      }, 10);
      console.log('Queried chaincode.');

      // check for running chaincode container
      const containers = await util.promisify(docker.listContainers).call(docker);
      expect(containers.filter(c => c.Names.find(name => name.includes(ccName) && name.includes(ccVersion))).length).to.be.at.least(2);
      console.log('Chaincode container running.');

      // check if chaincode is installed
      const chaincodes: Chaincode[] = await sdk1.ListChannels().then(res => res.channels ? res.channels.flatMap(c => c.chaincodes) : []);
      console.log(chaincodes);
   })

   step('should subscribe to chaincode events through websocket', async () => {
      this.timeout(1 * 60 * 1000); // 1 minute

      const socket = io(ORG1_URL);
      await (new Promise((resolve, reject) => {
         socket.on('connect', async () => {
            socket.on('heartbeat', data => {
               console.log('socket event', data);
               resolve(data);
            });

            socket.emit('subscribeChaincodeEvent', {
               channelName: 'main',
               chaincodeName: ccName,
               eventName: 'heartbeat'
            });

            // wait 10 secs
            await timeoutPromise(10*1000);

            // invoke chaincode to trigger event
            await postWithTolerance(`${ORG1_URL}/api/invoke`, headers1, {
               chaincode: ccName,
               method: 'invoke',
               args: ['a', 'b', '1']
            }, 10);
         });
      }));
   })

   it.skip('should subscribe to chaincode events through SDK', async () => {
      const eventPromise = sdk1.ChaincodeEvent({input: {
         channelName: 'main',
         chaincodeName: ccName,
         eventName: 'heartbeat'
      }});

      // invoke chaincode to trigger event
      await postWithTolerance(`${ORG1_URL}/api/invoke`, headers1, {
         chaincode: ccName,
         method: 'invoke',
         args: ['a', 'b', '1']
      }, 10);

      // wait 10 secs
      await timeoutPromise(10*1000);

      const event = await eventPromise;
      expect(event.chaincodeEvent.eventName).to.eql('heartbeat');
   })

   it.skip('should upgrade chaincode', async () => {
      // TODO pack and hash chaincode
      //const ccPack = await (new NodePackager).package('/tmp/chaincode');
      //const ccHash = sha3_512(Uint8Array.from(ccPack));
      await exec('apt update && apt install tar && tar -zcvf /tmp/chaincode.tar.gz -C /tmp/chaincode .');
      const ccPack = readFileSync('/tmp/chaincode.tar.gz');
      const ccHash = sha3_512(Uint8Array.from(ccPack));

      const ccVersion = '1.1';

      // send requests
      const ccInstallRequest: RequestCore = {
         _id: new ObjectId().toString(),
         type: 'chaincode_install',
         key: ccName,
         subkey: ccVersion
      };
      const ccInstantiateRequest: RequestCore = {
         _id: new ObjectId().toString(),
         type: 'chaincode_upgrade',
         key: ccName,
         subkey: ccVersion
      };
      await sendAndVerifyRequest(sdk1, sdk2, ccInstallRequest);
      await sendAndVerifyRequest(sdk1, sdk2, ccInstantiateRequest);

      // send votes
      await sendAndVerifyVote(sdk1, sdk2, <VoteCore>{
         rid: ccInstallRequest._id,
         value: 1
      });
      await sendAndVerifyVote(sdk1, sdk2, <VoteCore>{
         rid: ccInstantiateRequest._id,
         value: 1
      });

      await axios({
         method: 'POST',
         url: `${ORG1_SOUTH_URL}/setChaincodeContext`,
         headers: headers1,
         data: {
            name: ccName,
            version: ccVersion,
            lang: ccLang,
            init: ccInit
         }
      });
      await axios({
         method: 'POST',
         url: `${ORG2_SOUTH_URL}/setChaincodeContext`,
         headers: headers2,
         data: {
            name: ccName,
            version: ccVersion,
            lang: ccLang,
            init: ccInit
         }
      });
      await axios({
         method: 'POST',
         url: `${ORG1_SOUTH_URL}/installChaincode`,
         headers: Object.assign({
            'Content-Type': 'application/octet-stream'
         }, headers1),
         //encoding: null,
         data: ccPack
      });
      await axios({
         method: 'POST',
         url: `${ORG2_SOUTH_URL}/installChaincode`,
         headers: Object.assign({
            'Content-Type': 'application/octet-stream'
         }, headers2),
         //encoding: null,
         data: ccPack
      });
      await postWithTolerance(`${ORG2_SOUTH_URL}/upgradeChaincode`, headers2, undefined, 30);
      console.log('Upgraded chaincode.');
      await sleep(60*1000);

      // query chaincode until it is answering to make sure it is initialized
      // FIXME do this during normal initialization/upgrade?
      await postWithTolerance(`${ORG1_URL}/api/query`, headers1, {
         chaincode: ccName,
         method: 'query',
         args: ['a']
      }, 10);
      await postWithTolerance(`${ORG2_URL}/api/query`, headers2, {
         chaincode: ccName,
         method: 'query',
         args: ['a']
      }, 10);
      console.log('Queried chaincode.');

      await postWithTolerance(`${ORG1_URL}/api/invoke`, headers1, {
         chaincode: ccName,
         method: 'invoke',
         args: ['a', 'b', '1']
      }, 10);
      await postWithTolerance(`${ORG2_URL}/api/invoke`, headers2, {
         chaincode: ccName,
         method: 'invoke',
         args: ['a', 'b', '1']
      }, 10);
      console.log('Invoked chaincode.');

      // check for running chaincode container
      const containers = await util.promisify(docker.listContainers).call(docker);
      expect(containers.filter(c => c.Names.find(name => name.includes(ccName) && name.includes(ccVersion))).length).to.be.at.least(2);
      console.log('Chaincode container running.');

      // check if chaincode is installed
      const chaincodes: Chaincode[] = await sdk1.ListChannels().then(res => res.channels ? res.channels.flatMap(c => c.chaincodes) : []);
      console.log(chaincodes);
   });

   async function postWithTolerance(url, headers, data, waitSecs) {
      while(true) {
         const [err, res] = await to(axios({
            method: 'POST',
            url,
            headers,
            ...(data && {data})
         }));
         if(!err) {
            return res;
         }

         await utils.timeoutPromise((waitSecs || 30)*1000);
      }
   }
});
