import to from 'await-to-js';
import chai from 'chai';

import { ListAllOrgsRolesQuery, ListUsersQuery, LoginUserMutation, Sdk, User } from '../sdk/generated/queries';
import { adminCredentials, userCredentials } from './helpers/auth';
import { getUnauthenticatedSdk, getAuthenticatedSdk } from './helpers/graphl';

//chai.use(require('chai-as-promised'));
//chai.use(require('chai-http'));
const expect = chai.expect;

let uSdk: Sdk;
let aSdk: Sdk;
describe('graphql', function() {
  this.timeout(15*60*1000); // 15 minutes

  before(async () => {
    uSdk = await getUnauthenticatedSdk();
    aSdk = await getAuthenticatedSdk();
  });

  it('should create and login user', async () => {
    const [error0, result0] = await to(aSdk.PutUser(userCredentials));
    console.log(JSON.stringify(result0, undefined, 2));
    expect(error0).to.be.null;

    const [error1, result1] = await to(aSdk.ListUsers());
    console.log(JSON.stringify(result1, undefined, 2));
    expect(error1).to.be.null;
    expect(result1).to.not.be.null;

    const userId = result1.users.find(u => u.name === userCredentials.name)._id;
    const [error2, result2] = await to(aSdk.LoginUser({_id: userId, password: userCredentials.password}));
    console.log(JSON.stringify(result2, undefined, 2));

    expect(error2).to.be.null;
    expect(result2.loginUser).to.be.ok;
  });

  it('should do full query authenticated', async () => {
    const [error, result] = await to(aSdk.FullQuery());
    console.log(JSON.stringify(result, undefined, 2));

    expect(error).to.be.null;
  });

  it('should list consortia unauthenticated', async () => {
    const [error, result] = await to(uSdk.ListConsortia());
    console.log(JSON.stringify(result, undefined, 2));

    expect(error).to.be.null;
  });

  it('should list all organisation roles unauthenticated', async () => {
    const [error, result] = await to(uSdk.ListAllOrgsRoles());
    console.log(JSON.stringify(result, undefined, 2));

    expect(error).to.be.null;
    expect(result.organizations.length).to.be.eql(2);
    expect(result.organizations.map(o => o.name)).contains('Org1');
    expect(result.organizations.map(o => o.name)).contains('Org2');
    expect(result.organizations[0].roles.length).to.be.greaterThan(0);
  });

  it('should put user', async () => {
    const [error, result] = await to(aSdk.PutUser({name: 'Peter'}));
    console.log(JSON.stringify(result, undefined, 2));

    expect(error).to.be.null;
  });

  it('should put role', async () => {
    const [error, result] = await to(aSdk.PutRole({name: 'CIO'}));
    console.log(JSON.stringify(result, undefined, 2));

    expect(error).to.be.null;
  });
});
