import ObjectId from 'bson-objectid';
import chai from 'chai';
import sleep from 'sleep-promise';
import axios from 'axios';
import Docker from 'dockerode';
import { getHeaders } from './helpers/auth';
import { getAuthenticatedSdk } from './helpers/graphl';
import { Dapp, InstalledDapp, RequestCore, Sdk } from '../sdk/generated/queries';
import { sendAndVerifyRequest, sendAndVerifyVote } from './helpers/gov';

chai.use(require('chai-as-promised'));
//chai.use(require('chai-http'));

const expect = chai.expect;
const docker = new Docker({socketPath: process.env.DOCKER_HOST || '/var/run/docker.sock'});
const ORG1_URL = process.env.ORG1_URL || 'http://localhost';
const ORG2_URL = process.env.ORG2_URL || 'http://localhost';
const ADMIN_MODE = ('true' == process.env.ADMIN_MODE);

let headers1, sdk1: Sdk;
let headers2, sdk2: Sdk;
describe('dapp', function() {
  this.timeout(30*60*1000); // 30 minutes

  before(async () => {
    headers1 = await getHeaders(ORG1_URL);
    headers2 = await getHeaders(ORG2_URL);

    sdk1 = await getAuthenticatedSdk(ORG1_URL, headers1);
    sdk2 = await getAuthenticatedSdk(ORG2_URL, headers2);
  });

  step('should list installabe store dapps', async () => {
    const installableDapps: Dapp[] = await sdk1.ListDapps().then(result => result.dapps.installable);
    console.log(installableDapps);
  });

  step('should install dapp', async () => {
    // FIXME does not currently work admin mode, because no direct API is exposed yet
    if(ADMIN_MODE) {
      return;
    }

    const dAppHost = 'dMsg';
    const dAppId = new ObjectId('5f3058fa29904f24bfd1d2da').toString();
    const ccName = 'dMsg';
    const ccVersion = '1.0';

    const parentRequest: RequestCore = {
       _id: new ObjectId().toString(),
       type: 'dapp_install',
       key: dAppHost,
       subkey: dAppId,
    };
    const ccInstallRequest: RequestCore = {
      _id: new ObjectId().toString(),
      type: 'chaincode_install',
      key: 'dMsg',
      subkey: '1.0',
    };
    const ccInstantiateRequest: RequestCore = {
      _id: new ObjectId().toString(),
      type: 'chaincode_instantiate',
      key: 'dMsg',
      subkey: '1.0',
    };

    // send requests
    await sendAndVerifyRequest(sdk1, sdk2, Object.assign({parent: parentRequest._id}, ccInstallRequest));
    await sendAndVerifyRequest(sdk1, sdk2, Object.assign({parent: parentRequest._id}, ccInstantiateRequest));
    await sendAndVerifyRequest(sdk1, sdk2, Object.assign({children: [ccInstallRequest._id, ccInstantiateRequest._id]}, parentRequest));

    // only vote for parent
    await sendAndVerifyVote(sdk1, sdk2, {rid: parentRequest._id, value: 1});
    console.log('Installed dApp.');

    // wait for dApp installation to be completed
    // FIXME use wait on action
    await sleep(5*60*1000);
    //await axios.get(`${ORG1_URL}/api/waitAction?name=dapp_install%20${ccName}`);

    // check for running chaincode container
    const containers = await docker.listContainers();
    //console.log(containers);
    expect(containers.filter(c => c.Names.find(name => name.includes('dev') && name.includes(ccName) && name.includes(ccVersion))).length).to.be.greaterThan(0);
    
    // check if chaincode is installed
    const installedDapps: InstalledDapp[] = await sdk1.ListInstalledDapps().then(res => res.dapps.installed);
    console.log(installedDapps);
  });

  /*step('should refuse installing dapp twice', async () => {
  });

  step('should upgrade dapp', async () => {
  });*/
});
