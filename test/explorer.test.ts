import chai from 'chai';
import Docker from 'dockerode';

chai.use(require('chai-as-promised'));
//chai.use(require('chai-http'));

const expect = chai.expect;
const docker = new Docker({socketPath: process.env.DOCKER_HOST || '/var/run/docker.sock'});

describe('explorer', function() {
  step('should observe running explorer and explorerdb containers', async () => {
    const containers = await docker.listContainers();
    expect(containers.filter(c => c.Names.find(name => name.includes('explorer'))).length).to.be.at.least(3);
    // NOTE Explorer Org2 cannot be fully started because of port mapping conflict with Org1 explorer.
    // Therefore only three containers are listed.
  });
});
