import chai from 'chai';
import rp from 'request-promise';
import { Sdk } from '../sdk/generated/queries';
import { getAuthenticatedSdk } from './helpers/graphl';

chai.use(require('chai-as-promised'));
//chai.use(require('chai-http'));

const expect = chai.expect;
const ORG1_URL = process.env.ORG1_URL || 'http://localhost';
const ORG2_URL = process.env.ORG2_URL || 'http://localhost';

let sdk: Sdk;
describe('lobby', function () {
  this.timeout(1 * 60 * 1000); // 1 minute

  before(async () => {
    sdk = await getAuthenticatedSdk();
  });
  
  /*it('should query consortia', async () => {
    const consortia = await sdk.ListConsortia().then(res => res.lobby.consortia);
    console.log(consortia);
  });*/
  
  // NOTE cannot query requests and members because nodes were manually joined (without lobby support)
  /*it('should query requests', async () => {
    await axios.get(`${ORG1_URL}/api/requests`);
  });
  
  it('should query members', async () => {
    await axios.get(`${ORG1_URL}/api/members`);
  });*/
});
