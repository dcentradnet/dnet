import { GraphQLClient } from 'graphql-request';

import '../../image/src/polyfills';
import { getSdk, Sdk } from '../../sdk/generated/queries';
import { getHeaders, Headers } from './auth';

const ORG1_URL = process.env.ORG1_URL || 'http://localhost:8083';

export async function getUnauthenticatedSdk(orgUrl: string = ORG1_URL): Promise<Sdk> {
    const client = new GraphQLClient(`${orgUrl}/api/graphQL`);
    return getSdk(client);
}

export async function getAuthenticatedSdk(orgUrl: string = ORG1_URL, headers?: Headers): Promise<Sdk> {
    if(!headers) {
        headers = await getHeaders(orgUrl);
    }

    const client = new GraphQLClient(`${orgUrl}/api/graphQL`, {headers});
    return getSdk(client);
}
