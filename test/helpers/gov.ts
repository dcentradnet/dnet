import chai from 'chai';
import { Request as GovRequest, RequestCore as GovRequestCore, Sdk, VoteCore } from "../../sdk/generated/queries";

const expect = chai.expect;

const ORG1_URL = process.env.ORG1_URL || 'http://localhost';
const ORG2_URL = process.env.ORG2_URL || 'http://localhost';
const ADMIN_MODE = ('true' == process.env.ADMIN_MODE);

export function requestsEqual(a: GovRequest | GovRequestCore, b: GovRequest | GovRequestCore): boolean {
    return a.type === b.type && a.key === b.key && a.subkey === b.subkey;
}

export function votesEqual(a: VoteCore, b: VoteCore): boolean {
    return a.rid === b.rid && a.value === b.value;
}

export async function sendAndVerifyRequest(sdk1: Sdk, sdk2: Sdk, request: GovRequestCore): Promise<void> {
    // do not request in admin mode
    if(ADMIN_MODE) {
        return;
    }
    
    console.log('Sending request.', request);

    await sdk1.Request({request});
    await sdk2.Request({request});
    
    let requests: GovRequest[];
    requests = await sdk1.ListRequests().then(res => res.gov.requests);
    expect(requests.filter(r => requestsEqual(r, request)).length).to.be.equal(1);
    requests = await sdk2.ListRequests().then(res => res.gov.requests);
    expect(requests.filter(r => requestsEqual(r, request)).length).to.be.equal(1);
}

export async function sendAndVerifyVote(sdk1: Sdk, sdk2: Sdk, vote: VoteCore): Promise<void> {
    // do not vote in admin mode
    if(ADMIN_MODE) {
        return;
    }
    
    await sdk1.Vote({vote});
    await sdk2.Vote({vote});
    
    let requests: GovRequest[];
    requests = await sdk1.ListRequests().then(res => res.gov.requests);
    expect(requests.map(r => r.votes.filter(v => votesEqual(v, vote)).length).reduce((s, e) => s+e)).to.be.equal(2);
    requests = await sdk2.ListRequests().then(res => res.gov.requests);
    expect(requests.map(r => r.votes.filter(v => votesEqual(v, vote)).length).reduce((s, e) => s+e)).to.be.equal(2);
}
