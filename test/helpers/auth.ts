import axios from 'axios';

const ORG1_URL = process.env.ORG1_URL || 'http://localhost:8083';

export const adminCredentials = {
    username: 'Administrator',
    password: 'admin'
};

export const userCredentials = {
    name: 'Test',
    password: 'test'
};

export type Headers = {
    Authorization: string;
};

export async function getHeaders(orgUrl: string = ORG1_URL): Promise<Headers> {
    const token = await axios.post(`${orgUrl}/api/login`, adminCredentials).then(res => res.data.token);
    console.log('token', token);
  
    return {
        Authorization: `JWT ${token}`
    };
}
