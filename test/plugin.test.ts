import util from 'util';
import chai from 'chai';
import axios from 'axios';
import Docker from 'dockerode';
import ethers from 'ethers';
import Web3 from 'web3';

import * as utils from '../image/src/utils';
import generateSwarmKey from 'ipfs-swarm-key-gen';
import { getHeaders } from './helpers/auth';
import { getAuthenticatedSdk } from './helpers/graphl';
import { Sdk, Plugin } from '../sdk/generated/queries';

chai.use(require('chai-as-promised'));
//chai.use(require('chai-http'));

const expect = chai.expect;
const ORG1_URL = process.env.ORG1_URL || 'http://localhost';
const ORG2_URL = process.env.ORG2_URL || 'http://localhost';
const ADMIN_MODE = ('true' == process.env.ADMIN_MODE);

const docker = new Docker({socketPath: process.env.DOCKER_HOST || '/var/run/docker.sock'});

let headers1, sdk1: Sdk;
let headers2, sdk2: Sdk;
describe('plugin', function() {
  this.timeout(15*60*1000); // 15 minutes

  before(async () => {
    headers1 = await getHeaders(ORG1_URL);
    headers2 = await getHeaders(ORG2_URL);

    sdk1 = await getAuthenticatedSdk(ORG1_URL, headers1);
    sdk2 = await getAuthenticatedSdk(ORG2_URL, headers2);
  });

  /*step('should install IPFS plugin', async () => { 
    const swarmKey = generateSwarmKey();

    const installRes1 = await axios({
       method: 'POST',
       url: `${ORG1_URL}/api/installPlugin`,
       headers: headers1,
       data: {
         title: 'IPFS',
         parameters: {
           swarmKey
         }
       }
    }).then(res => res.data);
    const installRes2 = await axios({
       method: 'POST',
       url: `${ORG2_URL}/api/installPlugin`,
       headers: headers2,
       data: {
         title: 'IPFS',
         parameters: {
           swarmKey
         }
       }
    }).then(res => res.data);
    console.log('install result', installRes1, installRes2);
    
    const data = 'test';
    const ipfsPutRes = await axios({
       method: 'PUT,
       url: `${ORG1_URL}/api/plugins/ipfs/put`,
       headers: Object.assign({
         'Content-Type': 'text/plain'
       }, headers1),
       data,
    }).then(res => res.data);
    console.log('ipfs put result', ipfsPutRes);
    const ipfsGetRes = await axios.get(`${ORG2_URL}/api/plugins/ipfs/get`, {
       headers: headers2,
       qs: {
         'path': `/ipfs/${ipfsPutRes[0].path}`
       }
    }).then(res => res.data);

    // postcondition
    console.log('ipfs get result', ipfsGetRes);
    expect(ipfsGetRes).to.be.equal(data);

    // check if plugins are installed
    const installedPlugins: Plugin[] = await sdk1.ListPlugins().then(res => res.plugins.installed);
    console.log('installed plugins', installedPlugins);
  })*/

  step('should install Indy plugin', async () => { 
    const installRes = await axios({
      method: 'POST',
      url: `${ORG1_URL}/api/sofa/install-plugin`,
      headers: headers1,
      data: {
        plugin: {
          _id: 'babaf10e-7107-4501-b485-78358e5080a3',
          version: '1.0',
          parameters: []
        }
      }
    }).then(res => res.data);
    console.log('install result', installRes);

    // TODO verify some claim    

    // check if plugins are installed
    const installedPlugins: Plugin[] = await sdk1.ListPlugins().then(res => res.plugins.installed);
    console.log('installed plugins', installedPlugins);
  });

  step('should install and execute ETH transcribe plugin', async () => {
    return;
    // pull container
    await utils.pullImage('trufflesuite/ganache-cli:v6.7.0');

    // start new container if none exists
    /*container = await docker.createContainer({
      Image: 'trufflesuite/ganache-cli:v6.7.0',
      name: 'ganache',
      Cmd: ['-s', '0']
    });
    await container.start();

    // connect to network  
    const networkDescs = await docker.listNetworks();
    const networkDesc = networkDescs.find(desc => desc.Name == 'net_example');
    const network = docker.getNetwork(networkDesc.Id);
    await network.connect({Container: 'ganache'});

    // deploy truffle smart contract here
    await exec('npm run test_deployethtranscribe');*/

    // get network id
    const web3 = new Web3('http://ganache:8545');
    const networkId = await web3.eth.net.getId();

    // get contract address
    const artifacts = require('./eth-transcribe/build/contracts/FabricETHTranscribe.json');
    
    const installRes = await axios({
       method: 'POST',
       url: `${ORG1_URL}/api/installPlugin`,
       headers: headers1,
       data: {
         name: 'ETHTranscribe',
         args: {
           rpcUrl: 'http://ganache:8545',
           address: '0xe092b1fa25DF5786D151246E492Eed3d15EA4dAA',
           privKey: '0x0cc0c2de7e8c30525b4ca3b9e0b9703fb29569060d403261055481df7014f7fa',
           artifacts: {
             abi: artifacts.abi,
             address: artifacts.networks[networkId].address
           }
         }
       }
    }).then(res => res.data);
    console.log('install result', installRes.data);

    // check if plugins are installed
    const installedPlugins: Plugin[] = await sdk1.ListPlugins().then(res => res.plugins.installed);
    console.log('installed plugins', installedPlugins);
    
    const transcribeRes = await axios({
      method: 'POST',
      url: `${ORG1_URL}/api/plugins/eth-transcribe/invoke`,
      headers: headers1,
      data: {
        chaincode: "fabcar",
        method: "invoke",
        args: ["a", "b", "10"]
      }
    }).then(res => res.data);
    console.log('transcribe result', transcribeRes);

    // get contract handle
    const Contract = web3.eth.Contract;
    const contract = new Contract(artifacts.abi, artifacts.networks[networkId].address);

    //const transcribeTxHash = transcribeRes.transcribeReceipt.transactionHash;
    //expect(transcribeTxHash) to be hash

    const isTranscribed = await contract.methods.isTranscribed(ethers.utils.bigNumberify('0x'+transcribeRes.txId)).call();
    expect(isTranscribed).to.be.true;
  });
});
