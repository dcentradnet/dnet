import ObjectId from 'bson-objectid';
import chai from 'chai';
import axios from 'axios';
import Docker from 'dockerode';
import { getHeaders } from './helpers/auth';

chai.use(require('chai-as-promised'));
//chai.use(require('chai-http'));

const expect = chai.expect;
const docker = new Docker({socketPath: process.env.DOCKER_HOST || '/var/run/docker.sock'});
const ORG1_URL = process.env.ORG1_URL || 'http://localhost';
const ORG2_URL = process.env.ORG2_URL || 'http://localhost';

function requestsEqual(a, b) {
  return a.type == b.type && a.key == b.key && a.subkey == b.subkey;
}

function votesEqual(a, b) {
  return requestsEqual(a, b) && a.value == b.value;
}

let headers;
describe('governance', function() {
  this.timeout(15 * 60 * 1000); // 15 minutes

  before(async () => {
    headers = await getHeaders();
  });

  /*step('should deny vote if no request exists', async () => {
    await expect(axios.post(`${ORG1_URL}/api/vote`, {
       headers,
       data: {
         type: 'dapp_install',
         key: 'mongo',
         subkey: 'mongo',
         value: 1
       }
    })).to.be.rejected;
  });*/
  
  step('should create request', async () => {
    const request = {
       _id: new ObjectId().toString(),
       type: 'dapp_install',
       key: 'mongo',
       subkey: 'mongo'
    };
    
    await axios({
      method: 'POST',
      url: `${ORG1_URL}/api/sofa/request-broadcast`,
      headers,
      data: {request}
    });
    
    const requests = await axios({
      method: 'GET',
      url: `${ORG1_URL}/api/sofa/gov`,
      headers
    }).then(res => res.data.requests);
    console.log(requests);
    
    const num = requests.filter(r => requestsEqual(r, request)).length;
    expect(num).to.be.equal(1);
  });
  
  step('should call sync requests without error', async () => {
    const res = await axios({
      method: 'POST',
      url: `${ORG2_URL}/api/sofa/sync-requests`,
      headers
    });
    
    expect(res.status).to.be.equal(200);
  });
  
  // FIXME async conflict with 'should create request'
  /*step('should not create two equal requests', async () => {
    const request = {
       type: 'dapp_install',
       key: 'mongo',
       subkey: 'mongo'
    };
    
    await axios.post(`${ORG1_URL}/api/request`, {
       headers,
       data: request,
    });
    
    let failed = false;
    const res = await axios.get(`${ORG1_URL}/api/request`, {simple: true, resolveWithFullResponse: true}).catch(e => failed = true);
    console.log(res);
    expect(res.status).to.be.equal(303);
    expect(failed).to.be.equal(true);
    
    const num = res.filter(r => requestsEqual(r, request)).length;
    expect(num).to.be.equal(1);
  })*/
  
  /*step('should vote', async () => {
    const vote = {
       type: 'dapp_install',
       key: 'mongo',
       subkey: 'mongo',
       value: 1
    };
    
    await axios.post(`${ORG1_URL}/api/vote`, {
       headers: headers,
       data: vote,
    });
    
    const votes = await axios.get(`${ORG1_URL}/api/votes`).then(res => res.data);
    console.log(votes);
    expect(votes.filter(v => votesEqual(v, vote)).length).to.be.equal(1);
    
    // wait for dApp container to be up and running
    //await sleep(1000);
    
    // check for running dApp container
    return;
    const containers = await util.promisify(docker.listContainers).call(docker);
    console.log(containers);
    expect(containers.filter(c => c.Names[0] == '/'+vote.key).length).to.be.equal(1);
    
    // check if chaincode is installed
    const res = await axios.get(`${ORG1_URL}/api/chaincodes`).then(res => res.data);
    console.log(res);
  })*/
});
