import chai from 'chai';
import axios from 'axios';

import { Info, Sdk } from '../sdk/generated/queries';
import { getHeaders } from './helpers/auth';
import { getAuthenticatedSdk } from './helpers/graphl';

//chai.use(require('chai-as-promised'));
//chai.use(require('chai-http'));
const expect = chai.expect;
const ORG1_URL = process.env.ORG1_URL || 'http://localhost';
const ORG2_URL = process.env.ORG2_URL || 'http://localhost';
const ADMIN_MODE = ('true' == process.env.ADMIN_MODE);

let headers1, sdk1: Sdk;
let headers2, sdk2: Sdk;
describe('chaincode', function () {
   this.timeout(15 * 60 * 1000); // 15 minutes

   before(async () => {
      headers1 = await getHeaders(ORG1_URL);
      headers2 = await getHeaders(ORG2_URL);
  
      sdk1 = await getAuthenticatedSdk(ORG1_URL, headers1);
      sdk2 = await getAuthenticatedSdk(ORG2_URL, headers2);
   });

   it('should be able to query info', async () => {
      const CONS_DOMAIN = 'example';

      const info1: Info = await axios.get(`${ORG1_URL}/api/sofa/info`, {
         headers: headers1
      }).then(res => res.data);
      console.log(info1);
      expect(info1.consDomain).to.equal(CONS_DOMAIN);
      expect(info1.orgName).to.equal('Org1');
      //expect(info1.isJoined).to.be.true;

      // TODO check if valid public key

      const info2: Info = await axios.get(`${ORG2_URL}/api/sofa/info`, {
         headers: headers2,
      }).then(res => res.data);
      console.log(info2);
      expect(info2.consDomain).to.equal(CONS_DOMAIN);
      expect(info2.orgName).to.equal('Org2');
      expect(info2.isJoined).to.be.true;

      //expect(info1.publicIpV4).to.not.equal(info2.publicIpV4);
      expect(info1.publicKey).to.not.equal(info2.publicKey);
   });

   it('should be ready after first query', async () => {
      const ready1 = await axios.get(`${ORG1_URL}/api/ready`).then(res => res.data.isReady);
      //expect(ready1).to.be.true;

      const ready2 = await axios.get(`${ORG2_URL}/api/ready`).then(res => res.data.isReady);
      //expect(ready2).to.be.true;
   });
});
