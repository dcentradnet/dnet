/* tslint:disable */

import { expect } from "chai";
import dNetSDK from '../index';

describe('Test dNet SDK', function() {
    it('full query', async () => {
        const res = await dNetSDK.FullQuery();
        console.log(res);
        expect((res as any).errors).to.be.undefined;
    });

    it('list all orgs roles', async () => {
        const res = await dNetSDK.ListAllOrgsRoles();
        console.log(JSON.stringify((res as any).data.organizations));
        expect((res as any).errors).to.be.undefined;
    });
});