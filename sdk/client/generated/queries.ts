import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  JSON: any;
};



export type Action = {
  __typename?: 'Action';
  _id: Scalars['String'];
  log?: Maybe<Array<Maybe<Scalars['String']>>>;
  name: Scalars['String'];
  progress?: Maybe<Scalars['Float']>;
};

export type Buffer = {
  __typename?: 'Buffer';
  data?: Maybe<Array<Maybe<Scalars['Int']>>>;
  type?: Maybe<Scalars['String']>;
};

export type Chaincode = {
  __typename?: 'Chaincode';
  name: Scalars['String'];
};

export type ChaincodeEvent = {
  __typename?: 'ChaincodeEvent';
  blockNumber: Scalars['String'];
  eventName: Scalars['String'];
  payloadString: Scalars['String'];
  status: Scalars['String'];
  transactionId: Scalars['String'];
};

export type ChaincodeEventListenInput = {
  chaincodeName: Scalars['String'];
  channelName: Scalars['String'];
  eventName: Scalars['String'];
};

export type ChaincodeQuery = {
  __typename?: 'ChaincodeQuery';
  installed?: Maybe<Array<Chaincode>>;
  instantiated?: Maybe<Array<Chaincode>>;
};

export type Channel = {
  __typename?: 'Channel';
  _id: Scalars['String'];
  chaincodes?: Maybe<Array<Chaincode>>;
};

export type Consortium = {
  __typename?: 'Consortium';
  cid: Scalars['ID'];
  name: Scalars['String'];
  desc?: Maybe<Scalars['String']>;
  founder?: Maybe<Scalars['String']>;
  requests?: Maybe<Array<ConsortiumRequest>>;
  approvals?: Maybe<Array<ConsortiumApproval>>;
  members?: Maybe<Array<ConsortiumMember>>;
};


export type ConsortiumApprovalsArgs = {
  pub?: Maybe<Scalars['String']>;
};


export type ConsortiumMembersArgs = {
  pub?: Maybe<Scalars['String']>;
};

export type ConsortiumApproval = {
  __typename?: 'ConsortiumApproval';
  _id: Scalars['ID'];
  cid: Scalars['ID'];
  rpub?: Maybe<Scalars['String']>;
  pub?: Maybe<Scalars['String']>;
  sig?: Maybe<Scalars['String']>;
};

export type ConsortiumLocal = {
  __typename?: 'ConsortiumLocal';
  organizations?: Maybe<Array<Organization>>;
};

export type ConsortiumMember = {
  __typename?: 'ConsortiumMember';
  _id: Scalars['ID'];
  cid: Scalars['ID'];
  pub?: Maybe<Scalars['String']>;
  apub?: Maybe<Scalars['String']>;
  token?: Maybe<Scalars['String']>;
};

export type ConsortiumRequest = {
  __typename?: 'ConsortiumRequest';
  _id: Scalars['ID'];
  cid: Scalars['ID'];
  rpub: Scalars['String'];
  sig: Scalars['String'];
  confirmed?: Maybe<Scalars['Boolean']>;
  approvals?: Maybe<Array<ConsortiumApproval>>;
};

export type Container = {
  __typename?: 'Container';
  Health?: Maybe<Scalars['JSON']>;
  Name: Scalars['String'];
};

export type Dapp = {
  __typename?: 'Dapp';
  _id: Scalars['ID'];
  title: Scalars['String'];
  version: Scalars['String'];
  imageId?: Maybe<Scalars['String']>;
  dockerImage: Scalars['String'];
  pricing?: Maybe<Pricing>;
};

export type DappManualInstallInput = {
  dockerImage: Scalars['String'];
  options?: Maybe<Scalars['JSON']>;
  title: Scalars['String'];
  version: Scalars['String'];
};

export type Dapps = {
  __typename?: 'Dapps';
  installable?: Maybe<Array<Dapp>>;
  installed?: Maybe<Array<InstalledDapp>>;
};

export type FormField = {
  __typename?: 'FormField';
  type: Scalars['String'];
  name: Scalars['String'];
  label: Scalars['String'];
  value: Scalars['String'];
  required: Scalars['Boolean'];
};

export type Governance = {
  __typename?: 'Governance';
  request?: Maybe<Request>;
  requests?: Maybe<Array<Request>>;
};


export type GovernanceRequestArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type Info = {
  __typename?: 'Info';
  channelName?: Maybe<Scalars['String']>;
  consDomain?: Maybe<Scalars['String']>;
  isAdminMode?: Maybe<Scalars['Boolean']>;
  isJoined?: Maybe<Scalars['Boolean']>;
  orgName?: Maybe<Scalars['String']>;
  publicIpV4?: Maybe<Scalars['String']>;
  publicKey?: Maybe<Scalars['String']>;
};

export type InstalledDapp = {
  __typename?: 'InstalledDapp';
  dapp: Dapp;
  hostName: Scalars['String'];
};


export type Lobby = {
  __typename?: 'Lobby';
  consortia?: Maybe<Array<Consortium>>;
  joined?: Maybe<Consortium>;
  member?: Maybe<ConsortiumMember>;
  requested?: Maybe<Consortium>;
};

export type Mutation = {
  __typename?: 'Mutation';
  attachPermToRole?: Maybe<Scalars['Boolean']>;
  attachRoleToUser?: Maybe<Scalars['Boolean']>;
  chaincodeInvoke?: Maybe<Scalars['Boolean']>;
  installDapp?: Maybe<Scalars['Boolean']>;
  installPlugin?: Maybe<Scalars['Boolean']>;
  lobbyConsortiaAdd?: Maybe<Scalars['Boolean']>;
  lobbyConsortiumSet?: Maybe<Scalars['Boolean']>;
  lobbyRequestAdd?: Maybe<Scalars['Boolean']>;
  lobbyRequestRemove?: Maybe<Scalars['Boolean']>;
  loginUser?: Maybe<Scalars['String']>;
  networkExtend?: Maybe<Scalars['Boolean']>;
  networkInit?: Maybe<Scalars['Boolean']>;
  networkShutdown?: Maybe<Scalars['Boolean']>;
  putPermission?: Maybe<PermissionCore>;
  putRole?: Maybe<RoleCore>;
  putUser?: Maybe<UserCore>;
  removePermFromRole?: Maybe<Scalars['Boolean']>;
  removePermission?: Maybe<Scalars['Boolean']>;
  removeRole?: Maybe<Scalars['Boolean']>;
  removeRoleFromUser?: Maybe<Scalars['Boolean']>;
  removeUser?: Maybe<Scalars['Boolean']>;
  request?: Maybe<Scalars['Boolean']>;
  requestBroadcast?: Maybe<Scalars['Boolean']>;
  swarmInit?: Maybe<Scalars['Boolean']>;
  swarmJoin?: Maybe<Scalars['Boolean']>;
  swarmJoinCrypted?: Maybe<Scalars['Boolean']>;
  syncRequests?: Maybe<Scalars['Boolean']>;
  vote?: Maybe<Scalars['Int']>;
};


export type MutationAttachPermToRoleArgs = {
  permId?: Maybe<Scalars['String']>;
  roleId?: Maybe<Scalars['String']>;
};


export type MutationAttachRoleToUserArgs = {
  roleId?: Maybe<Scalars['String']>;
  userId?: Maybe<Scalars['String']>;
};


export type MutationChaincodeInvokeArgs = {
  args?: Maybe<Array<Maybe<Scalars['String']>>>;
  chaincode?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
  method?: Maybe<Scalars['String']>;
};


export type MutationInstallDappArgs = {
  dapp?: Maybe<DappManualInstallInput>;
};


export type MutationInstallPluginArgs = {
  plugin?: Maybe<PluginInstallInput>;
};


export type MutationLobbyConsortiaAddArgs = {
  cid: Scalars['String'];
  desc: Scalars['String'];
  name: Scalars['String'];
};


export type MutationLobbyConsortiumSetArgs = {
  cid: Scalars['String'];
};


export type MutationLobbyRequestAddArgs = {
  cid: Scalars['String'];
};


export type MutationLobbyRequestRemoveArgs = {
  cid: Scalars['String'];
};


export type MutationLoginUserArgs = {
  _id?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};


export type MutationNetworkExtendArgs = {
  consDomain: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
};


export type MutationNetworkInitArgs = {
  consDomain: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
};


export type MutationNetworkShutdownArgs = {
  consDomain: Scalars['String'];
  waitFinish?: Maybe<Scalars['Boolean']>;
};


export type MutationPutPermissionArgs = {
  _id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};


export type MutationPutRoleArgs = {
  _id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};


export type MutationPutUserArgs = {
  _id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};


export type MutationRemovePermFromRoleArgs = {
  permId?: Maybe<Scalars['String']>;
  roleId?: Maybe<Scalars['String']>;
};


export type MutationRemovePermissionArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type MutationRemoveRoleArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type MutationRemoveRoleFromUserArgs = {
  roleId?: Maybe<Scalars['String']>;
  userId?: Maybe<Scalars['String']>;
};


export type MutationRemoveUserArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type MutationRequestArgs = {
  request?: Maybe<RequestInput>;
};


export type MutationRequestBroadcastArgs = {
  request?: Maybe<RequestInput>;
};


export type MutationSwarmInitArgs = {
  params?: Maybe<SwarmInitParamsInput>;
};


export type MutationSwarmJoinArgs = {
  params?: Maybe<SwarmJoinParamsInput>;
};


export type MutationSwarmJoinCryptedArgs = {
  crypted: Scalars['String'];
  opub: Scalars['String'];
};


export type MutationVoteArgs = {
  vote?: Maybe<VoteInput>;
};

export type Notification = {
  __typename?: 'Notification';
  _id: Scalars['ID'];
  message: Scalars['String'];
  system?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['Int']>;
  url?: Maybe<Scalars['String']>;
};

export type Organization = {
  __typename?: 'Organization';
  _id: Scalars['String'];
  name: Scalars['String'];
  publicIpV4?: Maybe<Scalars['String']>;
  publicKey?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Role>>;
  users?: Maybe<Array<User>>;
};


export type OrganizationRolesArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type OrganizationUsersArgs = {
  _id?: Maybe<Scalars['String']>;
};

export type Permission = {
  __typename?: 'Permission';
  _id: Scalars['String'];
  name: Scalars['String'];
  roles?: Maybe<Array<RoleCore>>;
};

export type PermissionCore = {
  __typename?: 'PermissionCore';
  _id: Scalars['String'];
  name: Scalars['String'];
};

export type Plugin = {
  __typename?: 'Plugin';
  _id: Scalars['ID'];
  title: Scalars['String'];
  version: Scalars['String'];
  imageId?: Maybe<Scalars['String']>;
  pricing?: Maybe<Pricing>;
  parameters?: Maybe<Array<FormField>>;
  installed?: Maybe<Scalars['Boolean']>;
};

export type PluginInstallInput = {
  _id: Scalars['String'];
  parameters?: Maybe<Scalars['JSON']>;
  version: Scalars['String'];
};

export type Plugins = {
  __typename?: 'Plugins';
  installable?: Maybe<Array<Plugin>>;
  installed?: Maybe<Array<Plugin>>;
};

export type Pricing = {
  __typename?: 'Pricing';
  currency: Scalars['String'];
  initialPrice?: Maybe<Scalars['Int']>;
  pricePerMonth?: Maybe<Scalars['Int']>;
  paymentInfo?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  actions?: Maybe<Array<Action>>;
  chaincode?: Maybe<ChaincodeQuery>;
  chaincodeQuery?: Maybe<Buffer>;
  channels?: Maybe<Array<Channel>>;
  consortium?: Maybe<Consortium>;
  containers?: Maybe<Array<Container>>;
  dapps?: Maybe<Dapps>;
  gov?: Maybe<Governance>;
  info?: Maybe<Info>;
  lobby?: Maybe<Lobby>;
  organization?: Maybe<Organization>;
  organizations?: Maybe<Array<Organization>>;
  permissions?: Maybe<Array<Permission>>;
  plugins?: Maybe<Plugins>;
  replayNotifications?: Maybe<Array<Notification>>;
  roles?: Maybe<Array<Role>>;
  swarm?: Maybe<Swarm>;
  users?: Maybe<Array<User>>;
};


export type QueryChaincodeQueryArgs = {
  args?: Maybe<Array<Maybe<Scalars['String']>>>;
  chaincode?: Maybe<Scalars['String']>;
  method?: Maybe<Scalars['String']>;
};


export type QueryPermissionsArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type QueryRolesArgs = {
  _id?: Maybe<Scalars['String']>;
};


export type QueryUsersArgs = {
  _id?: Maybe<Scalars['String']>;
};

export type Request = {
  __typename?: 'Request';
  _id: Scalars['String'];
  children?: Maybe<Array<RequestCore>>;
  key: Scalars['String'];
  ownvote?: Maybe<VoteCore>;
  parent?: Maybe<RequestCore>;
  quorum?: Maybe<Array<Organization>>;
  settled?: Maybe<Scalars['Boolean']>;
  subkey: Scalars['String'];
  type: Scalars['String'];
  votes?: Maybe<Array<VoteCore>>;
};

export type RequestCore = {
  __typename?: 'RequestCore';
  _id: Scalars['String'];
  children?: Maybe<Array<Scalars['String']>>;
  key: Scalars['String'];
  ownvote?: Maybe<VoteCore>;
  parent?: Maybe<Scalars['String']>;
  quorum?: Maybe<Array<Scalars['String']>>;
  settled?: Maybe<Scalars['Boolean']>;
  subkey: Scalars['String'];
  type: Scalars['String'];
};

export type RequestInput = {
  _id: Scalars['String'];
  children?: Maybe<Array<Scalars['String']>>;
  key: Scalars['String'];
  parent?: Maybe<Scalars['String']>;
  subkey: Scalars['String'];
  type: Scalars['String'];
};

export type Role = {
  __typename?: 'Role';
  _id: Scalars['String'];
  name: Scalars['String'];
  users?: Maybe<Array<UserCore>>;
};

export type RoleCore = {
  __typename?: 'RoleCore';
  _id: Scalars['String'];
  name: Scalars['String'];
};

export type Subscription = {
  __typename?: 'Subscription';
  chaincodeEvent?: Maybe<ChaincodeEvent>;
  notification?: Maybe<Notification>;
  requestAdd?: Maybe<RequestCore>;
  statusUpdate?: Maybe<Info>;
};


export type SubscriptionChaincodeEventArgs = {
  input?: Maybe<ChaincodeEventListenInput>;
};

export type Swarm = {
  __typename?: 'Swarm';
  inspect?: Maybe<Scalars['JSON']>;
  nodes?: Maybe<Scalars['JSON']>;
};

export type SwarmInitParamsInput = {
  AdvertiseAddr?: Maybe<Scalars['String']>;
  ForceNewCluster?: Maybe<Scalars['Boolean']>;
  ListenAddr?: Maybe<Scalars['String']>;
};

export type SwarmJoinParamsInput = {
  AdvertiseAddr?: Maybe<Scalars['String']>;
  JoinToken?: Maybe<Scalars['String']>;
  ListenAddr?: Maybe<Scalars['String']>;
  RemoteAddrs?: Maybe<Array<Scalars['String']>>;
};

export type User = {
  __typename?: 'User';
  _id: Scalars['String'];
  name: Scalars['String'];
  passHash?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<RoleCore>>;
};

export type UserCore = {
  __typename?: 'UserCore';
  _id: Scalars['String'];
  name: Scalars['String'];
};

export type Vote = {
  __typename?: 'Vote';
  _id?: Maybe<Scalars['String']>;
  children?: Maybe<Array<RequestCore>>;
  request?: Maybe<RequestCore>;
  rid: Scalars['String'];
  settled?: Maybe<Scalars['Boolean']>;
  value: Scalars['Int'];
  voter?: Maybe<Organization>;
};

export type VoteCore = {
  __typename?: 'VoteCore';
  _id?: Maybe<Scalars['String']>;
  rid: Scalars['String'];
  settled?: Maybe<Scalars['Boolean']>;
  value: Scalars['Int'];
  voter?: Maybe<Organization>;
};

export type VoteInput = {
  _id?: Maybe<Scalars['String']>;
  rid: Scalars['String'];
  value: Scalars['Int'];
};

export type InfoFragment = (
  { __typename?: 'Info' }
  & Pick<Info, 'consDomain' | 'orgName' | 'channelName' | 'publicIpV4' | 'publicKey' | 'isAdminMode' | 'isJoined'>
);

export type OrganizationFragment = (
  { __typename?: 'Organization' }
  & Pick<Organization, '_id' | 'name' | 'publicIpV4' | 'publicKey'>
  & { users?: Maybe<Array<(
    { __typename?: 'User' }
    & UserNoNestingFragment
  )>>, roles?: Maybe<Array<(
    { __typename?: 'Role' }
    & RoleNoNestingFragment
  )>> }
);

export type UserFragment = (
  { __typename?: 'User' }
  & Pick<User, '_id' | 'name' | 'passHash'>
  & { roles?: Maybe<Array<(
    { __typename?: 'RoleCore' }
    & RoleCoreNoNestingFragment
  )>> }
);

export type RoleCoreFragment = (
  { __typename?: 'RoleCore' }
  & Pick<RoleCore, '_id' | 'name'>
);

export type RoleFragment = (
  { __typename?: 'Role' }
  & Pick<Role, '_id' | 'name'>
  & { users?: Maybe<Array<(
    { __typename?: 'UserCore' }
    & UserCoreNoNestingFragment
  )>> }
);

export type UserCoreFragment = (
  { __typename?: 'UserCore' }
  & Pick<UserCore, '_id' | 'name'>
);

export type ConsortiumFragment = (
  { __typename?: 'Consortium' }
  & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'>
  & { requests?: Maybe<Array<(
    { __typename?: 'ConsortiumRequest' }
    & ConsortiumRequestNoNestingFragment
  )>>, approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalNoNestingFragment
  )>>, members?: Maybe<Array<(
    { __typename?: 'ConsortiumMember' }
    & ConsortiumMemberNoNestingFragment
  )>> }
);

export type ConsortiumRequestFragment = (
  { __typename?: 'ConsortiumRequest' }
  & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'>
  & { approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalNoNestingFragment
  )>> }
);

export type ConsortiumApprovalFragment = (
  { __typename?: 'ConsortiumApproval' }
  & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>
);

export type ConsortiumMemberFragment = (
  { __typename?: 'ConsortiumMember' }
  & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token'>
);

export type SwarmFragment = (
  { __typename?: 'Swarm' }
  & Pick<Swarm, 'nodes' | 'inspect'>
);

export type ContainerFragment = (
  { __typename?: 'Container' }
  & Pick<Container, 'Name' | 'Health'>
);

export type ChannelFragment = (
  { __typename?: 'Channel' }
  & Pick<Channel, '_id'>
  & { chaincodes?: Maybe<Array<(
    { __typename?: 'Chaincode' }
    & ChaincodeNoNestingFragment
  )>> }
);

export type ChaincodeFragment = (
  { __typename?: 'Chaincode' }
  & Pick<Chaincode, 'name'>
);

export type ChaincodeQueryFragment = (
  { __typename?: 'ChaincodeQuery' }
  & { installed?: Maybe<Array<(
    { __typename?: 'Chaincode' }
    & ChaincodeNoNestingFragment
  )>>, instantiated?: Maybe<Array<(
    { __typename?: 'Chaincode' }
    & ChaincodeNoNestingFragment
  )>> }
);

export type ActionFragment = (
  { __typename?: 'Action' }
  & Pick<Action, '_id' | 'name' | 'progress' | 'log'>
);

export type LobbyFragment = (
  { __typename?: 'Lobby' }
  & { consortia?: Maybe<Array<(
    { __typename?: 'Consortium' }
    & ConsortiumNoNestingFragment
  )>>, requested?: Maybe<(
    { __typename?: 'Consortium' }
    & ConsortiumNoNestingFragment
  )>, joined?: Maybe<(
    { __typename?: 'Consortium' }
    & ConsortiumNoNestingFragment
  )>, member?: Maybe<(
    { __typename?: 'ConsortiumMember' }
    & ConsortiumMemberNoNestingFragment
  )> }
);

export type GovernanceFragment = (
  { __typename?: 'Governance' }
  & { request?: Maybe<(
    { __typename?: 'Request' }
    & RequestNoNestingFragment
  )>, requests?: Maybe<Array<(
    { __typename?: 'Request' }
    & RequestNoNestingFragment
  )>> }
);

export type RequestFragment = (
  { __typename?: 'Request' }
  & Pick<Request, '_id' | 'type' | 'key' | 'subkey' | 'settled'>
  & { parent?: Maybe<(
    { __typename?: 'RequestCore' }
    & RequestCoreNoNestingFragment
  )>, children?: Maybe<Array<(
    { __typename?: 'RequestCore' }
    & RequestCoreNoNestingFragment
  )>>, quorum?: Maybe<Array<(
    { __typename?: 'Organization' }
    & OrganizationNoNestingFragment
  )>>, votes?: Maybe<Array<(
    { __typename?: 'VoteCore' }
    & VoteCoreNoNestingFragment
  )>>, ownvote?: Maybe<(
    { __typename?: 'VoteCore' }
    & VoteCoreNoNestingFragment
  )> }
);

export type RequestCoreFragment = (
  { __typename?: 'RequestCore' }
  & Pick<RequestCore, '_id' | 'type' | 'key' | 'subkey' | 'parent' | 'children' | 'quorum' | 'settled'>
  & { ownvote?: Maybe<(
    { __typename?: 'VoteCore' }
    & VoteCoreNoNestingFragment
  )> }
);

export type VoteCoreFragment = (
  { __typename?: 'VoteCore' }
  & Pick<VoteCore, '_id' | 'rid' | 'value' | 'settled'>
  & { voter?: Maybe<(
    { __typename?: 'Organization' }
    & OrganizationNoNestingFragment
  )> }
);

export type PermissionFragment = (
  { __typename?: 'Permission' }
  & Pick<Permission, '_id' | 'name'>
  & { roles?: Maybe<Array<(
    { __typename?: 'RoleCore' }
    & RoleCoreNoNestingFragment
  )>> }
);

export type DappsFragment = (
  { __typename?: 'Dapps' }
  & { installable?: Maybe<Array<(
    { __typename?: 'Dapp' }
    & DappNoNestingFragment
  )>>, installed?: Maybe<Array<(
    { __typename?: 'InstalledDapp' }
    & InstalledDappNoNestingFragment
  )>> }
);

export type DappFragment = (
  { __typename?: 'Dapp' }
  & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingNoNestingFragment
  )> }
);

export type PricingFragment = (
  { __typename?: 'Pricing' }
  & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>
);

export type InstalledDappFragment = (
  { __typename?: 'InstalledDapp' }
  & Pick<InstalledDapp, 'hostName'>
  & { dapp: (
    { __typename?: 'Dapp' }
    & DappNoNestingFragment
  ) }
);

export type PluginsFragment = (
  { __typename?: 'Plugins' }
  & { installable?: Maybe<Array<(
    { __typename?: 'Plugin' }
    & PluginNoNestingFragment
  )>>, installed?: Maybe<Array<(
    { __typename?: 'Plugin' }
    & PluginNoNestingFragment
  )>> }
);

export type PluginFragment = (
  { __typename?: 'Plugin' }
  & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingNoNestingFragment
  )>, parameters?: Maybe<Array<(
    { __typename?: 'FormField' }
    & FormFieldNoNestingFragment
  )>> }
);

export type FormFieldFragment = (
  { __typename?: 'FormField' }
  & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>
);

export type NotificationFragment = (
  { __typename?: 'Notification' }
  & Pick<Notification, '_id' | 'system' | 'message' | 'url' | 'timestamp'>
);

export type BufferFragment = (
  { __typename?: 'Buffer' }
  & Pick<Buffer, 'type' | 'data'>
);

export type PermissionCoreFragment = (
  { __typename?: 'PermissionCore' }
  & Pick<PermissionCore, '_id' | 'name'>
);

export type ChaincodeEventFragment = (
  { __typename?: 'ChaincodeEvent' }
  & Pick<ChaincodeEvent, 'eventName' | 'payloadString' | 'blockNumber' | 'transactionId' | 'status'>
);

export type ConsortiumLocalFragment = (
  { __typename?: 'ConsortiumLocal' }
  & { organizations?: Maybe<Array<(
    { __typename?: 'Organization' }
    & OrganizationNoNestingFragment
  )>> }
);

export type VoteFragment = (
  { __typename?: 'Vote' }
  & Pick<Vote, '_id' | 'rid' | 'value' | 'settled'>
  & { request?: Maybe<(
    { __typename?: 'RequestCore' }
    & RequestCoreNoNestingFragment
  )>, children?: Maybe<Array<(
    { __typename?: 'RequestCore' }
    & RequestCoreNoNestingFragment
  )>>, voter?: Maybe<(
    { __typename?: 'Organization' }
    & OrganizationNoNestingFragment
  )> }
);

export type InfoNoNestingFragment = (
  { __typename?: 'Info' }
  & Pick<Info, 'consDomain' | 'orgName' | 'channelName' | 'publicIpV4' | 'publicKey' | 'isAdminMode' | 'isJoined'>
);

export type OrganizationNoNestingFragment = (
  { __typename?: 'Organization' }
  & Pick<Organization, '_id' | 'name' | 'publicIpV4' | 'publicKey'>
);

export type UserNoNestingFragment = (
  { __typename?: 'User' }
  & Pick<User, '_id' | 'name' | 'passHash'>
);

export type RoleCoreNoNestingFragment = (
  { __typename?: 'RoleCore' }
  & Pick<RoleCore, '_id' | 'name'>
);

export type RoleNoNestingFragment = (
  { __typename?: 'Role' }
  & Pick<Role, '_id' | 'name'>
);

export type UserCoreNoNestingFragment = (
  { __typename?: 'UserCore' }
  & Pick<UserCore, '_id' | 'name'>
);

export type ConsortiumNoNestingFragment = (
  { __typename?: 'Consortium' }
  & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'>
);

export type ConsortiumRequestNoNestingFragment = (
  { __typename?: 'ConsortiumRequest' }
  & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'>
);

export type ConsortiumApprovalNoNestingFragment = (
  { __typename?: 'ConsortiumApproval' }
  & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>
);

export type ConsortiumMemberNoNestingFragment = (
  { __typename?: 'ConsortiumMember' }
  & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token'>
);

export type SwarmNoNestingFragment = (
  { __typename?: 'Swarm' }
  & Pick<Swarm, 'nodes' | 'inspect'>
);

export type ContainerNoNestingFragment = (
  { __typename?: 'Container' }
  & Pick<Container, 'Name' | 'Health'>
);

export type ChannelNoNestingFragment = (
  { __typename?: 'Channel' }
  & Pick<Channel, '_id'>
);

export type ChaincodeNoNestingFragment = (
  { __typename?: 'Chaincode' }
  & Pick<Chaincode, 'name'>
);

export type ActionNoNestingFragment = (
  { __typename?: 'Action' }
  & Pick<Action, '_id' | 'name' | 'progress' | 'log'>
);

export type RequestNoNestingFragment = (
  { __typename?: 'Request' }
  & Pick<Request, '_id' | 'type' | 'key' | 'subkey' | 'settled'>
);

export type RequestCoreNoNestingFragment = (
  { __typename?: 'RequestCore' }
  & Pick<RequestCore, '_id' | 'type' | 'key' | 'subkey' | 'parent' | 'children' | 'quorum' | 'settled'>
);

export type VoteCoreNoNestingFragment = (
  { __typename?: 'VoteCore' }
  & Pick<VoteCore, '_id' | 'rid' | 'value' | 'settled'>
);

export type PermissionNoNestingFragment = (
  { __typename?: 'Permission' }
  & Pick<Permission, '_id' | 'name'>
);

export type DappNoNestingFragment = (
  { __typename?: 'Dapp' }
  & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'>
);

export type PricingNoNestingFragment = (
  { __typename?: 'Pricing' }
  & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>
);

export type InstalledDappNoNestingFragment = (
  { __typename?: 'InstalledDapp' }
  & Pick<InstalledDapp, 'hostName'>
);

export type PluginNoNestingFragment = (
  { __typename?: 'Plugin' }
  & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'>
);

export type FormFieldNoNestingFragment = (
  { __typename?: 'FormField' }
  & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>
);

export type NotificationNoNestingFragment = (
  { __typename?: 'Notification' }
  & Pick<Notification, '_id' | 'system' | 'message' | 'url' | 'timestamp'>
);

export type BufferNoNestingFragment = (
  { __typename?: 'Buffer' }
  & Pick<Buffer, 'type' | 'data'>
);

export type PermissionCoreNoNestingFragment = (
  { __typename?: 'PermissionCore' }
  & Pick<PermissionCore, '_id' | 'name'>
);

export type ChaincodeEventNoNestingFragment = (
  { __typename?: 'ChaincodeEvent' }
  & Pick<ChaincodeEvent, 'eventName' | 'payloadString' | 'blockNumber' | 'transactionId' | 'status'>
);

export type VoteNoNestingFragment = (
  { __typename?: 'Vote' }
  & Pick<Vote, '_id' | 'rid' | 'value' | 'settled'>
);

export type InfoDeepNestingFragment = (
  { __typename?: 'Info' }
  & Pick<Info, 'consDomain' | 'orgName' | 'channelName' | 'publicIpV4' | 'publicKey' | 'isAdminMode' | 'isJoined'>
);

export type OrganizationDeepNestingFragment = (
  { __typename?: 'Organization' }
  & Pick<Organization, '_id' | 'name' | 'publicIpV4' | 'publicKey'>
  & { users?: Maybe<Array<(
    { __typename?: 'User' }
    & UserDeepNestingFragment
  )>>, roles?: Maybe<Array<(
    { __typename?: 'Role' }
    & RoleDeepNestingFragment
  )>> }
);

export type UserDeepNestingFragment = (
  { __typename?: 'User' }
  & Pick<User, '_id' | 'name' | 'passHash'>
  & { roles?: Maybe<Array<(
    { __typename?: 'RoleCore' }
    & RoleCoreDeepNestingFragment
  )>> }
);

export type RoleCoreDeepNestingFragment = (
  { __typename?: 'RoleCore' }
  & Pick<RoleCore, '_id' | 'name'>
);

export type RoleDeepNestingFragment = (
  { __typename?: 'Role' }
  & Pick<Role, '_id' | 'name'>
  & { users?: Maybe<Array<(
    { __typename?: 'UserCore' }
    & UserCoreDeepNestingFragment
  )>> }
);

export type UserCoreDeepNestingFragment = (
  { __typename?: 'UserCore' }
  & Pick<UserCore, '_id' | 'name'>
);

export type ConsortiumDeepNestingFragment = (
  { __typename?: 'Consortium' }
  & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'>
  & { requests?: Maybe<Array<(
    { __typename?: 'ConsortiumRequest' }
    & ConsortiumRequestDeepNestingFragment
  )>>, approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalDeepNestingFragment
  )>>, members?: Maybe<Array<(
    { __typename?: 'ConsortiumMember' }
    & ConsortiumMemberDeepNestingFragment
  )>> }
);

export type ConsortiumRequestDeepNestingFragment = (
  { __typename?: 'ConsortiumRequest' }
  & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'>
  & { approvals?: Maybe<Array<(
    { __typename?: 'ConsortiumApproval' }
    & ConsortiumApprovalDeepNestingFragment
  )>> }
);

export type ConsortiumApprovalDeepNestingFragment = (
  { __typename?: 'ConsortiumApproval' }
  & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>
);

export type ConsortiumMemberDeepNestingFragment = (
  { __typename?: 'ConsortiumMember' }
  & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token'>
);

export type SwarmDeepNestingFragment = (
  { __typename?: 'Swarm' }
  & Pick<Swarm, 'nodes' | 'inspect'>
);

export type ContainerDeepNestingFragment = (
  { __typename?: 'Container' }
  & Pick<Container, 'Name' | 'Health'>
);

export type ChannelDeepNestingFragment = (
  { __typename?: 'Channel' }
  & Pick<Channel, '_id'>
  & { chaincodes?: Maybe<Array<(
    { __typename?: 'Chaincode' }
    & ChaincodeDeepNestingFragment
  )>> }
);

export type ChaincodeDeepNestingFragment = (
  { __typename?: 'Chaincode' }
  & Pick<Chaincode, 'name'>
);

export type ChaincodeQueryDeepNestingFragment = (
  { __typename?: 'ChaincodeQuery' }
  & { installed?: Maybe<Array<(
    { __typename?: 'Chaincode' }
    & ChaincodeDeepNestingFragment
  )>>, instantiated?: Maybe<Array<(
    { __typename?: 'Chaincode' }
    & ChaincodeDeepNestingFragment
  )>> }
);

export type ActionDeepNestingFragment = (
  { __typename?: 'Action' }
  & Pick<Action, '_id' | 'name' | 'progress' | 'log'>
);

export type LobbyDeepNestingFragment = (
  { __typename?: 'Lobby' }
  & { consortia?: Maybe<Array<(
    { __typename?: 'Consortium' }
    & ConsortiumDeepNestingFragment
  )>>, requested?: Maybe<(
    { __typename?: 'Consortium' }
    & ConsortiumDeepNestingFragment
  )>, joined?: Maybe<(
    { __typename?: 'Consortium' }
    & ConsortiumDeepNestingFragment
  )>, member?: Maybe<(
    { __typename?: 'ConsortiumMember' }
    & ConsortiumMemberDeepNestingFragment
  )> }
);

export type GovernanceDeepNestingFragment = (
  { __typename?: 'Governance' }
  & { request?: Maybe<(
    { __typename?: 'Request' }
    & RequestDeepNestingFragment
  )>, requests?: Maybe<Array<(
    { __typename?: 'Request' }
    & RequestDeepNestingFragment
  )>> }
);

export type RequestDeepNestingFragment = (
  { __typename?: 'Request' }
  & Pick<Request, '_id' | 'type' | 'key' | 'subkey' | 'settled'>
  & { parent?: Maybe<(
    { __typename?: 'RequestCore' }
    & RequestCoreDeepNestingFragment
  )>, children?: Maybe<Array<(
    { __typename?: 'RequestCore' }
    & RequestCoreDeepNestingFragment
  )>>, quorum?: Maybe<Array<(
    { __typename?: 'Organization' }
    & OrganizationDeepNestingFragment
  )>>, votes?: Maybe<Array<(
    { __typename?: 'VoteCore' }
    & VoteCoreDeepNestingFragment
  )>>, ownvote?: Maybe<(
    { __typename?: 'VoteCore' }
    & VoteCoreDeepNestingFragment
  )> }
);

export type RequestCoreDeepNestingFragment = (
  { __typename?: 'RequestCore' }
  & Pick<RequestCore, '_id' | 'type' | 'key' | 'subkey' | 'parent' | 'children' | 'quorum' | 'settled'>
  & { ownvote?: Maybe<(
    { __typename?: 'VoteCore' }
    & VoteCoreDeepNestingFragment
  )> }
);

export type VoteCoreDeepNestingFragment = (
  { __typename?: 'VoteCore' }
  & Pick<VoteCore, '_id' | 'rid' | 'value' | 'settled'>
  & { voter?: Maybe<(
    { __typename?: 'Organization' }
    & OrganizationDeepNestingFragment
  )> }
);

export type PermissionDeepNestingFragment = (
  { __typename?: 'Permission' }
  & Pick<Permission, '_id' | 'name'>
  & { roles?: Maybe<Array<(
    { __typename?: 'RoleCore' }
    & RoleCoreDeepNestingFragment
  )>> }
);

export type DappsDeepNestingFragment = (
  { __typename?: 'Dapps' }
  & { installable?: Maybe<Array<(
    { __typename?: 'Dapp' }
    & DappDeepNestingFragment
  )>>, installed?: Maybe<Array<(
    { __typename?: 'InstalledDapp' }
    & InstalledDappDeepNestingFragment
  )>> }
);

export type DappDeepNestingFragment = (
  { __typename?: 'Dapp' }
  & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingDeepNestingFragment
  )> }
);

export type PricingDeepNestingFragment = (
  { __typename?: 'Pricing' }
  & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>
);

export type InstalledDappDeepNestingFragment = (
  { __typename?: 'InstalledDapp' }
  & Pick<InstalledDapp, 'hostName'>
  & { dapp: (
    { __typename?: 'Dapp' }
    & DappDeepNestingFragment
  ) }
);

export type PluginsDeepNestingFragment = (
  { __typename?: 'Plugins' }
  & { installable?: Maybe<Array<(
    { __typename?: 'Plugin' }
    & PluginDeepNestingFragment
  )>>, installed?: Maybe<Array<(
    { __typename?: 'Plugin' }
    & PluginDeepNestingFragment
  )>> }
);

export type PluginDeepNestingFragment = (
  { __typename?: 'Plugin' }
  & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'>
  & { pricing?: Maybe<(
    { __typename?: 'Pricing' }
    & PricingDeepNestingFragment
  )>, parameters?: Maybe<Array<(
    { __typename?: 'FormField' }
    & FormFieldDeepNestingFragment
  )>> }
);

export type FormFieldDeepNestingFragment = (
  { __typename?: 'FormField' }
  & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>
);

export type NotificationDeepNestingFragment = (
  { __typename?: 'Notification' }
  & Pick<Notification, '_id' | 'system' | 'message' | 'url' | 'timestamp'>
);

export type BufferDeepNestingFragment = (
  { __typename?: 'Buffer' }
  & Pick<Buffer, 'type' | 'data'>
);

export type PermissionCoreDeepNestingFragment = (
  { __typename?: 'PermissionCore' }
  & Pick<PermissionCore, '_id' | 'name'>
);

export type ChaincodeEventDeepNestingFragment = (
  { __typename?: 'ChaincodeEvent' }
  & Pick<ChaincodeEvent, 'eventName' | 'payloadString' | 'blockNumber' | 'transactionId' | 'status'>
);

export type ConsortiumLocalDeepNestingFragment = (
  { __typename?: 'ConsortiumLocal' }
  & { organizations?: Maybe<Array<(
    { __typename?: 'Organization' }
    & OrganizationDeepNestingFragment
  )>> }
);

export type VoteDeepNestingFragment = (
  { __typename?: 'Vote' }
  & Pick<Vote, '_id' | 'rid' | 'value' | 'settled'>
  & { request?: Maybe<(
    { __typename?: 'RequestCore' }
    & RequestCoreDeepNestingFragment
  )>, children?: Maybe<Array<(
    { __typename?: 'RequestCore' }
    & RequestCoreDeepNestingFragment
  )>>, voter?: Maybe<(
    { __typename?: 'Organization' }
    & OrganizationDeepNestingFragment
  )> }
);

export type FullQueryQueryVariables = Exact<{ [key: string]: never; }>;


export type FullQueryQuery = (
  { __typename?: 'Query' }
  & { info?: Maybe<(
    { __typename?: 'Info' }
    & InfoFragment
  )>, organization?: Maybe<(
    { __typename?: 'Organization' }
    & OrganizationFragment
  )>, organizations?: Maybe<Array<(
    { __typename?: 'Organization' }
    & OrganizationFragment
  )>>, lobby?: Maybe<(
    { __typename?: 'Lobby' }
    & LobbyFragment
  )>, users?: Maybe<Array<(
    { __typename?: 'User' }
    & UserFragment
  )>>, roles?: Maybe<Array<(
    { __typename?: 'Role' }
    & RoleFragment
  )>>, dapps?: Maybe<(
    { __typename?: 'Dapps' }
    & DappsFragment
  )>, plugins?: Maybe<(
    { __typename?: 'Plugins' }
    & PluginsFragment
  )> }
);

export type ListRequestsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListRequestsQuery = (
  { __typename?: 'Query' }
  & { gov?: Maybe<(
    { __typename?: 'Governance' }
    & { requests?: Maybe<Array<(
      { __typename?: 'Request' }
      & RequestFragment
    )>> }
  )> }
);

export type ListConsortiaQueryVariables = Exact<{ [key: string]: never; }>;


export type ListConsortiaQuery = (
  { __typename?: 'Query' }
  & { lobby?: Maybe<(
    { __typename?: 'Lobby' }
    & { consortia?: Maybe<Array<(
      { __typename?: 'Consortium' }
      & ConsortiumFragment
    )>> }
  )> }
);

export type ListChannelsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListChannelsQuery = (
  { __typename?: 'Query' }
  & { channels?: Maybe<Array<(
    { __typename?: 'Channel' }
    & ChannelFragment
  )>> }
);

export type ListDappsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListDappsQuery = (
  { __typename?: 'Query' }
  & { dapps?: Maybe<(
    { __typename?: 'Dapps' }
    & { installable?: Maybe<Array<(
      { __typename?: 'Dapp' }
      & DappFragment
    )>>, installed?: Maybe<Array<(
      { __typename?: 'InstalledDapp' }
      & InstalledDappFragment
    )>> }
  )> }
);

export type ListInstalledDappsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListInstalledDappsQuery = (
  { __typename?: 'Query' }
  & { dapps?: Maybe<(
    { __typename?: 'Dapps' }
    & { installed?: Maybe<Array<(
      { __typename?: 'InstalledDapp' }
      & InstalledDappFragment
    )>> }
  )> }
);

export type ListPluginsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListPluginsQuery = (
  { __typename?: 'Query' }
  & { plugins?: Maybe<(
    { __typename?: 'Plugins' }
    & { installable?: Maybe<Array<(
      { __typename?: 'Plugin' }
      & PluginFragment
    )>>, installed?: Maybe<Array<(
      { __typename?: 'Plugin' }
      & PluginFragment
    )>> }
  )> }
);

export type ListAllOrgsRolesQueryVariables = Exact<{ [key: string]: never; }>;


export type ListAllOrgsRolesQuery = (
  { __typename?: 'Query' }
  & { organizations?: Maybe<Array<(
    { __typename?: 'Organization' }
    & Pick<Organization, '_id' | 'name'>
    & { roles?: Maybe<Array<(
      { __typename?: 'Role' }
      & Pick<Role, '_id' | 'name'>
    )>> }
  )>> }
);

export type ListUsersQueryVariables = Exact<{ [key: string]: never; }>;


export type ListUsersQuery = (
  { __typename?: 'Query' }
  & { users?: Maybe<Array<(
    { __typename?: 'User' }
    & UserFragment
  )>> }
);

export type QueryUserQueryVariables = Exact<{
  _id: Scalars['String'];
}>;


export type QueryUserQuery = (
  { __typename?: 'Query' }
  & { users?: Maybe<Array<(
    { __typename?: 'User' }
    & UserFragment
  )>> }
);

export type PutUserMutationVariables = Exact<{
  _id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  password?: Maybe<Scalars['String']>;
}>;


export type PutUserMutation = (
  { __typename?: 'Mutation' }
  & { putUser?: Maybe<(
    { __typename?: 'UserCore' }
    & UserCoreFragment
  )> }
);

export type RemoveUserMutationVariables = Exact<{
  _id: Scalars['String'];
}>;


export type RemoveUserMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'removeUser'>
);

export type LoginUserMutationVariables = Exact<{
  _id: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginUserMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'loginUser'>
);

export type ListRolesQueryVariables = Exact<{ [key: string]: never; }>;


export type ListRolesQuery = (
  { __typename?: 'Query' }
  & { roles?: Maybe<Array<(
    { __typename?: 'Role' }
    & RoleFragment
  )>> }
);

export type QueryRoleQueryVariables = Exact<{
  _id: Scalars['String'];
}>;


export type QueryRoleQuery = (
  { __typename?: 'Query' }
  & { roles?: Maybe<Array<(
    { __typename?: 'Role' }
    & RoleFragment
  )>> }
);

export type ChaincodeEventSubscriptionVariables = Exact<{
  input: ChaincodeEventListenInput;
}>;


export type ChaincodeEventSubscription = (
  { __typename?: 'Subscription' }
  & { chaincodeEvent?: Maybe<(
    { __typename?: 'ChaincodeEvent' }
    & ChaincodeEventFragment
  )> }
);

export type PutRoleMutationVariables = Exact<{
  _id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
}>;


export type PutRoleMutation = (
  { __typename?: 'Mutation' }
  & { putRole?: Maybe<(
    { __typename?: 'RoleCore' }
    & RoleCoreFragment
  )> }
);

export type RemoveRoleMutationVariables = Exact<{
  _id: Scalars['String'];
}>;


export type RemoveRoleMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'removeRole'>
);

export type AttachRoleToUserMutationVariables = Exact<{
  roleId: Scalars['String'];
  userId: Scalars['String'];
}>;


export type AttachRoleToUserMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'attachRoleToUser'>
);

export type RemoveRoleFromUserMutationVariables = Exact<{
  roleId: Scalars['String'];
  userId: Scalars['String'];
}>;


export type RemoveRoleFromUserMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'removeRoleFromUser'>
);

export type RequestMutationVariables = Exact<{
  request?: Maybe<RequestInput>;
}>;


export type RequestMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'request'>
);

export type VoteMutationVariables = Exact<{
  vote?: Maybe<VoteInput>;
}>;


export type VoteMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'vote'>
);

export type InstallDappMutationVariables = Exact<{
  dapp?: Maybe<DappManualInstallInput>;
}>;


export type InstallDappMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'installDapp'>
);

export type InstallPluginMutationVariables = Exact<{
  plugin?: Maybe<PluginInstallInput>;
}>;


export type InstallPluginMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'installPlugin'>
);

export const InfoFragmentDoc = gql`
    fragment Info on Info {
  consDomain
  orgName
  channelName
  publicIpV4
  publicKey
  isAdminMode
  isJoined
}
    `;
export const UserNoNestingFragmentDoc = gql`
    fragment UserNoNesting on User {
  _id
  name
  passHash
}
    `;
export const RoleNoNestingFragmentDoc = gql`
    fragment RoleNoNesting on Role {
  _id
  name
}
    `;
export const OrganizationFragmentDoc = gql`
    fragment Organization on Organization {
  _id
  name
  publicIpV4
  publicKey
  users {
    ...UserNoNesting
  }
  roles {
    ...RoleNoNesting
  }
}
    ${UserNoNestingFragmentDoc}
${RoleNoNestingFragmentDoc}`;
export const RoleCoreNoNestingFragmentDoc = gql`
    fragment RoleCoreNoNesting on RoleCore {
  _id
  name
}
    `;
export const UserFragmentDoc = gql`
    fragment User on User {
  _id
  name
  passHash
  roles {
    ...RoleCoreNoNesting
  }
}
    ${RoleCoreNoNestingFragmentDoc}`;
export const RoleCoreFragmentDoc = gql`
    fragment RoleCore on RoleCore {
  _id
  name
}
    `;
export const UserCoreNoNestingFragmentDoc = gql`
    fragment UserCoreNoNesting on UserCore {
  _id
  name
}
    `;
export const RoleFragmentDoc = gql`
    fragment Role on Role {
  _id
  name
  users {
    ...UserCoreNoNesting
  }
}
    ${UserCoreNoNestingFragmentDoc}`;
export const UserCoreFragmentDoc = gql`
    fragment UserCore on UserCore {
  _id
  name
}
    `;
export const ConsortiumRequestNoNestingFragmentDoc = gql`
    fragment ConsortiumRequestNoNesting on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
}
    `;
export const ConsortiumApprovalNoNestingFragmentDoc = gql`
    fragment ConsortiumApprovalNoNesting on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumMemberNoNestingFragmentDoc = gql`
    fragment ConsortiumMemberNoNesting on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const ConsortiumFragmentDoc = gql`
    fragment Consortium on Consortium {
  cid
  name
  desc
  founder
  requests {
    ...ConsortiumRequestNoNesting
  }
  approvals {
    ...ConsortiumApprovalNoNesting
  }
  members {
    ...ConsortiumMemberNoNesting
  }
}
    ${ConsortiumRequestNoNestingFragmentDoc}
${ConsortiumApprovalNoNestingFragmentDoc}
${ConsortiumMemberNoNestingFragmentDoc}`;
export const ConsortiumRequestFragmentDoc = gql`
    fragment ConsortiumRequest on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
  approvals {
    ...ConsortiumApprovalNoNesting
  }
}
    ${ConsortiumApprovalNoNestingFragmentDoc}`;
export const ConsortiumApprovalFragmentDoc = gql`
    fragment ConsortiumApproval on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumMemberFragmentDoc = gql`
    fragment ConsortiumMember on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const SwarmFragmentDoc = gql`
    fragment Swarm on Swarm {
  nodes
  inspect
}
    `;
export const ContainerFragmentDoc = gql`
    fragment Container on Container {
  Name
  Health
}
    `;
export const ChaincodeNoNestingFragmentDoc = gql`
    fragment ChaincodeNoNesting on Chaincode {
  name
}
    `;
export const ChannelFragmentDoc = gql`
    fragment Channel on Channel {
  _id
  chaincodes {
    ...ChaincodeNoNesting
  }
}
    ${ChaincodeNoNestingFragmentDoc}`;
export const ChaincodeFragmentDoc = gql`
    fragment Chaincode on Chaincode {
  name
}
    `;
export const ChaincodeQueryFragmentDoc = gql`
    fragment ChaincodeQuery on ChaincodeQuery {
  installed {
    ...ChaincodeNoNesting
  }
  instantiated {
    ...ChaincodeNoNesting
  }
}
    ${ChaincodeNoNestingFragmentDoc}`;
export const ActionFragmentDoc = gql`
    fragment Action on Action {
  _id
  name
  progress
  log
}
    `;
export const ConsortiumNoNestingFragmentDoc = gql`
    fragment ConsortiumNoNesting on Consortium {
  cid
  name
  desc
  founder
}
    `;
export const LobbyFragmentDoc = gql`
    fragment Lobby on Lobby {
  consortia {
    ...ConsortiumNoNesting
  }
  requested {
    ...ConsortiumNoNesting
  }
  joined {
    ...ConsortiumNoNesting
  }
  member {
    ...ConsortiumMemberNoNesting
  }
}
    ${ConsortiumNoNestingFragmentDoc}
${ConsortiumMemberNoNestingFragmentDoc}`;
export const RequestNoNestingFragmentDoc = gql`
    fragment RequestNoNesting on Request {
  _id
  type
  key
  subkey
  settled
}
    `;
export const GovernanceFragmentDoc = gql`
    fragment Governance on Governance {
  request {
    ...RequestNoNesting
  }
  requests {
    ...RequestNoNesting
  }
}
    ${RequestNoNestingFragmentDoc}`;
export const RequestCoreNoNestingFragmentDoc = gql`
    fragment RequestCoreNoNesting on RequestCore {
  _id
  type
  key
  subkey
  parent
  children
  quorum
  settled
}
    `;
export const OrganizationNoNestingFragmentDoc = gql`
    fragment OrganizationNoNesting on Organization {
  _id
  name
  publicIpV4
  publicKey
}
    `;
export const VoteCoreNoNestingFragmentDoc = gql`
    fragment VoteCoreNoNesting on VoteCore {
  _id
  rid
  value
  settled
}
    `;
export const RequestFragmentDoc = gql`
    fragment Request on Request {
  _id
  type
  key
  subkey
  parent {
    ...RequestCoreNoNesting
  }
  children {
    ...RequestCoreNoNesting
  }
  quorum {
    ...OrganizationNoNesting
  }
  votes {
    ...VoteCoreNoNesting
  }
  ownvote {
    ...VoteCoreNoNesting
  }
  settled
}
    ${RequestCoreNoNestingFragmentDoc}
${OrganizationNoNestingFragmentDoc}
${VoteCoreNoNestingFragmentDoc}`;
export const RequestCoreFragmentDoc = gql`
    fragment RequestCore on RequestCore {
  _id
  type
  key
  subkey
  parent
  children
  quorum
  ownvote {
    ...VoteCoreNoNesting
  }
  settled
}
    ${VoteCoreNoNestingFragmentDoc}`;
export const VoteCoreFragmentDoc = gql`
    fragment VoteCore on VoteCore {
  _id
  rid
  value
  settled
  voter {
    ...OrganizationNoNesting
  }
}
    ${OrganizationNoNestingFragmentDoc}`;
export const PermissionFragmentDoc = gql`
    fragment Permission on Permission {
  _id
  name
  roles {
    ...RoleCoreNoNesting
  }
}
    ${RoleCoreNoNestingFragmentDoc}`;
export const DappNoNestingFragmentDoc = gql`
    fragment DappNoNesting on Dapp {
  _id
  title
  version
  imageId
  dockerImage
}
    `;
export const InstalledDappNoNestingFragmentDoc = gql`
    fragment InstalledDappNoNesting on InstalledDapp {
  hostName
}
    `;
export const DappsFragmentDoc = gql`
    fragment Dapps on Dapps {
  installable {
    ...DappNoNesting
  }
  installed {
    ...InstalledDappNoNesting
  }
}
    ${DappNoNestingFragmentDoc}
${InstalledDappNoNestingFragmentDoc}`;
export const PricingNoNestingFragmentDoc = gql`
    fragment PricingNoNesting on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const DappFragmentDoc = gql`
    fragment Dapp on Dapp {
  _id
  title
  version
  imageId
  dockerImage
  pricing {
    ...PricingNoNesting
  }
}
    ${PricingNoNestingFragmentDoc}`;
export const PricingFragmentDoc = gql`
    fragment Pricing on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const InstalledDappFragmentDoc = gql`
    fragment InstalledDapp on InstalledDapp {
  hostName
  dapp {
    ...DappNoNesting
  }
}
    ${DappNoNestingFragmentDoc}`;
export const PluginNoNestingFragmentDoc = gql`
    fragment PluginNoNesting on Plugin {
  _id
  title
  version
  imageId
  installed
}
    `;
export const PluginsFragmentDoc = gql`
    fragment Plugins on Plugins {
  installable {
    ...PluginNoNesting
  }
  installed {
    ...PluginNoNesting
  }
}
    ${PluginNoNestingFragmentDoc}`;
export const FormFieldNoNestingFragmentDoc = gql`
    fragment FormFieldNoNesting on FormField {
  type
  name
  label
  value
  required
}
    `;
export const PluginFragmentDoc = gql`
    fragment Plugin on Plugin {
  _id
  title
  version
  imageId
  pricing {
    ...PricingNoNesting
  }
  parameters {
    ...FormFieldNoNesting
  }
  installed
}
    ${PricingNoNestingFragmentDoc}
${FormFieldNoNestingFragmentDoc}`;
export const FormFieldFragmentDoc = gql`
    fragment FormField on FormField {
  type
  name
  label
  value
  required
}
    `;
export const NotificationFragmentDoc = gql`
    fragment Notification on Notification {
  _id
  system
  message
  url
  timestamp
}
    `;
export const BufferFragmentDoc = gql`
    fragment Buffer on Buffer {
  type
  data
}
    `;
export const PermissionCoreFragmentDoc = gql`
    fragment PermissionCore on PermissionCore {
  _id
  name
}
    `;
export const ChaincodeEventFragmentDoc = gql`
    fragment ChaincodeEvent on ChaincodeEvent {
  eventName
  payloadString
  blockNumber
  transactionId
  status
}
    `;
export const ConsortiumLocalFragmentDoc = gql`
    fragment ConsortiumLocal on ConsortiumLocal {
  organizations {
    ...OrganizationNoNesting
  }
}
    ${OrganizationNoNestingFragmentDoc}`;
export const VoteFragmentDoc = gql`
    fragment Vote on Vote {
  _id
  rid
  value
  request {
    ...RequestCoreNoNesting
  }
  children {
    ...RequestCoreNoNesting
  }
  settled
  voter {
    ...OrganizationNoNesting
  }
}
    ${RequestCoreNoNestingFragmentDoc}
${OrganizationNoNestingFragmentDoc}`;
export const InfoNoNestingFragmentDoc = gql`
    fragment InfoNoNesting on Info {
  consDomain
  orgName
  channelName
  publicIpV4
  publicKey
  isAdminMode
  isJoined
}
    `;
export const SwarmNoNestingFragmentDoc = gql`
    fragment SwarmNoNesting on Swarm {
  nodes
  inspect
}
    `;
export const ContainerNoNestingFragmentDoc = gql`
    fragment ContainerNoNesting on Container {
  Name
  Health
}
    `;
export const ChannelNoNestingFragmentDoc = gql`
    fragment ChannelNoNesting on Channel {
  _id
}
    `;
export const ActionNoNestingFragmentDoc = gql`
    fragment ActionNoNesting on Action {
  _id
  name
  progress
  log
}
    `;
export const PermissionNoNestingFragmentDoc = gql`
    fragment PermissionNoNesting on Permission {
  _id
  name
}
    `;
export const NotificationNoNestingFragmentDoc = gql`
    fragment NotificationNoNesting on Notification {
  _id
  system
  message
  url
  timestamp
}
    `;
export const BufferNoNestingFragmentDoc = gql`
    fragment BufferNoNesting on Buffer {
  type
  data
}
    `;
export const PermissionCoreNoNestingFragmentDoc = gql`
    fragment PermissionCoreNoNesting on PermissionCore {
  _id
  name
}
    `;
export const ChaincodeEventNoNestingFragmentDoc = gql`
    fragment ChaincodeEventNoNesting on ChaincodeEvent {
  eventName
  payloadString
  blockNumber
  transactionId
  status
}
    `;
export const VoteNoNestingFragmentDoc = gql`
    fragment VoteNoNesting on Vote {
  _id
  rid
  value
  settled
}
    `;
export const InfoDeepNestingFragmentDoc = gql`
    fragment InfoDeepNesting on Info {
  consDomain
  orgName
  channelName
  publicIpV4
  publicKey
  isAdminMode
  isJoined
}
    `;
export const SwarmDeepNestingFragmentDoc = gql`
    fragment SwarmDeepNesting on Swarm {
  nodes
  inspect
}
    `;
export const ContainerDeepNestingFragmentDoc = gql`
    fragment ContainerDeepNesting on Container {
  Name
  Health
}
    `;
export const ChaincodeDeepNestingFragmentDoc = gql`
    fragment ChaincodeDeepNesting on Chaincode {
  name
}
    `;
export const ChannelDeepNestingFragmentDoc = gql`
    fragment ChannelDeepNesting on Channel {
  _id
  chaincodes {
    ...ChaincodeDeepNesting
  }
}
    ${ChaincodeDeepNestingFragmentDoc}`;
export const ChaincodeQueryDeepNestingFragmentDoc = gql`
    fragment ChaincodeQueryDeepNesting on ChaincodeQuery {
  installed {
    ...ChaincodeDeepNesting
  }
  instantiated {
    ...ChaincodeDeepNesting
  }
}
    ${ChaincodeDeepNestingFragmentDoc}`;
export const ActionDeepNestingFragmentDoc = gql`
    fragment ActionDeepNesting on Action {
  _id
  name
  progress
  log
}
    `;
export const ConsortiumApprovalDeepNestingFragmentDoc = gql`
    fragment ConsortiumApprovalDeepNesting on ConsortiumApproval {
  _id
  cid
  rpub
  pub
  sig
}
    `;
export const ConsortiumRequestDeepNestingFragmentDoc = gql`
    fragment ConsortiumRequestDeepNesting on ConsortiumRequest {
  _id
  cid
  rpub
  sig
  confirmed
  approvals {
    ...ConsortiumApprovalDeepNesting
  }
}
    ${ConsortiumApprovalDeepNestingFragmentDoc}`;
export const ConsortiumMemberDeepNestingFragmentDoc = gql`
    fragment ConsortiumMemberDeepNesting on ConsortiumMember {
  _id
  cid
  pub
  apub
  token
}
    `;
export const ConsortiumDeepNestingFragmentDoc = gql`
    fragment ConsortiumDeepNesting on Consortium {
  cid
  name
  desc
  founder
  requests {
    ...ConsortiumRequestDeepNesting
  }
  approvals {
    ...ConsortiumApprovalDeepNesting
  }
  members {
    ...ConsortiumMemberDeepNesting
  }
}
    ${ConsortiumRequestDeepNestingFragmentDoc}
${ConsortiumApprovalDeepNestingFragmentDoc}
${ConsortiumMemberDeepNestingFragmentDoc}`;
export const LobbyDeepNestingFragmentDoc = gql`
    fragment LobbyDeepNesting on Lobby {
  consortia {
    ...ConsortiumDeepNesting
  }
  requested {
    ...ConsortiumDeepNesting
  }
  joined {
    ...ConsortiumDeepNesting
  }
  member {
    ...ConsortiumMemberDeepNesting
  }
}
    ${ConsortiumDeepNestingFragmentDoc}
${ConsortiumMemberDeepNestingFragmentDoc}`;
export const RoleCoreDeepNestingFragmentDoc = gql`
    fragment RoleCoreDeepNesting on RoleCore {
  _id
  name
}
    `;
export const UserDeepNestingFragmentDoc = gql`
    fragment UserDeepNesting on User {
  _id
  name
  passHash
  roles {
    ...RoleCoreDeepNesting
  }
}
    ${RoleCoreDeepNestingFragmentDoc}`;
export const UserCoreDeepNestingFragmentDoc = gql`
    fragment UserCoreDeepNesting on UserCore {
  _id
  name
}
    `;
export const RoleDeepNestingFragmentDoc = gql`
    fragment RoleDeepNesting on Role {
  _id
  name
  users {
    ...UserCoreDeepNesting
  }
}
    ${UserCoreDeepNestingFragmentDoc}`;
export const OrganizationDeepNestingFragmentDoc = gql`
    fragment OrganizationDeepNesting on Organization {
  _id
  name
  publicIpV4
  publicKey
  users {
    ...UserDeepNesting
  }
  roles {
    ...RoleDeepNesting
  }
}
    ${UserDeepNestingFragmentDoc}
${RoleDeepNestingFragmentDoc}`;
export const VoteCoreDeepNestingFragmentDoc = gql`
    fragment VoteCoreDeepNesting on VoteCore {
  _id
  rid
  value
  settled
  voter {
    ...OrganizationDeepNesting
  }
}
    ${OrganizationDeepNestingFragmentDoc}`;
export const RequestCoreDeepNestingFragmentDoc = gql`
    fragment RequestCoreDeepNesting on RequestCore {
  _id
  type
  key
  subkey
  parent
  children
  quorum
  ownvote {
    ...VoteCoreDeepNesting
  }
  settled
}
    ${VoteCoreDeepNestingFragmentDoc}`;
export const RequestDeepNestingFragmentDoc = gql`
    fragment RequestDeepNesting on Request {
  _id
  type
  key
  subkey
  parent {
    ...RequestCoreDeepNesting
  }
  children {
    ...RequestCoreDeepNesting
  }
  quorum {
    ...OrganizationDeepNesting
  }
  votes {
    ...VoteCoreDeepNesting
  }
  ownvote {
    ...VoteCoreDeepNesting
  }
  settled
}
    ${RequestCoreDeepNestingFragmentDoc}
${OrganizationDeepNestingFragmentDoc}
${VoteCoreDeepNestingFragmentDoc}`;
export const GovernanceDeepNestingFragmentDoc = gql`
    fragment GovernanceDeepNesting on Governance {
  request {
    ...RequestDeepNesting
  }
  requests {
    ...RequestDeepNesting
  }
}
    ${RequestDeepNestingFragmentDoc}`;
export const PermissionDeepNestingFragmentDoc = gql`
    fragment PermissionDeepNesting on Permission {
  _id
  name
  roles {
    ...RoleCoreDeepNesting
  }
}
    ${RoleCoreDeepNestingFragmentDoc}`;
export const PricingDeepNestingFragmentDoc = gql`
    fragment PricingDeepNesting on Pricing {
  currency
  initialPrice
  pricePerMonth
  paymentInfo
}
    `;
export const DappDeepNestingFragmentDoc = gql`
    fragment DappDeepNesting on Dapp {
  _id
  title
  version
  imageId
  dockerImage
  pricing {
    ...PricingDeepNesting
  }
}
    ${PricingDeepNestingFragmentDoc}`;
export const InstalledDappDeepNestingFragmentDoc = gql`
    fragment InstalledDappDeepNesting on InstalledDapp {
  hostName
  dapp {
    ...DappDeepNesting
  }
}
    ${DappDeepNestingFragmentDoc}`;
export const DappsDeepNestingFragmentDoc = gql`
    fragment DappsDeepNesting on Dapps {
  installable {
    ...DappDeepNesting
  }
  installed {
    ...InstalledDappDeepNesting
  }
}
    ${DappDeepNestingFragmentDoc}
${InstalledDappDeepNestingFragmentDoc}`;
export const FormFieldDeepNestingFragmentDoc = gql`
    fragment FormFieldDeepNesting on FormField {
  type
  name
  label
  value
  required
}
    `;
export const PluginDeepNestingFragmentDoc = gql`
    fragment PluginDeepNesting on Plugin {
  _id
  title
  version
  imageId
  pricing {
    ...PricingDeepNesting
  }
  parameters {
    ...FormFieldDeepNesting
  }
  installed
}
    ${PricingDeepNestingFragmentDoc}
${FormFieldDeepNestingFragmentDoc}`;
export const PluginsDeepNestingFragmentDoc = gql`
    fragment PluginsDeepNesting on Plugins {
  installable {
    ...PluginDeepNesting
  }
  installed {
    ...PluginDeepNesting
  }
}
    ${PluginDeepNestingFragmentDoc}`;
export const NotificationDeepNestingFragmentDoc = gql`
    fragment NotificationDeepNesting on Notification {
  _id
  system
  message
  url
  timestamp
}
    `;
export const BufferDeepNestingFragmentDoc = gql`
    fragment BufferDeepNesting on Buffer {
  type
  data
}
    `;
export const PermissionCoreDeepNestingFragmentDoc = gql`
    fragment PermissionCoreDeepNesting on PermissionCore {
  _id
  name
}
    `;
export const ChaincodeEventDeepNestingFragmentDoc = gql`
    fragment ChaincodeEventDeepNesting on ChaincodeEvent {
  eventName
  payloadString
  blockNumber
  transactionId
  status
}
    `;
export const ConsortiumLocalDeepNestingFragmentDoc = gql`
    fragment ConsortiumLocalDeepNesting on ConsortiumLocal {
  organizations {
    ...OrganizationDeepNesting
  }
}
    ${OrganizationDeepNestingFragmentDoc}`;
export const VoteDeepNestingFragmentDoc = gql`
    fragment VoteDeepNesting on Vote {
  _id
  rid
  value
  request {
    ...RequestCoreDeepNesting
  }
  children {
    ...RequestCoreDeepNesting
  }
  settled
  voter {
    ...OrganizationDeepNesting
  }
}
    ${RequestCoreDeepNestingFragmentDoc}
${OrganizationDeepNestingFragmentDoc}`;
export const FullQueryDocument = gql`
    query FullQuery {
  info {
    ...Info
  }
  organization {
    ...Organization
  }
  organizations {
    ...Organization
  }
  lobby {
    ...Lobby
  }
  users {
    ...User
  }
  roles {
    ...Role
  }
  dapps {
    ...Dapps
  }
  plugins {
    ...Plugins
  }
}
    ${InfoFragmentDoc}
${OrganizationFragmentDoc}
${LobbyFragmentDoc}
${UserFragmentDoc}
${RoleFragmentDoc}
${DappsFragmentDoc}
${PluginsFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class FullQueryGQL extends Apollo.Query<FullQueryQuery, FullQueryQueryVariables> {
    document = FullQueryDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListRequestsDocument = gql`
    query ListRequests {
  gov {
    requests {
      ...Request
    }
  }
}
    ${RequestFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListRequestsGQL extends Apollo.Query<ListRequestsQuery, ListRequestsQueryVariables> {
    document = ListRequestsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListConsortiaDocument = gql`
    query ListConsortia {
  lobby {
    consortia {
      ...Consortium
    }
  }
}
    ${ConsortiumFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListConsortiaGQL extends Apollo.Query<ListConsortiaQuery, ListConsortiaQueryVariables> {
    document = ListConsortiaDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListChannelsDocument = gql`
    query ListChannels {
  channels {
    ...Channel
  }
}
    ${ChannelFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListChannelsGQL extends Apollo.Query<ListChannelsQuery, ListChannelsQueryVariables> {
    document = ListChannelsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListDappsDocument = gql`
    query ListDapps {
  dapps {
    installable {
      ...Dapp
    }
    installed {
      ...InstalledDapp
    }
  }
}
    ${DappFragmentDoc}
${InstalledDappFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListDappsGQL extends Apollo.Query<ListDappsQuery, ListDappsQueryVariables> {
    document = ListDappsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListInstalledDappsDocument = gql`
    query ListInstalledDapps {
  dapps {
    installed {
      ...InstalledDapp
    }
  }
}
    ${InstalledDappFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListInstalledDappsGQL extends Apollo.Query<ListInstalledDappsQuery, ListInstalledDappsQueryVariables> {
    document = ListInstalledDappsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListPluginsDocument = gql`
    query ListPlugins {
  plugins {
    installable {
      ...Plugin
    }
    installed {
      ...Plugin
    }
  }
}
    ${PluginFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListPluginsGQL extends Apollo.Query<ListPluginsQuery, ListPluginsQueryVariables> {
    document = ListPluginsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListAllOrgsRolesDocument = gql`
    query ListAllOrgsRoles {
  organizations {
    _id
    name
    roles {
      _id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ListAllOrgsRolesGQL extends Apollo.Query<ListAllOrgsRolesQuery, ListAllOrgsRolesQueryVariables> {
    document = ListAllOrgsRolesDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListUsersDocument = gql`
    query ListUsers {
  users {
    ...User
  }
}
    ${UserFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListUsersGQL extends Apollo.Query<ListUsersQuery, ListUsersQueryVariables> {
    document = ListUsersDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const QueryUserDocument = gql`
    query QueryUser($_id: String!) {
  users(_id: $_id) {
    ...User
  }
}
    ${UserFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class QueryUserGQL extends Apollo.Query<QueryUserQuery, QueryUserQueryVariables> {
    document = QueryUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const PutUserDocument = gql`
    mutation PutUser($_id: String, $name: String!, $password: String) {
  putUser(_id: $_id, name: $name, password: $password) {
    ...UserCore
  }
}
    ${UserCoreFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class PutUserGQL extends Apollo.Mutation<PutUserMutation, PutUserMutationVariables> {
    document = PutUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveUserDocument = gql`
    mutation RemoveUser($_id: String!) {
  removeUser(_id: $_id)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveUserGQL extends Apollo.Mutation<RemoveUserMutation, RemoveUserMutationVariables> {
    document = RemoveUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LoginUserDocument = gql`
    mutation LoginUser($_id: String!, $password: String!) {
  loginUser(_id: $_id, password: $password)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginUserGQL extends Apollo.Mutation<LoginUserMutation, LoginUserMutationVariables> {
    document = LoginUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ListRolesDocument = gql`
    query ListRoles {
  roles {
    ...Role
  }
}
    ${RoleFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ListRolesGQL extends Apollo.Query<ListRolesQuery, ListRolesQueryVariables> {
    document = ListRolesDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const QueryRoleDocument = gql`
    query QueryRole($_id: String!) {
  roles(_id: $_id) {
    ...Role
  }
}
    ${RoleFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class QueryRoleGQL extends Apollo.Query<QueryRoleQuery, QueryRoleQueryVariables> {
    document = QueryRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ChaincodeEventDocument = gql`
    subscription ChaincodeEvent($input: ChaincodeEventListenInput!) {
  chaincodeEvent(input: $input) {
    ...ChaincodeEvent
  }
}
    ${ChaincodeEventFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ChaincodeEventGQL extends Apollo.Subscription<ChaincodeEventSubscription, ChaincodeEventSubscriptionVariables> {
    document = ChaincodeEventDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const PutRoleDocument = gql`
    mutation PutRole($_id: String, $name: String!) {
  putRole(_id: $_id, name: $name) {
    ...RoleCore
  }
}
    ${RoleCoreFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class PutRoleGQL extends Apollo.Mutation<PutRoleMutation, PutRoleMutationVariables> {
    document = PutRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveRoleDocument = gql`
    mutation RemoveRole($_id: String!) {
  removeRole(_id: $_id)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveRoleGQL extends Apollo.Mutation<RemoveRoleMutation, RemoveRoleMutationVariables> {
    document = RemoveRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AttachRoleToUserDocument = gql`
    mutation AttachRoleToUser($roleId: String!, $userId: String!) {
  attachRoleToUser(roleId: $roleId, userId: $userId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AttachRoleToUserGQL extends Apollo.Mutation<AttachRoleToUserMutation, AttachRoleToUserMutationVariables> {
    document = AttachRoleToUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveRoleFromUserDocument = gql`
    mutation RemoveRoleFromUser($roleId: String!, $userId: String!) {
  removeRoleFromUser(roleId: $roleId, userId: $userId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveRoleFromUserGQL extends Apollo.Mutation<RemoveRoleFromUserMutation, RemoveRoleFromUserMutationVariables> {
    document = RemoveRoleFromUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RequestDocument = gql`
    mutation Request($request: RequestInput) {
  request(request: $request)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RequestGQL extends Apollo.Mutation<RequestMutation, RequestMutationVariables> {
    document = RequestDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const VoteDocument = gql`
    mutation Vote($vote: VoteInput) {
  vote(vote: $vote)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class VoteGQL extends Apollo.Mutation<VoteMutation, VoteMutationVariables> {
    document = VoteDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const InstallDappDocument = gql`
    mutation InstallDapp($dapp: DappManualInstallInput) {
  installDapp(dapp: $dapp)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class InstallDappGQL extends Apollo.Mutation<InstallDappMutation, InstallDappMutationVariables> {
    document = InstallDappDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const InstallPluginDocument = gql`
    mutation InstallPlugin($plugin: PluginInstallInput) {
  installPlugin(plugin: $plugin)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class InstallPluginGQL extends Apollo.Mutation<InstallPluginMutation, InstallPluginMutationVariables> {
    document = InstallPluginDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }