import './polyfills';
declare const _default: {
    FullQuery(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").FullQueryQuery>;
    ListRequests(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListRequestsQuery>;
    ListConsortia(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListConsortiaQuery>;
    ListChannels(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListChannelsQuery>;
    ListDapps(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListDappsQuery>;
    ListInstalledDapps(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListInstalledDappsQuery>;
    ListPlugins(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListPluginsQuery>;
    ListAllOrgsRoles(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListAllOrgsRolesQuery>;
    ListUsers(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListUsersQuery>;
    QueryUser(variables: import("./generated/queries").Exact<{
        _id: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").QueryUserQuery>;
    PutUser(variables: import("./generated/queries").Exact<{
        _id?: string;
        name: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").PutUserMutation>;
    RemoveUser(variables: import("./generated/queries").Exact<{
        _id: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").RemoveUserMutation>;
    LoginUser(variables: import("./generated/queries").Exact<{
        _id: string;
        password: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").LoginUserMutation>;
    ListRoles(variables?: import("./generated/queries").Exact<{
        [key: string]: never;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").ListRolesQuery>;
    QueryRole(variables: import("./generated/queries").Exact<{
        _id: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").QueryRoleQuery>;
    PutRole(variables: import("./generated/queries").Exact<{
        _id?: string;
        name: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").PutRoleMutation>;
    RemoveRole(variables: import("./generated/queries").Exact<{
        _id: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").RemoveRoleMutation>;
    AttachRoleToUser(variables: import("./generated/queries").Exact<{
        roleId: string;
        userId: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").AttachRoleToUserMutation>;
    RemoveRoleFromUser(variables: import("./generated/queries").Exact<{
        roleId: string;
        userId: string;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").RemoveRoleFromUserMutation>;
    InstallDapp(variables?: import("./generated/queries").Exact<{
        dapp?: import("./generated/queries").DappInput;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").InstallDappMutation>;
    InstallPlugin(variables?: import("./generated/queries").Exact<{
        plugin?: import("./generated/queries").PluginInput;
    }>, requestHeaders?: Record<string, string> | string[][] | import("graphql-request/dist/types.dom").Headers): Promise<import("./generated/queries").InstallPluginMutation>;
};
export default _default;
