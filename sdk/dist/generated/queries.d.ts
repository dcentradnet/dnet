import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
export declare type Maybe<T> = T | null;
export declare type Exact<T extends {
    [key: string]: unknown;
}> = {
    [K in keyof T]: T[K];
};
export declare type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]?: Maybe<T[SubKey]>;
};
export declare type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export declare type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    JSON: any;
};
export declare type Action = {
    __typename?: 'Action';
    log?: Maybe<Array<Maybe<Scalars['String']>>>;
    name: Scalars['String'];
    progress?: Maybe<Scalars['Float']>;
};
export declare type Buffer = {
    __typename?: 'Buffer';
    data?: Maybe<Array<Maybe<Scalars['Int']>>>;
    type?: Maybe<Scalars['String']>;
};
export declare type Chaincode = {
    __typename?: 'Chaincode';
    name: Scalars['String'];
};
export declare type ChaincodeQuery = {
    __typename?: 'ChaincodeQuery';
    installed?: Maybe<Array<Chaincode>>;
    instantiated?: Maybe<Array<Chaincode>>;
};
export declare type Channel = {
    __typename?: 'Channel';
    _id: Scalars['String'];
    chaincodes?: Maybe<Array<Chaincode>>;
};
export declare type Consortium = {
    __typename?: 'Consortium';
    cid: Scalars['String'];
    name: Scalars['String'];
    desc?: Maybe<Scalars['String']>;
    founder?: Maybe<Scalars['String']>;
    requests?: Maybe<Array<Maybe<ConsortiumRequest>>>;
    approvals?: Maybe<Array<Maybe<ConsortiumApproval>>>;
    members?: Maybe<Array<Maybe<ConsortiumMember>>>;
};
export declare type ConsortiumApprovalsArgs = {
    pub?: Maybe<Scalars['String']>;
};
export declare type ConsortiumMembersArgs = {
    pub?: Maybe<Scalars['String']>;
};
export declare type ConsortiumApproval = {
    __typename?: 'ConsortiumApproval';
    _id: Scalars['String'];
    cid: Scalars['String'];
    rpub?: Maybe<Scalars['String']>;
    pub?: Maybe<Scalars['String']>;
    sig?: Maybe<Scalars['String']>;
};
export declare type ConsortiumLocal = {
    __typename?: 'ConsortiumLocal';
    organizations?: Maybe<Array<Organization>>;
};
export declare type ConsortiumMember = {
    __typename?: 'ConsortiumMember';
    _id: Scalars['String'];
    cid: Scalars['String'];
    pub?: Maybe<Scalars['String']>;
    apub?: Maybe<Scalars['String']>;
    token?: Maybe<Scalars['String']>;
    name?: Maybe<Scalars['String']>;
};
export declare type ConsortiumRequest = {
    __typename?: 'ConsortiumRequest';
    _id: Scalars['String'];
    cid: Scalars['String'];
    rpub: Scalars['String'];
    sig: Scalars['String'];
    confirmed?: Maybe<Scalars['Boolean']>;
    approvals?: Maybe<Array<Maybe<ConsortiumApproval>>>;
};
export declare type Container = {
    __typename?: 'Container';
    Health?: Maybe<Scalars['JSON']>;
    Name: Scalars['String'];
};
export declare type Dapp = {
    __typename?: 'Dapp';
    _id: Scalars['String'];
    title: Scalars['String'];
    version: Scalars['String'];
    imageId?: Maybe<Scalars['String']>;
    dockerImage: Scalars['String'];
    pricing?: Maybe<Pricing>;
};
export declare type DappInput = {
    dockerImage: Scalars['String'];
    title: Scalars['String'];
    version: Scalars['String'];
};
export declare type Dapps = {
    __typename?: 'Dapps';
    installable?: Maybe<Array<Dapp>>;
    installed?: Maybe<Array<InstalledDapp>>;
};
export declare type FormField = {
    __typename?: 'FormField';
    type: Scalars['String'];
    name: Scalars['String'];
    label: Scalars['String'];
    value: Scalars['String'];
    required: Scalars['Boolean'];
};
export declare type Governance = {
    __typename?: 'Governance';
    requests?: Maybe<Array<Request>>;
};
export declare type Info = {
    __typename?: 'Info';
    channelName?: Maybe<Scalars['String']>;
    consDomain?: Maybe<Scalars['String']>;
    isAdminMode?: Maybe<Scalars['Boolean']>;
    isJoined?: Maybe<Scalars['Boolean']>;
    orgName?: Maybe<Scalars['String']>;
    publicIpV4?: Maybe<Scalars['String']>;
    publicKey?: Maybe<Scalars['String']>;
};
export declare type InstalledDapp = {
    __typename?: 'InstalledDapp';
    dapp: Dapp;
    hostName: Scalars['String'];
};
export declare type Lobby = {
    __typename?: 'Lobby';
    consortia?: Maybe<Array<Consortium>>;
    joined?: Maybe<Consortium>;
    member?: Maybe<ConsortiumMember>;
    requested?: Maybe<Consortium>;
};
export declare type Mutation = {
    __typename?: 'Mutation';
    attachRoleToUser?: Maybe<Scalars['Boolean']>;
    chaincodeInvoke?: Maybe<Scalars['Boolean']>;
    installDapp?: Maybe<Scalars['Boolean']>;
    installPlugin?: Maybe<Scalars['Boolean']>;
    loginUser?: Maybe<Scalars['String']>;
    putRole?: Maybe<RoleCore>;
    putUser?: Maybe<UserCore>;
    removeRole?: Maybe<Scalars['Boolean']>;
    removeRoleFromUser?: Maybe<Scalars['Boolean']>;
    removeUser?: Maybe<Scalars['Boolean']>;
    swarmInit?: Maybe<Scalars['Boolean']>;
    swarmJoin?: Maybe<Scalars['Boolean']>;
    vote?: Maybe<Scalars['Int']>;
};
export declare type MutationAttachRoleToUserArgs = {
    roleId?: Maybe<Scalars['String']>;
    userId?: Maybe<Scalars['String']>;
};
export declare type MutationChaincodeInvokeArgs = {
    args?: Maybe<Array<Maybe<Scalars['String']>>>;
    chaincode?: Maybe<Scalars['String']>;
    method?: Maybe<Scalars['String']>;
};
export declare type MutationInstallDappArgs = {
    dapp?: Maybe<DappInput>;
};
export declare type MutationInstallPluginArgs = {
    plugin?: Maybe<PluginInput>;
};
export declare type MutationLoginUserArgs = {
    _id?: Maybe<Scalars['String']>;
    password?: Maybe<Scalars['String']>;
};
export declare type MutationPutRoleArgs = {
    _id?: Maybe<Scalars['String']>;
    name?: Maybe<Scalars['String']>;
};
export declare type MutationPutUserArgs = {
    _id?: Maybe<Scalars['String']>;
    name?: Maybe<Scalars['String']>;
    password?: Maybe<Scalars['String']>;
};
export declare type MutationRemoveRoleArgs = {
    _id?: Maybe<Scalars['String']>;
};
export declare type MutationRemoveRoleFromUserArgs = {
    roleId?: Maybe<Scalars['String']>;
    userId?: Maybe<Scalars['String']>;
};
export declare type MutationRemoveUserArgs = {
    _id?: Maybe<Scalars['String']>;
};
export declare type MutationSwarmInitArgs = {
    params?: Maybe<SwarmInitParamsInput>;
};
export declare type MutationSwarmJoinArgs = {
    params?: Maybe<SwarmJoinParamsInput>;
};
export declare type MutationVoteArgs = {
    rid?: Maybe<Scalars['String']>;
    value?: Maybe<Scalars['Int']>;
};
export declare type Organization = {
    __typename?: 'Organization';
    _id: Scalars['String'];
    name: Scalars['String'];
    publicIpV4?: Maybe<Scalars['String']>;
    publicKey?: Maybe<Scalars['String']>;
    roles?: Maybe<Array<Role>>;
    users?: Maybe<Array<User>>;
};
export declare type OrganizationRolesArgs = {
    _id?: Maybe<Scalars['String']>;
};
export declare type OrganizationUsersArgs = {
    _id?: Maybe<Scalars['String']>;
};
export declare type Plugin = {
    __typename?: 'Plugin';
    _id: Scalars['String'];
    title: Scalars['String'];
    version: Scalars['String'];
    imageId?: Maybe<Scalars['String']>;
    pricing?: Maybe<Pricing>;
    parameters?: Maybe<Array<FormField>>;
    installed?: Maybe<Scalars['Boolean']>;
};
export declare type PluginInput = {
    title: Scalars['String'];
    version: Scalars['String'];
};
export declare type Plugins = {
    __typename?: 'Plugins';
    installable?: Maybe<Array<Plugin>>;
    installed?: Maybe<Array<Plugin>>;
};
export declare type Pricing = {
    __typename?: 'Pricing';
    currency: Scalars['String'];
    initialPrice?: Maybe<Scalars['Int']>;
    pricePerMonth?: Maybe<Scalars['Int']>;
    paymentInfo?: Maybe<Scalars['String']>;
};
export declare type Query = {
    __typename?: 'Query';
    actions?: Maybe<Array<Maybe<Action>>>;
    chaincode?: Maybe<ChaincodeQuery>;
    chaincodeQuery?: Maybe<Buffer>;
    channels?: Maybe<Array<Maybe<Channel>>>;
    consortium?: Maybe<Consortium>;
    containers?: Maybe<Array<Maybe<Container>>>;
    dapps?: Maybe<Dapps>;
    gov?: Maybe<Governance>;
    info?: Maybe<Info>;
    lobby?: Maybe<Lobby>;
    organization?: Maybe<Organization>;
    organizations?: Maybe<Array<Organization>>;
    plugins?: Maybe<Plugins>;
    roles?: Maybe<Array<Role>>;
    swarm?: Maybe<Swarm>;
    users?: Maybe<Array<User>>;
};
export declare type QueryChaincodeQueryArgs = {
    args?: Maybe<Array<Maybe<Scalars['String']>>>;
    chaincode?: Maybe<Scalars['String']>;
    method?: Maybe<Scalars['String']>;
};
export declare type QueryRolesArgs = {
    _id?: Maybe<Scalars['String']>;
};
export declare type QueryUsersArgs = {
    _id?: Maybe<Scalars['String']>;
};
export declare type Request = {
    __typename?: 'Request';
    _id: Scalars['String'];
    children?: Maybe<Array<RequestCore>>;
    key: Scalars['String'];
    ownvote?: Maybe<VoteCore>;
    parent?: Maybe<RequestCore>;
    settled?: Maybe<Scalars['Boolean']>;
    subkey: Scalars['String'];
    type: Scalars['String'];
    votes?: Maybe<Array<VoteCore>>;
};
export declare type RequestCore = {
    __typename?: 'RequestCore';
    _id: Scalars['String'];
    children?: Maybe<Array<Scalars['String']>>;
    key: Scalars['String'];
    ownvote?: Maybe<VoteCore>;
    parent?: Maybe<Scalars['String']>;
    settled?: Maybe<Scalars['Boolean']>;
    subkey: Scalars['String'];
    type: Scalars['String'];
};
export declare type Role = {
    __typename?: 'Role';
    _id: Scalars['String'];
    name: Scalars['String'];
    users?: Maybe<Array<UserCore>>;
};
export declare type RoleCore = {
    __typename?: 'RoleCore';
    _id: Scalars['String'];
    name: Scalars['String'];
};
export declare type Swarm = {
    __typename?: 'Swarm';
    inspect?: Maybe<Scalars['JSON']>;
    nodes?: Maybe<Scalars['JSON']>;
};
export declare type SwarmInitParamsInput = {
    AdvertiseAddr?: Maybe<Scalars['String']>;
    ForceNewCluster?: Maybe<Scalars['Boolean']>;
    ListenAddr?: Maybe<Scalars['String']>;
};
export declare type SwarmJoinParamsInput = {
    AdvertiseAddr?: Maybe<Scalars['String']>;
    JoinToken?: Maybe<Scalars['String']>;
    ListenAddr?: Maybe<Scalars['String']>;
    RemoteAddrs?: Maybe<Array<Scalars['String']>>;
};
export declare type User = {
    __typename?: 'User';
    _id: Scalars['String'];
    name: Scalars['String'];
    passHash?: Maybe<Scalars['String']>;
    roles?: Maybe<Array<RoleCore>>;
};
export declare type UserCore = {
    __typename?: 'UserCore';
    _id: Scalars['String'];
    name: Scalars['String'];
};
export declare type Vote = {
    __typename?: 'Vote';
    _id?: Maybe<Scalars['String']>;
    children?: Maybe<Array<RequestCore>>;
    request?: Maybe<RequestCore>;
    rid: Scalars['String'];
    settled?: Maybe<Scalars['Boolean']>;
    value: Scalars['Int'];
    voter?: Maybe<Organization>;
};
export declare type VoteCore = {
    __typename?: 'VoteCore';
    _id?: Maybe<Scalars['String']>;
    rid: Scalars['String'];
    settled?: Maybe<Scalars['Boolean']>;
    value: Scalars['Int'];
    voter?: Maybe<Organization>;
};
export declare type InfoFragment = ({
    __typename?: 'Info';
} & Pick<Info, 'consDomain' | 'orgName' | 'channelName' | 'publicIpV4' | 'publicKey' | 'isAdminMode' | 'isJoined'>);
export declare type OrganizationFragment = ({
    __typename?: 'Organization';
} & Pick<Organization, '_id' | 'name' | 'publicIpV4' | 'publicKey'> & {
    users?: Maybe<Array<({
        __typename?: 'User';
    } & UserNoNestingFragment)>>;
    roles?: Maybe<Array<({
        __typename?: 'Role';
    } & RoleNoNestingFragment)>>;
});
export declare type UserFragment = ({
    __typename?: 'User';
} & Pick<User, '_id' | 'name' | 'passHash'> & {
    roles?: Maybe<Array<({
        __typename?: 'RoleCore';
    } & RoleCoreNoNestingFragment)>>;
});
export declare type RoleCoreFragment = ({
    __typename?: 'RoleCore';
} & Pick<RoleCore, '_id' | 'name'>);
export declare type RoleFragment = ({
    __typename?: 'Role';
} & Pick<Role, '_id' | 'name'> & {
    users?: Maybe<Array<({
        __typename?: 'UserCore';
    } & UserCoreNoNestingFragment)>>;
});
export declare type UserCoreFragment = ({
    __typename?: 'UserCore';
} & Pick<UserCore, '_id' | 'name'>);
export declare type ConsortiumFragment = ({
    __typename?: 'Consortium';
} & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'> & {
    requests?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumRequest';
    } & ConsortiumRequestNoNestingFragment)>>>;
    approvals?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumApproval';
    } & ConsortiumApprovalNoNestingFragment)>>>;
    members?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumMember';
    } & ConsortiumMemberNoNestingFragment)>>>;
});
export declare type ConsortiumRequestFragment = ({
    __typename?: 'ConsortiumRequest';
} & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'> & {
    approvals?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumApproval';
    } & ConsortiumApprovalNoNestingFragment)>>>;
});
export declare type ConsortiumApprovalFragment = ({
    __typename?: 'ConsortiumApproval';
} & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>);
export declare type ConsortiumMemberFragment = ({
    __typename?: 'ConsortiumMember';
} & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token' | 'name'>);
export declare type SwarmFragment = ({
    __typename?: 'Swarm';
} & Pick<Swarm, 'nodes' | 'inspect'>);
export declare type ContainerFragment = ({
    __typename?: 'Container';
} & Pick<Container, 'Name' | 'Health'>);
export declare type ChannelFragment = ({
    __typename?: 'Channel';
} & Pick<Channel, '_id'> & {
    chaincodes?: Maybe<Array<({
        __typename?: 'Chaincode';
    } & ChaincodeNoNestingFragment)>>;
});
export declare type ChaincodeFragment = ({
    __typename?: 'Chaincode';
} & Pick<Chaincode, 'name'>);
export declare type ChaincodeQueryFragment = ({
    __typename?: 'ChaincodeQuery';
} & {
    installed?: Maybe<Array<({
        __typename?: 'Chaincode';
    } & ChaincodeNoNestingFragment)>>;
    instantiated?: Maybe<Array<({
        __typename?: 'Chaincode';
    } & ChaincodeNoNestingFragment)>>;
});
export declare type ActionFragment = ({
    __typename?: 'Action';
} & Pick<Action, 'name' | 'progress' | 'log'>);
export declare type LobbyFragment = ({
    __typename?: 'Lobby';
} & {
    consortia?: Maybe<Array<({
        __typename?: 'Consortium';
    } & ConsortiumNoNestingFragment)>>;
    requested?: Maybe<({
        __typename?: 'Consortium';
    } & ConsortiumNoNestingFragment)>;
    joined?: Maybe<({
        __typename?: 'Consortium';
    } & ConsortiumNoNestingFragment)>;
    member?: Maybe<({
        __typename?: 'ConsortiumMember';
    } & ConsortiumMemberNoNestingFragment)>;
});
export declare type GovernanceFragment = ({
    __typename?: 'Governance';
} & {
    requests?: Maybe<Array<({
        __typename?: 'Request';
    } & RequestNoNestingFragment)>>;
});
export declare type RequestFragment = ({
    __typename?: 'Request';
} & Pick<Request, '_id' | 'type' | 'key' | 'subkey' | 'settled'> & {
    parent?: Maybe<({
        __typename?: 'RequestCore';
    } & RequestCoreNoNestingFragment)>;
    children?: Maybe<Array<({
        __typename?: 'RequestCore';
    } & RequestCoreNoNestingFragment)>>;
    votes?: Maybe<Array<({
        __typename?: 'VoteCore';
    } & VoteCoreNoNestingFragment)>>;
    ownvote?: Maybe<({
        __typename?: 'VoteCore';
    } & VoteCoreNoNestingFragment)>;
});
export declare type RequestCoreFragment = ({
    __typename?: 'RequestCore';
} & Pick<RequestCore, '_id' | 'type' | 'key' | 'subkey' | 'parent' | 'children' | 'settled'> & {
    ownvote?: Maybe<({
        __typename?: 'VoteCore';
    } & VoteCoreNoNestingFragment)>;
});
export declare type VoteCoreFragment = ({
    __typename?: 'VoteCore';
} & Pick<VoteCore, '_id' | 'rid' | 'value' | 'settled'> & {
    voter?: Maybe<({
        __typename?: 'Organization';
    } & OrganizationNoNestingFragment)>;
});
export declare type DappsFragment = ({
    __typename?: 'Dapps';
} & {
    installable?: Maybe<Array<({
        __typename?: 'Dapp';
    } & DappNoNestingFragment)>>;
    installed?: Maybe<Array<({
        __typename?: 'InstalledDapp';
    } & InstalledDappNoNestingFragment)>>;
});
export declare type DappFragment = ({
    __typename?: 'Dapp';
} & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'> & {
    pricing?: Maybe<({
        __typename?: 'Pricing';
    } & PricingNoNestingFragment)>;
});
export declare type PricingFragment = ({
    __typename?: 'Pricing';
} & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>);
export declare type InstalledDappFragment = ({
    __typename?: 'InstalledDapp';
} & Pick<InstalledDapp, 'hostName'> & {
    dapp: ({
        __typename?: 'Dapp';
    } & DappNoNestingFragment);
});
export declare type PluginsFragment = ({
    __typename?: 'Plugins';
} & {
    installable?: Maybe<Array<({
        __typename?: 'Plugin';
    } & PluginNoNestingFragment)>>;
    installed?: Maybe<Array<({
        __typename?: 'Plugin';
    } & PluginNoNestingFragment)>>;
});
export declare type PluginFragment = ({
    __typename?: 'Plugin';
} & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'> & {
    pricing?: Maybe<({
        __typename?: 'Pricing';
    } & PricingNoNestingFragment)>;
    parameters?: Maybe<Array<({
        __typename?: 'FormField';
    } & FormFieldNoNestingFragment)>>;
});
export declare type FormFieldFragment = ({
    __typename?: 'FormField';
} & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>);
export declare type BufferFragment = ({
    __typename?: 'Buffer';
} & Pick<Buffer, 'type' | 'data'>);
export declare type ConsortiumLocalFragment = ({
    __typename?: 'ConsortiumLocal';
} & {
    organizations?: Maybe<Array<({
        __typename?: 'Organization';
    } & OrganizationNoNestingFragment)>>;
});
export declare type VoteFragment = ({
    __typename?: 'Vote';
} & Pick<Vote, '_id' | 'rid' | 'value' | 'settled'> & {
    request?: Maybe<({
        __typename?: 'RequestCore';
    } & RequestCoreNoNestingFragment)>;
    children?: Maybe<Array<({
        __typename?: 'RequestCore';
    } & RequestCoreNoNestingFragment)>>;
    voter?: Maybe<({
        __typename?: 'Organization';
    } & OrganizationNoNestingFragment)>;
});
export declare type InfoNoNestingFragment = ({
    __typename?: 'Info';
} & Pick<Info, 'consDomain' | 'orgName' | 'channelName' | 'publicIpV4' | 'publicKey' | 'isAdminMode' | 'isJoined'>);
export declare type OrganizationNoNestingFragment = ({
    __typename?: 'Organization';
} & Pick<Organization, '_id' | 'name' | 'publicIpV4' | 'publicKey'>);
export declare type UserNoNestingFragment = ({
    __typename?: 'User';
} & Pick<User, '_id' | 'name' | 'passHash'>);
export declare type RoleCoreNoNestingFragment = ({
    __typename?: 'RoleCore';
} & Pick<RoleCore, '_id' | 'name'>);
export declare type RoleNoNestingFragment = ({
    __typename?: 'Role';
} & Pick<Role, '_id' | 'name'>);
export declare type UserCoreNoNestingFragment = ({
    __typename?: 'UserCore';
} & Pick<UserCore, '_id' | 'name'>);
export declare type ConsortiumNoNestingFragment = ({
    __typename?: 'Consortium';
} & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'>);
export declare type ConsortiumRequestNoNestingFragment = ({
    __typename?: 'ConsortiumRequest';
} & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'>);
export declare type ConsortiumApprovalNoNestingFragment = ({
    __typename?: 'ConsortiumApproval';
} & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>);
export declare type ConsortiumMemberNoNestingFragment = ({
    __typename?: 'ConsortiumMember';
} & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token' | 'name'>);
export declare type SwarmNoNestingFragment = ({
    __typename?: 'Swarm';
} & Pick<Swarm, 'nodes' | 'inspect'>);
export declare type ContainerNoNestingFragment = ({
    __typename?: 'Container';
} & Pick<Container, 'Name' | 'Health'>);
export declare type ChannelNoNestingFragment = ({
    __typename?: 'Channel';
} & Pick<Channel, '_id'>);
export declare type ChaincodeNoNestingFragment = ({
    __typename?: 'Chaincode';
} & Pick<Chaincode, 'name'>);
export declare type ActionNoNestingFragment = ({
    __typename?: 'Action';
} & Pick<Action, 'name' | 'progress' | 'log'>);
export declare type RequestNoNestingFragment = ({
    __typename?: 'Request';
} & Pick<Request, '_id' | 'type' | 'key' | 'subkey' | 'settled'>);
export declare type RequestCoreNoNestingFragment = ({
    __typename?: 'RequestCore';
} & Pick<RequestCore, '_id' | 'type' | 'key' | 'subkey' | 'parent' | 'children' | 'settled'>);
export declare type VoteCoreNoNestingFragment = ({
    __typename?: 'VoteCore';
} & Pick<VoteCore, '_id' | 'rid' | 'value' | 'settled'>);
export declare type DappNoNestingFragment = ({
    __typename?: 'Dapp';
} & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'>);
export declare type PricingNoNestingFragment = ({
    __typename?: 'Pricing';
} & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>);
export declare type InstalledDappNoNestingFragment = ({
    __typename?: 'InstalledDapp';
} & Pick<InstalledDapp, 'hostName'>);
export declare type PluginNoNestingFragment = ({
    __typename?: 'Plugin';
} & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'>);
export declare type FormFieldNoNestingFragment = ({
    __typename?: 'FormField';
} & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>);
export declare type BufferNoNestingFragment = ({
    __typename?: 'Buffer';
} & Pick<Buffer, 'type' | 'data'>);
export declare type VoteNoNestingFragment = ({
    __typename?: 'Vote';
} & Pick<Vote, '_id' | 'rid' | 'value' | 'settled'>);
export declare type InfoDeepNestingFragment = ({
    __typename?: 'Info';
} & Pick<Info, 'consDomain' | 'orgName' | 'channelName' | 'publicIpV4' | 'publicKey' | 'isAdminMode' | 'isJoined'>);
export declare type OrganizationDeepNestingFragment = ({
    __typename?: 'Organization';
} & Pick<Organization, '_id' | 'name' | 'publicIpV4' | 'publicKey'> & {
    users?: Maybe<Array<({
        __typename?: 'User';
    } & UserDeepNestingFragment)>>;
    roles?: Maybe<Array<({
        __typename?: 'Role';
    } & RoleDeepNestingFragment)>>;
});
export declare type UserDeepNestingFragment = ({
    __typename?: 'User';
} & Pick<User, '_id' | 'name' | 'passHash'> & {
    roles?: Maybe<Array<({
        __typename?: 'RoleCore';
    } & RoleCoreDeepNestingFragment)>>;
});
export declare type RoleCoreDeepNestingFragment = ({
    __typename?: 'RoleCore';
} & Pick<RoleCore, '_id' | 'name'>);
export declare type RoleDeepNestingFragment = ({
    __typename?: 'Role';
} & Pick<Role, '_id' | 'name'> & {
    users?: Maybe<Array<({
        __typename?: 'UserCore';
    } & UserCoreDeepNestingFragment)>>;
});
export declare type UserCoreDeepNestingFragment = ({
    __typename?: 'UserCore';
} & Pick<UserCore, '_id' | 'name'>);
export declare type ConsortiumDeepNestingFragment = ({
    __typename?: 'Consortium';
} & Pick<Consortium, 'cid' | 'name' | 'desc' | 'founder'> & {
    requests?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumRequest';
    } & ConsortiumRequestDeepNestingFragment)>>>;
    approvals?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumApproval';
    } & ConsortiumApprovalDeepNestingFragment)>>>;
    members?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumMember';
    } & ConsortiumMemberDeepNestingFragment)>>>;
});
export declare type ConsortiumRequestDeepNestingFragment = ({
    __typename?: 'ConsortiumRequest';
} & Pick<ConsortiumRequest, '_id' | 'cid' | 'rpub' | 'sig' | 'confirmed'> & {
    approvals?: Maybe<Array<Maybe<({
        __typename?: 'ConsortiumApproval';
    } & ConsortiumApprovalDeepNestingFragment)>>>;
});
export declare type ConsortiumApprovalDeepNestingFragment = ({
    __typename?: 'ConsortiumApproval';
} & Pick<ConsortiumApproval, '_id' | 'cid' | 'rpub' | 'pub' | 'sig'>);
export declare type ConsortiumMemberDeepNestingFragment = ({
    __typename?: 'ConsortiumMember';
} & Pick<ConsortiumMember, '_id' | 'cid' | 'pub' | 'apub' | 'token' | 'name'>);
export declare type SwarmDeepNestingFragment = ({
    __typename?: 'Swarm';
} & Pick<Swarm, 'nodes' | 'inspect'>);
export declare type ContainerDeepNestingFragment = ({
    __typename?: 'Container';
} & Pick<Container, 'Name' | 'Health'>);
export declare type ChannelDeepNestingFragment = ({
    __typename?: 'Channel';
} & Pick<Channel, '_id'> & {
    chaincodes?: Maybe<Array<({
        __typename?: 'Chaincode';
    } & ChaincodeDeepNestingFragment)>>;
});
export declare type ChaincodeDeepNestingFragment = ({
    __typename?: 'Chaincode';
} & Pick<Chaincode, 'name'>);
export declare type ChaincodeQueryDeepNestingFragment = ({
    __typename?: 'ChaincodeQuery';
} & {
    installed?: Maybe<Array<({
        __typename?: 'Chaincode';
    } & ChaincodeDeepNestingFragment)>>;
    instantiated?: Maybe<Array<({
        __typename?: 'Chaincode';
    } & ChaincodeDeepNestingFragment)>>;
});
export declare type ActionDeepNestingFragment = ({
    __typename?: 'Action';
} & Pick<Action, 'name' | 'progress' | 'log'>);
export declare type LobbyDeepNestingFragment = ({
    __typename?: 'Lobby';
} & {
    consortia?: Maybe<Array<({
        __typename?: 'Consortium';
    } & ConsortiumDeepNestingFragment)>>;
    requested?: Maybe<({
        __typename?: 'Consortium';
    } & ConsortiumDeepNestingFragment)>;
    joined?: Maybe<({
        __typename?: 'Consortium';
    } & ConsortiumDeepNestingFragment)>;
    member?: Maybe<({
        __typename?: 'ConsortiumMember';
    } & ConsortiumMemberDeepNestingFragment)>;
});
export declare type GovernanceDeepNestingFragment = ({
    __typename?: 'Governance';
} & {
    requests?: Maybe<Array<({
        __typename?: 'Request';
    } & RequestDeepNestingFragment)>>;
});
export declare type RequestDeepNestingFragment = ({
    __typename?: 'Request';
} & Pick<Request, '_id' | 'type' | 'key' | 'subkey' | 'settled'> & {
    parent?: Maybe<({
        __typename?: 'RequestCore';
    } & RequestCoreDeepNestingFragment)>;
    children?: Maybe<Array<({
        __typename?: 'RequestCore';
    } & RequestCoreDeepNestingFragment)>>;
    votes?: Maybe<Array<({
        __typename?: 'VoteCore';
    } & VoteCoreDeepNestingFragment)>>;
    ownvote?: Maybe<({
        __typename?: 'VoteCore';
    } & VoteCoreDeepNestingFragment)>;
});
export declare type RequestCoreDeepNestingFragment = ({
    __typename?: 'RequestCore';
} & Pick<RequestCore, '_id' | 'type' | 'key' | 'subkey' | 'parent' | 'children' | 'settled'> & {
    ownvote?: Maybe<({
        __typename?: 'VoteCore';
    } & VoteCoreDeepNestingFragment)>;
});
export declare type VoteCoreDeepNestingFragment = ({
    __typename?: 'VoteCore';
} & Pick<VoteCore, '_id' | 'rid' | 'value' | 'settled'> & {
    voter?: Maybe<({
        __typename?: 'Organization';
    } & OrganizationDeepNestingFragment)>;
});
export declare type DappsDeepNestingFragment = ({
    __typename?: 'Dapps';
} & {
    installable?: Maybe<Array<({
        __typename?: 'Dapp';
    } & DappDeepNestingFragment)>>;
    installed?: Maybe<Array<({
        __typename?: 'InstalledDapp';
    } & InstalledDappDeepNestingFragment)>>;
});
export declare type DappDeepNestingFragment = ({
    __typename?: 'Dapp';
} & Pick<Dapp, '_id' | 'title' | 'version' | 'imageId' | 'dockerImage'> & {
    pricing?: Maybe<({
        __typename?: 'Pricing';
    } & PricingDeepNestingFragment)>;
});
export declare type PricingDeepNestingFragment = ({
    __typename?: 'Pricing';
} & Pick<Pricing, 'currency' | 'initialPrice' | 'pricePerMonth' | 'paymentInfo'>);
export declare type InstalledDappDeepNestingFragment = ({
    __typename?: 'InstalledDapp';
} & Pick<InstalledDapp, 'hostName'> & {
    dapp: ({
        __typename?: 'Dapp';
    } & DappDeepNestingFragment);
});
export declare type PluginsDeepNestingFragment = ({
    __typename?: 'Plugins';
} & {
    installable?: Maybe<Array<({
        __typename?: 'Plugin';
    } & PluginDeepNestingFragment)>>;
    installed?: Maybe<Array<({
        __typename?: 'Plugin';
    } & PluginDeepNestingFragment)>>;
});
export declare type PluginDeepNestingFragment = ({
    __typename?: 'Plugin';
} & Pick<Plugin, '_id' | 'title' | 'version' | 'imageId' | 'installed'> & {
    pricing?: Maybe<({
        __typename?: 'Pricing';
    } & PricingDeepNestingFragment)>;
    parameters?: Maybe<Array<({
        __typename?: 'FormField';
    } & FormFieldDeepNestingFragment)>>;
});
export declare type FormFieldDeepNestingFragment = ({
    __typename?: 'FormField';
} & Pick<FormField, 'type' | 'name' | 'label' | 'value' | 'required'>);
export declare type BufferDeepNestingFragment = ({
    __typename?: 'Buffer';
} & Pick<Buffer, 'type' | 'data'>);
export declare type ConsortiumLocalDeepNestingFragment = ({
    __typename?: 'ConsortiumLocal';
} & {
    organizations?: Maybe<Array<({
        __typename?: 'Organization';
    } & OrganizationDeepNestingFragment)>>;
});
export declare type VoteDeepNestingFragment = ({
    __typename?: 'Vote';
} & Pick<Vote, '_id' | 'rid' | 'value' | 'settled'> & {
    request?: Maybe<({
        __typename?: 'RequestCore';
    } & RequestCoreDeepNestingFragment)>;
    children?: Maybe<Array<({
        __typename?: 'RequestCore';
    } & RequestCoreDeepNestingFragment)>>;
    voter?: Maybe<({
        __typename?: 'Organization';
    } & OrganizationDeepNestingFragment)>;
});
export declare type FullQueryQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type FullQueryQuery = ({
    __typename?: 'Query';
} & {
    info?: Maybe<({
        __typename?: 'Info';
    } & InfoFragment)>;
    organization?: Maybe<({
        __typename?: 'Organization';
    } & OrganizationFragment)>;
    organizations?: Maybe<Array<({
        __typename?: 'Organization';
    } & OrganizationFragment)>>;
    lobby?: Maybe<({
        __typename?: 'Lobby';
    } & LobbyFragment)>;
    users?: Maybe<Array<({
        __typename?: 'User';
    } & UserFragment)>>;
    roles?: Maybe<Array<({
        __typename?: 'Role';
    } & RoleFragment)>>;
    dapps?: Maybe<({
        __typename?: 'Dapps';
    } & DappsFragment)>;
    plugins?: Maybe<({
        __typename?: 'Plugins';
    } & PluginsFragment)>;
});
export declare type ListRequestsQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListRequestsQuery = ({
    __typename?: 'Query';
} & {
    gov?: Maybe<({
        __typename?: 'Governance';
    } & {
        requests?: Maybe<Array<({
            __typename?: 'Request';
        } & RequestFragment)>>;
    })>;
});
export declare type ListConsortiaQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListConsortiaQuery = ({
    __typename?: 'Query';
} & {
    lobby?: Maybe<({
        __typename?: 'Lobby';
    } & {
        consortia?: Maybe<Array<({
            __typename?: 'Consortium';
        } & ConsortiumFragment)>>;
    })>;
});
export declare type ListChannelsQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListChannelsQuery = ({
    __typename?: 'Query';
} & {
    channels?: Maybe<Array<Maybe<({
        __typename?: 'Channel';
    } & ChannelFragment)>>>;
});
export declare type ListDappsQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListDappsQuery = ({
    __typename?: 'Query';
} & {
    dapps?: Maybe<({
        __typename?: 'Dapps';
    } & {
        installable?: Maybe<Array<({
            __typename?: 'Dapp';
        } & DappFragment)>>;
        installed?: Maybe<Array<({
            __typename?: 'InstalledDapp';
        } & InstalledDappFragment)>>;
    })>;
});
export declare type ListInstalledDappsQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListInstalledDappsQuery = ({
    __typename?: 'Query';
} & {
    dapps?: Maybe<({
        __typename?: 'Dapps';
    } & {
        installed?: Maybe<Array<({
            __typename?: 'InstalledDapp';
        } & InstalledDappFragment)>>;
    })>;
});
export declare type ListPluginsQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListPluginsQuery = ({
    __typename?: 'Query';
} & {
    plugins?: Maybe<({
        __typename?: 'Plugins';
    } & {
        installable?: Maybe<Array<({
            __typename?: 'Plugin';
        } & PluginFragment)>>;
        installed?: Maybe<Array<({
            __typename?: 'Plugin';
        } & PluginFragment)>>;
    })>;
});
export declare type ListAllOrgsRolesQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListAllOrgsRolesQuery = ({
    __typename?: 'Query';
} & {
    organizations?: Maybe<Array<({
        __typename?: 'Organization';
    } & Pick<Organization, '_id' | 'name'> & {
        roles?: Maybe<Array<({
            __typename?: 'Role';
        } & Pick<Role, '_id' | 'name'>)>>;
    })>>;
});
export declare type ListUsersQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListUsersQuery = ({
    __typename?: 'Query';
} & {
    users?: Maybe<Array<({
        __typename?: 'User';
    } & UserFragment)>>;
});
export declare type QueryUserQueryVariables = Exact<{
    _id: Scalars['String'];
}>;
export declare type QueryUserQuery = ({
    __typename?: 'Query';
} & {
    users?: Maybe<Array<({
        __typename?: 'User';
    } & UserFragment)>>;
});
export declare type PutUserMutationVariables = Exact<{
    _id?: Maybe<Scalars['String']>;
    name: Scalars['String'];
}>;
export declare type PutUserMutation = ({
    __typename?: 'Mutation';
} & {
    putUser?: Maybe<({
        __typename?: 'UserCore';
    } & UserCoreFragment)>;
});
export declare type RemoveUserMutationVariables = Exact<{
    _id: Scalars['String'];
}>;
export declare type RemoveUserMutation = ({
    __typename?: 'Mutation';
} & Pick<Mutation, 'removeUser'>);
export declare type LoginUserMutationVariables = Exact<{
    _id: Scalars['String'];
    password: Scalars['String'];
}>;
export declare type LoginUserMutation = ({
    __typename?: 'Mutation';
} & Pick<Mutation, 'loginUser'>);
export declare type ListRolesQueryVariables = Exact<{
    [key: string]: never;
}>;
export declare type ListRolesQuery = ({
    __typename?: 'Query';
} & {
    roles?: Maybe<Array<({
        __typename?: 'Role';
    } & RoleFragment)>>;
});
export declare type QueryRoleQueryVariables = Exact<{
    _id: Scalars['String'];
}>;
export declare type QueryRoleQuery = ({
    __typename?: 'Query';
} & {
    roles?: Maybe<Array<({
        __typename?: 'Role';
    } & RoleFragment)>>;
});
export declare type PutRoleMutationVariables = Exact<{
    _id?: Maybe<Scalars['String']>;
    name: Scalars['String'];
}>;
export declare type PutRoleMutation = ({
    __typename?: 'Mutation';
} & {
    putRole?: Maybe<({
        __typename?: 'RoleCore';
    } & RoleCoreFragment)>;
});
export declare type RemoveRoleMutationVariables = Exact<{
    _id: Scalars['String'];
}>;
export declare type RemoveRoleMutation = ({
    __typename?: 'Mutation';
} & Pick<Mutation, 'removeRole'>);
export declare type AttachRoleToUserMutationVariables = Exact<{
    roleId: Scalars['String'];
    userId: Scalars['String'];
}>;
export declare type AttachRoleToUserMutation = ({
    __typename?: 'Mutation';
} & Pick<Mutation, 'attachRoleToUser'>);
export declare type RemoveRoleFromUserMutationVariables = Exact<{
    roleId: Scalars['String'];
    userId: Scalars['String'];
}>;
export declare type RemoveRoleFromUserMutation = ({
    __typename?: 'Mutation';
} & Pick<Mutation, 'removeRoleFromUser'>);
export declare type InstallDappMutationVariables = Exact<{
    dapp?: Maybe<DappInput>;
}>;
export declare type InstallDappMutation = ({
    __typename?: 'Mutation';
} & Pick<Mutation, 'installDapp'>);
export declare type InstallPluginMutationVariables = Exact<{
    plugin?: Maybe<PluginInput>;
}>;
export declare type InstallPluginMutation = ({
    __typename?: 'Mutation';
} & Pick<Mutation, 'installPlugin'>);
export declare const InfoFragmentDoc: import("graphql").DocumentNode;
export declare const UserNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const RoleNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const OrganizationFragmentDoc: import("graphql").DocumentNode;
export declare const RoleCoreNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const UserFragmentDoc: import("graphql").DocumentNode;
export declare const RoleCoreFragmentDoc: import("graphql").DocumentNode;
export declare const UserCoreNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const RoleFragmentDoc: import("graphql").DocumentNode;
export declare const UserCoreFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumRequestNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumApprovalNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumMemberNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumRequestFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumApprovalFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumMemberFragmentDoc: import("graphql").DocumentNode;
export declare const SwarmFragmentDoc: import("graphql").DocumentNode;
export declare const ContainerFragmentDoc: import("graphql").DocumentNode;
export declare const ChaincodeNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ChannelFragmentDoc: import("graphql").DocumentNode;
export declare const ChaincodeFragmentDoc: import("graphql").DocumentNode;
export declare const ChaincodeQueryFragmentDoc: import("graphql").DocumentNode;
export declare const ActionFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const LobbyFragmentDoc: import("graphql").DocumentNode;
export declare const RequestNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const GovernanceFragmentDoc: import("graphql").DocumentNode;
export declare const RequestCoreNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const VoteCoreNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const RequestFragmentDoc: import("graphql").DocumentNode;
export declare const RequestCoreFragmentDoc: import("graphql").DocumentNode;
export declare const OrganizationNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const VoteCoreFragmentDoc: import("graphql").DocumentNode;
export declare const DappNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const InstalledDappNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const DappsFragmentDoc: import("graphql").DocumentNode;
export declare const PricingNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const DappFragmentDoc: import("graphql").DocumentNode;
export declare const PricingFragmentDoc: import("graphql").DocumentNode;
export declare const InstalledDappFragmentDoc: import("graphql").DocumentNode;
export declare const PluginNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const PluginsFragmentDoc: import("graphql").DocumentNode;
export declare const FormFieldNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const PluginFragmentDoc: import("graphql").DocumentNode;
export declare const FormFieldFragmentDoc: import("graphql").DocumentNode;
export declare const BufferFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumLocalFragmentDoc: import("graphql").DocumentNode;
export declare const VoteFragmentDoc: import("graphql").DocumentNode;
export declare const InfoNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const SwarmNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ContainerNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ChannelNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ActionNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const BufferNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const VoteNoNestingFragmentDoc: import("graphql").DocumentNode;
export declare const InfoDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const SwarmDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ContainerDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ChaincodeDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ChannelDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ChaincodeQueryDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ActionDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumApprovalDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumRequestDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumMemberDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const LobbyDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const RoleCoreDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const UserDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const UserCoreDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const RoleDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const OrganizationDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const VoteCoreDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const RequestCoreDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const RequestDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const GovernanceDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const PricingDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const DappDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const InstalledDappDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const DappsDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const FormFieldDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const PluginDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const PluginsDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const BufferDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const ConsortiumLocalDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const VoteDeepNestingFragmentDoc: import("graphql").DocumentNode;
export declare const FullQueryDocument: import("graphql").DocumentNode;
export declare const ListRequestsDocument: import("graphql").DocumentNode;
export declare const ListConsortiaDocument: import("graphql").DocumentNode;
export declare const ListChannelsDocument: import("graphql").DocumentNode;
export declare const ListDappsDocument: import("graphql").DocumentNode;
export declare const ListInstalledDappsDocument: import("graphql").DocumentNode;
export declare const ListPluginsDocument: import("graphql").DocumentNode;
export declare const ListAllOrgsRolesDocument: import("graphql").DocumentNode;
export declare const ListUsersDocument: import("graphql").DocumentNode;
export declare const QueryUserDocument: import("graphql").DocumentNode;
export declare const PutUserDocument: import("graphql").DocumentNode;
export declare const RemoveUserDocument: import("graphql").DocumentNode;
export declare const LoginUserDocument: import("graphql").DocumentNode;
export declare const ListRolesDocument: import("graphql").DocumentNode;
export declare const QueryRoleDocument: import("graphql").DocumentNode;
export declare const PutRoleDocument: import("graphql").DocumentNode;
export declare const RemoveRoleDocument: import("graphql").DocumentNode;
export declare const AttachRoleToUserDocument: import("graphql").DocumentNode;
export declare const RemoveRoleFromUserDocument: import("graphql").DocumentNode;
export declare const InstallDappDocument: import("graphql").DocumentNode;
export declare const InstallPluginDocument: import("graphql").DocumentNode;
export declare type SdkFunctionWrapper = <T>(action: (requestHeaders?: Record<string, string>) => Promise<T>, operationName: string) => Promise<T>;
export declare function getSdk(client: GraphQLClient, withWrapper?: SdkFunctionWrapper): {
    FullQuery(variables?: FullQueryQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<FullQueryQuery>;
    ListRequests(variables?: ListRequestsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListRequestsQuery>;
    ListConsortia(variables?: ListConsortiaQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListConsortiaQuery>;
    ListChannels(variables?: ListChannelsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListChannelsQuery>;
    ListDapps(variables?: ListDappsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListDappsQuery>;
    ListInstalledDapps(variables?: ListInstalledDappsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListInstalledDappsQuery>;
    ListPlugins(variables?: ListPluginsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListPluginsQuery>;
    ListAllOrgsRoles(variables?: ListAllOrgsRolesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListAllOrgsRolesQuery>;
    ListUsers(variables?: ListUsersQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListUsersQuery>;
    QueryUser(variables: QueryUserQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<QueryUserQuery>;
    PutUser(variables: PutUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<PutUserMutation>;
    RemoveUser(variables: RemoveUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<RemoveUserMutation>;
    LoginUser(variables: LoginUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<LoginUserMutation>;
    ListRoles(variables?: ListRolesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ListRolesQuery>;
    QueryRole(variables: QueryRoleQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<QueryRoleQuery>;
    PutRole(variables: PutRoleMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<PutRoleMutation>;
    RemoveRole(variables: RemoveRoleMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<RemoveRoleMutation>;
    AttachRoleToUser(variables: AttachRoleToUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AttachRoleToUserMutation>;
    RemoveRoleFromUser(variables: RemoveRoleFromUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<RemoveRoleFromUserMutation>;
    InstallDapp(variables?: InstallDappMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<InstallDappMutation>;
    InstallPlugin(variables?: InstallPluginMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<InstallPluginMutation>;
};
export declare type Sdk = ReturnType<typeof getSdk>;
