"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/no-empty-function */
var path_1 = require("path");
var graphql_tools_1 = require("graphql-tools");
var graphql_file_loader_1 = require("@graphql-tools/graphql-file-loader");
var schema = graphql_tools_1.loadSchemaSync(path_1.join(__dirname, 'models/schema.graphql'), {
    loaders: [new graphql_file_loader_1.GraphQLFileLoader()]
});
var users = [{ _id: '0', name: 'Administrator' }, { _id: '1', name: 'Fred' }];
var roles = [{ _id: '0', name: 'Administrators' }, { _id: '1', name: 'Accountants' }];
// define local resolvers
var orgName = process.env.CLI_DOMAIN || 'Org1';
var resolvers = {
    Info: {
        consDomain: function () { return process.env.CONS_DOMAIN; },
        orgName: function () { return orgName; },
        channelName: function () { return process.env.CHANNEL_NAME; },
        publicIpV4: function () { return '127.0.0.1'; },
        //publicIpV6: ipV6,
        //publicKey: () => crypt.getPublicKeyString(),
        isAdminMode: function () { return false; },
        isJoined: function () { return true; }
    },
    Organization: {
        _id: function (orgName) { return orgName; },
        name: function (orgName) { return orgName; },
        users: function () { return users; },
        roles: function () { return roles; },
    },
    Swarm: {
    //nodes: () => docker.listNodes(),
    //inspect: () => docker.swarmInspect()
    },
    Channel: {
        _id: function () { return '0'; },
        chaincodes: function () { return []; }
    },
    Lobby: {
        consortia: function () { return []; },
    },
    Request: {
    /*parent: async(request: GovRequestCore) => gov.getRequestById(request._id),
    children: (request: GovRequestCore) => request.children.map(c => gov.getRequestById(c)),
    votes: (request: GovRequestCore, _args, context) => context.votesByRequest[request._id] || [],
    ownvote: async(request: GovRequestCore, _args, context) => {
        const votes = context.votesByRequest[request._id] || [];
        let ownvote = null;
        if(votes) {
            ownvote = votes.find((vote: GovVote) => vote.voter.name === orgName);
        }
        return ownvote;
    },
    settled: async(request: GovRequestCore, _args, context) => {
        const votes = context.votesByRequest[request._id] || [];
        return !!votes && votes.some((vote: GovVote) => vote.voter.name === orgName && vote.settled)
    }*/
    },
    Governance: {
    /*requests: async(_parent, _args, context): Promise<GovRequestCore[]> => {
        context.votesByRequest = await collectVotesByRequestFromAllOrgs();
        return gov.requests();
    }*/
    },
    User: {
        roles: function () { return roles; }
    },
    Role: {
        users: function () { return users; }
    },
    Dapps: {
    /*installable: async() => dapps.listStoreApps(),
    installed: async() => dapps.listRunning()*/
    },
    Plugins: {
    /*installable: async() => plugins.listInstallable(),
    installed: async() => plugins.listRunning()*/
    },
    Query: {
        organization: function () { return 'Org1'; },
        //consortium: () => lobby.joined(),
        organizations: function () { return ['Org1', 'Org2']; },
        swarm: function () { return ({}); },
        containers: function () { return []; },
        channels: function () { return []; },
        actions: function () { return []; },
        lobby: function () { return ({}); },
        gov: function () { return ({}); },
        users: function () { return users; },
        roles: function () { return roles; },
        dapps: function () { return ({}); },
        plugins: function () { return ({}); },
        chaincodeQuery: function () { return []; },
    },
    Mutation: {
        swarmInit: function (_parent, _a, _b) {
            var params = _a.params;
            var user = _b.user;
        },
        swarmJoin: function (_parent, _a, _b) {
            var params = _a.params;
            var user = _b.user;
        },
        vote: function (_parent, vote, _a) {
            var user = _a.user;
        },
        chaincodeInvoke: function (_parent, args, _a) {
            var user = _a.user;
        },
        loginUser: function (_parent, args) {
            return 'TestToken';
        },
        putUser: function (_parent, args, _a) {
            var user = _a.user;
        },
        putRole: function (_parent, args, _a) {
            var user = _a.user;
        },
        removeUser: function (_parent, args, _a) {
            var user = _a.user;
        },
        removeRole: function (_parent, args, _a) {
            var user = _a.user;
        },
        attachRoleToUser: function (_parent, args, _a) {
            var user = _a.user;
        },
        removeRoleFromUser: function (_parent, args, _a) {
            var user = _a.user;
        },
    }
};
exports.default = graphql_tools_1.addResolversToSchema({ schema: schema, resolvers: resolvers });
