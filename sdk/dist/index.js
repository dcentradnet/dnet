"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("./polyfills");
var cross_fetch_1 = require("cross-fetch");
var graphql_request_1 = require("graphql-request");
var queries_1 = require("./generated/queries");
var mock_graphql_client_1 = require("./mock_graphql_client");
var client;
if (process.env.CLI_DOMAIN) {
    client = new graphql_request_1.GraphQLClient(process.env.CLI_DOMAIN, {
        timeout: 100000,
        fetch: cross_fetch_1.fetch
    });
}
else {
    client = mock_graphql_client_1.mockClient;
}
exports.default = queries_1.getSdk(client);
