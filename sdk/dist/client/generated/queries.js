"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InfoNoNestingFragmentDoc = exports.VoteNoNestingFragmentDoc = exports.ContainerFragmentDoc = exports.ChaincodeFragmentDoc = exports.OrganizationFragmentDoc = exports.RoleNoNestingFragmentDoc = exports.UserNoNestingFragmentDoc = exports.UserCoreFragmentDoc = exports.ConsortiumMemberFragmentDoc = exports.PluginFragmentDoc = exports.FormFieldNoNestingFragmentDoc = exports.FormFieldFragmentDoc = exports.BufferFragmentDoc = exports.ConsortiumLocalFragmentDoc = exports.ConsortiumRequestFragmentDoc = exports.RoleCoreFragmentDoc = exports.PluginsFragmentDoc = exports.PluginNoNestingFragmentDoc = exports.ActionFragmentDoc = exports.InstalledDappFragmentDoc = exports.ChannelFragmentDoc = exports.ChaincodeNoNestingFragmentDoc = exports.PricingFragmentDoc = exports.SwarmFragmentDoc = exports.DappFragmentDoc = exports.PricingNoNestingFragmentDoc = exports.ConsortiumApprovalFragmentDoc = exports.DappsFragmentDoc = exports.InstalledDappNoNestingFragmentDoc = exports.DappNoNestingFragmentDoc = exports.ConsortiumFragmentDoc = exports.ConsortiumApprovalNoNestingFragmentDoc = exports.ConsortiumRequestNoNestingFragmentDoc = exports.VoteCoreFragmentDoc = exports.RoleFragmentDoc = exports.UserCoreNoNestingFragmentDoc = exports.RequestCoreFragmentDoc = exports.UserFragmentDoc = exports.RoleCoreNoNestingFragmentDoc = exports.RequestFragmentDoc = exports.VoteCoreNoNestingFragmentDoc = exports.InfoFragmentDoc = exports.GovernanceFragmentDoc = exports.RequestNoNestingFragmentDoc = exports.VoteFragmentDoc = exports.OrganizationNoNestingFragmentDoc = exports.RequestCoreNoNestingFragmentDoc = exports.LobbyFragmentDoc = exports.ConsortiumMemberNoNestingFragmentDoc = exports.ConsortiumNoNestingFragmentDoc = void 0;
exports.InstallDappDocument = exports.ListAllOrgsRolesGQL = exports.ListAllOrgsRolesDocument = exports.ListPluginsGQL = exports.ListPluginsDocument = exports.ListInstalledDappsGQL = exports.ListInstalledDappsDocument = exports.ListDappsGQL = exports.ListDappsDocument = exports.ListChannelsGQL = exports.ListChannelsDocument = exports.ListConsortiaGQL = exports.ListConsortiaDocument = exports.FullQueryGQL = exports.FullQueryDocument = exports.ContainerDeepNestingFragmentDoc = exports.BufferDeepNestingFragmentDoc = exports.ConsortiumLocalDeepNestingFragmentDoc = exports.PluginsDeepNestingFragmentDoc = exports.PluginDeepNestingFragmentDoc = exports.FormFieldDeepNestingFragmentDoc = exports.ActionDeepNestingFragmentDoc = exports.ChannelDeepNestingFragmentDoc = exports.ChaincodeDeepNestingFragmentDoc = exports.SwarmDeepNestingFragmentDoc = exports.DappsDeepNestingFragmentDoc = exports.InstalledDappDeepNestingFragmentDoc = exports.DappDeepNestingFragmentDoc = exports.PricingDeepNestingFragmentDoc = exports.InfoDeepNestingFragmentDoc = exports.GovernanceDeepNestingFragmentDoc = exports.RequestDeepNestingFragmentDoc = exports.VoteDeepNestingFragmentDoc = exports.RequestCoreDeepNestingFragmentDoc = exports.VoteCoreDeepNestingFragmentDoc = exports.OrganizationDeepNestingFragmentDoc = exports.RoleDeepNestingFragmentDoc = exports.UserCoreDeepNestingFragmentDoc = exports.UserDeepNestingFragmentDoc = exports.RoleCoreDeepNestingFragmentDoc = exports.LobbyDeepNestingFragmentDoc = exports.ConsortiumDeepNestingFragmentDoc = exports.ConsortiumMemberDeepNestingFragmentDoc = exports.ConsortiumRequestDeepNestingFragmentDoc = exports.ConsortiumApprovalDeepNestingFragmentDoc = exports.ContainerNoNestingFragmentDoc = exports.BufferNoNestingFragmentDoc = exports.ActionNoNestingFragmentDoc = exports.ChannelNoNestingFragmentDoc = exports.SwarmNoNestingFragmentDoc = void 0;
exports.InstallPluginGQL = exports.InstallPluginDocument = exports.InstallDappGQL = void 0;
var graphql_tag_1 = __importDefault(require("graphql-tag"));
var core_1 = require("@angular/core");
var Apollo = __importStar(require("apollo-angular"));
exports.ConsortiumNoNestingFragmentDoc = graphql_tag_1.default(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    fragment ConsortiumNoNesting on Consortium {\n  cid\n  name\n  desc\n  founder\n}\n    "], ["\n    fragment ConsortiumNoNesting on Consortium {\n  cid\n  name\n  desc\n  founder\n}\n    "])));
exports.ConsortiumMemberNoNestingFragmentDoc = graphql_tag_1.default(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n    fragment ConsortiumMemberNoNesting on ConsortiumMember {\n  _id\n  cid\n  pub\n  apub\n  token\n  name\n}\n    "], ["\n    fragment ConsortiumMemberNoNesting on ConsortiumMember {\n  _id\n  cid\n  pub\n  apub\n  token\n  name\n}\n    "])));
exports.LobbyFragmentDoc = graphql_tag_1.default(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n    fragment Lobby on Lobby {\n  consortia {\n    ...ConsortiumNoNesting\n  }\n  requested {\n    ...ConsortiumNoNesting\n  }\n  joined {\n    ...ConsortiumNoNesting\n  }\n  member {\n    ...ConsortiumMemberNoNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment Lobby on Lobby {\n  consortia {\n    ...ConsortiumNoNesting\n  }\n  requested {\n    ...ConsortiumNoNesting\n  }\n  joined {\n    ...ConsortiumNoNesting\n  }\n  member {\n    ...ConsortiumMemberNoNesting\n  }\n}\n    ", "\n", ""])), exports.ConsortiumNoNestingFragmentDoc, exports.ConsortiumMemberNoNestingFragmentDoc);
exports.RequestCoreNoNestingFragmentDoc = graphql_tag_1.default(templateObject_4 || (templateObject_4 = __makeTemplateObject(["\n    fragment RequestCoreNoNesting on RequestCore {\n  _id\n  type\n  key\n  subkey\n  parent\n  children\n  settled\n}\n    "], ["\n    fragment RequestCoreNoNesting on RequestCore {\n  _id\n  type\n  key\n  subkey\n  parent\n  children\n  settled\n}\n    "])));
exports.OrganizationNoNestingFragmentDoc = graphql_tag_1.default(templateObject_5 || (templateObject_5 = __makeTemplateObject(["\n    fragment OrganizationNoNesting on Organization {\n  _id\n  name\n  publicIpV4\n  publicKey\n}\n    "], ["\n    fragment OrganizationNoNesting on Organization {\n  _id\n  name\n  publicIpV4\n  publicKey\n}\n    "])));
exports.VoteFragmentDoc = graphql_tag_1.default(templateObject_6 || (templateObject_6 = __makeTemplateObject(["\n    fragment Vote on Vote {\n  _id\n  rid\n  value\n  request {\n    ...RequestCoreNoNesting\n  }\n  children {\n    ...RequestCoreNoNesting\n  }\n  settled\n  voter {\n    ...OrganizationNoNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment Vote on Vote {\n  _id\n  rid\n  value\n  request {\n    ...RequestCoreNoNesting\n  }\n  children {\n    ...RequestCoreNoNesting\n  }\n  settled\n  voter {\n    ...OrganizationNoNesting\n  }\n}\n    ", "\n", ""])), exports.RequestCoreNoNestingFragmentDoc, exports.OrganizationNoNestingFragmentDoc);
exports.RequestNoNestingFragmentDoc = graphql_tag_1.default(templateObject_7 || (templateObject_7 = __makeTemplateObject(["\n    fragment RequestNoNesting on Request {\n  _id\n  type\n  key\n  subkey\n  settled\n}\n    "], ["\n    fragment RequestNoNesting on Request {\n  _id\n  type\n  key\n  subkey\n  settled\n}\n    "])));
exports.GovernanceFragmentDoc = graphql_tag_1.default(templateObject_8 || (templateObject_8 = __makeTemplateObject(["\n    fragment Governance on Governance {\n  requests {\n    ...RequestNoNesting\n  }\n}\n    ", ""], ["\n    fragment Governance on Governance {\n  requests {\n    ...RequestNoNesting\n  }\n}\n    ", ""])), exports.RequestNoNestingFragmentDoc);
exports.InfoFragmentDoc = graphql_tag_1.default(templateObject_9 || (templateObject_9 = __makeTemplateObject(["\n    fragment Info on Info {\n  consDomain\n  orgName\n  channelName\n  publicIpV4\n  publicKey\n  isAdminMode\n  isJoined\n}\n    "], ["\n    fragment Info on Info {\n  consDomain\n  orgName\n  channelName\n  publicIpV4\n  publicKey\n  isAdminMode\n  isJoined\n}\n    "])));
exports.VoteCoreNoNestingFragmentDoc = graphql_tag_1.default(templateObject_10 || (templateObject_10 = __makeTemplateObject(["\n    fragment VoteCoreNoNesting on VoteCore {\n  _id\n  rid\n  value\n  settled\n}\n    "], ["\n    fragment VoteCoreNoNesting on VoteCore {\n  _id\n  rid\n  value\n  settled\n}\n    "])));
exports.RequestFragmentDoc = graphql_tag_1.default(templateObject_11 || (templateObject_11 = __makeTemplateObject(["\n    fragment Request on Request {\n  _id\n  type\n  key\n  subkey\n  parent {\n    ...RequestCoreNoNesting\n  }\n  children {\n    ...RequestCoreNoNesting\n  }\n  votes {\n    ...VoteCoreNoNesting\n  }\n  ownvote {\n    ...VoteCoreNoNesting\n  }\n  settled\n}\n    ", "\n", ""], ["\n    fragment Request on Request {\n  _id\n  type\n  key\n  subkey\n  parent {\n    ...RequestCoreNoNesting\n  }\n  children {\n    ...RequestCoreNoNesting\n  }\n  votes {\n    ...VoteCoreNoNesting\n  }\n  ownvote {\n    ...VoteCoreNoNesting\n  }\n  settled\n}\n    ", "\n", ""])), exports.RequestCoreNoNestingFragmentDoc, exports.VoteCoreNoNestingFragmentDoc);
exports.RoleCoreNoNestingFragmentDoc = graphql_tag_1.default(templateObject_12 || (templateObject_12 = __makeTemplateObject(["\n    fragment RoleCoreNoNesting on RoleCore {\n  _id\n  name\n}\n    "], ["\n    fragment RoleCoreNoNesting on RoleCore {\n  _id\n  name\n}\n    "])));
exports.UserFragmentDoc = graphql_tag_1.default(templateObject_13 || (templateObject_13 = __makeTemplateObject(["\n    fragment User on User {\n  _id\n  name\n  passHash\n  roles {\n    ...RoleCoreNoNesting\n  }\n}\n    ", ""], ["\n    fragment User on User {\n  _id\n  name\n  passHash\n  roles {\n    ...RoleCoreNoNesting\n  }\n}\n    ", ""])), exports.RoleCoreNoNestingFragmentDoc);
exports.RequestCoreFragmentDoc = graphql_tag_1.default(templateObject_14 || (templateObject_14 = __makeTemplateObject(["\n    fragment RequestCore on RequestCore {\n  _id\n  type\n  key\n  subkey\n  parent\n  children\n  ownvote {\n    ...VoteCoreNoNesting\n  }\n  settled\n}\n    ", ""], ["\n    fragment RequestCore on RequestCore {\n  _id\n  type\n  key\n  subkey\n  parent\n  children\n  ownvote {\n    ...VoteCoreNoNesting\n  }\n  settled\n}\n    ", ""])), exports.VoteCoreNoNestingFragmentDoc);
exports.UserCoreNoNestingFragmentDoc = graphql_tag_1.default(templateObject_15 || (templateObject_15 = __makeTemplateObject(["\n    fragment UserCoreNoNesting on UserCore {\n  _id\n  name\n}\n    "], ["\n    fragment UserCoreNoNesting on UserCore {\n  _id\n  name\n}\n    "])));
exports.RoleFragmentDoc = graphql_tag_1.default(templateObject_16 || (templateObject_16 = __makeTemplateObject(["\n    fragment Role on Role {\n  _id\n  name\n  users {\n    ...UserCoreNoNesting\n  }\n}\n    ", ""], ["\n    fragment Role on Role {\n  _id\n  name\n  users {\n    ...UserCoreNoNesting\n  }\n}\n    ", ""])), exports.UserCoreNoNestingFragmentDoc);
exports.VoteCoreFragmentDoc = graphql_tag_1.default(templateObject_17 || (templateObject_17 = __makeTemplateObject(["\n    fragment VoteCore on VoteCore {\n  _id\n  rid\n  value\n  settled\n  voter {\n    ...OrganizationNoNesting\n  }\n}\n    ", ""], ["\n    fragment VoteCore on VoteCore {\n  _id\n  rid\n  value\n  settled\n  voter {\n    ...OrganizationNoNesting\n  }\n}\n    ", ""])), exports.OrganizationNoNestingFragmentDoc);
exports.ConsortiumRequestNoNestingFragmentDoc = graphql_tag_1.default(templateObject_18 || (templateObject_18 = __makeTemplateObject(["\n    fragment ConsortiumRequestNoNesting on ConsortiumRequest {\n  _id\n  cid\n  rpub\n  sig\n  confirmed\n}\n    "], ["\n    fragment ConsortiumRequestNoNesting on ConsortiumRequest {\n  _id\n  cid\n  rpub\n  sig\n  confirmed\n}\n    "])));
exports.ConsortiumApprovalNoNestingFragmentDoc = graphql_tag_1.default(templateObject_19 || (templateObject_19 = __makeTemplateObject(["\n    fragment ConsortiumApprovalNoNesting on ConsortiumApproval {\n  _id\n  cid\n  rpub\n  pub\n  sig\n}\n    "], ["\n    fragment ConsortiumApprovalNoNesting on ConsortiumApproval {\n  _id\n  cid\n  rpub\n  pub\n  sig\n}\n    "])));
exports.ConsortiumFragmentDoc = graphql_tag_1.default(templateObject_20 || (templateObject_20 = __makeTemplateObject(["\n    fragment Consortium on Consortium {\n  cid\n  name\n  desc\n  founder\n  requests {\n    ...ConsortiumRequestNoNesting\n  }\n  approvals {\n    ...ConsortiumApprovalNoNesting\n  }\n  members {\n    ...ConsortiumMemberNoNesting\n  }\n}\n    ", "\n", "\n", ""], ["\n    fragment Consortium on Consortium {\n  cid\n  name\n  desc\n  founder\n  requests {\n    ...ConsortiumRequestNoNesting\n  }\n  approvals {\n    ...ConsortiumApprovalNoNesting\n  }\n  members {\n    ...ConsortiumMemberNoNesting\n  }\n}\n    ", "\n", "\n", ""])), exports.ConsortiumRequestNoNestingFragmentDoc, exports.ConsortiumApprovalNoNestingFragmentDoc, exports.ConsortiumMemberNoNestingFragmentDoc);
exports.DappNoNestingFragmentDoc = graphql_tag_1.default(templateObject_21 || (templateObject_21 = __makeTemplateObject(["\n    fragment DappNoNesting on Dapp {\n  _id\n  title\n  version\n  imageId\n  dockerImage\n}\n    "], ["\n    fragment DappNoNesting on Dapp {\n  _id\n  title\n  version\n  imageId\n  dockerImage\n}\n    "])));
exports.InstalledDappNoNestingFragmentDoc = graphql_tag_1.default(templateObject_22 || (templateObject_22 = __makeTemplateObject(["\n    fragment InstalledDappNoNesting on InstalledDapp {\n  hostName\n}\n    "], ["\n    fragment InstalledDappNoNesting on InstalledDapp {\n  hostName\n}\n    "])));
exports.DappsFragmentDoc = graphql_tag_1.default(templateObject_23 || (templateObject_23 = __makeTemplateObject(["\n    fragment Dapps on Dapps {\n  installable {\n    ...DappNoNesting\n  }\n  installed {\n    ...InstalledDappNoNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment Dapps on Dapps {\n  installable {\n    ...DappNoNesting\n  }\n  installed {\n    ...InstalledDappNoNesting\n  }\n}\n    ", "\n", ""])), exports.DappNoNestingFragmentDoc, exports.InstalledDappNoNestingFragmentDoc);
exports.ConsortiumApprovalFragmentDoc = graphql_tag_1.default(templateObject_24 || (templateObject_24 = __makeTemplateObject(["\n    fragment ConsortiumApproval on ConsortiumApproval {\n  _id\n  cid\n  rpub\n  pub\n  sig\n}\n    "], ["\n    fragment ConsortiumApproval on ConsortiumApproval {\n  _id\n  cid\n  rpub\n  pub\n  sig\n}\n    "])));
exports.PricingNoNestingFragmentDoc = graphql_tag_1.default(templateObject_25 || (templateObject_25 = __makeTemplateObject(["\n    fragment PricingNoNesting on Pricing {\n  currency\n  initialPrice\n  pricePerMonth\n  paymentInfo\n}\n    "], ["\n    fragment PricingNoNesting on Pricing {\n  currency\n  initialPrice\n  pricePerMonth\n  paymentInfo\n}\n    "])));
exports.DappFragmentDoc = graphql_tag_1.default(templateObject_26 || (templateObject_26 = __makeTemplateObject(["\n    fragment Dapp on Dapp {\n  _id\n  title\n  version\n  imageId\n  dockerImage\n  pricing {\n    ...PricingNoNesting\n  }\n}\n    ", ""], ["\n    fragment Dapp on Dapp {\n  _id\n  title\n  version\n  imageId\n  dockerImage\n  pricing {\n    ...PricingNoNesting\n  }\n}\n    ", ""])), exports.PricingNoNestingFragmentDoc);
exports.SwarmFragmentDoc = graphql_tag_1.default(templateObject_27 || (templateObject_27 = __makeTemplateObject(["\n    fragment Swarm on Swarm {\n  nodes\n  inspect\n}\n    "], ["\n    fragment Swarm on Swarm {\n  nodes\n  inspect\n}\n    "])));
exports.PricingFragmentDoc = graphql_tag_1.default(templateObject_28 || (templateObject_28 = __makeTemplateObject(["\n    fragment Pricing on Pricing {\n  currency\n  initialPrice\n  pricePerMonth\n  paymentInfo\n}\n    "], ["\n    fragment Pricing on Pricing {\n  currency\n  initialPrice\n  pricePerMonth\n  paymentInfo\n}\n    "])));
exports.ChaincodeNoNestingFragmentDoc = graphql_tag_1.default(templateObject_29 || (templateObject_29 = __makeTemplateObject(["\n    fragment ChaincodeNoNesting on Chaincode {\n  name\n}\n    "], ["\n    fragment ChaincodeNoNesting on Chaincode {\n  name\n}\n    "])));
exports.ChannelFragmentDoc = graphql_tag_1.default(templateObject_30 || (templateObject_30 = __makeTemplateObject(["\n    fragment Channel on Channel {\n  _id\n  chaincodes {\n    ...ChaincodeNoNesting\n  }\n}\n    ", ""], ["\n    fragment Channel on Channel {\n  _id\n  chaincodes {\n    ...ChaincodeNoNesting\n  }\n}\n    ", ""])), exports.ChaincodeNoNestingFragmentDoc);
exports.InstalledDappFragmentDoc = graphql_tag_1.default(templateObject_31 || (templateObject_31 = __makeTemplateObject(["\n    fragment InstalledDapp on InstalledDapp {\n  hostName\n  dapp {\n    ...DappNoNesting\n  }\n}\n    ", ""], ["\n    fragment InstalledDapp on InstalledDapp {\n  hostName\n  dapp {\n    ...DappNoNesting\n  }\n}\n    ", ""])), exports.DappNoNestingFragmentDoc);
exports.ActionFragmentDoc = graphql_tag_1.default(templateObject_32 || (templateObject_32 = __makeTemplateObject(["\n    fragment Action on Action {\n  name\n  progress\n  log\n}\n    "], ["\n    fragment Action on Action {\n  name\n  progress\n  log\n}\n    "])));
exports.PluginNoNestingFragmentDoc = graphql_tag_1.default(templateObject_33 || (templateObject_33 = __makeTemplateObject(["\n    fragment PluginNoNesting on Plugin {\n  _id\n  title\n  version\n  imageId\n  installed\n}\n    "], ["\n    fragment PluginNoNesting on Plugin {\n  _id\n  title\n  version\n  imageId\n  installed\n}\n    "])));
exports.PluginsFragmentDoc = graphql_tag_1.default(templateObject_34 || (templateObject_34 = __makeTemplateObject(["\n    fragment Plugins on Plugins {\n  installable {\n    ...PluginNoNesting\n  }\n  installed {\n    ...PluginNoNesting\n  }\n}\n    ", ""], ["\n    fragment Plugins on Plugins {\n  installable {\n    ...PluginNoNesting\n  }\n  installed {\n    ...PluginNoNesting\n  }\n}\n    ", ""])), exports.PluginNoNestingFragmentDoc);
exports.RoleCoreFragmentDoc = graphql_tag_1.default(templateObject_35 || (templateObject_35 = __makeTemplateObject(["\n    fragment RoleCore on RoleCore {\n  _id\n  name\n}\n    "], ["\n    fragment RoleCore on RoleCore {\n  _id\n  name\n}\n    "])));
exports.ConsortiumRequestFragmentDoc = graphql_tag_1.default(templateObject_36 || (templateObject_36 = __makeTemplateObject(["\n    fragment ConsortiumRequest on ConsortiumRequest {\n  _id\n  cid\n  rpub\n  sig\n  confirmed\n  approvals {\n    ...ConsortiumApprovalNoNesting\n  }\n}\n    ", ""], ["\n    fragment ConsortiumRequest on ConsortiumRequest {\n  _id\n  cid\n  rpub\n  sig\n  confirmed\n  approvals {\n    ...ConsortiumApprovalNoNesting\n  }\n}\n    ", ""])), exports.ConsortiumApprovalNoNestingFragmentDoc);
exports.ConsortiumLocalFragmentDoc = graphql_tag_1.default(templateObject_37 || (templateObject_37 = __makeTemplateObject(["\n    fragment ConsortiumLocal on ConsortiumLocal {\n  organizations {\n    ...OrganizationNoNesting\n  }\n}\n    ", ""], ["\n    fragment ConsortiumLocal on ConsortiumLocal {\n  organizations {\n    ...OrganizationNoNesting\n  }\n}\n    ", ""])), exports.OrganizationNoNestingFragmentDoc);
exports.BufferFragmentDoc = graphql_tag_1.default(templateObject_38 || (templateObject_38 = __makeTemplateObject(["\n    fragment Buffer on Buffer {\n  type\n  data\n}\n    "], ["\n    fragment Buffer on Buffer {\n  type\n  data\n}\n    "])));
exports.FormFieldFragmentDoc = graphql_tag_1.default(templateObject_39 || (templateObject_39 = __makeTemplateObject(["\n    fragment FormField on FormField {\n  type\n  name\n  label\n  value\n  required\n}\n    "], ["\n    fragment FormField on FormField {\n  type\n  name\n  label\n  value\n  required\n}\n    "])));
exports.FormFieldNoNestingFragmentDoc = graphql_tag_1.default(templateObject_40 || (templateObject_40 = __makeTemplateObject(["\n    fragment FormFieldNoNesting on FormField {\n  type\n  name\n  label\n  value\n  required\n}\n    "], ["\n    fragment FormFieldNoNesting on FormField {\n  type\n  name\n  label\n  value\n  required\n}\n    "])));
exports.PluginFragmentDoc = graphql_tag_1.default(templateObject_41 || (templateObject_41 = __makeTemplateObject(["\n    fragment Plugin on Plugin {\n  _id\n  title\n  version\n  imageId\n  pricing {\n    ...PricingNoNesting\n  }\n  parameters {\n    ...FormFieldNoNesting\n  }\n  installed\n}\n    ", "\n", ""], ["\n    fragment Plugin on Plugin {\n  _id\n  title\n  version\n  imageId\n  pricing {\n    ...PricingNoNesting\n  }\n  parameters {\n    ...FormFieldNoNesting\n  }\n  installed\n}\n    ", "\n", ""])), exports.PricingNoNestingFragmentDoc, exports.FormFieldNoNestingFragmentDoc);
exports.ConsortiumMemberFragmentDoc = graphql_tag_1.default(templateObject_42 || (templateObject_42 = __makeTemplateObject(["\n    fragment ConsortiumMember on ConsortiumMember {\n  _id\n  cid\n  pub\n  apub\n  token\n  name\n}\n    "], ["\n    fragment ConsortiumMember on ConsortiumMember {\n  _id\n  cid\n  pub\n  apub\n  token\n  name\n}\n    "])));
exports.UserCoreFragmentDoc = graphql_tag_1.default(templateObject_43 || (templateObject_43 = __makeTemplateObject(["\n    fragment UserCore on UserCore {\n  _id\n  name\n}\n    "], ["\n    fragment UserCore on UserCore {\n  _id\n  name\n}\n    "])));
exports.UserNoNestingFragmentDoc = graphql_tag_1.default(templateObject_44 || (templateObject_44 = __makeTemplateObject(["\n    fragment UserNoNesting on User {\n  _id\n  name\n  passHash\n}\n    "], ["\n    fragment UserNoNesting on User {\n  _id\n  name\n  passHash\n}\n    "])));
exports.RoleNoNestingFragmentDoc = graphql_tag_1.default(templateObject_45 || (templateObject_45 = __makeTemplateObject(["\n    fragment RoleNoNesting on Role {\n  _id\n  name\n}\n    "], ["\n    fragment RoleNoNesting on Role {\n  _id\n  name\n}\n    "])));
exports.OrganizationFragmentDoc = graphql_tag_1.default(templateObject_46 || (templateObject_46 = __makeTemplateObject(["\n    fragment Organization on Organization {\n  _id\n  name\n  publicIpV4\n  publicKey\n  users {\n    ...UserNoNesting\n  }\n  roles {\n    ...RoleNoNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment Organization on Organization {\n  _id\n  name\n  publicIpV4\n  publicKey\n  users {\n    ...UserNoNesting\n  }\n  roles {\n    ...RoleNoNesting\n  }\n}\n    ", "\n", ""])), exports.UserNoNestingFragmentDoc, exports.RoleNoNestingFragmentDoc);
exports.ChaincodeFragmentDoc = graphql_tag_1.default(templateObject_47 || (templateObject_47 = __makeTemplateObject(["\n    fragment Chaincode on Chaincode {\n  name\n}\n    "], ["\n    fragment Chaincode on Chaincode {\n  name\n}\n    "])));
exports.ContainerFragmentDoc = graphql_tag_1.default(templateObject_48 || (templateObject_48 = __makeTemplateObject(["\n    fragment Container on Container {\n  Name\n}\n    "], ["\n    fragment Container on Container {\n  Name\n}\n    "])));
exports.VoteNoNestingFragmentDoc = graphql_tag_1.default(templateObject_49 || (templateObject_49 = __makeTemplateObject(["\n    fragment VoteNoNesting on Vote {\n  _id\n  rid\n  value\n  settled\n}\n    "], ["\n    fragment VoteNoNesting on Vote {\n  _id\n  rid\n  value\n  settled\n}\n    "])));
exports.InfoNoNestingFragmentDoc = graphql_tag_1.default(templateObject_50 || (templateObject_50 = __makeTemplateObject(["\n    fragment InfoNoNesting on Info {\n  consDomain\n  orgName\n  channelName\n  publicIpV4\n  publicKey\n  isAdminMode\n  isJoined\n}\n    "], ["\n    fragment InfoNoNesting on Info {\n  consDomain\n  orgName\n  channelName\n  publicIpV4\n  publicKey\n  isAdminMode\n  isJoined\n}\n    "])));
exports.SwarmNoNestingFragmentDoc = graphql_tag_1.default(templateObject_51 || (templateObject_51 = __makeTemplateObject(["\n    fragment SwarmNoNesting on Swarm {\n  nodes\n  inspect\n}\n    "], ["\n    fragment SwarmNoNesting on Swarm {\n  nodes\n  inspect\n}\n    "])));
exports.ChannelNoNestingFragmentDoc = graphql_tag_1.default(templateObject_52 || (templateObject_52 = __makeTemplateObject(["\n    fragment ChannelNoNesting on Channel {\n  _id\n}\n    "], ["\n    fragment ChannelNoNesting on Channel {\n  _id\n}\n    "])));
exports.ActionNoNestingFragmentDoc = graphql_tag_1.default(templateObject_53 || (templateObject_53 = __makeTemplateObject(["\n    fragment ActionNoNesting on Action {\n  name\n  progress\n  log\n}\n    "], ["\n    fragment ActionNoNesting on Action {\n  name\n  progress\n  log\n}\n    "])));
exports.BufferNoNestingFragmentDoc = graphql_tag_1.default(templateObject_54 || (templateObject_54 = __makeTemplateObject(["\n    fragment BufferNoNesting on Buffer {\n  type\n  data\n}\n    "], ["\n    fragment BufferNoNesting on Buffer {\n  type\n  data\n}\n    "])));
exports.ContainerNoNestingFragmentDoc = graphql_tag_1.default(templateObject_55 || (templateObject_55 = __makeTemplateObject(["\n    fragment ContainerNoNesting on Container {\n  Name\n}\n    "], ["\n    fragment ContainerNoNesting on Container {\n  Name\n}\n    "])));
exports.ConsortiumApprovalDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_56 || (templateObject_56 = __makeTemplateObject(["\n    fragment ConsortiumApprovalDeepNesting on ConsortiumApproval {\n  _id\n  cid\n  rpub\n  pub\n  sig\n}\n    "], ["\n    fragment ConsortiumApprovalDeepNesting on ConsortiumApproval {\n  _id\n  cid\n  rpub\n  pub\n  sig\n}\n    "])));
exports.ConsortiumRequestDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_57 || (templateObject_57 = __makeTemplateObject(["\n    fragment ConsortiumRequestDeepNesting on ConsortiumRequest {\n  _id\n  cid\n  rpub\n  sig\n  confirmed\n  approvals {\n    ...ConsortiumApprovalDeepNesting\n  }\n}\n    ", ""], ["\n    fragment ConsortiumRequestDeepNesting on ConsortiumRequest {\n  _id\n  cid\n  rpub\n  sig\n  confirmed\n  approvals {\n    ...ConsortiumApprovalDeepNesting\n  }\n}\n    ", ""])), exports.ConsortiumApprovalDeepNestingFragmentDoc);
exports.ConsortiumMemberDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_58 || (templateObject_58 = __makeTemplateObject(["\n    fragment ConsortiumMemberDeepNesting on ConsortiumMember {\n  _id\n  cid\n  pub\n  apub\n  token\n  name\n}\n    "], ["\n    fragment ConsortiumMemberDeepNesting on ConsortiumMember {\n  _id\n  cid\n  pub\n  apub\n  token\n  name\n}\n    "])));
exports.ConsortiumDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_59 || (templateObject_59 = __makeTemplateObject(["\n    fragment ConsortiumDeepNesting on Consortium {\n  cid\n  name\n  desc\n  founder\n  requests {\n    ...ConsortiumRequestDeepNesting\n  }\n  approvals {\n    ...ConsortiumApprovalDeepNesting\n  }\n  members {\n    ...ConsortiumMemberDeepNesting\n  }\n}\n    ", "\n", "\n", ""], ["\n    fragment ConsortiumDeepNesting on Consortium {\n  cid\n  name\n  desc\n  founder\n  requests {\n    ...ConsortiumRequestDeepNesting\n  }\n  approvals {\n    ...ConsortiumApprovalDeepNesting\n  }\n  members {\n    ...ConsortiumMemberDeepNesting\n  }\n}\n    ", "\n", "\n", ""])), exports.ConsortiumRequestDeepNestingFragmentDoc, exports.ConsortiumApprovalDeepNestingFragmentDoc, exports.ConsortiumMemberDeepNestingFragmentDoc);
exports.LobbyDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_60 || (templateObject_60 = __makeTemplateObject(["\n    fragment LobbyDeepNesting on Lobby {\n  consortia {\n    ...ConsortiumDeepNesting\n  }\n  requested {\n    ...ConsortiumDeepNesting\n  }\n  joined {\n    ...ConsortiumDeepNesting\n  }\n  member {\n    ...ConsortiumMemberDeepNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment LobbyDeepNesting on Lobby {\n  consortia {\n    ...ConsortiumDeepNesting\n  }\n  requested {\n    ...ConsortiumDeepNesting\n  }\n  joined {\n    ...ConsortiumDeepNesting\n  }\n  member {\n    ...ConsortiumMemberDeepNesting\n  }\n}\n    ", "\n", ""])), exports.ConsortiumDeepNestingFragmentDoc, exports.ConsortiumMemberDeepNestingFragmentDoc);
exports.RoleCoreDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_61 || (templateObject_61 = __makeTemplateObject(["\n    fragment RoleCoreDeepNesting on RoleCore {\n  _id\n  name\n}\n    "], ["\n    fragment RoleCoreDeepNesting on RoleCore {\n  _id\n  name\n}\n    "])));
exports.UserDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_62 || (templateObject_62 = __makeTemplateObject(["\n    fragment UserDeepNesting on User {\n  _id\n  name\n  passHash\n  roles {\n    ...RoleCoreDeepNesting\n  }\n}\n    ", ""], ["\n    fragment UserDeepNesting on User {\n  _id\n  name\n  passHash\n  roles {\n    ...RoleCoreDeepNesting\n  }\n}\n    ", ""])), exports.RoleCoreDeepNestingFragmentDoc);
exports.UserCoreDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_63 || (templateObject_63 = __makeTemplateObject(["\n    fragment UserCoreDeepNesting on UserCore {\n  _id\n  name\n}\n    "], ["\n    fragment UserCoreDeepNesting on UserCore {\n  _id\n  name\n}\n    "])));
exports.RoleDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_64 || (templateObject_64 = __makeTemplateObject(["\n    fragment RoleDeepNesting on Role {\n  _id\n  name\n  users {\n    ...UserCoreDeepNesting\n  }\n}\n    ", ""], ["\n    fragment RoleDeepNesting on Role {\n  _id\n  name\n  users {\n    ...UserCoreDeepNesting\n  }\n}\n    ", ""])), exports.UserCoreDeepNestingFragmentDoc);
exports.OrganizationDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_65 || (templateObject_65 = __makeTemplateObject(["\n    fragment OrganizationDeepNesting on Organization {\n  _id\n  name\n  publicIpV4\n  publicKey\n  users {\n    ...UserDeepNesting\n  }\n  roles {\n    ...RoleDeepNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment OrganizationDeepNesting on Organization {\n  _id\n  name\n  publicIpV4\n  publicKey\n  users {\n    ...UserDeepNesting\n  }\n  roles {\n    ...RoleDeepNesting\n  }\n}\n    ", "\n", ""])), exports.UserDeepNestingFragmentDoc, exports.RoleDeepNestingFragmentDoc);
exports.VoteCoreDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_66 || (templateObject_66 = __makeTemplateObject(["\n    fragment VoteCoreDeepNesting on VoteCore {\n  _id\n  rid\n  value\n  settled\n  voter {\n    ...OrganizationDeepNesting\n  }\n}\n    ", ""], ["\n    fragment VoteCoreDeepNesting on VoteCore {\n  _id\n  rid\n  value\n  settled\n  voter {\n    ...OrganizationDeepNesting\n  }\n}\n    ", ""])), exports.OrganizationDeepNestingFragmentDoc);
exports.RequestCoreDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_67 || (templateObject_67 = __makeTemplateObject(["\n    fragment RequestCoreDeepNesting on RequestCore {\n  _id\n  type\n  key\n  subkey\n  parent\n  children\n  ownvote {\n    ...VoteCoreDeepNesting\n  }\n  settled\n}\n    ", ""], ["\n    fragment RequestCoreDeepNesting on RequestCore {\n  _id\n  type\n  key\n  subkey\n  parent\n  children\n  ownvote {\n    ...VoteCoreDeepNesting\n  }\n  settled\n}\n    ", ""])), exports.VoteCoreDeepNestingFragmentDoc);
exports.VoteDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_68 || (templateObject_68 = __makeTemplateObject(["\n    fragment VoteDeepNesting on Vote {\n  _id\n  rid\n  value\n  request {\n    ...RequestCoreDeepNesting\n  }\n  children {\n    ...RequestCoreDeepNesting\n  }\n  settled\n  voter {\n    ...OrganizationDeepNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment VoteDeepNesting on Vote {\n  _id\n  rid\n  value\n  request {\n    ...RequestCoreDeepNesting\n  }\n  children {\n    ...RequestCoreDeepNesting\n  }\n  settled\n  voter {\n    ...OrganizationDeepNesting\n  }\n}\n    ", "\n", ""])), exports.RequestCoreDeepNestingFragmentDoc, exports.OrganizationDeepNestingFragmentDoc);
exports.RequestDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_69 || (templateObject_69 = __makeTemplateObject(["\n    fragment RequestDeepNesting on Request {\n  _id\n  type\n  key\n  subkey\n  parent {\n    ...RequestCoreDeepNesting\n  }\n  children {\n    ...RequestCoreDeepNesting\n  }\n  votes {\n    ...VoteCoreDeepNesting\n  }\n  ownvote {\n    ...VoteCoreDeepNesting\n  }\n  settled\n}\n    ", "\n", ""], ["\n    fragment RequestDeepNesting on Request {\n  _id\n  type\n  key\n  subkey\n  parent {\n    ...RequestCoreDeepNesting\n  }\n  children {\n    ...RequestCoreDeepNesting\n  }\n  votes {\n    ...VoteCoreDeepNesting\n  }\n  ownvote {\n    ...VoteCoreDeepNesting\n  }\n  settled\n}\n    ", "\n", ""])), exports.RequestCoreDeepNestingFragmentDoc, exports.VoteCoreDeepNestingFragmentDoc);
exports.GovernanceDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_70 || (templateObject_70 = __makeTemplateObject(["\n    fragment GovernanceDeepNesting on Governance {\n  requests {\n    ...RequestDeepNesting\n  }\n}\n    ", ""], ["\n    fragment GovernanceDeepNesting on Governance {\n  requests {\n    ...RequestDeepNesting\n  }\n}\n    ", ""])), exports.RequestDeepNestingFragmentDoc);
exports.InfoDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_71 || (templateObject_71 = __makeTemplateObject(["\n    fragment InfoDeepNesting on Info {\n  consDomain\n  orgName\n  channelName\n  publicIpV4\n  publicKey\n  isAdminMode\n  isJoined\n}\n    "], ["\n    fragment InfoDeepNesting on Info {\n  consDomain\n  orgName\n  channelName\n  publicIpV4\n  publicKey\n  isAdminMode\n  isJoined\n}\n    "])));
exports.PricingDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_72 || (templateObject_72 = __makeTemplateObject(["\n    fragment PricingDeepNesting on Pricing {\n  currency\n  initialPrice\n  pricePerMonth\n  paymentInfo\n}\n    "], ["\n    fragment PricingDeepNesting on Pricing {\n  currency\n  initialPrice\n  pricePerMonth\n  paymentInfo\n}\n    "])));
exports.DappDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_73 || (templateObject_73 = __makeTemplateObject(["\n    fragment DappDeepNesting on Dapp {\n  _id\n  title\n  version\n  imageId\n  dockerImage\n  pricing {\n    ...PricingDeepNesting\n  }\n}\n    ", ""], ["\n    fragment DappDeepNesting on Dapp {\n  _id\n  title\n  version\n  imageId\n  dockerImage\n  pricing {\n    ...PricingDeepNesting\n  }\n}\n    ", ""])), exports.PricingDeepNestingFragmentDoc);
exports.InstalledDappDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_74 || (templateObject_74 = __makeTemplateObject(["\n    fragment InstalledDappDeepNesting on InstalledDapp {\n  hostName\n  dapp {\n    ...DappDeepNesting\n  }\n}\n    ", ""], ["\n    fragment InstalledDappDeepNesting on InstalledDapp {\n  hostName\n  dapp {\n    ...DappDeepNesting\n  }\n}\n    ", ""])), exports.DappDeepNestingFragmentDoc);
exports.DappsDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_75 || (templateObject_75 = __makeTemplateObject(["\n    fragment DappsDeepNesting on Dapps {\n  installable {\n    ...DappDeepNesting\n  }\n  installed {\n    ...InstalledDappDeepNesting\n  }\n}\n    ", "\n", ""], ["\n    fragment DappsDeepNesting on Dapps {\n  installable {\n    ...DappDeepNesting\n  }\n  installed {\n    ...InstalledDappDeepNesting\n  }\n}\n    ", "\n", ""])), exports.DappDeepNestingFragmentDoc, exports.InstalledDappDeepNestingFragmentDoc);
exports.SwarmDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_76 || (templateObject_76 = __makeTemplateObject(["\n    fragment SwarmDeepNesting on Swarm {\n  nodes\n  inspect\n}\n    "], ["\n    fragment SwarmDeepNesting on Swarm {\n  nodes\n  inspect\n}\n    "])));
exports.ChaincodeDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_77 || (templateObject_77 = __makeTemplateObject(["\n    fragment ChaincodeDeepNesting on Chaincode {\n  name\n}\n    "], ["\n    fragment ChaincodeDeepNesting on Chaincode {\n  name\n}\n    "])));
exports.ChannelDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_78 || (templateObject_78 = __makeTemplateObject(["\n    fragment ChannelDeepNesting on Channel {\n  _id\n  chaincodes {\n    ...ChaincodeDeepNesting\n  }\n}\n    ", ""], ["\n    fragment ChannelDeepNesting on Channel {\n  _id\n  chaincodes {\n    ...ChaincodeDeepNesting\n  }\n}\n    ", ""])), exports.ChaincodeDeepNestingFragmentDoc);
exports.ActionDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_79 || (templateObject_79 = __makeTemplateObject(["\n    fragment ActionDeepNesting on Action {\n  name\n  progress\n  log\n}\n    "], ["\n    fragment ActionDeepNesting on Action {\n  name\n  progress\n  log\n}\n    "])));
exports.FormFieldDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_80 || (templateObject_80 = __makeTemplateObject(["\n    fragment FormFieldDeepNesting on FormField {\n  type\n  name\n  label\n  value\n  required\n}\n    "], ["\n    fragment FormFieldDeepNesting on FormField {\n  type\n  name\n  label\n  value\n  required\n}\n    "])));
exports.PluginDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_81 || (templateObject_81 = __makeTemplateObject(["\n    fragment PluginDeepNesting on Plugin {\n  _id\n  title\n  version\n  imageId\n  pricing {\n    ...PricingDeepNesting\n  }\n  parameters {\n    ...FormFieldDeepNesting\n  }\n  installed\n}\n    ", "\n", ""], ["\n    fragment PluginDeepNesting on Plugin {\n  _id\n  title\n  version\n  imageId\n  pricing {\n    ...PricingDeepNesting\n  }\n  parameters {\n    ...FormFieldDeepNesting\n  }\n  installed\n}\n    ", "\n", ""])), exports.PricingDeepNestingFragmentDoc, exports.FormFieldDeepNestingFragmentDoc);
exports.PluginsDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_82 || (templateObject_82 = __makeTemplateObject(["\n    fragment PluginsDeepNesting on Plugins {\n  installable {\n    ...PluginDeepNesting\n  }\n  installed {\n    ...PluginDeepNesting\n  }\n}\n    ", ""], ["\n    fragment PluginsDeepNesting on Plugins {\n  installable {\n    ...PluginDeepNesting\n  }\n  installed {\n    ...PluginDeepNesting\n  }\n}\n    ", ""])), exports.PluginDeepNestingFragmentDoc);
exports.ConsortiumLocalDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_83 || (templateObject_83 = __makeTemplateObject(["\n    fragment ConsortiumLocalDeepNesting on ConsortiumLocal {\n  organizations {\n    ...OrganizationDeepNesting\n  }\n}\n    ", ""], ["\n    fragment ConsortiumLocalDeepNesting on ConsortiumLocal {\n  organizations {\n    ...OrganizationDeepNesting\n  }\n}\n    ", ""])), exports.OrganizationDeepNestingFragmentDoc);
exports.BufferDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_84 || (templateObject_84 = __makeTemplateObject(["\n    fragment BufferDeepNesting on Buffer {\n  type\n  data\n}\n    "], ["\n    fragment BufferDeepNesting on Buffer {\n  type\n  data\n}\n    "])));
exports.ContainerDeepNestingFragmentDoc = graphql_tag_1.default(templateObject_85 || (templateObject_85 = __makeTemplateObject(["\n    fragment ContainerDeepNesting on Container {\n  Name\n}\n    "], ["\n    fragment ContainerDeepNesting on Container {\n  Name\n}\n    "])));
exports.FullQueryDocument = graphql_tag_1.default(templateObject_86 || (templateObject_86 = __makeTemplateObject(["\n    query FullQuery {\n  info {\n    ...Info\n  }\n  organization {\n    ...Organization\n  }\n  organizations {\n    ...Organization\n  }\n  lobby {\n    ...Lobby\n  }\n  users {\n    ...User\n  }\n  roles {\n    ...Role\n  }\n  dapps {\n    ...Dapps\n  }\n  plugins {\n    ...Plugins\n  }\n}\n    ", "\n", "\n", "\n", "\n", "\n", "\n", ""], ["\n    query FullQuery {\n  info {\n    ...Info\n  }\n  organization {\n    ...Organization\n  }\n  organizations {\n    ...Organization\n  }\n  lobby {\n    ...Lobby\n  }\n  users {\n    ...User\n  }\n  roles {\n    ...Role\n  }\n  dapps {\n    ...Dapps\n  }\n  plugins {\n    ...Plugins\n  }\n}\n    ", "\n", "\n", "\n", "\n", "\n", "\n", ""])), exports.InfoFragmentDoc, exports.OrganizationFragmentDoc, exports.LobbyFragmentDoc, exports.UserFragmentDoc, exports.RoleFragmentDoc, exports.DappsFragmentDoc, exports.PluginsFragmentDoc);
var FullQueryGQL = /** @class */ (function (_super) {
    __extends(FullQueryGQL, _super);
    function FullQueryGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.FullQueryDocument;
        return _this;
    }
    FullQueryGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], FullQueryGQL);
    return FullQueryGQL;
}(Apollo.Query));
exports.FullQueryGQL = FullQueryGQL;
exports.ListConsortiaDocument = graphql_tag_1.default(templateObject_87 || (templateObject_87 = __makeTemplateObject(["\n    query ListConsortia {\n  lobby {\n    consortia {\n      ...Consortium\n    }\n  }\n}\n    ", ""], ["\n    query ListConsortia {\n  lobby {\n    consortia {\n      ...Consortium\n    }\n  }\n}\n    ", ""])), exports.ConsortiumFragmentDoc);
var ListConsortiaGQL = /** @class */ (function (_super) {
    __extends(ListConsortiaGQL, _super);
    function ListConsortiaGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.ListConsortiaDocument;
        return _this;
    }
    ListConsortiaGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ListConsortiaGQL);
    return ListConsortiaGQL;
}(Apollo.Query));
exports.ListConsortiaGQL = ListConsortiaGQL;
exports.ListChannelsDocument = graphql_tag_1.default(templateObject_88 || (templateObject_88 = __makeTemplateObject(["\n    query ListChannels {\n  channels {\n    ...Channel\n  }\n}\n    ", ""], ["\n    query ListChannels {\n  channels {\n    ...Channel\n  }\n}\n    ", ""])), exports.ChannelFragmentDoc);
var ListChannelsGQL = /** @class */ (function (_super) {
    __extends(ListChannelsGQL, _super);
    function ListChannelsGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.ListChannelsDocument;
        return _this;
    }
    ListChannelsGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ListChannelsGQL);
    return ListChannelsGQL;
}(Apollo.Query));
exports.ListChannelsGQL = ListChannelsGQL;
exports.ListDappsDocument = graphql_tag_1.default(templateObject_89 || (templateObject_89 = __makeTemplateObject(["\n    query ListDapps {\n  dapps {\n    installable {\n      ...Dapp\n    }\n    installed {\n      ...InstalledDapp\n    }\n  }\n}\n    ", "\n", ""], ["\n    query ListDapps {\n  dapps {\n    installable {\n      ...Dapp\n    }\n    installed {\n      ...InstalledDapp\n    }\n  }\n}\n    ", "\n", ""])), exports.DappFragmentDoc, exports.InstalledDappFragmentDoc);
var ListDappsGQL = /** @class */ (function (_super) {
    __extends(ListDappsGQL, _super);
    function ListDappsGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.ListDappsDocument;
        return _this;
    }
    ListDappsGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ListDappsGQL);
    return ListDappsGQL;
}(Apollo.Query));
exports.ListDappsGQL = ListDappsGQL;
exports.ListInstalledDappsDocument = graphql_tag_1.default(templateObject_90 || (templateObject_90 = __makeTemplateObject(["\n    query ListInstalledDapps {\n  dapps {\n    installed {\n      ...InstalledDapp\n    }\n  }\n}\n    ", ""], ["\n    query ListInstalledDapps {\n  dapps {\n    installed {\n      ...InstalledDapp\n    }\n  }\n}\n    ", ""])), exports.InstalledDappFragmentDoc);
var ListInstalledDappsGQL = /** @class */ (function (_super) {
    __extends(ListInstalledDappsGQL, _super);
    function ListInstalledDappsGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.ListInstalledDappsDocument;
        return _this;
    }
    ListInstalledDappsGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ListInstalledDappsGQL);
    return ListInstalledDappsGQL;
}(Apollo.Query));
exports.ListInstalledDappsGQL = ListInstalledDappsGQL;
exports.ListPluginsDocument = graphql_tag_1.default(templateObject_91 || (templateObject_91 = __makeTemplateObject(["\n    query ListPlugins {\n  plugins {\n    installable {\n      ...Plugin\n    }\n    installed {\n      ...Plugin\n    }\n  }\n}\n    ", ""], ["\n    query ListPlugins {\n  plugins {\n    installable {\n      ...Plugin\n    }\n    installed {\n      ...Plugin\n    }\n  }\n}\n    ", ""])), exports.PluginFragmentDoc);
var ListPluginsGQL = /** @class */ (function (_super) {
    __extends(ListPluginsGQL, _super);
    function ListPluginsGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.ListPluginsDocument;
        return _this;
    }
    ListPluginsGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ListPluginsGQL);
    return ListPluginsGQL;
}(Apollo.Query));
exports.ListPluginsGQL = ListPluginsGQL;
exports.ListAllOrgsRolesDocument = graphql_tag_1.default(templateObject_92 || (templateObject_92 = __makeTemplateObject(["\n    query ListAllOrgsRoles {\n  organizations {\n    _id\n    name\n    roles {\n      _id\n      name\n    }\n  }\n}\n    "], ["\n    query ListAllOrgsRoles {\n  organizations {\n    _id\n    name\n    roles {\n      _id\n      name\n    }\n  }\n}\n    "])));
var ListAllOrgsRolesGQL = /** @class */ (function (_super) {
    __extends(ListAllOrgsRolesGQL, _super);
    function ListAllOrgsRolesGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.ListAllOrgsRolesDocument;
        return _this;
    }
    ListAllOrgsRolesGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ListAllOrgsRolesGQL);
    return ListAllOrgsRolesGQL;
}(Apollo.Query));
exports.ListAllOrgsRolesGQL = ListAllOrgsRolesGQL;
exports.InstallDappDocument = graphql_tag_1.default(templateObject_93 || (templateObject_93 = __makeTemplateObject(["\n    mutation InstallDapp($dapp: DappInput) {\n  installDapp(dapp: $dapp)\n}\n    "], ["\n    mutation InstallDapp($dapp: DappInput) {\n  installDapp(dapp: $dapp)\n}\n    "])));
var InstallDappGQL = /** @class */ (function (_super) {
    __extends(InstallDappGQL, _super);
    function InstallDappGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.InstallDappDocument;
        return _this;
    }
    InstallDappGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], InstallDappGQL);
    return InstallDappGQL;
}(Apollo.Mutation));
exports.InstallDappGQL = InstallDappGQL;
exports.InstallPluginDocument = graphql_tag_1.default(templateObject_94 || (templateObject_94 = __makeTemplateObject(["\n    mutation InstallPlugin($plugin: PluginInput) {\n  installPlugin(plugin: $plugin)\n}\n    "], ["\n    mutation InstallPlugin($plugin: PluginInput) {\n  installPlugin(plugin: $plugin)\n}\n    "])));
var InstallPluginGQL = /** @class */ (function (_super) {
    __extends(InstallPluginGQL, _super);
    function InstallPluginGQL() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.document = exports.InstallPluginDocument;
        return _this;
    }
    InstallPluginGQL = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], InstallPluginGQL);
    return InstallPluginGQL;
}(Apollo.Mutation));
exports.InstallPluginGQL = InstallPluginGQL;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29, templateObject_30, templateObject_31, templateObject_32, templateObject_33, templateObject_34, templateObject_35, templateObject_36, templateObject_37, templateObject_38, templateObject_39, templateObject_40, templateObject_41, templateObject_42, templateObject_43, templateObject_44, templateObject_45, templateObject_46, templateObject_47, templateObject_48, templateObject_49, templateObject_50, templateObject_51, templateObject_52, templateObject_53, templateObject_54, templateObject_55, templateObject_56, templateObject_57, templateObject_58, templateObject_59, templateObject_60, templateObject_61, templateObject_62, templateObject_63, templateObject_64, templateObject_65, templateObject_66, templateObject_67, templateObject_68, templateObject_69, templateObject_70, templateObject_71, templateObject_72, templateObject_73, templateObject_74, templateObject_75, templateObject_76, templateObject_77, templateObject_78, templateObject_79, templateObject_80, templateObject_81, templateObject_82, templateObject_83, templateObject_84, templateObject_85, templateObject_86, templateObject_87, templateObject_88, templateObject_89, templateObject_90, templateObject_91, templateObject_92, templateObject_93, templateObject_94;
