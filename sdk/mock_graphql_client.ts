/* eslint-disable @typescript-eslint/no-empty-function */
import { graphql } from 'graphql';
import { GraphQLClient } from 'graphql-request';
import { RequestDocument } from 'graphql-request/dist/types';
import mockSchema from './mock_schema';

export const mockClient = {
  url: '',
  options: '',
  //constructor(url: string, options?: Dom.RequestInit);
  rawRequest: async() => {
  },
  /**
   * Send a GraphQL document to the server.
   */
  request: async(document: RequestDocument, variables?: {[key: string]: any;}) => {
    const source = typeof document === 'string' ? document : (document as any).loc.source.body;
    return graphql(mockSchema, source, undefined, undefined, variables);
  },
  setHeaders: async() => {},
  /**
   * Attach a header to the client. All subsequent requests will have this header.
   */
  setHeader: (key: string, value: string) => {}
} as unknown as GraphQLClient;