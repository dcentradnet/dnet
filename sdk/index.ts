import './polyfills';

import { fetch } from 'cross-fetch';
import { GraphQLClient } from 'graphql-request';
import { getSdk } from './generated/queries';
import { mockClient } from './mock_graphql_client';

let client;
if(process.env.CLI_DOMAIN) {
  client = new GraphQLClient(process.env.CLI_DOMAIN, {
    timeout: 100000,
    fetch: fetch
  });
} else {
  client = mockClient;
}
export default getSdk(client);
  