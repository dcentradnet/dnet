/* eslint-disable @typescript-eslint/no-empty-function */
import { join } from 'path';
import { loadSchemaSync, addResolversToSchema } from 'graphql-tools';
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { Chaincode, Role, SwarmInitParamsInput, SwarmJoinParamsInput, User, VoteCore } from './generated/queries';

const schema = loadSchemaSync(join(__dirname, 'models/schema.graphql'), {
    loaders: [new GraphQLFileLoader()]
});

const users: User[] = [{_id: '0', name: 'Administrator'}, {_id: '1', name: 'Fred'}];
const roles: Role[] = [{_id: '0', name: 'Administrators'}, {_id: '1', name: 'Accountants'}];

// define local resolvers
const orgName = process.env.CLI_DOMAIN || 'Org1';
const resolvers = {
    Info: {
        consDomain: () => process.env.CONS_DOMAIN,
        orgName: () => orgName,
        channelName: () => process.env.CHANNEL_NAME,
        publicIpV4: () => '127.0.0.1',
        //publicIpV6: ipV6,
        //publicKey: () => crypt.getPublicKeyString(),
        isAdminMode: () => false,
        isJoined: () => true
    },
    Organization: {
        _id: orgName => orgName,
        name: orgName => orgName,
        users: (): User[] => users,
        roles: (): Role[] => roles,
        //dapps: () => ({}),
        //plugins: () => ({}),
        //publicIpV4: () => utils.getPublicIp(),
        //publicIpV6: ipV6,
        //publicKey: () => crypt.getPublicKeyString(),
    },
    Swarm: {
        //nodes: () => docker.listNodes(),
        //inspect: () => docker.swarmInspect()
    },
    Channel: {
        _id: () => '0',
        chaincodes: (): Chaincode[] => []
    },
    Lobby: {
        consortia: () => [], // TODO use real consortia?
        //requested: async(): Promise<Consortium | undefined> => lobby.requested(),
        //joined: async(): Promise<Consortium | undefined> => lobby.joined(),
        /*member: async(): Promise<ConsortiumMember | undefined> => {
            const requestedCid = await lobby.requestedCid();
            const pub = await crypt.getPublicKeyString();
            if(requestedCid) {
                return lobby.listMembers(requestedCid, pub).then(m => m[0]);
            }
        }*/
    },
    Request: {
        /*parent: async(request: GovRequestCore) => gov.getRequestById(request._id),
        children: (request: GovRequestCore) => request.children.map(c => gov.getRequestById(c)),
        votes: (request: GovRequestCore, _args, context) => context.votesByRequest[request._id] || [],
        ownvote: async(request: GovRequestCore, _args, context) => {
            const votes = context.votesByRequest[request._id] || [];
            let ownvote = null;
            if(votes) {
                ownvote = votes.find((vote: GovVote) => vote.voter.name === orgName);
            }
            return ownvote;
        },
        settled: async(request: GovRequestCore, _args, context) => {
            const votes = context.votesByRequest[request._id] || [];
            return !!votes && votes.some((vote: GovVote) => vote.voter.name === orgName && vote.settled)
        }*/
    },
    Governance: {
        /*requests: async(_parent, _args, context): Promise<GovRequestCore[]> => {
            context.votesByRequest = await collectVotesByRequestFromAllOrgs();
            return gov.requests();
        }*/
    },
    User: {
        roles: (): Role[] => roles
    },
    Role: {
        users: (): User[] => users
    },
    Dapps: {
        /*installable: async() => dapps.listStoreApps(),
        installed: async() => dapps.listRunning()*/
    },
    Plugins: {
        /*installable: async() => plugins.listInstallable(),
        installed: async() => plugins.listRunning()*/
    },

    Query: {
        organization: () => 'Org1',
        //consortium: () => lobby.joined(),
        organizations: () => ['Org1', 'Org2'],

        swarm: () => ({}),
        containers: () => [],
        channels: () => [],
        actions: () => [],

        lobby: () => ({}),
        gov: () => ({}),

        users: (): User[] => users,
        roles: (): Role[] => roles,
        dapps: () => ({}),
        plugins: () => ({}),

        chaincodeQuery: () => [],
    },

    Mutation: {
        swarmInit: (_parent, {params}: {params: SwarmInitParamsInput}, {user}: {user: User}) => {
        },
        swarmJoin: (_parent, {params}: {params: SwarmJoinParamsInput}, {user}: {user: User}) => {
        },

        vote: (_parent, vote: VoteCore, {user}: {user: User}) => {
        },

        chaincodeInvoke: (_parent, args, {user}: {user: User}) => {
        },

        loginUser: (_parent, args) => {
            return 'TestToken';
        },

        putUser: (_parent, args, {user}: {user: User}) => {
        },
        putRole: (_parent, args, {user}: {user: User}) => {
        },

        removeUser: (_parent, args, {user}: {user: User}) => {
        },
        removeRole: (_parent, args, {user}: {user: User}) => {
        },

        attachRoleToUser: (_parent, args, {user}: {user: User}) => {
        },
        removeRoleFromUser: (_parent, args, {user}: {user: User}) => {
        },

        /*installPlugin: async(_parent, {plugin}: {plugin: Plugin}, {user, express}: {user: User, express: Express.Express}) => {
        },
        installDapp: async(_parent, {dapp}: {dapp: Dapp}, {user, express}: {user: User, express: Express.Express}) => {
        },*/
    }
};

export default addResolversToSchema({schema, resolvers});
