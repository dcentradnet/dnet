provider "aws" {
  shared_credentials_file   = "$HOME/.aws/credentials"
  profile                   = "default"
  region                    = "eu-west-1"
}

module "ami" {
  source = "github.com/terraform-community-modules/tf_aws_ubuntu_ami"
  region = "${var.region}"
  distribution = "xenial"
  architecture = "amd64"
  virttype = "hvm"
  storagetype = "ebs-ssd"
}

resource "aws_instance" "dNet1" {
  ami               = "${coalesce(var.image_id, module.ami.ami_id)}"
  instance_type     = "t3.large"
}

resource "aws_instance" "dNet2" {
  ami               = "${coalesce(var.image_id, module.ami.ami_id)}"
  instance_type     = "t3.large"
}