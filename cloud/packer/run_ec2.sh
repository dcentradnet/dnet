#!/bin/bash

INSTANCE_ID="`curl http://169.254.169.254/latest/meta-data/instance-id`"
export ORG_NAME="`aws ec2 describe-tags --region eu-central-1 --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=ORG_NAME"  --output=text | cut -f5`"
echo $ORG_NAME

bash /home/ubuntu/run_dNet.sh cons $ORG_NAME 80 false false
