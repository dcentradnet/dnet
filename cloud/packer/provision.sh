sudo apt-get update
sudo apt-get update
sudo apt-get install -y curl wget jq git awscli
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $(whoami)
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update
sudo apt-get install -y nodejs yarn build-essential

sudo docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
sudo docker pull -q registry.gitlab.com/dcentradnet/dnet:$CI_COMMIT_REF_SLUG
sudo docker tag registry.gitlab.com/dcentradnet/dnet:$CI_COMMIT_REF_SLUG dcentra/dnet

sudo docker pull -q mongo:4
sudo docker pull -q hyperledger/fabric-ca:1.4.9
sudo docker pull -q hyperledger/fabric-orderer:2.2
sudo docker pull -q hyperledger/fabric-peer:2.2
sudo docker pull -q couchdb:3.1.1
sudo docker pull -q hyperledger/explorer:0.3.9.3
sudo docker pull -q hyperledger/explorer-db:0.3.9.3

sudo rm -rf /var/lib/apt/lists/*

sudo mv dnet.service /etc/systemd/system/
sudo systemctl enable dnet
